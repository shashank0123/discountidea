<?php 
class Admin_model extends CI_Model
{
	var $admin_table = 'tbl_admin';
	public function adminLogin($username,$password)
	{
		$this->db->where('username',$username);
		$this->db->where('password',$password);
		$data = $this->db->get($this->admin_table);
		return $data->result();		
	}
	
	public function checkAdminByEmail($email)
	{
		$this->db->where('email',$email);
		$data = $this->db->get($this->admin_table);
		return $data->result();		
	}
	
	public function checkAdminByToken($key)
	{
		$this->db->where('forgot_password',$key);
		$data = $this->db->get($this->admin_table);
		return $data->result();		
	}
	
	public function checkAdminByID($id)
	{
		$this->db->where('id',$id);
		$data = $this->db->get($this->admin_table);
		return $data->result();		
	}
	public function updateAdminBy_Id($data,$id)
	{
		$this->db->where('id', $id);
		return $this->db->update($this->admin_table,$data);	
	}
	
	public function update_AdminById($data,$id)
	{
		$this->db->where('id', $id);
		return $this->db->update('tbl_admin',$data);	
	}
	
	public function updateAdminById($data,$id)
	{
		$this->db->where('id', $id);
		return $this->db->update('table_user',$data);	
	}
	
	function delete($id)
	{
		$this->db->where('id', $id);
		return $this->db->delete('tbl_user');
	}
	
	public function insertData($data)
	{
		$returnData = $this->db->insert('table_user',$data);
		return  $returnData;
	}
	
	public function updateData($data)
	{
		$returnData = $this->db->update($this->admin_table,$data);
		return $returnData;
	}
	
	public function getAllUsersvender()
	{
		$this->db->where('vender_type','1');
		$data = $this->db->get('table_user');
		//print_r($data);
		return $data->result();
	}
	
	public function selectUserByID($id)
	{
		$this->db->where('id',$id);
		$data = $this->db->get('table_user');
		return $data->result();
	}
	
	public function userLogin($id,$password)
	{
		$this->db->where('id',$id);
		$this->db->where('password',$password);
		//$this->db->where('status',1);
		$data = $this->db->get($this->admin_table);
		return $data->result();
	}
	public function selectvendername()
	{
		$this->db->where('vender_type','1');
		$data=$this->db->get('table_user');
		return $data->result();		
	}
	public function get_last_command_no()
	{
		$this->db->order_by('command_number','desc');
		$query=$this->db->get('tbl_income');
		 return $query->result_array();
	}
	public function insert_vendor_income($data)
	{
		//$this->db->set('command_date','NOW()');
		$returnData = $this->db->insert('tbl_income',$data);
		return  $returnData;
	}
	public function select_vendor_income()
	{
		$data = $this->db->get('tbl_income');
		return $data->result();
	}
	public function select_vendor_income_by_id($id)
	{
		$this->db->where('id',$id);
		$data = $this->db->get('tbl_income');
		return $data->result();
	}
	public function check_vandor_income($uid,$order_id,$pro_id)
	{
		$this->db->where('product_id',$pro_id);
		$this->db->where('vendor_id',$uid);
		$this->db->where('order_id',$order_id);
		$data = $this->db->get('tbl_income');
		return $data->result();
	}
	public function update_vandor_income_by_id($data,$id)
	{
		$this->db->where('id', $id);
		return $this->db->update('tbl_income',$data);	
	}
	public function select_vendor_return_deducation()
	{
		$data = $this->db->get('tbl_return_deducation');
		return $data->result();
	}
	public function select_vendor_return_deducation_by_id($id)
	{
		$this->db->where('id',$id);
		$data = $this->db->get('tbl_return_deducation');
		return $data->result();
	}
	public function update_vandor_return_deducation_by_id($data,$id)
	{
		$this->db->where('id', $id);
		return $this->db->update('tbl_return_deducation',$data);	
	}
	
	public function insert_cancel_report($data)
	{
		$returnData = $this->db->insert('tbl_return_deducation',$data);
		return  $returnData;
	}
	
	public function check_cancel_report($uid,$order_id,$pro_id)
	{
		$this->db->where('product_id',$pro_id);
		$this->db->where('vendor_id',$uid);
		$this->db->where('order_id',$order_id);
		$data = $this->db->get('tbl_return_deducation');
		return $data->result();

	}
	
	public function get_vendor_payout_by_id($id)
	{
		$this->db->where('vendor_id',$id);
		$this->db->order_by('id','desc');
		//$this->db->select_sum('vendor_id','order_id');
		
		$data = $this->db->get('tbl_income')->result();
		return $data;
	}
	
	public function get_vendor_refund_by_id($id)
	{
		
		$this->db->where('vendor_id',$id);
		$this->db->order_by('id','desc');
		$data = $this->db->get('tbl_return_deducation')->result();
		return $data;
	}
	
	public function insert_final_report($data)
	{
		//echo "deepak";
		//print_r($data); die;
		$returnData = $this->db->insert('tbl_all_income',$data);
		return  $returnData;
	}
	
	public function vendor_credit_final()
	{
		$data = $this->db->get('tbl_all_income')->result();
		return $data;
	}
	
	public function final_income_by_id($id)
	{
		//SUM(final_remittance) AS total_remittance
		//$this->db->select();

		//$this->db->join('tbl_order', 'tbl_order.id=tbl_income.order_id');
		//$where_array=array('tbl_order.requestapprovalstatus'=>'Refund Approved','tbl_order.requestfor'=>'Cancel','tbl_order.status'=>'delivered successfully','vendor_id'=>$id,'command_number'=>$comand_no);
		//$this->db->order_by('order_id','desc');
		//$this->db->where($where_array);
		
		 
	//$query=$this->db->get('tbl_income');
	return $this->db->query("select *, date as adate from tbl_all_income where id='".$id."'")->result();
		// return $query->result_array();
	}
	//get_cancel_orders_amount
public function get_cancel_orders_amount($id){
	$this->db->select('*,SUM(final_remittance) AS total_final_remittance');

		//$this->db->join('tbl_order', 'tbl_order.id=tbl_income.order_id');
		$where_array=array('order_id'=>$id);
		//$this->db->order_by('order_id','desc');
		$this->db->where($where_array);
		
		 
	$query=$this->db->get('tbl_return_deducation');
		 return $query->result_array();
}
public function final_success_by_id($id,$comand_no)
	{
				$this->db->select('*,SUM(sell_amount) AS total_sell_amount,SUM(final_remittance) AS total_remittance');

		//$this->db->join('tbl_order', 'tbl_order.id=tbl_income.order_id');
		$where_array=array('vendor_id'=>$id,'command_number'=>$comand_no);
		$this->db->order_by('order_id','desc');
		$this->db->where($where_array);
		
		 
	$query=$this->db->get('tbl_income');
		 return $query->result_array();
	}
	

	//$this->db->join('tbl_income', 'tbl_return_deducation.vendor_id = tbl_income.vendor_id', 'outer'); 
	//$query = $this->db->get('tbl_return_deducation');
	//return $query->result();

	   /*function result_getall(){

    $this->db->select('tbl_income.*,tbl_return_deducation.*');
    $this->db->from('tbl_income');
    $this->db->join('tbl_return_deducation', 'tbl_income.answerid = tbl_return_deducation.cid', 'join'); 
    $query = $this->db->get();
    return $query->result();

    }*/
	

	
	public function update_final_income($data,$id)
	{
		$this->db->where('id', $id);

		return $this->db->update('tbl_all_income',$data);	
	}
	
	// admin model helpdesk
	
	function insert_help_desk($data, $vender_id, $pid)
	{
		date_default_timezone_set("Asia/Kolkata");
		
		$data1 = array(
		'uid' => $data['uid'],
		'order_product' => $data['order_product'],
		'product_id' => $pid,
		'vender_id' => $vender_id,
        'type' => $data['type'],
        'subject' => $data['subject'],
        'query' => $data['query'],
		'file' => $data['file'],		
		'status' => 'pending',
		'queryTime' => date("Y-m-d H:i:s")
		);
		
		$this->db->insert('tbl_helpdesk', $data1);
		
	}
	
	public function getAllQuery()
	{
		$this->db->order_by('id','DESC');
		$data = $this->db->get('tbl_helpdesk');
		return $data->result();
	}
	
	function selectuserby_id($id)
	{
		$this->db->where('id',$id);
		$data = $this->db->get('table_user')->result();
		return $data;		
	}
	
	function selectadminby_id($id)
	{
		$this->db->where('id',$id);
		$data = $this->db->get('tbl_admin')->result();
		return $data;		
	}
	
	function insert_comment($data)
	{
		date_default_timezone_set("Asia/Kolkata");
		
		$data1 = array(
		'uid' => $data['uid'],
		'comment' => $data['comment'],
		'query_id' => $data['query_id'],
        'comment_type' => 'admin',
		'comment_time' => strtotime("now"),
		'comment_date' => date("Y-m-d  H:i:s")
		);
		
		$this->db->insert('tbl_helpdesk_comments', $data1);
		
		$data2 = array(
		'status' => $data['status']
		);
		
		$this->db->where('id', $data['query_id']);
		$this->db->update('tbl_helpdesk', $data2);
		if($data['status']=='close')
		{
			$insert_id=$data['query_id'];
			$user=$this->selectuserby_id($data['uid']);
			
			$admin_id=$this->selectadminby_id(1);
			
			$data3=$this->getHelpdeskById($insert_id);
			$vender_ids=$this->selectuserby_id($data3[0]->vender_id);
			$depart=$this->select_depart($data3[0]->type);
          // print_r($data3); die;
            $message = "<table bgcolor='#FFFFFF' cellspacing='0' cellpadding='10' border='0' width='650' style='border:1px solid #e0e0e0;'>
			<tbody><tr style='background:#e0e0e0;' ><td valign='top'><a rel='nofollow' target='_blank' href='https://www.discountidea.com/' ><img src='http://www.discountidea.com/new/assets/front/images/stick-logo.png' alt='discountidea.com' style='margin-bottom:10px;' border='0'></a></td>
			</tr></tbody></table>";
			$message .= "<p>Hello ".$user[0]->fname.",</p><p><br>We are obliged to serve you better ! <br><br>Your concern has been noticed by our help desk with your ticket ID #".$insert_id."<br><br>This query has been closed by admin.If any query please feel free to contact again.";
			
			$message .= "<table bgcolor='#FFFFFF' cellspacing='0' cellpadding='10' border='0' width='650' style='border:1px solid #e0e0e0;'>
			<tbody><tr><th>Ticket ID</th><th>User ID</th><th>Order ID</th>";
			$message .= "<th>Product ID</th><th>Vendor Name</th><th>User Contact</th><th>Vendor Contact</th><th>Department</th></tr>";
			
			$message .= "<tr><td>".$insert_id."</td><td>".$data3[0]->uid."</td><td>".$data3[0]->order_product."</td><td>DI-".$data3[0]->product_id."</td><td>".$vender_ids[0]->fname."</td><td>".$user[0]->mobile."</td><td>".$vender_ids[0]->mobile."</td><td>".$depart[0]->title."</td></tr></tbody></table><br><br>";
			
			$message .= "<strong>Subject:</strong><br>".$data3[0]->subject."<br><br><strong>Query:</strong><br>".$data['comment'].""; 
			
			$message .=     "<br><br>Regards,<br>Help Desk Team,<br>www.discountidea.com";
            //$message .=     ".</p><p>- ".$data['query']."</p>";
			//print_r($message);
			$subject = "Help Desk Discount Idea (Do not reply it is a system Generated E-mail";
			$to=$user[0]->email.','.$vender_ids[0]->email.','.$depart[0]->email; //die;
			//$to=$vender_ids[0]->email;
			$cc=$admin_id[0]->email;
			$from='info@discountidea.com';
		$this->sendmail($from, $to, $cc, $subject, $message);
		}
	}
	
	function select_depart($userid)
	{
		$this->db->where('id',$userid);
		$data=$this->db->get('tbl_helpdesk_department');
		return $data->result();
	}
	
	function sendmail($from, $to, $cc, $subject, $message)
	{
		//echo ' from: '.$from.' to: '.$to.' cc: '.$cc.' subject: '.$subject.' message: '.$message; die;
		$config = Array(
								'protocol' => 'smtp',
								'smtp_host' => 'ssl://cs7.hostgator.in',
								'smtp_port' => 465,
								'smtp_user' => 'info@discountidea.com',
								'smtp_pass' => 'vikas1234',
                                'mailtype'  => 'html',
                                'newline' =>"\r\n"
							);
            
            $this->email->initialize($config);
            $this->email->from($from);
            //$to= 'indiandiscountidea@gmail.com';
            $this->email->to($to);
			$this->email->cc($cc);
			//$this->email->bcc($bcc);
            
            $this->email->subject($subject);
            
			
            $this->email->message($message);
			$this->email->send();
           /* if ($this->email->send()) {
               echo 'Email Successfully Send !';
            } else {
                echo '<p class="error_msg">Invalid Gmail Account or Password !</p>';
            }*/
	}
	
	function getcommentById($id)
	{
		
		$this->db->select('*');
		$this->db->where('query_id',$id);
		$this->db->order_by('comment_time', 'DESC');
		$data = $this->db->get('tbl_helpdesk_comments');
		return $data->result();
	}
	function getHelpDepart()
	{
		$this->db->select('*');
		$data = $this->db->get('tbl_helpdesk_department');
		return $data->result();
	}
	function edit_department($id)
	{
		$this->db->select('*');
		$this->db->where('id',$id);
		$data = $this->db->get('tbl_helpdesk_department');
		return $data->result();
	}
	public function insertDepartmentData($data)
	{
		$this->db->insert('tbl_helpdesk_department',$data);
		$insert_id = $this->db->insert_id();		 
		return  $insert_id;
	}
	public function selectDepartmentByID($id)
	{
		$this->db->where('id',$id);
		$data = $this->db->get('tbl_helpdesk_department');
		return $data->result();
	}
	public function updateDepartmentData($id,$data)
	{
		$this->db->where('id',$id);
		$returnData = $this->db->update('tbl_helpdesk_department',$data);
		return $returnData;
	}
	function getLoginUserVar($var)
	{
		$data = $this->session->userdata($var);
		return $data;
	} 
	function getHelpdeskByUserId($userid)
	{ 
		$this->db->select('*');
		$data=$this->db->get('tbl_helpdesk');
		return $data->result();
	}
	function getHelpdeskByUserId2($userid)
	{ 
		$this->db->select('*');
		$this->db->where('status','pending');
		$data=$this->db->get('tbl_helpdesk');
		return $data->result();
	}
	function getHelpdeskByUserId3($userid)
	{ 
		$this->db->select('*');
		$this->db->where('status','close');
		$data=$this->db->get('tbl_helpdesk');
		return $data->result();
	}
	function getHelpdeskByUserId4($userid)
	{ 
		$this->db->select('*');
		$this->db->where('status','open');
		$data=$this->db->get('tbl_helpdesk');
		return $data->result();
	}
	function getOrderByUserId($userid)
	{
		$this->db->where('user_id',$userid);
		$data=$this->db->get('tbl_order');
		return $data->result();
	} 
	
	function getHelpdeskById($userid)
	{
		$this->db->where('id',$userid);
		$data=$this->db->get('tbl_helpdesk');
		return $data->result();
	}
	
	public function getAllSubAdmin()
	{
		$this->db->where('type',2);
		$data = $this->db->get($this->admin_table);
		return $data->result();		
	}
	public function checkAdminByUsername($username)
	{
		$this->db->where('username',$username);
		$data = $this->db->get($this->admin_table);
		return $data->result();		
	}
	
	public function insertAdmin($data)
	{
		return $this->db->insert($this->admin_table,$data);	
	}
	
	public function selectAdminById($id)
	{
		$this->db->where('id',$id);
		$data = $this->db->get($this->admin_table);
		return $data->result();		
	}
	public function updateAdminById1($id,$data)
	{
		$this->db->where('id', $id);
		return $this->db->update($this->admin_table,$data);	
	}
	public function deleteAdmin($id)
	{
		$this->db->where('id',$id);
		$data = $this->db->delete($this->admin_table);		
	}
	public function insert_pincode($data){
		$this->db->truncate('tbl_admin_pincode');
		$result = $this->db->insert_batch('tbl_admin_pincode', $data);
		return $result;
	}
	
}