<?php 
class product_model extends CI_Model{
	public function selectAllProduct()
	{
		$data=$this->db->get("tbl_product");
		return $data->result();
	}
	
	public function select_active_products()
	{
		$this->db->where('status',1);
		$data=$this->db->get("tbl_product");
		return $data->result();
	}
       
        public function ProductCategory($catid)
        {
            $sql = 'SELECT cat.*,pcat.title as ptitle FROM `tbl_category` cat inner join tbl_category pcat on pcat.id=cat.parent_id where cat.id= ?';
            $its_status = $this->db->query($sql, array($catid));
            return $its_status->row();
        }
	
	public function select_active_products_bycategoryid_array($ids)
	{
		//$this->db->where('category_id',$id);
		$this->db->where_in('category_id', $ids);
		$this->db->where('status',1);
		$data=$this->db->get("tbl_product");
		return $data->result();
	}
	public function listing(){
		$data=$this->db->get("tbl_product_color");
		return $data->result();
	}
	public function selectProductById($id){
         $this->db->where('id',$id);
         $data=$this->db->get("tbl_product");
         return $data->result();
	}
	public function Productupdate($id,$data){
		$this->db->where('id',$id);
		return $this->db->update("tbl_product",$data);	
	}
	function insert($data)
	{
		$this->db->insert('tbl_product', $data);
		$result= $this->db->insert_id();
		return $result;		
	}
	function insertProductImage($data)
	{
		$result= $this->db->insert('tbl_product_image', $data);
		return $result;
	}

	public function copyProduct($productID=0)
	{		
		try{
			if($productID<1){
				return 0;
			}
			else {
				$prdCols = $this->db->list_fields('tbl_product'); //echo "<pre>";print_r($prdCols);die;
				unset($prdCols[0]);
				$prdCol  = implode($prdCols,',');
				$selCol  = str_replace(array(',update_date',',status'),array('',''),$prdCol);
				$copyCol = str_replace(array('create_date',',update_date',',status'),array(time(),'',''),$prdCol);
				//echo "<pre>";print_r($qry);die;
				$newProdID = ($this->db->query("INSERT INTO tbl_product ($selCol) SELECT $copyCol FROM tbl_product WHERE id=".$productID)) ? $this->db->insert_id() : 0; //echo $newProdID;die;
				if($newProdID>0){
					$this->db->query("INSERT INTO tbl_product_filters (p_id,o_id) SELECT ".$newProdID.",o_id FROM tbl_product_filters WHERE p_id=".$productID);
					$this->db->query("INSERT INTO tbl_product_image (product_id,image,image_title,status) SELECT ".$newProdID.",image,image_title,status FROM tbl_product_image WHERE product_id=".$productID);
					$this->db->query("INSERT INTO tbl_product_terms (pro_id,title,content,create_date) SELECT ".$newProdID.",title,content,'".date('Y-m-d')."' FROM tbl_product_terms WHERE pro_id=".$productID);
				}
				return $newProdID;
			}
		}
		catch(Exception $e){
			print($e->getMessage());exit;
		}
	}
	
	public function setStatus($productID=0,$statusID=0)
	{		
		try{
			if($productID<1){
				return 0;
			}
			else {
				$this->db->where('id',(int)$productID);
				return ($this->db->update('tbl_product',array('status'=>$statusID))) ? 1 : -1;
			}
		}
		catch(Exception $e){
			print($e->getMessage());exit;
		}
	}

	public function addProductSizeQuantity($productID=0)
	{		
		try{
			if($productID<1){
				return 0;
			}
			else {
				$sizes = ($this->input->post("sizes") && count($this->input->post("sizes"))>0) ? $this->input->post("sizes") : array();
				$quan  = ($this->input->post("quan") && count($this->input->post("quan"))>0) ? $this->input->post("quan") : array();
				
				if(count($sizes)>0) {
					foreach($sizes as $key=>$size){
						$quantity = (($quan[$key]) && $quan[$key]>0) ? $quan[$key] : 0;
						$this->db->select('size_stock_id');
						$this->db->where('product_id',$productID);
						$this->db->where('size_filter_id',$size);
						$data=$this->db->get("tbl_product_size_stock")->row(); //print_r($sizes);die;
						if(($data->size_stock_id)){
							$this->db->where('size_stock_id',(int)$data->size_stock_id);
							$this->db->update('tbl_product_size_stock',array('stock_quantity'=>$quantity));
						}
						else {
							$this->db->insert('tbl_product_size_stock',array('product_id'=>$productID,'size_filter_id'=>$size,'stock_quantity'=>$quantity));
						}
					}
				}
			}
			return TRUE;
		}
		catch(Exception $e){
			print($e->getMessage());exit;
		}
	}

	function categoryProductListing($id,$limit = null, $start= null)
	{
		$this->db->where('status',1);
		$this->db->where('category_id',$id);
		$this->db->limit($limit, $start);
		$data=$this->db->get("tbl_product");
		return $data->result();
	}
	
	function maxProductPrice($id,$limit = null, $start= null)
	{
		$this->db->select_max('spacel_price');
		$this->db->where('status',1);
		$this->db->where('category_id',$id);
		$this->db->limit($limit, $start);
		$data=$this->db->get("tbl_product");
		return $data->result();
	}
	
	function minProductPrice($id,$limit = null, $start= null)
	{
		$this->db->select_min('spacel_price');
		$this->db->where('status',1);
		$this->db->where('category_id',$id);
		$this->db->limit($limit, $start);
		$data=$this->db->get("tbl_product");
		return $data->result();
	}
	
	public function getTotalcategoryProduct($id)
	{
		$this->db->where('category_id',$id); 
        $query = $this->db->get('tbl_product');
        return $query->num_rows();
    }
	
	function selectActiveProductByCategoryId($id)
	{
		$this->db->where('status',1);
		$this->db->where('category_id',$id);
		$data=$this->db->get("tbl_product");
		//echo $this->db->last_query();die;
		return $data->result();
	}
	public function selectProductSingleImage($id){
		$this->db->where('product_id',$id); 
		$this->db->limit(1);
		$data=$this->db->get("tbl_product_image");
		return $data->result();
	}
	public function selectProductImages($id){
		$this->db->where('product_id',$id); 
		$data=$this->db->get("tbl_product_image");
		return $data->result();
	}

	public function selectUserImages($id){
		$this->db->where('user_id',$id); 
		$data=$this->db->get("tbl_product_image");
		return $data->result();
	}
	public function selectProductImageById($id){
		$this->db->where('id',$id);
		$data=$this->db->get("tbl_product_image");
		return $data->result();
	}
    function deleteProductImageById($id)
	{
		$this->db->where('id', $id);
		return $this->db->delete('tbl_product_image');
	}
	function deleteimage($id)
	{
		$this->db->where('product_id', $id);
		return $this->db->delete('tbl_product_image');
	}
	function selectAllReletedProduct($ids)
	{
                $this->db->where('status', 1);
		$this->db->where_in('id', $ids);
		$data=$this->db->get("tbl_product");
		return $data->result();
	}
	
	function update_image($id,$data){
		$this->db->where('id',$id);
		$data=$this->db->update("tbl_product_image",$data);
		return $data;
	}
	public function selectProductDoubleImage($id){
		$this->db->where('product_id',$id); 
		$this->db->limit(2);
		$data=$this->db->get("tbl_product_image");
		return $data->result();
	}
	function insertsizeproductbyid($sizedata)
	{
		$qry=$this->db->insert('tb_sizeproduct',$sizedata);
		return $qry;		
	}
	function deletesize($id)
	{
		$this->db->where('pro_id',$id);
		return $this->db->delete('tb_sizeproduct');
		
	}
	function insertcolorproductbyid($colordata)
	{
		$qry=$this->db->insert('tb_colorproduct',$colordata);
		return $qry;
	}
	function updatesizebyid($data)
	{
		$qry=$this->db->insert("tb_sizeproduct",$data);
	}
	function updatecolorbyid($data)
	{		
		$qry=$this->db->insert("tb_colorproduct",$data);		
	}
	function deletecolor($id)
	{
		$this->db->where('pro_id',$id);
		return $this->db->delete('tb_colorproduct');		
	}
	
	public function selectProductByVedorID($vender_id){
         $this->db->where('vender_type',$vender_id);
         $data=$this->db->get("tbl_product");
         return $data->result();
	}

	public function getProductForGrouping($vender_id){
         $this->db->select('id,name');
         $this->db->where('vender_type',$vender_id);
         //$this->db->where('parent_id',0);
         $this->db->where('status',1);
         $this->db->order_by('name','asc');
         $data=$this->db->get("tbl_product");
         return $data->result();
	}

	public function getProductGroup($data=array()){
         $parentID  = ($data[0]->parent_id) ? $data[0]->parent_id : 0;
         $productID = ($data[0]->id) ? $data[0]->id : 0;
         if($parentID>0 || $productID>0) {
	         $this->db->select('tbl_product.id,tbl_product.category_id,tbl_product.name,tbl_product_image.image');
	         $this->db->join('tbl_product_image','tbl_product_image.product_id=tbl_product.id', 'inner');
	         $this->db->where("tbl_product.id!=",$productID,FALSE);
	         if($parentID>0) 
	         	$this->db->where("(tbl_product.id=$parentID OR tbl_product.parent_id=$parentID)");
	         else
	         	$this->db->where('tbl_product.parent_id',$productID);
	         $this->db->where('tbl_product.status',1);
	         $this->db->group_by('tbl_product_image.product_id');
	         $data=$this->db->get("tbl_product"); //echo $this->db->last_query();die;
	         return $data->result();
     	}
     	else {
     		return array();
     	}
	}
        function selectactiveallproduct()
	{
		$this->db->where('status',1);
		$data=$this->db->get('tbl_product');
		return $data->result();
	}
	function ExportCSV()
	{

        $this->load->dbutil();
        $this->load->helper('file');
        $this->load->helper('download');
        $delimiter = ",";
        $newline = "\r\n";
        $filename = "filename_you_wish.csv";
        $query = "SELECT * FROM tbl_product WHERE status='1'";
        $result = $this->db->query($query);
        $data = $this->dbutil->csv_from_result($result, $delimiter, $newline);
        $res=force_download($filename, $data);
		return $res;
	}


	function generateCSV()
	{

  //       $this->load->dbutil();
  //       $this->load->helper('file');
  //       $this->load->helper('download');
  //       $delimiter = ",";
  //       $newline = "\r\n";
  //       $filename = "samplelist.csv";
       	
  //       $new = ['name',	'hsn_code',	'gst_code',	'brand',	'height',	'length',	'width',	'weight',	'short_description',	'description',	'price',	'discount'];

  //       $data = $this->dbutil->csv_from_result([], $delimiter, $newline);
  //       var_dump($data);
  //       die;
  //       $res=force_download($filename, $data);
		// return $res;

		$data[] = ['name',	'brand',	'height',	'length',	'width',	'weight',	'short_description',	'description',	'price',	'discount'];
            header("Content-type: application/csv");
            header("Content-Disposition: attachment; filename=\"test".".csv\"");
            header("Pragma: no-cache");
            header("Expires: 0");
            $handle = fopen('php://output', 'w');
            $data2 = $this->filters_model->getProductSizeQuantity(0);
            foreach ($data2 as $key => $value) {
            	array_push($data[0], $value->title.'_quantity');
            }
           	for ($i=1; $i <= 8; $i++) { 
           		array_push($data[0], 'image_'.$i);
           	}

            foreach ($data as $data_array) {
                fputcsv($handle, $data_array);
            }
                fclose($handle);
            exit;
	}
	
	function selectreviewbyproid($id)
	{
		$this->db->where('pro_id',$id);
		$data=$this->db->get('tbl_review');
		return $data->result();
		
	}
	function selectreviewid($id)
	{
		$this->db->where('id',$id);
		$data=$this->db->get('tbl_review');
		return $data->result();
	}
	function homeproductall($filed)
	{
		$this->db->where($filed,1);
		$data=$this->db->get('tbl_product');
		return $data->result();
	}
	function selecproducthomerand()
	{
               $this->db->where('status',1);
		$this->db->order_by('id','RANDOM');
		$this->db->limit(8);
		$data=$this->db->get('tbl_product');
		return $data->result();
	}
	function selecthomeproductall($fild)
	{
		$this->db->where($fild,1);
		$data=$this->db->get('tbl_product');
		return $data->result();		
	}
	
	function getRecentlyViewedProduct()
	{
		session_start();	
		if(isset($_SESSION['mydata']))
		{
			return preg_replace("/[^a-zA-Z 0-9]+/", "",$_SESSION['mydata']);
		}
		else
		{
			return array(0);
 		}	
	}
	
	function getproductdiscountwise($dicount,$discount1=0)
	{
		//$this->db->where($filed,1);
		$this->db->where("descount >= ",$dicount);
		if($discount1>0)
			$this->db->where("descount < ",$discount1);
        $this->db->where('status',1);
		$data=$this->db->get('tbl_product');
		return $data->result();	
	}
	
	
	function select_product_terms($id)
	{
		$this->db->where('pro_id',$id);
		$data=$this->db->get('tbl_product_terms');
		return $data->result();		
	}
	
	function delete_product_terms($id)
	{
		$this->db->where('id',$id);
		$data=$this->db->delete('tbl_product_terms');
	}
	function get_product_status($id)
	{
		$this->db->select('status');
		$this->db->where('id',$id);
		return $this->db->get('tbl_product')->row();	
		 print $this->db->last_query(); die;
		
	}

	############################################
	public function getHSNCodes()
	{
		return $this->db->get("tbl_hsn")->result();
	}

	public function getTaxCodes()
	{
		return $this->db->get("tbl_taxcodes")->result();
	}
}