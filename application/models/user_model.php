<?php
class User_model extends CI_Model
{
    public $table = 'table_user';
	function selectAllUser()
	{
		$data = $this->db->get($this->table)->result();
		return $data;		
	}
	
	function selectAllActiveUser()
	{
		$this->db->where('status',1);
		$data = $this->db->get($this->table)->result();
		return $data;		
	}
	
	public function get_query($sql){
	
	return $this->db->query($sql)->result();
	}
	function insert_user($data)
	{
		$result= $this->db->insert($this->table, $data);
		return $result;
	}
	
	function insert_help_desk($data, $vender_id, $pid, $file)
	{
		date_default_timezone_set("Asia/Kolkata");
		//print_r($file); die;
		
		if($_FILES['file']['name']!="")
			{
				$logo = createUniqueFile($_FILES['file']['name']);
				$file_temp = $_FILES['file']['tmp_name'];
				$path = 'uploads/helpdesk/'.$logo;				
				move_uploaded_file($file_temp,$path);				
			}
		
		
		//
		$data1 = array(
		'uid' => $data['uid'],
		'order_product' => $data['order_product'],
		'product_id' => $pid,
		'vender_id' => $vender_id,
        'type' => $data['type'],
        'subject' => $data['subject'],
        'query' => $data['query'],
		'file' => $logo,
		//'executive' => $data['executive'],
		'status' => 'pending',
		'queryTime' => date("Y-m-d H:i:s"),
		'queryDate' => date("Y-m-d")
		);
		
		$this->db->insert('tbl_helpdesk', $data1);
		
		$insert_id=$this->db->insert_id();
		
		$user=$this->selectuserby_id($data['uid']);
		$vender_ids=$this->selectuserby_id($vender_id);
		$admin_id=$this->selectadminby_id(1);
		$depart=$this->select_depart($data['type']);
		
           //print_r(); die;
            $message = "<table bgcolor='#FFFFFF' cellspacing='0' cellpadding='10' border='0' width='650' style='border:1px solid #e0e0e0;'>
			<tbody><tr style='background:#e0e0e0;' ><td valign='top'><a rel='nofollow' target='_blank' href='https://www.discountidea.com/' ><img src='http://www.discountidea.com/new/assets/front/images/stick-logo.png' alt='discountidea.com' style='margin-bottom:10px;' border='0'></a></td>
			</tr></tbody></table>";
			$message .= "<p>Hello ".$user[0]->fname.",</p><p><br>We are obliged to serve you better ! <br><br>Your concern has been noticed by our help desk with your ticket ID #".$insert_id."<br><br>We would request you to have patience till the time we coordinate with seller to discuss on your feedback and concern.<br><br>";
			$message .= "<table bgcolor='#FFFFFF' cellspacing='0' cellpadding='10' border='0' width='650' style='border:1px solid #e0e0e0;'>
			<tbody><tr><th>Ticket ID</th><th>User ID</th><th>Order ID</th>";
			$message .= "<th>Product ID</th><th>Vendor Name</th><th>User Contact</th><th>Vendor Contact</th><th>Department</th></tr>";
			
			$message .= "<tr><td>".$insert_id."</td><td>".$data['uid']."</td><td>".$data['order_product']."</td><td>DI-".$pid."</td><td>".$vender_ids[0]->fname."</td><td>".$user[0]->mobile."</td><td>".$vender_ids[0]->mobile."</td><td>".$depart[0]->title."</td></tr></tbody></table><br><br>";
			
			$message .= "<strong>Subject:</strong><br>".$data['subject']."<br><br><strong>Query:</strong><br>".$data['query']."";
			
			$message .=     "<br><br>Regards,<br>Help Desk Team,<br>www.discountidea.com";
            //$message .=     ".</p><p>- ".$data['query']."</p>";
			//print_r($message); die;
			$subject = "Help Desk Discount Idea (Do not reply it is a system Generated E-mail";
			$from='info@discountidea.com';
			$to=$user[0]->email.','.$vender_ids[0]->email.','.$depart[0]->email;
			$cc=$admin_id[0]->email;
		$this->sendmail($from, $to, $cc, $subject, $message);
		
	}
	
	function insert_comment($data)
	{
		date_default_timezone_set("Asia/Kolkata");
		//print_r($vender_id); die;
		
		
			
		$data1 = array(
		'uid' => $data['uid'],
		'comment' => $data['comment'],
		'query_id' => $data['query_id'],
        
		
		'comment_type' => 'user',
		'comment_time' => strtotime("now"),
		'comment_date' => date("Y-m-d  H:i:s")
		);
		//print_r($data1); die;
		$this->db->insert('tbl_helpdesk_comments', $data1);
		
	}
	
	function sendmail($from, $to, $cc='', $subject, $message)
	{
		//echo ' from: '.$from.' to: '.$to.' cc: '.$cc.' subject: '.$subject.' message: '.$message; die;
		$config = Array(
								'protocol' => 'smtp',
								'smtp_host' => 'ssl://cs7.hostgator.in',
								'smtp_port' => 465,
								'smtp_user' => 'info@discountidea.com',
								'smtp_pass' => 'vikas1234',
                                 'mailtype'  => 'html',
                                'newline' =>"\r\n"
							);
            
             $this->email->initialize($config);
            $this->email->from($from);
            //$to= 'indiandiscountidea@gmail.com';
            $this->email->to($to);
            if(!empty($cc)){
			$this->email->cc($cc);
					}
			//$this->email->bcc($bcc);
            
            $this->email->subject($subject);
            
			
            $this->email->message($message);
			$this->email->send();
			//print_r($this->email);
			//die;
           /* if ($this->email->send()) {
               echo 'Email Successfully Send !';
            } else {
                echo '<p class="error_msg">Invalid Gmail Account or Password !</p>';
            }*/
	}
	
	function getcommentById($id)
	{
		
		$this->db->select('*');
		$this->db->where('query_id',$id);
		$this->db->order_by('comment_time', 'DESC');
		$data = $this->db->get('tbl_helpdesk_comments');
		return $data->result();
	}
	function getHelpDepart()
	{
		$this->db->select('*');
		$data = $this->db->get('tbl_helpdesk_department');
		return $data->result();
	}
	
	function selectuserby_email($email)
	{
		$this->db->where('email',$email);
		$data = $this->db->get($this->table)->result();
		return $data;		
	}
	
	function selectUserByActivateToken($token)
	{
		$this->db->where('activate_token',$token);
		$data = $this->db->get($this->table)->result();
		return $data;		
	}
	
	function user_login($email,$password)
	{
		//$this->db->where('profile_type',$profile_type);
		$this->db->where('email',$email);
		$this->db->where('password',$password);
		$data = $this->db->get($this->table)->result();
		return $data;		
	}
	
	function selectuserby_id($id)
	{
		$this->db->where('id',$id);
		$data = $this->db->get($this->table)->result();
		return $data;		
	}
	
	function selectadminby_id($id)
	{
		$this->db->where('id',$id);
		$data = $this->db->get('tbl_admin')->result();
		return $data;		
	}
	
	function selectUserByPassword($id,$pwd)
	{
		//echo "dsfsdfdd";die;
		$this->db->where('id',$id);
		$this->db->where('password',$pwd);
		$data = $this->db->get($this->table);
		//print_r($data);
		return $data;		
	}
	
	function update($id,$data)
	{
		$this->db->where('id', $id);
		return $this->db->update($this->table,$data);
	}
	
	function delete($id)
	{
		$this->db->where('id', $id);
		return $this->db->delete($this->table);
	}
	
	function getLoginUserVar($var)
	{
		$data = $this->session->userdata($var);
		return $data;
	}
	
	// experience query	
	function insert_user_exp($data)
	{
		$result= $this->db->insert('tbl_experience', $data);
		return $result;
	}
	function select_user_exp($id)
	{
		$this->db->where('user_id',$id);
		$data = $this->db->get('tbl_experience')->result();
		return $data;
	}
	function delete_user_exp($id)
	{
		$this->db->where('id', $id);
		return $this->db->delete('tbl_experience');
	}
	
	// education query
	function insert_user_edu($data)
	{
		$result= $this->db->insert('tbl_education', $data);
		return $result;
	}
	function select_user_edu($id)
	{
		$this->db->where('user_id',$id);
		$data = $this->db->get('tbl_education')->result();
		return $data;
	}
	function delete_user_edu($id)
	{
		$this->db->where('id', $id);
		return $this->db->delete('tbl_education');
	}
	
	// specialization query
	function insert_user_spl($data)
	{
		$result= $this->db->insert('tbl_specialization', $data);
		return $result;
	}
	function select_user_spl($id)
	{
		$this->db->where('user_id',$id);
		$data = $this->db->get('tbl_specialization')->result();
		return $data;
	}
	function delete_user_spl($id)
	{
		$this->db->where('id', $id);
		return $this->db->delete('tbl_specialization');
	}
	function checkUserTokenForgotPassword($token)
	{
		$this->db->where('password_token', $token);
		$data = $this->db->get('tbl_users');
		return $data->result();
	}
	function checkUserTokenForgotPasswordnew($token)
	{
		$this->db->where('activate_token', $token);
		$data = $this->db->get('table_user');
		return $data->result();
	}
	function insertUserWishlistItem($data)
	{
		$res = $this->db->insert('tbl_wishlist',$data);
		return $res;
	}
	function checkUserWishlistItem($uid,$pid)
	{
		$this->db->where('user_id', $uid);
		$this->db->where('pro_id', $pid);
		$data = $this->db->get('tbl_wishlist');
		return $data->result();
	}
	function selectUserWishlistItem($uid)
	{
		$this->db->where('user_id', $uid);
		$data = $this->db->get('tbl_wishlist');
		return $data->result();
	}
	
	function productwislist($id)
	{
		$this->db->where('status',1);
		$this->db->where('id',$id);
		$data=$this->db->get('tbl_product');
		return $data->result();
	}
	function productwilimage($id)
	{
		
		$this->db->where('product_id',$id);
		$data=$this->db->get('tbl_product_image');
		return $data->result();
	}
	
	function getallproductdetails($id)	{
		
		$this->db->where('id',$id);
		$data=$this->db->get('tbl_product');
		return $data->result();
	}
	function whislistdelete($id)
	{
		$this->db->where('pro_id',$id);
		$data=$this->db->delete('tbl_wishlist');
		return $data;
				
	}
	function myorderdetail1($userid)
	{
		$this->db->where('user_id',$userid);
		$data=$this->db->get('tbl_item');
		return $data->result();
	}
	function getOrderItemsByOrderId($order_id)
	{
		$this->db->where('order_id',$order_id);
		$data=$this->db->get('tbl_item');
		return $data->result();
	}
	function getOrderItemsByProductId($pro_id)
	{
		$this->db->where('pro_id',$pro_id);
		$data=$this->db->get('tbl_item');
		return $data->result();
	}
	function getSoldOutProducts($pro_id)
	{
		$this->db->where('pro_id',$pro_id);
		$data=$this->db->get('tbl_item');
		$sold_qty = 0;
		if(count($data->result())>0)
		{
			foreach($data->result() as $res_data)
			{
				$sold_qty += $res_data->qty;
			}
		}
		return $sold_qty;	
	}
	function getOrderByUserId($userid)
	{
		$this->db->where('user_id',$userid);
		$data=$this->db->get('tbl_order');
		return $data->result();
	}
	
	function getHelpdeskByUserId($userid)
	{
		$this->db->where('uid',$userid);
		$data=$this->db->get('tbl_helpdesk');
		return $data->result();
	}
	function select_depart($userid)
	{
		$this->db->where('id',$userid);
		$data=$this->db->get('tbl_helpdesk_department');
		return $data->result();
	}
	function getHelpdeskById($userid)
	{
		$this->db->where('id',$userid);
		$data=$this->db->get('tbl_helpdesk');
		return $data->result();
	}
	function getOrderByOrderId($order_id)
	{
		$this->db->where('id',$order_id);
		$data=$this->db->get('tbl_order');
		return $data->result();
	}
	function productmyorderdetail($ord)
	{
		$this->db->where('id',$ord);
		$data=$this->db->get('tbl_order');
		return $data->result();
	}
	function productmyorderproductimag($pro_id)
	{
		$this->db->where('product_id',$pro_id);
		$data=$this->db->get('tbl_product_image');
		return $data->result();
	}
	function productmyorderprice($id)
	{
		$this->db->where('id',$id);
		$data=$this->db->get('tbl_product');
		return $data->result();
	}
	function selectorder($id)
	{
		$this->db->where('id',$id);
		$data= $this->db->get("tbl_order");
		return $data->result();		
	}
	function detailcomment($data)
	{
		$data=$this->db->insert('tbl_review',$data);
		return $data;
	}
	function selectreviewbyid($id)
	{
		$this->db->where('pro_id',$id);
		//$this->db->where('status',1);
		$data=$this->db->get('tbl_review');
		return $data->result();
		
	}
	function searchallproduct($searchproduct)
	{
		$this->db->where('category_id',$searchproduct);
		$this->db->like('name',$searchproduct);
		$this->db->or_like('price',$searchproduct);
		$data=$this->db->get('tbl_product');
		//print_r($data);die;
		return $data->result();
		
	}
	
	function selectallreviewcount($id)
	{
		$this->db->where('pro_id',$id);
		$data=$this->db->get('tbl_review');
		//print_r($data);
		return $data->num_rows();
	}
	
	function select_vendor_by_product_id($id)
	{ 
		error_reporting(0);
		$this->db->where('id',$id);
		$data=$this->db->get('tbl_product')->result();
		if($data[0]->vender_type!="")
		{
			$uid = $data[0]->vender_type;	
		}	
		else
		{
			$uid = 0;
		}
		if($uid==0)
		{
			$array = array('id'=>'admin','name'=>'admin','email'=>'admin','mobile'=>'admin','address'=>'admin');
			return $array;
		}
		else
		{
			$this->db->where('id',$uid);
			$user = $this->db->get($this->table)->result();
			$array = array('id'=>$user[0]->id,'name'=>$user[0]->name,'email'=>$user[0]->email,'mobile'=>$user[0]->mobile,'address'=>$user[0]->address);
			return $array;
		}		
	}
	
	function select_vendor_by_productid($id)
	{ 
		error_reporting(0);
		$this->db->where('id',$id);
		$data=$this->db->get('tbl_product')->result();
		if($data[0]->vender_type!="")
		{
			$uid = $data[0]->vender_type;
            $this->db->where('id',$uid);
			$vandor = $this->db->get($this->table)->result();
			return $vandor;	
		}
		else
		{
			return array();
		}
	}
	
	function insert_order_comment($data)
	{
		$data=$this->db->insert('tbl_order_comments',$data);
		return $data;
	}
	function select_order_comment($id)
	{
		$this->db->where('order_id',$id);
		$data=$this->db->get('tbl_order_comments');
		return $data->result();
	}						public function getWalletStatementHistory($uid)	{	$this->db->where('user_id',$uid);				$data = $this->db->get('tbl_walletstatement');			return $data->result();	}
}
?>