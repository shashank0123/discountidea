<?php 

class Category_model extends CI_Model

{

	public function selectAllCategory()

	{

		$data=$this->db->get("tbl_category");

		return $data->result();

	}

	public function selectCategoryForHome()

	{

		$this->db->where('display_on_home', 1);

		$data=$this->db->get("tbl_category");

		return $data->result();

	}

	public function updateBycategory($id,$data)

	{

		$this->db->where('id', $id);

		return $this->db->update("tbl_category",$data);	

	}

	public function selectCategoryByID($id)

	{

        $this->db->where('id',$id);

		$data = $this->db->get("tbl_category");

		return $data->result();	

	}

	public function insertCategory($data)

	{

		$this->db->insert("tbl_category",$data);

	}

	public function listallcategory(){

		$this->db->where("status",1);

		$data=$this->db->get("tbl_category");

		return $data->result();

	}

	public function deletecategory($id)

	{

		$this->db->where('id', $id);

		$this->db->delete('tbl_category');

	}

	

	public function selectAllParentCategory()

	{

		$this->db->where('parent_id',0);

		$this->db->where("status",1);

		$data=$this->db->get("tbl_category");

		return $data->result();

	}

	

	public function selectAllCategoryParentID($id=0,$isshowmcategory='')

	{

		$this->db->where('parent_id',$id);

		$this->db->where("status",1);
		if($isshowmcategory!=""){
			$this->db->where("display_on_mcategory",$isshowmcategory);
		}

		$data=$this->db->get("tbl_category");

		return $data->result();

	}

	

	// category images 

	public function insertCategoryImages($data)

	{

		$this->db->insert("tbl_category_image",$data);

	}

	 public function selectAllCategoryImagesByid($id)

	{

		$this->db->where('id',$id);

		$data=$this->db->get("tbl_category_image");

		return $data->result();

	}

    public function selectAllCategoryImages($id)

	{

		$this->db->where('category_id',$id);

		$data=$this->db->get("tbl_category_image");

		return $data->result();

	}

	public function deletecategoryimagesbyid($id)

	{

		$this->db->where('id', $id);

		$this->db->delete('tbl_category_image');

	}

	function getNavigation($cid)

	{

		$html ="";

		$this->db->where("status",1);

		$this->db->where("parent_id",$cid);

		$data=$this->db->get("tbl_category");

		$categorydata = $data->result();

		if(count($categorydata)>0)

		{

			$html .= '<div style="width:1032px;" class="wrap-popup">';

            $html .= '<div class="popup">';

            $html .= '<div class="row">';

			foreach($categorydata as $category)

			{
                             $sql = 'SELECT cat.*,pcat.title as ptitle FROM `tbl_category` cat inner join tbl_category pcat on pcat.id=cat.parent_id where cat.id= ?';
                             $sws_cat = $this->db->query($sql, array($category->id))->row();

				$html .= '<div class="col-sm-2">';

				$html .= "<h3><a href='".base_url(slugurl($sws_cat->ptitle).'/'.slugurl($category->title)).'/'.encodeurlval($category->id)."'>".$category->title."</a></h3>";

				$child_nav = $this->countChildNav($category->id);

				if(count($child_nav)>0)

				{

					$html .= '<ul class="nav">';

                  	foreach($child_nav as $child_category)

                    {

						$html .=  '<li>';

						$html .= "<a href='".base_url('products/'.slugurl($child_category->title)).'/'.encodeurlval($child_category->id)."'>".$child_category->title."</a>";

						//$html .=  $child_category->title;

						$html .=  '</li>';

					}  					

					$html .= '</ul>';

				}

				$html .= '<br></div>';

			}

			$html .= '</div>';

            $html .= '</div>';

            $html .= '</div>';

		}

		return $html;

	}

	function countChildNav($cid)

	{

		$this->db->where("status",1);

		$this->db->where("parent_id",$cid);

		$data=$this->db->get("tbl_category");

		$categorydata = $data->result();

		return $categorydata;		

	}

	

	function getchilcategoryinonearray($id)

	{

		$data = $this->getallsubcategory($id);

		$array = array();

		if(count($data)>0)

		{

			foreach($data as $key => $val)

			{

				array_push($array,$val[0]);

				//echo $val[0].'<br>';

			}

			array_push($array,$id);

		}

		return $array;

	}

	

	function getallsubcategory($id=0)

	{   

		$category_data = array();

		$this->db->where("parent_id",$id);

		$cat_data=$this->db->get("tbl_category");

		$categorydata = $cat_data->result();

		if(count($categorydata)>0)

		{

			$category_data[] = array($categorydata[0]->id);

			$children = $this->getallsubcategory($categorydata[0]->id);

			if($children)

			{

				$category_data = array_merge($children, $category_data);

			}

			return $category_data;

		}	

	}	

	

	function breadcrumbs($id,$currect_id=0)

	{   

		$category_data = array();

		$this->db->where("id",$id);

		$cat_data=$this->db->get("tbl_category");

		$categorydata = $cat_data->result();

		if(count($categorydata)>0)

		{

			$arrow = ($currect_id==0)?'<span>»</span>':'';

			$class = ($currect_id!=0)?'style="font-weight:bold;"':'';				

			$url = ($currect_id==0)?base_url('products/'.slugurl($categorydata[0]->title)).'/'.encodeurlval($categorydata[0]->id):'#';	

			$breadcrumbs = "<li ".$class.">";

			$breadcrumbs .= "<a href='".$url."'>".$categorydata[0]->title.$arrow.'</a>';

			$breadcrumbs .= "</li>";

			$category_data[] = array('breadcrumbs' => $breadcrumbs);

			$children = $this->breadcrumbs($categorydata[0]->parent_id,0);

			if($children)

			{

				$category_data = array_merge($children, $category_data);

			}

			return $category_data;

		}	

	}



	function getAdminMenus($cid)

	{

		$html ="";

		$this->db->where("status",1);

		$this->db->where("parent_id",$cid);

		$data=$this->db->get("tbl_category");

		$categorydata = $data->result();

		if(count($categorydata)>0)

		{

			$html .= '<ul>';

			foreach($categorydata as $category)

			{

				$html .= '<li>';

				$html .= "<div class='mytree' ><a>".$category->title."</a></div>";				

				$html .= '<span style="float: right; margin-top: -3%;"> ';

				$html .='<a class="btn btn-warning btn-xs" href="'.site_url('index.php/categorycontroller/manageimage/'.$category->id.'').'" title="Manage category Image"><i class="fa fa-picture-o" aria-hidden="true"></i></a>';

				$html .='<a class="btn btn-primary btn-xs" href="'.site_url('index.php/categorycontroller/edit/'.$category->id.'').'" title="edit this record"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>';

				$html .='<a class="btn btn-info btn-xs" href="'.site_url('index.php/categorycontroller/deletecategory/'.$category->id.'').'" title="delete this record" onclick="return confirm('."Are you sure you want to delete this item?".');"><i class="fa fa-trash" aria-hidden="true"></i></a>';

				$html .= "</span>";

				

				$child_nav = $this->countChildNav($category->id);

				if(count($child_nav)>0)

				{

					$html .= '<ul>';

                  	foreach($child_nav as $child_category)

                    {

						$html .=  '<li>';

						$html .=  "<div class='mytree'><a>".$child_category->title."</a></div>";

						$html .= '<span style="float: right; margin-top: -3%;"> ';

						$html .='<a class="btn btn-warning btn-xs" href="'.site_url('index.php/categorycontroller/manageimage/'.$child_category->id.'').'" title="Manage category Image"><i class="fa fa-picture-o" aria-hidden="true"></i></a>';

						$html .='<a class="btn btn-primary btn-xs" href="'.site_url('index.php/categorycontroller/edit/'.$child_category->id.'').'" title="edit this record"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>';

						$html .='<a class="btn btn-info btn-xs" href="'.site_url('index.php/categorycontroller/deletecategory/'.$child_category->id.'').'" title="delete this record" onclick="return confirm('."Are you sure you want to delete this item?".');"><i class="fa fa-trash" aria-hidden="true"></i></a>';

						$html .= "</span>";

						

						$html .=  $this->getAdminMenus($category->id);

						$html .=  '</li>';

					}  					

					$html .= '</ul>';

				}

				$html .= '</li>';

			}

			$html .= '</ul>';           

		}

		return $html;

	}

	

	function getLeftSideBarFilterMenus($cid)

	{

		$html ="";		  

	       $html .= '<ul class="children">';

		   $child_nav = $this->countChildNav($cid);

		   foreach($child_nav as $child_category)

           {

			    $child_nav2 = $this->countChildNav($child_category->id); 

				$li_class = (count($child_nav2)>0)?'cat-parent':'';

				$html .=  '<li class="cat-item '.$li_class.'">';

				$html .=  "<a href='".base_url('products/'.slugurl($child_category->title)).'/'.encodeurlval($child_category->id)."'>".$child_category->title."</a>";

				$html .=  $this->category_model->getLeftSideBarFilterMenus($child_category->id);

				$html .= '</li>'; 

		   }

		   $html .= '</ul>';  

		return $html;

	}

	

	// category space manage

	function getSelectOption($id,$eid=0)

	{   

		$category_data = array();		

		$this->db->where("parent_id",$id);

		$cat_data=$this->db->get("tbl_category");

		$categorydata = $cat_data->result();		

		if($categorydata>0)

		{

			foreach($categorydata as $category)

			{	

			    $class = (count($this->countChildNav($category->id))>0)?'style="font-weight: bold;"':'';

				$count = $this->managespace(count($this->childcatids($category->parent_id)));

			    //print_r($count); 

				$selected = ($category->id==$eid)?'selected':'';

				echo "<option value='".$category->id."' ".$selected." $class>".$count. $category->title."</option>";

				$this->getSelectOption($category->id,$eid);		

			}

		}	

	}

	function getRoottOption($id=0)

	{   

		$category_data = array();		

		$this->db->where("parent_id",$id);
		$this->db->order_by('title','asc');
		$cat_data=$this->db->get("tbl_category");

		$categorydata = $cat_data->result();		

		if($categorydata>0)

		{

			echo "<option value='' ".$selected." $class>Select Category</option>";
			foreach($categorydata as $category)

			{	

			 //    $class = (count($this->countChildNav($category->id))>0)?'style="font-weight: bold;"':'';

				// $count = $this->managespace(count($this->childcatids($category->parent_id)));

			 //    //print_r($count); 

				// $selected = ($category->id==$eid)?'selected':'';

				echo "<option value='".$category->id."' ".$selected." $class>".$count. $category->title."</option>";

				//$this->getSelectOption($category->id,$eid);		

			}

		}	

	}

	public function getSubCategory($catID)
	{
		$this->db->where("parent_id",$catID);
		if($catID==0)
			$this->db->where("status",'1');
		$this->db->order_by('title','asc');
		return $this->db->get("tbl_category")->result();
	}

	public function getProductCategoryLevel($catID)
	{
		$this->db->select('t1.id as catId1, t2.id as catId2, t3.id as catId3');
		$this->db->join('tbl_category as t2', 't2.id=t1.parent_id', 'left');
		$this->db->join('tbl_category as t3', 't3.id=t2.parent_id', 'left');
		$this->db->where("t1.id",$catID);
		return $this->db->get("tbl_category as t1")->row();
	}

	public function getProductCategoryNLevel($catID)
	{
		$this->db->select('t1.id, t1.parent_id');
		$this->db->where("t1.id",$catID);
		$row = $this->db->get("tbl_category as t1")->row();
		$cats = '';
		if(count($cats)>0){
			$cats .= $row->id.':'.$row->parent_id.'^';
			if($row->parent_id) {
				$cats .= $this->getProductCategoryNLevel($row->parent_id);
			}
		}
		return $cats;
	}

	public function getCategoryChild($catID)
	{
		$this->db->select('id,title');
		$this->db->where("parent_id",$catID);
		if($catID==0)
			$this->db->where("status",'1');
		$this->db->order_by('title','asc');
		$listCat = $this->db->get("tbl_category")->result();
		$data['0'] = ($catID==0) ? 'Select Category' : 'Select Sub Category';
		if(count($listCat)>0) {
			foreach($listCat as $list){
				$data[$list->id] = $list->title;
			}
		}
		return $data;
	}

	function getsubRoottOption($id=0)
	{   

		$category_data = array();		

		$this->db->where("parent_id",$id);

		$cat_data=$this->db->get("tbl_category");

		$categorydata = $cat_data->result();
		$html='';

		$html.=' <tr id="ca_1">
				<td>Sub Category</td>
				<td>
				<select id="root" name="category_id" onChange="get_subcat()" class="form-control">';		

		if($categorydata>0)

		{

			foreach($categorydata as $category)

			{				

				$html.= "<option value='".$category->id."' ".$selected." $class>".$count. $category->title."</option>";


			}
			$html.='</td> </tr>';
			return $html;

		}	

	}

	function childcatids($id)

	{   

		$category_data = array();		

		$this->db->where("id",$id);

		$cat_data=$this->db->get("tbl_category");

		$categorydata = $cat_data->result();		

		if(count($categorydata)>0)

		{  	        

			$ids = $categorydata[0]->id;		

			$category_data[] = array('ids' => $ids);

			$children = $this->childcatids($categorydata[0]->parent_id);

			if($children)

			{

				$category_data = array_merge($children, $category_data);

			}

			return $category_data;

		}	

	}

	

	function managespace($number)

	{	

		$html = "";

		for($i=1; $i<=$number; $i++)

		{

			$html .= '----';

		}

		return $html.'&nbsp;';

	}

	// home category

	public function select_all_home_category()

	{

		$data=$this->db->get("tbl_homecategory");

		return $data->result();

	}

	public function select_active_home_category()

	{

		$this->db->where('status', 1);

		$data=$this->db->get("tbl_homecategory");

		return $data->result();

	}

	public function select_home_category_by_id($id)

	{

		$this->db->where('id', $id);

		$data=$this->db->get("tbl_homecategory");

		return $data->result();

	}

	public function insert_home_category_images($data)

	{

		$this->db->insert("tbl_homecategory",$data);

	}

	public function update_home_category_byid($id,$data)

	{

		$this->db->where('id', $id);

		return $this->db->update("tbl_homecategory",$data);	

	}

	public function delete_home_category_byid($id)

	{

		$this->db->where('id', $id);

		$this->db->delete('tbl_homecategory');

	}

	public function search_Suggestion($qtext)

	{
		$this->db->select('name');
		$this->db->like('name', $qtext);
		$this->db->limit(20);
		$rows = $this->db->get('tbl_product')->result();
		$results = array();
		if(count($rows)>0){
			foreach($rows as $key=>$row){
				$results[$key] = $row->name;
			}
		}

		return $results;
	}

}	