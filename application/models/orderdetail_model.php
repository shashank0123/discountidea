<?php
class Orderdetail_model extends CI_Model
{
	function selectorder()
	{      $this->db->order_by('id','desc'); 
		$data= $this->db->get("tbl_order");
		return $data->result();		
	}
	
	function select_order_status_success()
	{
		$this->db->where('status','delivered successfully');
		$data= $this->db->get("tbl_order");
		return $data->result();		
	}
	
	function select_order_refund()
	{
		$this->db->where(array('requestapprovalstatus'=>'Refund Approved','status'=>'delivered successfully'));
		$data= $this->db->get("tbl_order");
		return $data->result();		
	}
	
	function selectusertable($id)
	{
		
		$this->db->where('id',$id);
		$data=$this->db->get('table_user');
		//print_r($data);
		return $data->result();
	}
	function selectorderitme($id)
	{
		$this->db->where('order_id',$id);
		$data= $this->db->get("tbl_item");
		return $data->result();		
	}
	
	function selectorderitme_by_vandor($id)
	{
		$this->db->where('vendor',$id);
		$data= $this->db->get("tbl_item");
		return $data->result();		
	}
	
	public function update_order($id,$data){
		$this->db->where("id",$id);
		return  $this->db->update("tbl_order",$data);
	
	}
	
	function select_order_item($id)
	{
		$this->db->where('id',$id);
		$data= $this->db->get("tbl_item");
		return $data->result();		
	}
       function select_order_With_item()
	{       $this->db->select('tbl_item.*,tbl_order.*,tbl_item.total_amount as proAmount');
                $this->db->from('tbl_order');      
		$this->db->join('tbl_item', 'tbl_item.order_id = tbl_order.id', 'left');
                $this->db->order_by('tbl_order.id','desc'); 
		$data= $this->db->get();
		return $data->result();		
	}
	
	public function update_order_item($id,$pid,$data){
		$this->db->where(array("order_id"=>$id,"pro_id"=>$pid,"user_cancel"=>0));
		return  $this->db->update("tbl_item",$data);
	
	}

	/*added for shipment */
	public function select_order($id){
				$this->db->where("id",$id);
				$data= $this->db->get('tbl_order');
				return $data->row();	
	
	}
	public function select_order_items($id){
				$this->db->where("order_id",$id);
				$data= $this->db->get();
				return $data->row();	
	
	}
	public function select_deleviry_address($id){
		$this->db->where("ord_id",$id);
		$data= $this->db->get('shiping_detail');
		return $data->row();	
	}

	public function store_shipping_detail($data=array())
	{
		return ($this->db->insert('tbl_order_shipping_detail',$data)) ? '1' : '0';
	}

	public function update_shipping_detail($data=array(),$id)
	{
		$this->db->where("id",$id);
		return ($this->db->update('tbl_order',$data)) ? '1' : '0';

	}

	public function updateOrderPackingSlip($id=0,$data=array())
	{
		$this->db->where("order_id",$id);
		return ($this->db->update('tbl_order_shipping_detail',$data)) ? 1 : 0;
	}

	public function getOrderShippingDetail($orderID=0)
	{
		return ($orderID>0) ? $this->db->select()->where('order_id',(int)$orderID)->get('tbl_order_shipping_detail')->row() : array();
	}

	/*
	* shipping_charge_status
	*/

	public function shipping_charge_status($orderID='')
	{
				$this->db->select("TBOR.shipping_charge_status,TBOR.shipping_charge,TBOR.deleviry_company_id");
				$this->db->where('TBOR.id',$orderID);	
	 	 return $this->db->get('tbl_order AS TBOR')->row();
	 	
	}

	/*
	* shipping_payment_detail for vendor
	*/

	public function shipping_payment_detail($vendorID='')
	{
				$this->db->select("tvs.order_id,tsc.name,tvs.mihpayid,tvs.amount,tvs.status,tvs.added_date");
				$this->db->from('tbl_vendor_shipping_payu_response as tvs ');
				$this->db->join('tbl_order as tos','tos.id = tvs.order_id','inner');
				$this->db->join('tbl_shipping_companies as tsc','tsc.id = tos.deleviry_company_id','inner');
				$this->db->where('tvs.vendor_id',$vendorID);	
	 	 return $this->db->get()->result();
	 	
	}

	/*
	* shipping_payment_report for admin area 
	*/

	public function shipping_payment_report()
	{
				$this->db->select("tvs.order_id,tu.fname,tu.lname,tsc.name,tvs.mihpayid,tvs.amount,tos.total_amount,tvs.status,tu.mobile,tvs.added_date");
				$this->db->from('tbl_vendor_shipping_payu_response as tvs ');
				$this->db->join('tbl_order as tos','tos.id = tvs.order_id','inner');
				$this->db->join('table_user as tu','tu.id = tvs.vendor_id','inner');
				$this->db->join('tbl_shipping_companies as tsc','tsc.id = tos.deleviry_company_id','inner');
	 			return $this->db->get()->result();
	 	
	}

	public function clculate_product_weight($proid='')
	{
		$this->db->select('height,length,width,weight');
		$this->db->where('id',$proid);
	  	return $this->db->get('tbl_product')->row();

	}

	
}
?>