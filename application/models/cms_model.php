<?php
class Cms_model extends CI_Model
{
	function selectAllCms()
	{
		$data= $this->db->get("tbl_cms");
		return $data->result();		
	}
	
	function selectAllActiveCms()
	{
		$this->db->where('status', 1);
		$data= $this->db->get("tbl_cms");
		return $data->result();		
	}
	
	function selectCmsById($id)
	{
		$this->db->where('id', $id);
		$data= $this->db->get('tbl_cms');
		return $data->result();
	}
	
	function insert($data)
	{
		$result= $this->db->insert('tbl_cms', $data);
		return $result;
	}
	
	function update($id,$data)
	{
		$this->db->where('id', $id);
		return $this->db->update('tbl_cms',$data);
	}
	
	function delete($id)
	{
		$this->db->where('id', $id);
		return $this->db->delete('tbl_cms');
	}
function selectprivacypolicy($privacy)
	{
		$this->db->where('id',$privacy);
		$data=$this->db->get('tbl_cms');
		return $data->result();
	}

function FrontPagesActive($url)
	{
		$this->db->where('status', 1);
$this->db->where('url', $url);
		$data= $this->db->get("tbl_cms");
		return $data->row();		
	}
	
	
}
?>