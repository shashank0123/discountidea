<?php

class Vendor_model extends CI_Model
{
	
	// vendor model helpdesk
	
	function insert_help_desk($data, $vender_id, $pid)
	{
		date_default_timezone_set("Asia/Kolkata");
		
		$data1 = array(
		'uid' => $data['uid'],
		'order_product' => $data['order_product'],
		'product_id' => $pid,
		'vender_id' => $vender_id,
        'type' => $data['type'],
        'subject' => $data['subject'],
        'query' => $data['query'],
		'file' => $data['file'],		
		'status' => 'pending',
		'queryTime' => date("Y-m-d H:i:s")
		);
		
		$this->db->insert('tbl_helpdesk', $data1);
		
	}
	
	function insert_comment($data)
	{
		date_default_timezone_set("Asia/Kolkata");
		
		$data1 = array(
		'uid' => $data['uid'],
		'comment' => $data['comment'],
		'query_id' => $data['query_id'],
        'comment_type' => 'vendor',
		'comment_time' => strtotime("now"),
		'comment_date' => date("Y-m-d  H:i:s")
		);
		
		$this->db->insert('tbl_helpdesk_comments', $data1);
		
	}
	function getcommentById($id)
	{
		
		$this->db->select('*');
		$this->db->where('query_id',$id);
		$this->db->order_by('comment_time', 'DESC');
		$data = $this->db->get('tbl_helpdesk_comments');
		return $data->result();
	}
	function getHelpDepart()
	{
		$this->db->select('*');
		$data = $this->db->get('tbl_helpdesk_department');
		return $data->result();
	}
	function getLoginUserVar($var)
	{
		$data = $this->session->userdata($var);
		return $data;
	} 
	function getHelpdeskByUserId($userid)
	{ 
		$this->db->where('vender_id',$userid);
		$data=$this->db->get('tbl_helpdesk');
		return $data->result();
	}
	function getOrderByUserId($userid)
	{
		$this->db->where('user_id',$userid);
		$data=$this->db->get('tbl_order');
		return $data->result();
	} 
	
	function getHelpdeskById($userid)
	{
		$this->db->where('id',$userid);
		$data=$this->db->get('tbl_helpdesk');
		return $data->result();
	}
	
	public function insert($tbl,$data)
	{
	         $this->db->insert($tbl,$data);
		return $this->db->insert_id();
	}
	public function getVendorDetail($vendorID='')
	{
	           $this->db->select("fname,lname,email,mobile");
	           $this->db->where("id",$vendorID);
		return $this->db->get('table_user')->row();
	}
	public function vendor_shipping_payu_response($data=array())
	{
		
		if(($this->db->insert('tbl_vendor_shipping_payu_response',$data)))
		{
			$deleviry_company_id="1";
		$shipping_charge_status="1";
			if($this->update_shippig_detail($data['order_id'],$deleviry_company_id,$data['amount'],$shipping_charge_status))
				return '1';
		}
	}
	public function update_shippig_detail($orderID='',$deleviry_company_id='',$shipping_charge='',$shipping_charge_status='')
	{	
		$data=array(
			'deleviry_company_id'=>$deleviry_company_id,
			'shipping_charge'=>$shipping_charge,
			'shipping_charge_status'=>$shipping_charge_status
		);
		$this->db->where('id',$orderID);
		$this->db->update("tbl_order",$data);
		return TRUE;
		//print $this->db->last_query(); die;
	}
	
}