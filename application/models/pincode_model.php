<?php
class Pincode_model extends CI_Model
{
	function selectAllPincode($limit='',$offset='')
	{
		/*if(!empty($limlt){
		
		}
		*/
		if($limit!=''){
		     $this->db->limit($limit, $offset);
		}
        	$this->db->order_by('pincode','asc');
		$data= $this->db->get("tbl_pincode");
		return $data->result();		
	}
	
	
	function checkpincode($ids,$pin)
	{
		if(count($ids)>0)
			$this->db->where_in('id', $ids);
		$this->db->where('pincode', $pin);
		$data= $this->db->get("tbl_pincode");
		return $data->result();		
	}
	function checkpincodeby_pincode($pin)
	{
		$this->db->where('pincode', $pin);
		$data= $this->db->get("tbl_pincode");
		return $data->result();		
	}
	function selectAllActivePincode()
	{
                $this->db->order_by('pincode','ASC');
		$data= $this->db->get("tbl_pincode");
		return $data->result();		
	}
	
	function selectPincodeById($id)
	{
		$this->db->where('id', $id);
		$data= $this->db->get('tbl_pincode');
		return $data->result();
	}
	
	function insert($data)
	{

	$result = $this->db->insert_batch('tbl_pincode', $data);
	return $result;
		
	}
	
function upload_sampledata_csv()
{
$fp = fopen($_FILES['userfile']['tmp_name'],'r') or die("can't open file");
while($csv_line = fgetcsv($fp,1024)) 
{
for ($i = 0, $j = count($csv_line); $i < $j; $i++) 
{
$insert_csv = array();
echo $insert_csv['id'] = $csv_line[0];
 echo $insert_csv['pincode'] = $csv_line[1];
echo $insert_csv['day'] = $csv_line[2];
}
$data = array(
'id' => $insert_csv['id'] ,
'pincode' => $insert_csv['pincode'],
'day' => $insert_csv['day']);

$data['crane_features']=$this->db->insert('tbl_pincode', $data);
}
fclose($fp) or die("can't close file");
$data['success']="success";
return $data;
}
function get_car_features_info()
{
$get_cardetails=$this->db->query("select * from tbl_pincode");
return $get_cardetails;
}

	function update($id,$data)
	{
		$this->db->where('id', $id);
		return $this->db->update('tbl_pincode',$data);
	}
	
	function delete($id)
	{
		$this->db->where('id', $id);
		return $this->db->delete('tbl_pincode');
	}
		function selectAllPincode_withoutid($array=""){
	
		if(!empty($array)){
		//echo"----------------";
		//	print_r($array);
		//echo"----------------";
			$this->db->where_not_in('id', $array);
		}
	        	$this->db->order_by('pincode','asc');
			$data= $this->db->get("tbl_pincode");
		return $data->result();		
	}

	##########################################
	function getHSN($limit='',$offset='')
	{
		if($limit!=''){
		     $this->db->limit($limit, $offset);
		}
        	$this->db->order_by('hsn_code','asc');
		$data= $this->db->get("tbl_hsn");
		return $data->result();		
	}

	function hsn_add($data)
	{
		return ($this->db->insert('tbl_hsn', $data)) ? TRUE : FALSE;
	}

	function hsn_detail($id)
	{
		$this->db->where('hsn_id', $id);
		return $this->db->get('tbl_hsn')->row();
	}

	function hsn_edit($id,$data)
	{
		$this->db->where('hsn_id', $id);
		return ($this->db->update('tbl_hsn',$data)) ? TRUE : FALSE;
	}
	
	function hsn_delete($id)
	{
		$this->db->where('hsn_id', $id);
		return ($this->db->delete('tbl_hsn')) ? TRUE : FALSE;
	}

	function hsn_add_array($data)
	{
		if(count($data)>0){
			$newHsn = array();
			foreach ($data as $key => $row) {
				$this->db->where('hsn_code', $row['hsn_code']);
				$detail = $this->db->get('tbl_hsn')->row();
				if(count($detail)>0){
					$this->db->where('hsn_id', $detail->hsn_id);
					$this->db->update('tbl_hsn',$row);
				}
				else {
					$newHsn[] = $row;	
				}
			}
			if(count($newHsn)>0){
				return ($this->db->insert_batch('tbl_hsn', $newHsn)) ? TRUE : FALSE;
			}
			return TRUE;
		}
		return FALSE;
	}

	##########################################
	function getTaxCode($limit='',$offset='')
	{
		if($limit!=''){
		    $this->db->limit($limit, $offset);
		}
        $this->db->order_by('tax_code','asc');
		$data= $this->db->get("tbl_taxcodes");
		return $data->result();		
	}

	function taxcode_add($data)
	{
		return ($this->db->insert('tbl_taxcodes', $data)) ? TRUE : FALSE;
	}

	function taxcode_detail($id)
	{
		return $this->db->where('taxcode_id', $id)->get('tbl_taxcodes')->row();
	}

	function taxcode_edit($id,$data)
	{
		return ($this->db->where('taxcode_id', $id)->update('tbl_taxcodes',$data)) ? TRUE : FALSE;
	}
	
	function taxcode_delete($id)
	{
		return ($this->db->where('taxcode_id', $id)->delete('tbl_taxcodes')) ? TRUE : FALSE;
	}

	function taxcode_add_array($data,$taxCodes)
	{
		if(count($data)>0){
			$this->db->where_in('tax_code', $taxCodes)->delete('tbl_taxcodes');
			return ($this->db->insert_batch('tbl_taxcodes', $data)) ? TRUE : FALSE;
		}
		return FALSE;
	}
}
?>