<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "home";
$route['vendor/loginuser'] = "vendor/loginuser";
$route['index.php/admin/dashboard'] = "index.php/admin/dashboard";
$route['index.php/admin/forgot_password'] = "index.php/admin/forgot_password";
$route['home/(:any)'] = "home/$1";
$route['faq/(:any)'] = "faq/$1";
$route['user/(:any)'] = "user/$1";
$route['index.php/blog/display/(:any)'] = "index.php/blog/display/$1";

$route['vendor/getwalletbalance/(:any)'] = "vendor/getwalletbalance/$1";
$route['admin/dashboard'] = "admin/dashboard";
$route['homepagepopup/(:any)/(:any)'] = "homepagepopup/$1/$2";
$route['homepagepopup/(:any)'] = "homepagepopup/$1";
$route['banner/(:any)/(:any)'] = "banner/$1/$2";
$route['banner/(:any)'] = "banner/$1";
$route['categorycontroller/(:any)/(:any)'] = "categorycontroller/$1/$2";
$route['categorycontroller/(:any)'] = "categorycontroller/$1";
$route['blogcategory/(:any)/(:any)'] = "blogcategory/$1/$2";
$route['blogcategory/(:any)'] = "blogcategory/$1";
$route['blog/(:any)/(:any)'] = "blog/$1/$2";
$route['blog/(:any)'] = "blog/$1";
$route['admin/edit_blogbanner'] = "admin/edit_blogbanner";
$route['admin/userList'] = "admin/userList";
$route['admin/adduser'] = "admin/adduser";
$route['vendor/(:any)/(:any)'] = "vendor/$1/$2";
$route['vendor/(:any)'] = "vendor/$1";
$route['adminproducts/(:any)/(:any)'] = "adminproducts/$1/$2";
$route['adminproducts/(:any)'] = "adminproducts/$1";
$route['pincode/(:any)/(:any)'] = "pincode/$1/$2";
$route['pincode/(:any)'] = "pincode/$1";
$route['filters/(:any)/(:any)'] = "filters/$1/$2";
$route['filters/(:any)'] = "filters/$1";
$route['offer/(:any)/(:any)'] = "offer/$1/$2";
$route['offer/(:any)'] = "offer/$1";
$route['cms/(:any)/(:any)'] = "cms/$1/$2";
$route['cms/(:any)'] = "cms/$1";
$route['newsletter/(:any)/(:any)'] = "newsletter/$1/$2";
$route['newsletter/(:any)'] = "newsletter/$1";
$route['our_client/(:any)/(:any)'] = "our_client/$1/$2";
$route['our_client/(:any)'] = "our_client/$1";
$route['order/(:any)'] = "order/$1";
$route['admin_meta_tag/(:any)'] = "admin_meta_tag/$1";
$route['add_pages/(:any)'] = "add_pages/$1";
$route['block/(:any)'] = "block/$1";
$route['admin/setting'] = "admin/setting";
$route['admin/distribute_vendor_money'] = "admin/distribute_vendor_money";
$route['admin/seller_credit_settlment'] = "admin/seller_credit_settlment";
$route['admin/vandor_income'] = "admin/vandor_income";
$route['admin/vandor_return_deductions'] = "admin/vandor_return_deductions";
$route['admin/edit_credit_settlment/(:any)'] = "admin/edit_credit_settlment/$1";
$route['admin/edit_vendor_return_deducation/(:any)'] = "admin/edit_vendor_return_deducation/$1";
$route['admin/help_desk'] = "admin/help_desk";
$route['admin/download_all_helpdesk_query_mis'] = "admin/download_all_helpdesk_query_mis";
$route['admin/manage_department'] = "admin/manage_department";
$route['admin/add_department'] = "admin/add_department";
$route['admin/edit_department/(:any)'] = "admin/edit_department/$1";
$route['admin/logout'] = "admin/logout";

$route['cart/addtocart'] = "cart/addtocart";
$route['cart/remove/(:any)'] = "cart/remove/$1";
$route['cart/checkout'] = "cart/checkout";
$route['cart/update'] = "cart/update";
$route['cart/emptycart'] = "cart/emptycart";
$route['adminproducts/(:any)/(:any)'] = "adminproducts/$1/$2";
$route['admin/user_ligin/(:any)'] = "admin/user_ligin/$1";
$route['admin/(:any)'] = "admin/$1";
$route['readers/(:any)'] = "pages/index/$1";
$route['user/loginuser'] = "user/loginuser";



//$route['products/(:any)/(:any)'] = "home/products_listing/$1/$2";
//$route['product/(:any)/(:any)'] = "home/product_detail/$1/$2";

$route['(:any)/(:any)/(:any)/(:any)'] = "home/product_detail/$3/$4";
$route['(:any)/(:any)/(:any)'] = "home/products_listing/$2/$3";
$route['(:any)/(:any)'] = "home/products_listing/$1/$2";
// $route['home/selectprocutcoment/(:any)'] = "home/selectprocutcoment";

$route['readers/public-event'] = "public_event_controller";
$route['readers/public_event_controller/user_data_submit'] ="public_event_controller/user_data_submit";


$route['earnyourdiscount'] = "vendor/earnyourdiscount";
$route['404_override'] = '';

include_once(FCPATH."route.php");
/* End of file routes.php */
/* Location: ./application/config/routes.php */