<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Pincode extends CI_Controller 
{
	function listing()
	{		

		$off = $this->uri->segment(3);
		
		$data['PINCODEDATA']= $this->pincode_model->selectAllPincode(200,$off);
		$this->load->view('admin/pincode/list',$data);
	}
	
	
	function add()
	{		
		if(isset($_POST['savedata']))
		{						
			$data['pincode'] = $this->input->post('pincode');
			$data['day'] = $this->input->post('day');
		
			$this->pincode_model->insert($data);			
			$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully saved.</div>');
			redirect('index.php/pincode/listing');			
		}
		$this->load->view('admin/pincode/add');
	}
	
   function uploadcsvview()
   {		
		
		if(isset($_POST['sub']))
		{						
			$data['pincode'] = $this->input->post('pincode');
			$data['day'] = $this->input->post('day');

		//print_r($_FILES['uploadFile']);
		$a = $_FILES['uploadFile'];
		$csv = array_map('str_getcsv', file($a['tmp_name']));
		$csv_pin = array();
		foreach ($csv as $row) {
			$n = $row[0];
			     array_push($csv_pin,array('pincode'=>$n));

		}
			$this->pincode_model->insert($csv_pin);			
			$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully saved.</div>');
			redirect('index.php/pincode/listing');			
		}
		$this->load->view('admin/pincode/uploadcsvview');	
		
	}
 
	function edit()
	{		
		$args=func_get_args();
		if(isset($_POST['updatedata']))
		{			
			$data['pincode'] = $this->input->post('pincode');
			$data['day'] = $this->input->post('day');
			$this->pincode_model->update($args[0],$data);
			$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully updated.</div>');
			redirect('index.php/pincode/listing');
		}
		$data['EDITPINCODE']= $this->pincode_model->selectPincodeById($args[0]);
		$this->load->view('admin/pincode/edit',$data);
	}
	
	function delete()
	{
		$args=func_get_args();		
		$this->pincode_model->delete($args[0]);
		$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully deleted.</div>');
		redirect('index.php/pincode/listing');
	}
    
	function getcartitempincode()
	{
		$cart_data = $this->cart->contents();
		$available_pins = "";
		foreach($cart_data as $items)
		{			
			$pid = $items['id'];
			$product = $this->product_model->selectProductById($pid);
			if(!empty($product[0]->pincode_ids))
			{
				$available_pins .= $product[0]->pincode_ids;
			}				
		}
		if($available_pins!="")
		{	
			return explode(",",$available_pins);
		}
		else
		{
			return array();
		}	
	}

	function checkpincodeoncheckout()
	{
		$vailable_pins = $this->getcartitempincode();
		$pincode = $_POST['pincode'];	
		$data = $this->pincode_model->checkpincode($vailable_pins,$pincode);
		if(count($data)>0)
		{
			//echo '<div class="alert alert-success">Delivery in '.$data[0]->day.' days.</div>';
		}
		else
		{
			echo '<div class="alert alert-danger">Delivery not available in this pincode.</div>';
		}
	}
	
	function checkpincode()
	{	
	//ajay	start
		
		$pincode  = $_POST['pincode'];
		$pro_pin = $_POST['pro_pin'];
		$pro_pin = $this->product_model->selectProductById($pro_pin); 

		if(!empty(json_decode($pro_pin[0]->pincode_ids))){
			//var_dump($pro_pin[0]->pincode_ids);
			$pro_pin =json_decode($pro_pin[0]->pincode_ids);	
			
				$newpin = array();
				foreach ($pro_pin as $row) {
					$days = $row->days;
					$data = $this->db->get_where('tbl_pincode',array('id' => $row->id));
					$data = $data->result_array();
					$pin = $data[0]['pincode'];		
					if($pin == $pincode){
						$remain_days = $days;
						break;
					}
					else{
						$remain_days = 0;
					}
		//ajay stop		
				}
		
		}else{
			/********Check in admin default pincode if no any pin found in product table**********/
		
			$data = $this->db->get_where('tbl_admin_pincode',array('pincode' => $pincode))->result_array();
			//print_r($data);
			if(!empty($data)){
				 $remain_days = $data[0]['days'];	
			}else{
				$remain_days = 0;
			}			
		}
		
		
		

		if(!empty($remain_days) && $remain_days != 0)
		{
			echo '<div class="alert alert-success">Delivery in '.$remain_days.' days.</div>';
		}
		else
		{
			echo '<div class="alert alert-danger">Delivery not available in this area.</div>';
		}
		
	}

	###########################
	function hsn_list()
	{
		$off = $this->uri->segment(3);		
		$data['rows']= $this->pincode_model->getHSN(200,$off);
		$this->load->view('admin/pincode/hsn_list',$data);
	}
	
	function hsn_add()
	{
		if(isset($_POST['savedata']))
		{						
			$data['hsn_code'] = $this->input->post('hsn_code');
			$data['description'] = $this->input->post('description');
		
			$this->pincode_model->hsn_add($data);			
			$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully saved.</div>');
			redirect('index.php/pincode/hsn_list');			
		}
		$this->load->view('admin/pincode/hsn_add');
	}

	function hsn_edit()
	{
		$args=func_get_args();
		if(isset($_POST['savedata']))
		{			
			$data['hsn_code'] = $this->input->post('hsn_code');
			$data['description'] = $this->input->post('description');
			$this->pincode_model->hsn_edit($args[0],$data);
			$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully updated.</div>');
			redirect('index.php/pincode/hsn_list');
		}
		$data['detail']= $this->pincode_model->hsn_detail($args[0]);
		$this->load->view('admin/pincode/hsn_edit',$data);
	}
	
	function hsn_delete()
	{
		$args=func_get_args();		
		$this->pincode_model->hsn_delete($args[0]);
		$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully deleted.</div>');
		redirect('index.php/pincode/hsn_list');
	}
	
 	function hsn_uploadcsv()
    {
    	if(isset($_POST['sub']))
		{						
			$a = $_FILES['uploadFile'];
			$csv = array_map('str_getcsv', file($a['tmp_name']));
			$csv_data = array();
			foreach ($csv as $row) {
				$hsn_code = $row[0];
				$description = $row[1];
				array_push($csv_data,array('hsn_code'=>$hsn_code,'description'=>$description));
			}
			$this->pincode_model->hsn_add_array($csv_data);			
			$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully saved.</div>');
			redirect('index.php/pincode/hsn_list');			
		}
		$this->load->view('admin/pincode/hsn_uploadcsv');		
	}

	###########################
	function taxcode_list()
	{
		$off = $this->uri->segment(3);		
		$data['rows']= $this->pincode_model->getTaxCode(200,$off);
		$this->load->view('admin/pincode/taxcode_list',$data);
	}
	
	function taxcode_add()
	{
		if(isset($_POST['savedata']))
		{						
			$data['tax_code'] 		= $this->input->post('tax_code');
			$data['tax_percentage'] = $this->input->post('tax_percentage');		
			$this->pincode_model->taxcode_add($data);			
			$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully saved.</div>');
			redirect('index.php/pincode/taxcode_list');			
		}
		$this->load->view('admin/pincode/taxcode_add');
	}

	function taxcode_edit()
	{
		$args=func_get_args();
		if(isset($_POST['savedata']))
		{			
			$data['tax_code'] = $this->input->post('tax_code');
			$data['tax_percentage'] = $this->input->post('tax_percentage');
			$this->pincode_model->taxcode_edit($args[0],$data);
			$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully updated.</div>');
			redirect('index.php/pincode/taxcode_list');
		}
		$data['detail']= $this->pincode_model->taxcode_detail($args[0]);
		$this->load->view('admin/pincode/taxcode_edit',$data);
	}
	
	function taxcode_delete()
	{
		$args=func_get_args();
		$this->pincode_model->taxcode_delete($args[0]);
		$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully deleted.</div>');
		redirect('index.php/pincode/taxcode_list');
	}
	
 	function taxcode_uploadcsv()
    {
    	if(isset($_POST['sub']))
		{						
			$a = $_FILES['uploadFile'];
			$csv = array_map('str_getcsv', file($a['tmp_name']));
			$csv_data = array();
			$taxCodes = array();
			foreach ($csv as $row) {
				$tax_code = $row[0];
				$tax_percentage = $row[1];
				array_push($csv_data,array('tax_code'=>$tax_code,'tax_percentage'=>$tax_percentage));
				$taxCodes[] = $tax_code;
			}
			$this->pincode_model->taxcode_add_array($csv_data,$taxCodes);			
			$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully uploaded.</div>');
			redirect('index.php/pincode/taxcode_list');			
		}
		$this->load->view('admin/pincode/taxcode_uploadcsv');		
	}
}