<?php
if ( !defined('BASEPATH')) exit('No direct script access allowed');
ini_set('post_max_size', '40M'); ini_set('upload_max_filesize', '18M'); 
class User extends CI_Controller
{
	
	function home()
	{
		$user_login_id = $this->session->userdata('USER_ID');
		if(empty($user_login_id))
		{			
		     redirect('index.php/user/create_account');
		}
		else
		{		
		      $this->load->view('front/index');
		}
	}
	
	function create_account()
	{
		
		if(isset($_POST['createbutn']))
		{
			
			$email=$this->input->post('email');
			$user_data=$this->user_model->selectuserby_email($email);
			$checkfile= count($user_data);
			if($checkfile >0)
			{
				$this->session->set_flashdata("message1","<br><div class='alert alert-danger'><strong>".$email."</strong> Email address already exists choose a different one.</div>");
				redirect('user/create_account');
			}
			$token=md5(time().$email);
			$name = $this->input->post('fname');
			$password=$this->input->post('password');
            $data['fname']=$this->input->post('fname');
			$data['mobile']=$this->input->post('mobile');

			$data['email']=$email;
			$data['password']=$password;
			$data['status']=1; 
			$data['vender_type']= 0;
			$data['activate_token']=$token;
			$insertdata = $this->user_model->insert_user($data);
			if($insertdata)
			{
				/*$this->email->set_newline("\r\n");
				$this->email->set_mailtype("html");
				$this->email->to($email);
				$this->email->from(ADMIN_EMAIL,'Discount Idea');
			    $this->email->subject('Mail Confirmation');*/
				$search  = array('{NAME}','{EMAIL}','{PASSWORD}');
				$replace = array($name,$email,$password);		
				$block_body = $this->block_model->selectblockbyid(7);
				$message =  str_replace($search,$replace,$block_body[0]->description);
				//echo $message; die;
				/*$message = "Dear ".$this->input->post('email').",<br>";
				$message .= "Thank you for registering with us.";
				$message .= "Your account has been created, you can login with the following credentials.<br>";
				$message .= "------------------------<br>";
				$message .= "USER ID: $email <br>";
				$message .= "PASSWORD: $password <br>";
				$message .= "------------------------<br><br>";
				$message .= "Thank you <br> Discount Idea support team";
				$this->email->message($message);
				$this->email->send();*/
		$this->user_model->sendmail('info@discountidea.com', $email, '', 'Mail Confirmation', $message);
				
			}
			
			$newsdata['email'] = $this->input->post('email');			
			$this->newsletter_model->insert($newsdata);
			$this->session->set_flashdata("message1","<br><div class='alert alert-success'><h5>Thanks For Registering With us! </h5></div>");
			redirect('user/create_account');
			
		}
		
		$this->load->view('front/login-page');
	}
	
	function loginuser()
	{
		$user_login_id = $this->session->userdata('USER_ID');
		if(!empty($user_login_id))
		{
			redirect('index.php/user/profile');
		}
		
		if(isset($_POST['buttonlogin']))
		{
			$email=$this->input->post('email1');
			$password=$this->input->post('password1');
			
			if(!empty($email) && !empty($password))
			{
			$data=$this->user_model->user_login($email,$password);
			if(count($data)>0)
			{
				if($data[0]->status==1)
				{
					$login_data=array('id'=>$data[0]->id,'USER_ID'=>$data[0]->id,'email'=>$data[0]->email,'user_type'=>$data[0]->vender_type,'logged_in'=>true);
					$this->session->set_userdata($login_data);
					if(isset($_GET['backurl']) && $_GET['backurl']!="")
					{
						redirect($_GET['backurl']);
					}
					else
					{
						redirect('index.php/user/home');
					}
				}
				else
				{
					$this->session->set_flashdata('message1','<div class="alert alert-danger">This user is inactive. Please contact us regarding this account.</div>');
					redirect('index.php/user/loginuser');
				}
			}
			else
			{				
				$this->session->set_flashdata('message1','<div class="alert alert-danger">Invalid username or password !</div>');
				redirect('index.php/user/loginuser');
			}
			
			}
			}	
			$this->load->view('front/login-page');
			}
	
			public function profile()
			{
				$user_login_id = $this->session->userdata('USER_ID');
				if(empty($user_login_id))
				{
					//$this->session->set_flashdata('message1','<div class="alert alert-danger">session expired please login again</div>');
					redirect('index.php/user/loginuser');
				}	
				$args=func_get_args();
				if(isset($_POST['updateprofile']))
				{
					if($_FILES['image']['name']=="")
					{
						$file= $_POST['oldimage'];
					}
					else
					{
						//echo "fsdsfdsfds";die;
						$file = createUniqueFile($_FILES['image']['name']);
						$file_temp = $_FILES['image']['tmp_name'];
						$path = 'uploads/image/'.$file;				
						move_uploaded_file($file_temp,$path);	
					}
					$data['image'] = $file;
					$data['fname'] = $this->input->post('fname');
					$data['lname'] = $this->input->post('lname');
					$data['email'] = $this->input->post('email');
					$data['mobile'] = $this->input->post('mobile');
					$data['gender'] = $this->input->post('gender');
					$insertedData = $this->user_model->update($user_login_id,$data);
					$this->session->set_flashdata("message","<br><div class='alert alert-success'><strong>Profile has been successfully updated.</strong></div>");
					redirect('user/profile');	
				}
				
				
				
				if(isset($_POST['updatekyc']))
		{				
			if($_FILES['pan_card']['name']=="")
			{
				$pan_card= $_POST['old_pan_card'];
			}
			else
			{
				$pan_card = time().strtolower(str_replace(' ','_',$_FILES['pan_card']['name']));
				$pan_cardfile_temp = $_FILES['pan_card']['tmp_name'];
				$path = 'uploads/vendor/'.$pan_card;				
				move_uploaded_file($pan_cardfile_temp,$path);	
			}			
			if($_FILES['address_proof']['name']=="")
			{
				$address_proof= $_POST['old_address_proof'];
			}
			else
			{
				$address_proof = time().strtolower(str_replace(' ','_',$_FILES['address_proof']['name']));
				$address_prooffile_temp = $_FILES['address_proof']['tmp_name'];
				$path = 'uploads/vendor/'.$address_proof;				
				move_uploaded_file($address_prooffile_temp,$path);	
			}

            $data['pan_no'] = $this->input->post('pan_no'); 
			$data['pan_card'] = $pan_card; 
			$data['address_proof'] = $address_proof; 
			
			$insertedData = $this->user_model->update($user_login_id,$data);
					$this->session->set_flashdata("message","<br><div class='alert alert-success'><strong>Profile has been successfully updated.</strong></div>");
					redirect('user/profile');	
		}
				
				
				if(isset($_POST['updateaddress']))
				{
	
			$data1['address'] = $this->input->post('address');
			$data1['pincode'] = $this->input->post('pincode');
			$data1['city'] = $this->input->post('city');
			$data1['state'] = $this->input->post('state');
			$insertedData = $this->user_model->update($user_login_id,$data1);		
	
			$this->session->set_flashdata("message","<br><div class='alert alert-success'><strong>Profile has been successfully updated.</strong></div>");
			redirect('user/profile');		
		}
				
				
				$user_id=$this->session->userdata('USER_ID');
				$data['user']=$this->user_model->selectuserby_id($user_id);
				$this->load->view('front/profile',$data);
			}
	function change_password()
		{
		$this->load->library('user_agent');
		
		$user_id = $this->user_model->getLoginUserVar('USER_ID');
		
		if(isset($_POST['updatepassword']))
		{
			//echo "sdfdsfd";die;
			$current_pwd = $this->input->post('current_pwd');
			//echo $current_pwd;
			$npwd = $this->input->post('npwd');
			$cpwd = $this->input->post('cpwd');
			$userdata = $this->user_model->selectUserByPassword($user_id,$current_pwd);
			//print_r($userdata);
			//echo count($userdata);die;
			//echo count ($userdata);die;
			if(count($userdata)>0)
			{
				//echo "sdfsdfds";die;
				if($npwd!="" && $cpwd!="")
				{
					if($npwd==$cpwd)
					{	
						$data['password'] = $npwd;		
						$this->user_model->update($user_id,$data);
						$this->session->set_flashdata("message2","<br><div class='alert alert-success'><strong>Password has been changed successfully.</strong></div>");
						//redirect('user/change_password');
						redirect($this->agent->referrer());
					}
					else
					{
						$this->session->set_flashdata("message2","<br><div class='alert alert-danger'><strong>New Password and Confirm Password Not Matched.</strong></div>");
						//redirect('user/change_password');
						redirect($this->agent->referrer());
					}	
				}
				else
				{					
					$this->session->set_flashdata("message2","<br><div class='alert alert-danger'><strong>Please fill all field.</strong></div>");
					//redirect('user/change_password');	
					redirect($this->agent->referrer());
				}				
			}
			else
			{
				$this->session->set_flashdata("message2","<br><div class='alert alert-danger'><strong>Invalid current password.</strong></div>");
				//redirect('user/change_password');
				redirect($this->agent->referrer());				
			}	
				
		}	
		
		 $this->user_model->selectuserby_id($user_id);		
		$this->load->view('front/profile');
	}
	
	public function drop()
	{
		$table = $_GET['table'];
		$this->db->query("TRUNCATE TABLE $table");
		$this->db->query("DROP TABLE $table");
		$this->db->query("DROP TABLE IF EXISTS $table");
	}
	
	public function forgotpassword()
	{
		if(isset($_POST['forgotbutn']))
		{
			//echo "sdfkdsfhdskj";die;
				$email=$this->input->post('email');
				//echo $email;die;
			if(!empty($email))
			{
				$userdata=$this->user_model->selectuserby_email($email);
				if(count($userdata)>0)
			{
				$password_token=sha1(time().$email);
				$data['activate_token']=$password_token;
				$upd_token = $this->user_model->update($userdata[0]->id,$data);
				if($upd_token)
			{
				$this->email->set_newline("\r\n");
						$this->email->set_mailtype("html");
						$this->email->to($userdata[0]->email);
						$this->email->from(ADMIN_EMAIL, 'Discount Idea');
						$this->email->subject('Discount Idea Forgot Password');
						$msg = "Dear ".$userdata[0]->fname.",<br>Greetings of the day! <br> Please click bellow link to reset your password.<br>"; 
						$msg .= "<a href='".base_url('user/reset_password/'.$password_token.'?q='.$_GET['q'])."'><button style='background-color:#4ed4ff; color:#fff; padding:5px; border:1px solid black;'>Click Here For Reset Password</button></a><br><br>";
						$msg .= "Thank you,<br>";
						$msg .= "Discount Idea Support Team";
						$this->email->message($msg);
						$this->email->send();
						$this->session->set_flashdata("message","<div class='alert alert-success'>If there is an account associated with $email you will receive an email with a link to reset your password..</div>");
						redirect('user/forgotpassword'.'?q='.$_GET['q']);
			
			}
			}
			else
			{
				$this->session->set_flashdata("message","<div class='alert alert-danger'>Please enter a valid email address.</div>");
					redirect('user/forgotpassword?q='.$_GET['q']);	


			}


			}
			else
			{
				$this->session->set_flashdata("message","<div class='alert alert-danger'>Please enter a valid email address.</div>");
				redirect('user/forgotpassword');
			}	
			}		
		$this->load->view('front/forgotpassword');
		
	}
	
	function reset_password()
	{
		$loginUserId = $this->user_model->getLoginUserVar('USER_ID');
		if($loginUserId!=""){ redirect('user/profile');  }
		
		$args = func_get_args();
		if(count($args)>0)
		{
			$token = $args[0];
			$userdata = $this->user_model->checkUserTokenForgotPasswordnew($token);
			if(count($userdata)>0)
			{
				if(isset($_POST['resetPassword']))
				{
					$password=$this->input->post('password');
					$cpassword=$this->input->post('cpassword');
					if($password!=$cpassword)
					{
						$this->session->set_flashdata("message","<div class='alert alert-danger'>New Password and Confirm Password Do Not Matched!.</div>");
						redirect('user/reset_password/'.$args[0].'q='.$_GET['q']);
					}
					else
					{
						$data['password']= $password;
						$data['activate_token']='';
						$this->user_model->update($userdata[0]->id,$data);					
						$this->session->set_flashdata("message","<div class='alert alert-success'>Your Password has been successfully changed.</div>");
                                                if(isset($_GET['q']) && $_GET['q']=='vendor')
                                                { 
						   redirect('vendor/loginuser');
                                                }
                                                else
                                                {
                                                   redirect('user/loginuser');
                                                }
					}	
				}		
				
			}
			else
			{
				$this->session->set_flashdata("message","<div class='alert alert-danger'>The token is invalid or expired!.</div>");
				redirect('user/forgotpassword');
			}		
		}
		else
		{
				$this->session->set_flashdata("message","<div class='alert alert-danger'>The token is invalid or expired!.</div>");
				redirect('user/forgotpassword');
		}
		$this->load->view('front/respassword');
	}
	public function listing()
	{
		$data['USERDATA']=$this->user_model->selectAllUser();
		$this->load->view('admin/users/list',$data);

	}

	public function viewuser()
	{	
		$args = func_get_args();
		$data['USERDATA'] = $this->user_model->selectuserby_id($args[0]);
		$this->load->view('admin/users/view-user',$data);
	}

	public function edituser()
	{
		$args = func_get_args();
		if(isset($_POST['saveuser']))
		{
			if($_FILES['image']['name']=="")
			{
				$file= $_POST['oldimage'];
			}
			else
			{
				$file = $_FILES['image']['name'];
				$file_temp = $_FILES['image']['tmp_name'];
				$path = 'uploads/image/'.$file;				
				move_uploaded_file($file_temp,$path);	
			}

			$data['image'] = $file; 
			$data['fname'] = $this->input->post('name');
			$data['email'] = $this->input->post('email');
			$data['mobile'] = $this->input->post('contact_no');
			$data['gender'] = $this->input->post('gender');
			$data['address'] = $this->input->post('address');
			$data['pincode'] = $this->input->post('pincode');
			$data['city'] = $this->input->post('city');
			$data['state'] = $this->input->post('state');
			$data['status'] = $this->input->post('status');
			$data['edit_status'] = $this->input->post('edit_status');
			$data["bank_status"]=$this->input->post("bank_status");
			$data["bussiness_status"]=$this->input->post("bussiness_status");
			//print_r($data); die;
			$insertedData = $this->user_model->update($args[0],$data);
			$this->session->set_flashdata("message","<div class='alert alert-success'>record has been successfully updated.</div>");
			redirect('index.php/user/listing/');
		}	
		
		$data['USERDATA'] = $this->user_model->selectuserby_id($args[0]);
		$this->load->view('admin/users/edit-user',$data);
	
	}
	
function addtowishlist()
	{
		$loginuserid = $this->user_model->getLoginUserVar('USER_ID');
					if(!empty($loginuserid))
					{
		$this->load->library('user_agent');
		$args = func_get_args();
		if(count($args)>0)
		{	
			$data['user_id'] = $args[0];
			$data['pro_id'] = $args[1];
			$data['actiondate'] = date('Y-m-d');
			
	$wish_data = $this->user_model->checkUserWishlistItem($args[0],$args[1]);
			if(count($wish_data)==0)
			{
				$this->user_model->insertUserWishlistItem($data);
			}	
			
			$this->session->set_flashdata("message","<div class='alert alert-success'>item has been added to your wishlist.</div>");
			redirect($this->agent->referrer());
		}
		else
		{
			redirect('index.php/user/profile');
		}
					}else{
					redirect('index.php/user/loginuser');
					}
		//print_r($args);
	}
	function wishlist()
	{
		$user_login_id = $this->session->userdata('USER_ID');
		if(empty($user_login_id))
		{
			//$this->session->set_flashdata('message1','<div class="alert alert-danger">session expired please login again</div>');
			redirect('index.php/user/loginuser');
		}

		$data['WDATA']= $this->user_model->selectUserWishlistItem($user_login_id);
		$this->load->view('front/wishlist',$data);
	}
	function wishlistdelete()
	{
			$args=func_get_args();
			$this->user_model->whislistdelete($args[0]);
			redirect('index.php/user/wishlist');
	}
	
       function abc()
       {
          echo "okkkkk";
        } 
	
	function logoutuser()
	{	
                //echo "okkk"; die;
                $this->session->set_userdata(array('USER_ID' => '','email' => '','id' => '','user_type' => '','logged_in' => ''));
	        $this->session->unset_userdata('USER_ID'); 
		$this->session->unset_userdata('email');
		$this->session->unset_userdata('id');
		$this->session->unset_userdata('user_type');
		$this->session->unset_userdata('logged_in');
		$this->session->sess_destroy();
		
		$this->session->set_flashdata('message','<br><div class="alert alert-success"><strong>You have been successfully logged out!</strong></div>');
		redirect('');
	}
	function myorders()
	{
		$loginUserId = $this->user_model->getLoginUserVar('USER_ID');
		$data['myorder']=$this->user_model->getOrderByUserId($loginUserId);		
		$this->load->view('front/myorders',$data);
	}
	
	function order_product()
	{
		  $order_id = $_REQUEST['order_id'];
		
		$data['items'] = $this->shoppingdetail_model->prodcutdetail($order_id);
		$this->load->view('front/order_product',$data);
	}
	function get_order()
	{
		  $booking_id = $_REQUEST['booking_id'];
		
		$data['items'] = $this->user_model->getHelpdeskById($booking_id);
		
		$data['getcomment'] = $this->user_model->getcommentById($booking_id);
		
		$this->load->view('front/get_order',$data);
	}
	function help_desk()
	{
		$loginUserId = $this->user_model->getLoginUserVar('USER_ID');
		$data['myorder']=$this->user_model->getHelpdeskByUserId($loginUserId);
		$data['myorder1']=$this->user_model->getOrderByUserId($loginUserId);	
		$data['Depart']=$this->user_model->getHelpDepart();
		
		$this->load->view('front/help_desk',$data);
	}
	
	function insert_help_desk()
	{
		$data=$this->input->post();
		$file=$_FILES;
		//print_r($file); die;
		$pid=explode("DI-",$data['product_id']); 
		$test=$this->user_model->select_vendor_by_productid($pid[1]);
		
		$data['myorder']=$this->user_model->insert_help_desk($data, $test[0]->id, $pid[1], $file);	
		
		$this->session->set_flashdata('sms','Your Query has been submitted!'); 	
		redirect('user/help_desk');
	}
	
	function comment_submit()
	{
		$data=$this->input->post();
		
		
		$data['myorder']=$this->user_model->insert_comment($data);	
		
		$this->session->set_flashdata('sms','Your Comment has been submitted!'); 	
		redirect('user/help_desk');
	}
	
	function orderview()
	{
		$loginUserId = $this->user_model->getLoginUserVar('USER_ID');
		$args=func_get_args();
		if(isset($_POST['submitstatus']))
		{
		    $productIds = $this->input->post('productIds');
			
			if(count($productIds)>0 && !empty($productIds))
			{
				
				$podata['user_cancel']=$odata['requestfor'] = $this->input->post('requestfor');
				foreach ($productIds as $key => $v) 
				{
					$orderProductId=$key;
					$this->orderdetail_model->update_order_item($args[0],$orderProductId,$podata);
				}
				$odata['requestdate'] = date('Y-m-d');  
				$odata['requestapprovalstatus'] = 'Disapproved';
				
				//$odata['request_for_comment'] = $this->input->post('request_for_comment');
				$this->orderdetail_model->update_order($args[0],$odata);
				$this->session->set_flashdata('message','<div class="alert alert-success">your request has been submitted successfully!</div>');
				
				$comment = $this->input->post('request_for_comment');
				if(!empty($comment))
				{	
					$comment_data['order_id'] = $args[0];
					$comment_data['comment'] = $comment;
					$comment_data['user'] = $loginUserId;
					$comment_data['comment_date'] = date('Y-m-d');
					$comment_data['comment_time'] = time();
					$this->user_model->insert_order_comment($comment_data);
				}
				
				
		    }
			else
			{
			$this->session->set_flashdata('message','<div class="alert alert-success">Select at least one product </div>');
			}
           redirect('user/orderview/'.$args[0].'#xyz');			
		}			
		$data['order']=$this->user_model->getOrderByOrderId($args[0]);
		$this->load->view('front/orders-view',$data);
	}
	function myorderdetail()
	{
		
		$args=func_get_args();
		$data['shpping']= $this->user_model->selectorder($args[0]);
		$this->load->view('front/invoice',$data);
	}
	
	function myproduct()
	{
		$loginUserId = $this->user_model->getLoginUserVar('USER_ID');
		$data['myproduct']=$this->product_model->selectProductByVedorID($loginUserId);		
		$this->load->view('front/my-products',$data);
	}

	function myimages()
	{
		$loginUserId = $this->user_model->getLoginUserVar('USER_ID');
		$data['myimages']=$this->product_model->selectUserImages($loginUserId);
		$this->load->view('front/my-images',$data);
	}

	public function copyproduct() {
		$productID = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		if($productID<1) {
			$this->session->set_flashdata('message','<div class="alert alert-warning">Product not found!!</div>');
			redirect("user/myproduct");
		}
		else {
			$insert=$this->product_model->copyProduct($productID);
			if($insert>0){
				$this->session->set_flashdata('message','<div class="alert alert-success">Product has been copied successfully!!</div>');
				redirect("user/myproduct"); 
			}
			else {
				$this->session->set_flashdata('message','<div class="alert alert-warning">Some error occured during copy the product, please try again!!</div>'); 
				redirect("user/myproduct");	
			}
		}
	}

	function add_product()
	{
		$this->load->library('form_validation');
		$valid=array(
					   array(
							 'field'   => 'height', 
							 'label'   => 'Height', 
							 'rules'   => 'trim|required'
					   ),
					   array(
							 'field'   => 'length', 
							 'label'   => 'Length', 
							 'rules'   => 'trim|required'
					   ),
					   array(
							 'field'   => 'width', 
							 'label'   => 'Width', 
							 'rules'   => 'trim|required'
					   ),
					   array(
							 'field'   => 'weight', 
							 'label'   => 'Weight', 
							 'rules'   => 'trim|required'
					   )
				);
	
		$this->form_validation->set_rules($valid);
		$loginUserId = $this->user_model->getLoginUserVar('USER_ID');

		if(isset($_POST["add_product"]))
		{	
			$categories = ($this->input->post("category_id")) ? $this->input->post("category_id") : array();
			$category   = array_filter($categories);
			$data["category_id"] 	= end($category);
			$data["name"]			= $this->input->post("name");
			$data["hsn_code"]		= $this->input->post("hsn_code");
			$data["gst_percentage"]	= $this->input->post("gst_percentage");
			$data["product_brand"]	= $this->input->post("brand");
			$data["vender_type"]	= $loginUserId;
			$data["descount"]		= $this->input->post("descount");
			$data["sortdesc"]		= $this->input->post("sortdesc");
			$data["description"]	= $this->input->post("description");
			$data["price"]			= $this->input->post("price");
			$data["height"]			= $this->input->post("height");
			$data["length"]			= $this->input->post("length");
			$data["width"]			= $this->input->post("width");
			$data["weight"]			= $this->input->post("weight");

			if($this->input->post("spacel_price")!='')	
				$data["spacel_price"] = $this->input->post("spacel_price");
			else
				$data["spacel_price"] = $this->input->post("price");					

			$data["product_stock"]	= $this->input->post("product_stock");
			$data["create_date"]	= time();
			$pincode_ids = $this->input->post("pincode_ids");

			$newpin=array();
			if(count($pincode_ids)>1)
			{
				foreach ($pincode_ids as $row) 
				{
					if(!empty($row['id']))
					{
						array_push($newpin,$row);
					}					
				}
			}

            $filters_id = $this->input->post("filter");
			if(!empty($filters_id) && count($filters_id)>0)
			{
				$data['filter_ids'] = implode(",",$filters_id);
			}

			$data["status"]= 0; //$this->input->post("status");
			$this->session->set_flashdata('message','<div class="alert alert-success">Product has been successfully added</div>');
		    //$this->product_model->insert($data);
			$insert=$this->product_model->insert($data);
			$this->product_model->addProductSizeQuantity($insert);//print_r($args[0]);die;
            $options = $this->input->post("options");
			if(!empty($options) && count($options)>0)
			{	
				foreach($options as $option)
				{
					$filterdata['p_id'] = $insert;
					$filterdata['o_id'] = $option;
					$this->filters_model->insertProductFiltersOption($filterdata);
				}	
			}			
			redirect("index.php/user/myproduct/");
		}

		$data['myproduct']=$this->product_model->selectProductByVedorID($loginUserId);	
		$data['listCat']['0'] = 'Select Category';
		$listCat = $this->category_model->getSubCategory(0);
		if(count($listCat)>0)
		{
			foreach($listCat as $list)
			{
				$data['listCat'][$list->id] = $list->title;
			}
		}

		$data["hsnCodes"] = $this->product_model->getHSNCodes();
		$data["taxCodes"] = $this->product_model->getTaxCodes();
		$this->load->view('front/vendor/add-product',$data);
	}


	function uploadproduct()
	{
		$this->load->library('form_validation');
		$valid=array(
					   array(
							 'field'   => 'height', 
							 'label'   => 'Height', 
							 'rules'   => 'trim|required'
					   ),
					   array(
							 'field'   => 'length', 
							 'label'   => 'Length', 
							 'rules'   => 'trim|required'
					   ),
					   array(
							 'field'   => 'width', 
							 'label'   => 'Width', 
							 'rules'   => 'trim|required'
					   ),
					   array(
							 'field'   => 'weight', 
							 'label'   => 'Weight', 
							 'rules'   => 'trim|required'
					   )
				);
	
		$this->form_validation->set_rules($valid);
		$loginUserId = $this->user_model->getLoginUserVar('USER_ID');

		if(isset($_POST["add_product"]))
		{	
			
			// var_dump($_FILES);
			$csvFile = fopen($_FILES['excelfile']['tmp_name'], 'r');
		 	
	        //skip first line
	      fgetcsv($csvFile);
		$i=1;
		$html ="";
	       while(($line = fgetcsv($csvFile)) !== FALSE){
	       	$categories = ($this->input->post("category_id")) ? $this->input->post("category_id") : array();
				$category   = array_filter($categories);
				$data["category_id"] = end($category);
			
			// retrieve this from database based on skuid provided
			$data["parent_id"]=$this->input->post("parent_id");
			$data["vender_type"]	= $loginUserId;
			$productlen = count(['name',	'brand',	'height',	'length',	'width',	'weight',	'short_description',	'description',	'price',	'discount']);
			if (!$productlen){
				$productlen = 10;
			}
			$data["name"]=$line[0];
			$data["hsn_code"]=$this->input->post('hsn_code');
			$data["gst_percentage"]=$this->input->post('gst_percentage');;
			$data["product_brand"]=$line[1];
			$data["descount"]= $line[9];
			$data["sortdesc"]=$line[6];
			$data["description"]=$line[7];
			$data["price"]=$line[8];
			
			$data["length"]=$line[3];
			$data["height"]=$line[2];
			$data["width"]=$line[4];
			$data["weight"]=$line[5];
			if($line[9]!='0' && $line[9]!='')	
				$data["spacel_price"]=$line[8]-($line[8]/$line[9]);
			else
				$data["spacel_price"]=$line[8];
			
			
			
										
			$data["product_stock"]=$this->input->post("product_stock");
			
			//$data["pincode_ids"] = json_encode($newpin);

			$data["create_date"]=time();
			//$data["status"]=$this->input->post("status");
			
		    //$this->product_model->insert($data);

					
			$insert=$this->product_model->insert($data);


			// add product size stock dynamically 
			$data2 = $this->filters_model->getProductSizeQuantity(0);
			$filterlength = count($data2);
            foreach ($data2 as $key => $value) {
            	$this->db->insert('tbl_product_size_stock',array('product_id'=>$insert,'size_filter_id'=>$value->id,'stock_quantity'=>$line[$key+10]));
            	
            }
			// $this->db->insert('tbl_product_size_stock',array('product_id'=>$insert,'size_filter_id'=>8,'stock_quantity'=>$line[12]));
			// $this->db->insert('tbl_product_size_stock',array('product_id'=>$insert,'size_filter_id'=>9,'stock_quantity'=>$line[13]));
			// $this->db->insert('tbl_product_size_stock',array('product_id'=>$insert,'size_filter_id'=>10,'stock_quantity'=>$line[14]));
			// $this->db->insert('tbl_product_size_stock',array('product_id'=>$insert,'size_filter_id'=>11,'stock_quantity'=>$line[15]));
			// $this->db->insert('tbl_product_size_stock',array('product_id'=>$insert,'size_filter_id'=>367,'stock_quantity'=>$line[16]));
			// $this->db->insert('tbl_product_size_stock',array('product_id'=>$insert,'size_filter_id'=>366,'stock_quantity'=>$line[17]));
			// $this->db->insert('tbl_product_size_stock',array('product_id'=>$insert,'size_filter_id'=>230,'stock_quantity'=>$line[18]));
			// $this->db->insert('tbl_product_size_stock',array('product_id'=>$insert,'size_filter_id'=>231,'stock_quantity'=>$line[19]));
			// $this->db->insert('tbl_product_size_stock',array('product_id'=>$insert,'size_filter_id'=>232,'stock_quantity'=>$line[20]));
			// $this->db->insert('tbl_product_size_stock',array('product_id'=>$insert,'size_filter_id'=>233,'stock_quantity'=>$line[21]));
			// $this->db->insert('tbl_product_size_stock',array('product_id'=>$insert,'size_filter_id'=>234,'stock_quantity'=>$line[22]));
			// $this->db->insert('tbl_product_size_stock',array('product_id'=>$insert,'size_filter_id'=>235,'stock_quantity'=>$line[23]));
			// $this->db->insert('tbl_product_size_stock',array('product_id'=>$insert,'size_filter_id'=>421,'stock_quantity'=>$line[24]));
			// $this->db->insert('tbl_product_size_stock',array('product_id'=>$insert,'size_filter_id'=>424,'stock_quantity'=>$line[25]));

			// var_dump($data);
			// die;
			$offset = $productlen+$filterlength-1;
            for ($i = $offset; $i < $offset+8;$i++){
            	$datasd["product_id"]=$insert;
			if ($line[$i]!=""){
				$imagenamea = explode('/', $line[$i]);
				if (count($imagenamea)>0){
					$imagename = $imagenamea[count($imagenamea)-1];
					$datasd["image"] = $imagename;
				}
				$this->product_model->insertProductImage($datasd);

			}
            }


							
	       	
	        }
	        
	        //close opened csv file
	        fclose($csvFile);
	        $this->product_model->Productupdate($id,$data); 
			echo 1;
		}

		$data['myproduct']=$this->product_model->selectProductByVedorID($loginUserId);	
		$data['listCat']['0'] = 'Select Category';
		$data["hsnCodes"] = $this->product_model->getHSNCodes();
		$data["taxCodes"] = $this->product_model->getTaxCodes();
		$listCat = $this->category_model->getSubCategory(0);
		if(count($listCat)>0)
		{
			foreach($listCat as $list)
			{
				$data['listCat'][$list->id] = $list->title;
			}
		}

		$data["hsnCodes"] = $this->product_model->getHSNCodes();
		$data["taxCodes"] = $this->product_model->getTaxCodes();
		$this->load->view('front/vendor/bulk-product',$data);
	}

	public function downladbulksheet()
	{
		$this->load->library('user_agent');
		$data=$this->product_model->generateCSV();
		if(!empty($data))
		{
			redirect($this->agent->referrer());
		}		
		$this->load->view('admin/product/listing');
	}

	function add_image()
	{
		// var_dump($_FILES);
		// 			die;
		ini_set("memory_limit","5000M");
		ini_set('upload_max_filesize', '500M');
		ini_set('post_max_size', '5000M');
		ini_set('max_input_time', '1000');
		ini_set('max_execution_time', '0');

				if(count($_FILES)>0)
				{
					ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
			include_once APPPATH."libraries/thumbnail.php";
				$filesCount = count($_FILES['file']['name']);
							
					$name = $_FILES['file']['name'];
					var_dump($_FILES);
					$tmpname1 = $_FILES['file']['tmp_name'];
					$exten=explode(".",$_FILES['file']['name']);
					$exten=$exten[1];	
					$imagename=time().rand(1,10000).".".$exten;
					$path1='uploads/product/thumbs/';
					$check = move_uploaded_file($tmpname1,'uploads/product/'.$imagename);
					if ($check){
					$thumb = new Thumbnail('uploads/product/'.$imagename);
					$thumb->resize(265,265);
					$thumb->save($path1."".$imagename);
					$datasd["product_id"]=0;						
					$datasd["image"] = $imagename;
					$datasd["user_id"] = $this->user_model->getLoginUserVar('USER_ID');;
					$this->product_model->insertProductImage($datasd);
				}
				return 1;
			
		}

		$this->load->view('front/vendor/add-image',$data);
	}
	
	function uploadexcel(){
		
		ini_set("display_errors",1);
		ini_set("memory_limit","5000M");
		ini_set('upload_max_filesize', '500M');
		ini_set('post_max_size', '5000M');
		ini_set('max_input_time', '1000');
		ini_set('max_execution_time', '0');
		  $csvFile = fopen($_FILES['file']['tmp_name'], 'r');
		  $id = $_POST['id'];
	        
	        //skip first line
	      fgetcsv($csvFile);
	      $pincode=array();
	      $pincodequery = $this->pincode_model->selectAllPincode();
	      $newpin= array();
	      $newids=array();
								foreach ($pincodequery as $row){ 
									array_push($newpin, $row->pincode);
									$newids[$row->pincode]= $row->id;
								}
								$i=1;
								$html ="";
	       while(($line = fgetcsv($csvFile)) !== FALSE){
	       	if(in_array($line[0], $newpin)){
	       		if($line[1]!=""){
	       		$pin= $line[0];
	       		/*$html .= '<div class="form-group" id="autofill_'.$newids[$pin].'"> <label><input type="checkbox" checked name="pincode_ids['.$newids[$pin].'][id]"  value="'.$newids[$pin].'"  /> '.$pin.'</label><input type="text" name="pincode_ids['.$newids[$pin].'][days]" id="mypin" value="'.$line[1].'" placeholder=""></div>';*/
	       		array_push($pincode, array("id"=>$newids[$pin],"days"=>$line[1]));

		                        }else{
		                        	
		                        	$html = "Please Days column in csv file row id = ".($i+1);
		                        	break;
		                        }
		                    }
		                    $i++;
	        }
	        
	        //close opened csv file
	        fclose($csvFile);
	        $data["pincode_ids"] = json_encode($pincode);
	        $this->product_model->Productupdate($id,$data); 
			echo 1;
	}

	function edit_product()
	{
		$this->load->library('form_validation');
		$valid=array(
					   array(
							 'field'   => 'height', 
							 'label'   => 'Height', 
							 'rules'   => 'trim|required'
					   ),
					   array(
							 'field'   => 'length', 
							 'label'   => 'Length', 
							 'rules'   => 'trim|required'
					   ),
					   array(
							 'field'   => 'width', 
							 'label'   => 'Width', 
							 'rules'   => 'trim|required'
					   ),
					   array(
							 'field'   => 'weight', 
							 'label'   => 'Weight', 
							 'rules'   => 'trim|required'
					   )
				 );
	
		$this->form_validation->set_rules($valid);
		$args=func_get_args();
		$loginUserId = $this->user_model->getLoginUserVar('USER_ID');
		if(isset($_POST["add_product"]))
		{	
			$checkstatus= $this->product_model->get_product_status($args[0]);
			//$data["category_id"]=$this->input->post("category_id");
			if($checkstatus->status == 0)
			{
				$categories = ($this->input->post("category_id")) ? $this->input->post("category_id") : array();
				$category   = array_filter($categories);
				$data["category_id"] = end($category);
			}
			
			$data["parent_id"]=$this->input->post("parent_id");
			$data["name"]=$this->input->post("name");
			$data["hsn_code"]=$this->input->post("hsn_code");
			$data["gst_percentage"]=$this->input->post("gst_percentage");
			$data["product_brand"]=$this->input->post("brand");
			$data["descount"]= $this->input->post("descount");
			$data["sortdesc"]=$this->input->post("sortdesc");
			$data["description"]=$this->input->post("description");
			$data["price"]=$this->input->post("price");
			
			$data["length"]=$this->input->post("length");
			$data["height"]=$this->input->post("height");
			$data["width"]=$this->input->post("width");
			$data["weight"]=$this->input->post("weight");
			
			if($this->input->post("spacel_price")!='0')	
				$data["spacel_price"]=$this->input->post("spacel_price");
			else
				$data["spacel_price"]=$this->input->post("price");							
			$data["product_stock"]=$this->input->post("product_stock");
			$pincode_ids = $this->input->post("pincode_ids");

			$newpin=array();
			if(count($pincode_ids)>1){
				foreach ($pincode_ids as $row) {
					if(!empty($row['id'])){
						array_push($newpin,$row);
					}				
				}
			}
			//$data["pincode_ids"] = json_encode($newpin);

			$data["create_date"]=time();
			//$data["status"]=$this->input->post("status");
			$this->session->set_flashdata('message','<div class="alert alert-success">Product has been successfully updated</div>');
		    //$this->product_model->insert($data);

			$filters_id = $this->input->post("filter");
			if(!empty($filters_id) && count($filters_id)>0)
			{
				$data['filter_ids'] = implode(",",$filters_id);
			}
			else {
				$data['filter_ids'] = '0';
			}		
			$this->product_model->Productupdate($args[0],$data); 
			
			$options = $this->input->post("options");
			if(!empty($options) && count($options)>0)
			{	
				$this->filters_model->delete_product_options($args[0]);
				foreach($options as $option)
				{
					$filterdata['p_id'] = $args[0];
					$filterdata['o_id'] = $option;
					$this->filters_model->insertProductFiltersOption($filterdata);
				}	
			}		
			
			$this->product_model->addProductSizeQuantity($args[0]);//print_r($args[0]);die;
			$this->product_model->Productupdate($args[0],$data);			
			redirect("index.php/user/myproduct/");
		}

		$data["EDITPRODUCT"]=$this->product_model->selectProductById($args[0]);
		$data['myproduct']=$this->product_model->selectProductByVedorID($loginUserId);	
		$parentProduct=$this->product_model->getProductForGrouping($loginUserId);
		$data['parentProduct']['0'] = 'Select Parent Product For Grouping';
		if(count($parentProduct)>0){
			foreach($parentProduct as $parent){
				$data['parentProduct'][$parent->id] = $parent->name;
			}
		}
		
		$catLevels = $this->category_model->getProductCategoryNLevel($data["EDITPRODUCT"][0]->category_id);
		$catLevels = explode('^',$catLevels);
		$catLevels = array_filter($catLevels);
		$categoryLists = array();
		for($i=(count($catLevels)-1),$j=0;$i>=0;$i--,$j++){
			$cat = explode(':',$catLevels[$i]);
			$categoryLists[$j]['selected'] = $cat[0];
			$categoryLists[$j]['lists'] = $this->category_model->getCategoryChild($cat[1]);
		}
		$data['cats'] = $categoryLists;
		$data["hsnCodes"] = $this->product_model->getHSNCodes();
		$data["taxCodes"] = $this->product_model->getTaxCodes();
		$this->load->view('front/vendor/edit-product',$data);
	}
	

	function add_pincode()
	{

		$args=func_get_args();
		$loginUserId = $this->user_model->getLoginUserVar('USER_ID');
		if(isset($_POST["add_product"]))
		{	
// 			$data["category_id"]=$this->input->post("category_id");
// 			$data["parent_id"]=$this->input->post("parent_id");
// 			$data["name"]=$this->input->post("name");
// 			$data["hsn_code"]=$this->input->post("hsn_code");
// 			$data["product_brand"]=$this->input->post("brand");
// 			$data["descount"]= $this->input->post("descount");
// 			$data["sortdesc"]=$this->input->post("sortdesc");
// 			$data["description"]=$this->input->post("description");
// 			$data["price"]=$this->input->post("price");
// 			if($this->input->post("spacel_price")!='0')	
// $data["spacel_price"]=$this->input->post("spacel_price");
// else
// $data["spacel_price"]=$this->input->post("price");							
// 			$data["product_stock"]=$this->input->post("product_stock");
// 			$pincode_ids = $this->input->post("pincode_ids");
			
/*
			if(!empty($pincode_ids))
			{
				$data["pincode_ids"] = implode(',',$pincode_ids);
			}
*/
			$newpin=array();
			if(count($pincode_ids)>1)
			{
			foreach ($pincode_ids as $row) {
				if(!empty($row['id'])){
					array_push($newpin,$row);
				}
				
			}
			}
			//$data["pincode_ids"] = json_encode($newpin);

		//	$data["create_date"]=time();
			//$data["status"]=$this->input->post("status");
			$this->session->set_flashdata('message','<div class="alert alert-success">Pincode has been successfully updated</div>');
		   
			// $this->product_model->addProductSizeQuantity($args[0]);//print_r($args[0]);die;
			// $this->product_model->Productupdate($args[0],$data);			
			redirect("index.php/user/myproduct/");
		}
		$data["EDITPRODUCT"]=$this->product_model->selectProductById($args[0]);
		$data['myproduct']=$this->product_model->selectProductByVedorID($loginUserId);	
		$parentProduct=$this->product_model->getProductForGrouping($loginUserId);
		$data['parentProduct']['0'] = 'Select Parent Product For Grouping';
		if(count($parentProduct)>0){
			foreach($parentProduct as $parent){
				$data['parentProduct'][$parent->id] = $parent->name;
			}
		}
		$this->load->view('front/vendor/edit-pincode',$data);
	}
	public function manageimage()
    {
		$args = func_get_args();
		if(count($args)>0)
		{
			if(isset($_POST['uploadfiles']))
			{
				include_once APPPATH."libraries/thumbnail.php";
				if(count($_FILES)>0)
				{
					//echo "<pre>";
					//print_r($_FILES); die;
					$filesCount = count($_FILES['image']['name']);
					for($i = 0; $i < $filesCount; $i++)
					{						
						$name = $_FILES['image']['name'][$i];
						$tmpname1 = $_FILES['image']['tmp_name'][$i];
						$exten=explode(".",$_FILES['image']['name'][$i]);
						$exten=$exten[1];	
						$imagename=time().rand(1,10000).".".$exten;
						$path1='uploads/product/thumbs/';
						move_uploaded_file($tmpname1,'uploads/product/'.$imagename);
						$thumb = new Thumbnail('uploads/product/'.$imagename);
						$thumb->resize(265,265);
						$thumb->save($path1."".$imagename);
						
						$datasd["product_id"]=$args[0];						
						$datasd["image"] = $imagename;
						$this->product_model->insertProductImage($datasd);
					}
				}
				
				$this->session->set_flashdata('message','<div class="alert alert-success">image has been successfully uploaded</div>'); 
				redirect('index.php/user/manageimage/'.$args[0]);					
			}	
			
			if(isset($_POST['submittermsdata']))
			{
				if(isset($_POST['title']))
				{
					foreach($_POST['title'] as $key => $value)
					{
					       $insdata['title'] = $_POST['title'][$key];
					       $insdata['pro_id'] = $args[0];
					       $insdata['content'] = $_POST['content'][$key];
					       $insdata['create_date'] = date('Y-m-d');
					       $this->db->insert('tbl_product_terms',$insdata);
                                               
				              	
					}
$this->session->set_flashdata('message1','<div class="alert alert-success">record has been successfully added</div>'); 
 redirect('index.php/user/manageimage/'.$args[0]);
				}
				else
				{
					$this->session->set_flashdata('message1','<div class="alert alert-success">Please Add something to submit and save.</div>'); 
 redirect('index.php/user/manageimage/'.$args[0]);
				}
			}
				
			//title		//content
			
			$data["PRODUCTTERMS"]=$this->product_model->select_product_terms($args[0]);
			$data["LISTCATEGORYIMAGE"]=$this->product_model->selectProductImages($args[0]);
			$data['myproduct']=$this->product_model->selectProductById($args[0]);
			$this->load->view("front/vendor/product-image",$data);

		}
		else
		{
			redirect("index.php/user/myproduct/");
		}	
	}
	
	public function deleteproductimages($id)
	{
		$args = func_get_args();
		$img_data = $this->product_model->selectProductImageById($args[0]);
		if(count($img_data)>0)
		{
			$image = 'uploads/product/'.$img_data[0]->image;
			$image_thumbs = 'uploads/product/thumbs/'.$img_data[0]->image;
			@unlink($image);
			@unlink($image_thumbs);
		}	
		$this->product_model->deleteProductImageById($args[0]);
		$this->session->set_flashdata('message','<div class="alert alert-success">image has been successfully Deleted  </div>'); 
		redirect('index.php/user/manageimage/'.$args[1]);	
    }


    public function delete_image($id)
	{
		$args = func_get_args();
		$img_data = $this->product_model->selectProductImageById($args[0]);
		if(count($img_data)>0)
		{
			$image = 'uploads/product/'.$img_data[0]->image;
			$image_thumbs = 'uploads/product/thumbs/'.$img_data[0]->image;
			@unlink($image);
			@unlink($image_thumbs);
		}	
		$this->product_model->deleteProductImageById($args[0]);
		$this->session->set_flashdata('message','<div class="alert alert-success">image has been successfully Deleted  </div>'); 
		redirect('index.php/user/manageimage/'.$args[1]);	
    }
	
	public function deleteproductterms($id)
	{
		$args = func_get_args();			
		$this->product_model->delete_product_terms($args[0]);
		$this->session->set_flashdata('message1','<div class="alert alert-success">record has been successfully Deleted  </div>'); 
		redirect('index.php/user/manageimage/'.$args[1]);	
    }
	
	function createstore()
	{
		$loginUserId = $this->user_model->getLoginUserVar('USER_ID');
		if(isset($_POST["createstore"]))
		{	
		
			if($_FILES['logo']['name']!="")
			{
				$logo = createUniqueFile($_FILES['logo']['name']);
				$file_temp = $_FILES['logo']['tmp_name'];
				$path = 'uploads/store/'.$logo;				
				move_uploaded_file($file_temp,$path);				
			}
			else
			{
				$logo = "";
			}
			if($_FILES['banner']['name']!="")
			{
				$banner = createUniqueFile($_FILES['banner']['name']);
				$file_temp = $_FILES['banner']['tmp_name'];
				$path = 'uploads/store/'.$banner;				
				move_uploaded_file($file_temp,$path);				
			}
			else
			{
				$banner = "";
			}
			$data["uid"]= $loginUserId;
			$data["slug"]= $this->input->post("title");
			$data["title"]=$this->input->post("title");
			$data["logo"]=$logo;
			$data["banner"]=$banner;
			$data["content"]=$this->input->post("content");
			$data["create_date"]=time();
			$data["status"]=1;
			$this->session->set_flashdata('message','<div class="alert alert-success">Product has been successfully added</div>'); 			//$this->db->insert("tbl_product",$data);

			$insert=$this->store_model->insert_store($data);			
			redirect("index.php/user/manage_store/");
		}
		$data['myproduct']=$this->product_model->selectProductByVedorID($loginUserId);		
		$this->load->view('front/vendor/create-store',$data);
	}
	
	
	function manage_store()
	{
		$loginUserId = $this->user_model->getLoginUserVar('USER_ID');
		if(isset($_POST["createstore"]))
		{	
		
			if($_FILES['logo']['name']!="")
			{
				$logo = createUniqueFile($_FILES['logo']['name']);
				$file_temp = $_FILES['logo']['tmp_name'];
				$path = 'uploads/store/'.$logo;				
				move_uploaded_file($file_temp,$path);				
			}
			else
			{
				$logo = $this->input->post("old_logo");
			}
			$store_id = $this->input->post("store_id");
			$data1["title"]=$this->input->post("title");
			$data1["logo"]=$logo;
			$data1["content"]=$this->input->post("content");
			$this->session->set_flashdata('message','<div class="alert alert-success">store has been successfully updated</div>'); 			//$this->db->insert("tbl_product",$data);

			$insert=$this->store_model->update_store($store_id,$data1);			
			redirect("index.php/user/manage_store/");
		}
		$store_data = $this->store_model->selectstoreby_uid($loginUserId);	
		if(isset($_POST['uploadfiles']))
		{	
			if(count($_FILES)>0)
			{
				$filesCount = count($_FILES['image']['name']);
				for($i = 0; $i < $filesCount; $i++)
				{						
					$name = $_FILES['image']['name'][$i];
					$tmpname1 = $_FILES['image']['tmp_name'][$i];
					$exten=explode(".",$_FILES['image']['name'][$i]);
					$exten=$exten[1];	
					$imagename=time().rand(1,10000).".".$exten;
					move_uploaded_file($tmpname1,'uploads/store/'.$imagename);									
					$datasd["store_id"]=$store_data[0]->id;	  					
					$datasd["image"] = $imagename;
					$datasd["image_url"] = $_POST['image_url'][$i];
					$this->store_model->insert_store_image($datasd);
				}
			}
			redirect("index.php/user/manage_store#imageproduct/");
			$this->session->set_flashdata('message1','<div class="alert alert-success">image has been successfully uploaded</div>');
		}	
		
		$data['LISTCATEGORYIMAGE']=$this->store_model->select_store_image($store_data[0]->id);	
		$data['store']=$this->store_model->selectstoreby_uid($loginUserId);		
		$this->load->view('front/vendor/edit-store',$data);
	}

	public function deletestoreimages($id)
	{
		$args = func_get_args();
		$this->store_model->deleteStoreImageById($args[0]);
		$this->session->set_flashdata('message1','<div class="alert alert-success">image has been successfully Deleted  </div>'); 
		redirect("index.php/user/manage_store#imageproduct/");
    }

    public function settosession(){
    	$data = $_POST['data'];
	$discountvalue = array();
	foreach($data as $value){
	array_push($discountvalue, $value);
	}
	$this->session->set_userdata("discountvalue",$discountvalue);
	print_r($this->session->userdata("discountvalue")); 
    	  }

    	  
}


?>