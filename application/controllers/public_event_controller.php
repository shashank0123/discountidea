<?php 

if ( !defined('BASEPATH')) exit('No direct script access allowed');
class public_event_controller extends CI_Controller
{

	public function index()
	{
		$this->load->database();

		$user_login_id = $this->session->userdata('USER_ID');
		if(!empty($user_login_id))
		{
		$this->load->model('public_event_model');
	    $data=$this->public_event_model->get_detail($user_login_id);
	    // echo "<pre>";
	    // print_r($data);die;
		$this->load->view('public-event',$data);
		}
		else
		{
		$this->load->view('public-event');
		}	
		
	}

	public function user_data_submit()
	{    $this->load->model('public_event_model');
		if($this->input->post())
		{	$id=$this->session->userdata('USER_ID');
			$store['talent']=	$this->input->post('talent');
			$store['dob']=	$this->input->post('dob');
			$result=$this->public_event_model->user_data_save($id,$store);
			if($result)
		{
			$storeu['url']=	$this->input->post('url');
			$storeu['user_id']=$this->session->userdata('USER_ID');
			if($this->public_event_model->user_video_save($storeu))
			{
				
				redirect('readers/public-event', 'refresh');
			}
		}
		}
		
}
// public function likes()
// {
//    var addlikes=documnet.getElementById('add').value;
//    addlikes++;
//    document.getElementById(add).innerHTML=addlikes;
// }
}
