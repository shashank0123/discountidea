<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categorycontroller extends CI_Controller {

	public function add()
	{
		$args=func_get_args();
        if(isset($_POST["add"]))
		{
				
			//$data["image"] = $file;
			$data['parent_id']=$this->input->post('parent_id');		
			$data['meta_title']=$this->input->post('metatile');
			$data['meta_keyword']=$this->input->post('metakeyword');
			$data['meta_description']=$this->input->post('metadescription');
			$data['display_on_home']=$this->input->post('display_on_home');
			$data['display_on_mcategory']=$this->input->post('display_on_mcategory');
			$data['title']=$this->input->post('name');
			$data["createdate"]= time();
			$data['status']=$this->input->post('status');
			$filters_id = $this->input->post('filters_id');
			if(!empty($filters_id))
			{				
				$data['filters_id'] = implode(",",$filters_id);	
			}
				
			$this->category_model->insertCategory($data);
			$this->session->set_flashdata('message','<div class="alert alert-success">Add Category successsully</div>'); 
			redirect("index.php/categorycontroller/listing/"); 
		}
		$this->load->view('admin/category/add');
	}

	public function getSubCategory($catID,$rowpos=0)
	{
		$data['rowpos'] = $rowpos;
		$lists = $this->category_model->getSubCategory($catID);
		$data['cats'] = array();
		$data['cats']['0'] = 'Select Sub Category';
		if(count($lists)>0) {
			foreach($lists as $list){
				$data['cats'][$list->id] = $list->title;
			}
		}
		$this->load->view('admin/category/cat_level',$data);
	}

	public function listing()
	{
		$data["LISTCATEGORY"]=$this->category_model->selectAllCategory();
		$this->load->view("admin/category/listing",$data);
	}
	
	public function edit()
	{
		$args=func_get_args();
		if(isset($_POST["edit"]))
		{
			/*if($_FILES['image']['name']!=""){
				$file = time().'_'.$_FILES['image']['name'];
				$file_tmp = $_FILES['image']['tmp_name'];
				$path = 'uploads/category/'.$file;
				move_uploaded_file($file_tmp,$path);
			}
			else
			{
				$file = $_POST['oldImage'];
			}
			
						
			$data["image"] = $file;*/
			$data['meta_title']=$this->input->post('metatitle');
			$data['meta_keyword']=$this->input->post('metakeyword');
			$data['meta_description']=$this->input->post('metadescription');
			$data['parent_id']=$this->input->post('parent_id');
			$data['display_on_home']=$this->input->post('display_on_home');
			$data['display_on_mcategory']=$this->input->post('display_on_mcategory');
			$data['title']=$this->input->post('name');
			$data["createdate"]= time();
			$data['status']=$this->input->post('status');
			
			$filters_id = $this->input->post('filters_id');
			
			//print_r($filters_id); die();
			if(!empty($filters_id) && count($filters_id)>0)
			{				
				$data['filters_id'] = implode(",",$filters_id);	
			}
			
			$this->session->set_flashdata('message','<div class="alert alert-success">You have been successsully update planes</div>'); 	
			$this->category_model->updateBycategory($args[0],$data);
			
			redirect('index.php/categorycontroller/listing/');  
		}
		$data["EDITCATEGORY"]=$this->category_model->selectCategoryByID($args[0]);
		$this->load->view("admin/category/edit",$data);
	}
	
	public function deletecategory($id)
	{
		$args = func_get_args();
		$this->category_model->deletecategory($args[0]);
		$this->session->set_flashdata('message','<div class="alert alert-success">You have been successsully Delete Category</div>'); 
		redirect('index.php/categorycontroller/listing/'.$id);  
    }
	
	public function manageimage()
    {
		$args = func_get_args();
		if(count($args)>0)
		{
			if(isset($_POST['uploadfiles']))
			{
				if(count($_FILES)>0)
				{
					$filesCount = count($_FILES['image']['name']);
					for($i = 0; $i < $filesCount; $i++)
					{
						$file = time().'_'.$_FILES['image']['name'][$i];
						$file_tmp = $_FILES['image']['tmp_name'][$i];
						$image_title=@$_POST["image_title"][$i];
						$path = 'uploads/category/'.$file;
						move_uploaded_file($file_tmp,$path);
						$datasd["category_id"]=$args[0];
						
						$datasd["image"] = $file;
						$datasd["url"] = $_POST['url'][$i];
						$this->category_model->insertCategoryImages($datasd);
					}
				}
				
				$this->session->set_flashdata('message','<div class="alert alert-success">file has been successfully uploaded</div>'); 
				redirect('index.php/categorycontroller/manageimage/'.$args[0]);					
			}	
			
			$data["LISTCATEGORYIMAGE"]=$this->category_model->selectAllCategoryImages($args[0]);
			$this->load->view("admin/category/manage-images",$data);			
		}
		else
		{
			redirect('index.php/categorycontroller/listing');
		}	
	}
		
	public function deletecategoryimages($id)
	{
		$args = func_get_args();
		$img_data = $this->category_model->selectAllCategoryImagesByid($args[0]);
		if(count($img_data)>0)
		{
			$image = 'uploads/category/'.$img_data[0]->image;
			unlink($image);
		}	
		$this->category_model->deletecategoryimagesbyid($args[0]);
		$this->session->set_flashdata('message','<div class="alert alert-success">file successsully Deleted  </div>'); 
		redirect('index.php/categorycontroller/manageimage/'.$args[1]);	
    }	
	
	public function home_category()
	{
		$data["LISTCATEGORY"]=$this->category_model->select_all_home_category();
		$this->load->view("admin/category/home-category-listing",$data);
	}
	
	public function home_category_add()
	{
		$args=func_get_args();
        if(isset($_POST["add"]))
		{
			$data1['title']=$this->input->post('title');
			$data1['status']=$this->input->post('status');
			$product_ids = $this->input->post('product_ids');
			if(!empty($product_ids))
			{				
				$data1['product_ids'] = implode(",",$product_ids);	
			}
				
			$this->category_model->insert_home_category_images($data1);
			$this->session->set_flashdata('message','<div class="alert alert-success">record has been successfully added</div>'); 
			redirect("index.php/categorycontroller/home_category/"); 
		}
		$data["LISTCATEGORY"]=$this->category_model->select_all_home_category();
		$this->load->view("admin/category/home-category-add",$data);
	}
	
	public function home_category_edit()
	{
		$args=func_get_args();
        if(isset($_POST["edit"]))
		{
			$data1['title']=$this->input->post('title');
			$data1['status']=$this->input->post('status');
			$product_ids = $this->input->post('product_ids');
			if(!empty($product_ids))
			{				
				$data1['product_ids'] = implode(",",$product_ids);	
			}
				
			$this->category_model->update_home_category_byid($args[0],$data1);
			$this->session->set_flashdata('message','<div class="alert alert-success">record has been successfully added</div>'); 
			redirect("index.php/categorycontroller/home_category/"); 
		}
		$data["EDITCATEGORY"]=$this->category_model->select_home_category_by_id($args[0]);
		$this->load->view("admin/category/home-category-edit",$data);
	}
	
	public function delete_home_category($id)
	{
		$args = func_get_args();
		$this->category_model->delete_home_category_byid($args[0]);
		$this->session->set_flashdata('message','<div class="alert alert-success">record has been successsully Deleted  </div>'); 
		redirect('index.php/categorycontroller/home_category/');	
    }

    public function get_sub_category($cat_id)
	{
		//$cat_id=$this->input->post('cat_id');
		$data = $this->category_model->getsubRoottOption($cat_id);
		echo   $data; exit;	
    }	
}