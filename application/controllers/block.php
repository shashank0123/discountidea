<?php
class Block extends CI_controller
{	
	function listing()
	{			
		$data['BLOCKDATA']=$this->block_model->selectallstaticblock();
		$this->load->view('admin/block/list',$data);
	}
	
	function add()
	{
		if(isset($_POST['savedata']))
		{
			$data['title']=$this->input->post('title');
			$data['description']=$this->input->post('description');
			$this->block_model->insertdata($data);
			$this->session->set_flashdata('message','<div class="alert alert-success">data insert successsully</div>');
			redirect('index.php/block/listing');			
		}		
		$this->load->view('admin/block/add');		
	}
	function edit()
	{
		$args=func_get_args();		
		if(isset($_POST['updatedata']))
		{
			$data['title']=$this->input->post('title');
			$data['description']=$this->input->post('description');
			$this->block_model->updatedata($args[0],$data);
			$this->session->set_flashdata('message','<div class="alert alert-success">data Update successsully</div>');
			redirect('index.php/block/listing');			
		}			
		$data['EDITBLOCK']=$this->block_model->selectblockbyid($args[0]);
		$this->load->view('admin/block/edit',$data);		
	}
	function deletemeta($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('tbl_metatag');
		
		$this->session->set_flashdata('message','<div class="alert alert-success">Meta Tag Delete successsully</div>');
			redirect('index.php/admin_meta_tag');
		
		
	}
	
}

?>