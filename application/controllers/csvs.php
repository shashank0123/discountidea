<?php
 defined('BASEPATH') OR exit('No direct script access allowed');
    class csv extends CI_Controller {
     
        function __construct() {
            parent::__construct();
            $this->load->model('csv_model_pin');
            $this->load->library('csvimport');
        }
     
        function index() {
            $data['addressbook'] = $this->csv_model_pin->get_addressbook();
            $this->load->view('admin/csvindex', $data);
        }
     
        function importcsv() {
            $data['addressbook'] = $this->csv_model_pin->get_addressbook();
            $data['error'] = '';    //initialize image upload error array to empty
     
            $config['upload_path'] = 'uploads/Csv_folder/';
            $config['allowed_types'] = 'csv';
            $config['max_size'] = '1000';
     
            $this->load->library('upload', $config);
     
     
            // If upload failed, display error
            if (!$this->upload->do_upload()) {
                $data['error'] = $this->upload->display_errors();
               $this->session->set_flashdata('success', 'Csv Data Not uploaded');
                 
                $this->load->view('admin/csvindex', $data);
            } else {
                $file_data = $this->upload->data();
                $file_path =  'uploads/Csv_folder/'.$file_data['file_name'];
     
                if ($this->csvimport->get_array($file_path)) {
                    $csv_array = $this->csvimport->get_array($file_path);
                   //if ($csv_array->num_rows()) {
                    foreach ($csv_array as $row) {
                        $insert_data = array(
                                   'city_id'=>$row['city_id'],
                                    'pincode'=>$row['pincode'],
                                    'day'=>$row['day'],
                                    
                        );
                        $this->csv_model_pin->insert_csv($insert_data);
                    }
               // }
                    $this->session->set_flashdata('success', 'Csv Data Imported successfully');
                    //redirect(base_url().'csv');
                    redirect("index.php/adminproducts/listing/");
                   // redirect("Csv");
                    //echo "<pre>"; print_r($insert_data);
                } else 
                    $data['error'] = "Error occured";
                    $this->load->view('admin/csvindex', $data);
                }
     
            } 
     
    }
    /*END OF FILE*/