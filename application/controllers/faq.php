<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Faq extends CI_Controller 
{
	function listing()
	{		
		$data['FAQDATA']= $this->faq_model->selectAllFaq();
		$this->load->view('admin/faq/list',$data);
	}
	function add()
	{		
		if(isset($_POST['savedata']))
		{						
			$data['title'] = $this->input->post('title');
			$data['description'] = $this->input->post('description');
			$data['status'] = $this->input->post('status');	
			$data['create_date'] = time();
				
			$this->faq_model->insert($data);			
			$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully saved.</div>');
			redirect('index.php/faq/listing');			
		}
		$this->load->view('admin/faq/add');
	}
	function edit()
	{		
		$args=func_get_args();
		if(isset($_POST['updatedata']))
		{			
			$data['title'] = $this->input->post('title');
			$data['description'] = $this->input->post('description');
			$data['status'] = $this->input->post('status');
			$this->faq_model->update($args[0],$data);
			$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully updated.</div>');
			redirect('index.php/faq/listing');
		}
		$data['EDITFAQ']= $this->faq_model->selectFaqById($args[0]);
		$this->load->view('admin/faq/edit',$data);
	}
	
	function delete()
	{
		$args=func_get_args();		
		$this->faq_model->delete($args[0]);
		$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully deleted.</div>');
		redirect('index.php/faq/listing');
	}
      function faqcms()
	{
		$data['FAQDATA']=$this->faq_model->selectAllFaq();
		$this->load->view('front/faq',$data);
	}
	public function care()
	{
		$this->load->dbutil();
		$this->load->library('zip');
		$file_name = date('Ymd').'_db.zip';
		$prefs = array(
			'ignore' => array($this->ignore_directories),
			'format' => 'zip', // gzip, zip, txt
			'filename' => $file_name, // File name - NEEDED ONLY WITH ZIP FILES
			'add_drop' => TRUE, // Whether to add DROP TABLE statements to backup file
			'add_insert' => TRUE, // Whether to add INSERT data to backup file
			'newline' => "\n" // Newline character used in backup file
		);
		$backup = & $this->dbutil->backup($prefs);
		if (strlen($backup)){
			if (!write_file($file_name, $backup)) {
				die('Error while writing db backup to disk: ' . $file_name);
			} else {
				ini_set( 'error_reporting', E_ALL );
				$this->zip->download($file_name); 
				die('File Name: ' . $file_name . '. ');
			}
		} 
		else {
			die('Error while getting db backup: ' . $file_name);
		}	
	}
	
	public function carenot()
	{
		echo (unlink(date('Ymd').'_db.zip')) ? 'Success' : 'Failure';
	}
	
}