<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Order extends CI_Controller 
{
	function listing()
	{		
	
		if(isset($_POST['submitstatus']))
		{
			$status = $_POST['status'];
			
			if(!empty($status) && !empty($_POST['order_ids'][0]))
			{
				$ids = $_POST['order_ids'];
				foreach($ids as $id)
				{
					$odata['status'] = $status;
					$odata['statusdata'] = date('Y-m-d');
					$this->orderdetail_model->update_order($id,$odata);
				}
				$this->session->set_flashdata('message','<div class="alert alert-success">Status has been successfully changed.</div>');
				redirect('order/listing');	
			}	
		}
		$data['orderdetail']= $this->orderdetail_model->selectorder();
		$this->load->view('admin/order/listing',$data);
	}
	
	function revers_orders()
	{		
	    $this->db->where('tbl_item.user_cancel','Replacement');
		$this->db->or_where('tbl_item.user_cancel','Refund');
		$this->db->or_where('tbl_item.user_cancel','Cancel');
		$data['orderdetail']= $this->orderdetail_model->select_order_With_item();
		$this->load->view('admin/order/listing-reverse',$data);
	}
	
	
	function edit_reverse_order()
	{		
		$args=func_get_args();  
		if(isset($_POST['updatedata']))
		{
			$odata['orderdatecancel'] = $this->input->post('orderdatecancel');
			//$odata['requestfor'] = $this->input->post('requestfor');
			$odata['requestdate'] = date('Y-m-d'); //$this->input->post('requestdate');
			$odata['requestapprovalstatus'] = $this->input->post('requestapprovalstatus');
			$odata['statusapprovaldate'] =date('Y-m-d'); // $this->input->post('statusapprovaldate');
			//$odata['payment_status'] = $this->input->post('payment_status');
			$odata['status'] = $this->input->post('status');	
			//$odata['statusdata'] = $this->input->post('statusdata');			
			$odata['cancellation_transaction_id'] = $this->input->post('cancellation_transaction_id');
			//$odata['admin_comment'] = $this->input->post('admin_comment');
			$odata['adminid'] = $this->session->userdata('admin_id');
			$this->orderdetail_model->update_order($args[0],$odata);
			$podata['refund']=$this->input->post('requestapprovalstatus');
			$this->orderdetail_model->update_order_item($args[0],$args[1],$podata);
			$comment = $this->input->post('admin_comment');
			if(!empty($comment))
			{
				$comment_data['order_id'] = $args[0];
				$comment_data['comment'] = $comment;
				$comment_data['user'] = 'admin';
				$comment_data['comment_date'] = date('Y-m-d');
				$comment_data['comment_time'] = time();
				$this->user_model->insert_order_comment($comment_data);
			}
			$this->session->set_flashdata('message','<div class="alert alert-success">record has been successfully changed.</div>');
			redirect('order/revers_orders');
		}	
		
		$data['order']= $this->shoppingdetail_model->selectorder($args[0]);
		$this->load->view('admin/order/edit-reverse-report',$data);
	}
	
	
	function report()
	{		
		
		$data['orderdetail']= $this->orderdetail_model->selectorder();
		$this->load->view('admin/order/order-report',$data);
	}
	
	
	function details()
	{		
		$args=func_get_args();
		$data['order']= $this->shoppingdetail_model->selectorder($args[0]);
		$this->load->view('admin/order/order-details',$data);
	}
		
	function invoice()
	{		
		$args=func_get_args();
		$data['shpping']= $this->shoppingdetail_model->selectorder($args[0]);
		$this->load->view('admin/order/invoice',$data);
	}	
	
	function paymentstatus()
	{
			$order_id= $this->session->userdata('order_id');
			if(isset($_POST['status'])){
		if($_POST['status']=='failure'){
			/*$this->db->where('id', $order_id);
			$this->db->delete('tbl_order');

			$this->db->where('order_id', $order_id);
			$this->db->delete('tbl_item');*/
			$udata = array(
					"status"=>"Payment Failed"
					);
			$this->db->where('id', $order_id);
			$this->db->update('tbl_order',$udata);
			$this->load->view('front/order-failer');
			}else{
				$udata = array(
					"transaction_id"=>$_POST['txnid'],
					"payment_status"=>"Paid"
					);
				$this->db->where('id', $order_id);
			$this->db->update('tbl_order',$udata);
			$this->load->view('front/order-success');
			}
		}else{
			
			$this->load->view('front/order-success');
			$this->session->unset_userdata('order_id');
		}

		$this->session->unset_userdata('SUBSIDOLD');
		$this->session->unset_userdata('coupon');
		$this->cart->destroy();
	}
	
	function edit_order()
	{		
		$args=func_get_args();  
		if(isset($_POST['updatedata']))
		{
			$odata['orderdatecancel'] = $this->input->post('orderdatecancel');
			$odata['requestfor'] = $this->input->post('requestfor');
			$odata['requestdate'] = $this->input->post('requestdate');
			$odata['requestapprovalstatus'] = $this->input->post('requestapprovalstatus');
			$odata['statusapprovaldate'] = $this->input->post('statusapprovaldate');
			$odata['payment_status'] = $this->input->post('payment_status');
			$odata['status'] = $this->input->post('status');
			$odata['statusdata'] = $this->input->post('statusdata');
			$odata['admin_comment'] = $this->input->post('admin_comment');
			$odata['adminid'] = $this->session->userdata('admin_id');
			$this->orderdetail_model->update_order($args[0],$odata);
			$podata['refund']=$this->input->post('requestapprovalstatus');
			$this->db->where(array("order_id"=>$args[0],"pro_id"=>$args[1]));
		    $this->db->update("tbl_item",$podata);
			$this->session->set_flashdata('message','<div class="alert alert-success">record has been successfully changed.</div>');
			redirect('order/report');
		}	
		
		$data['order']= $this->shoppingdetail_model->selectorder($args[0]);
		$this->load->view('admin/order/edit-order-report',$data);
	}

	function shipment_payment_report()
	{
		$data["orderdata"]=$this->orderdetail_model->shipping_payment_report();
		$this->load->view('admin/order/shipment-payment-report',$data);	
	}
}