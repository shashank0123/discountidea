<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Admin extends CI_Controller
{
	
	public function index()
	{
		$this->load->model('admin_model');
		
		if(isset($_POST['loginbutton']))
		{
			$username = $this->input->post('username');
			$password = $this->input->post('password');			
			
			if(!empty($username) && !empty($password))
			{
				$admin_data = $this->admin_model->adminLogin($username,$password);
				
				if(count($admin_data)>0)
				{
					$login_data = array(
						'admin_id'=>$admin_data[0]->id,
						'admin_username'=>$admin_data[0]->username,
						'admin_type' =>'0',
						'logged_in'=>true);						
					$this->session->set_userdata($login_data);
					redirect('index.php/admin/dashboard');
				}
				else
				{
					$this->session->set_flashdata('message','<div class="isa_error">Invalid username or password !</div>');
					redirect('index.php/admin');
				}	
			}
			else
			{
				$this->session->set_flashdata('message','<div class="isa_error">Invalid username or password !</div>');
				redirect('index.php/admin');
			}					
		}	
		$this->load->view('admin/login');
	}
	
	public function settings()
	{
		$this->load->model('admin_model');
		$admin_id = $this->session->userdata('admin_id'); 
		if(isset($_POST['updatedata']))
		{
			
            $password = $this->input->post('pwd');
			$cpassword = $this->input->post('cpwd');
			if(!empty($password) && !empty($cpassword))
			{
				if($password==$cpassword)
				{
				  $data['password'] = $password;
				}	
				$this->admin_model->update_AdminById($data,$admin_id);
				$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully updated.</div>');
			}
			else
			{
				$this->session->set_flashdata('message','<div class="alert alert-danger">please fill password.</div>');
			}	
			
			redirect('index.php/admin/settings');
		}
		
		$data['admindata']= $this->admin_model->checkAdminByID($admin_id);
		$this->load->view('admin/settings',$data);
	}
	
	public function forgot_password()
	{
		$this->load->model('admin_model');
		if(isset($_POST['submitbutton']))
		{
			$email = $this->input->post('email');
			if(!empty($email))
			{
				$admin_data = $this->admin_model->checkAdminByEmail($email);
				if(count($admin_data)>0)
				{
					$token = md5($admin_data[0]->email.time().$admin_data[0]->id);
					$data['forgot_password'] = $token;
					$this->admin_model->updateAdminById($admin_data[0]->id,$data);
					
					$this->email->set_newline("\r\n");
					$this->email->set_mailtype("html");
					$this->email->to($email);
					$this->email->from(ADMIN_EMAIL, 'Samridh Bharat Forgot Password');
					$this->email->subject('Samridh Bharat Forgot Password');
					
					$reset_password_link = base_url('index.php/admin/resetpassword/'.$token);
					
					$button_reset = "<div style='margin:0 auto;margin-top: 20px;  margin-bottom: 20px;width:180px;line-height:35px;text-align:center;background:#ff5c01;text-transform:uppercase;border-radius:5px'>
    <a href='".$reset_password_link."' style='width:100%;text-decoration:none;color:white;'> reset password</a>
</div>";
					// message start
					$message = "<div style=' max-width: 70%; padding:10px; border: 2px solid #948c8c; margin: 0 auto; text-align: center; margin-top:12%'>";	
					$message .= "<h2>Dear Admin</h2>,<br>";
					$message .= "Want to change your password? Please click on the link given below to reset the password of your Samridh Bharat.<br>";
					$message .= $button_reset;
					$message .= "If you are not able to click on the above link, please copy and paste the entire URL into your browser's address bar and press Enter.";
					$message .= '<br>'.$reset_password_link;					
					$message .= "<br><br>";
					$message .= "Thank you <br> Samridh Bharat support team</div>";
					// message end
					
					//echo $message; die();
					
					$this->email->message($message);
					$this->email->send();
					
					$this->session->set_flashdata('message','<div class="isa_success">password change instructions send on your email address</div>');
					redirect('index.php/admin/forgot_password');
					
				}
				else
				{
					$this->session->set_flashdata('message','<div class="isa_error">Invalid email address!</div>');
					redirect('index.php/admin/forgot_password');
				}	
			}
			else
			{
				$this->session->set_flashdata('message','<div class="isa_error">Please enter email address!</div>');
				redirect('index.php/admin/forgot_password');
			}	
		}	
		
		$this->load->view('admin/forgot-password');
	}
	
	public function resetpassword()
	{
		$this->load->model('admin_model');
		$args = func_get_args();
		if(count($args)>0)
		{
			$admin_data = $this->admin_model->checkAdminByToken($args[0]);
			if(count($admin_data)>0)
			{
				
			}
			else
			{
				$this->session->set_flashdata('message','<div class="isa_error">Invalid token!</div>');
				redirect('index.php/admin/index');
			}		
		}
		else
		{
			$this->session->set_flashdata('message','<div class="isa_error">Invalid token!</div>');
			redirect('index.php/admin/index');
		}	
		$this->load->view('admin/reset-password');
	}
	
	public function dashboard() 
	{
		
		$this->load->view('admin/dashboard');
	}
	
	public function adduser()
	{
		if(isset($_POST['saveuser']))
		{
			
			$email= $this->input->post('email');
			$user_data=$this->user_model->selectuserby_email($email);
			$checkfile= count($user_data);
			if($checkfile >0)
			{
				//echo "sdfdsfds";die;
				
				$this->session->set_flashdata("message","<br><div class='alert alert-danger'><strong>".$email."</strong> Email address already exists choose a different one.</div>");
				redirect('user/create_account');
			}
			$token=md5(time().$email);
			$data['activate_token']=$token;
			$data['fname'] = $this->input->post('username');
			if($_FILES['image']['name']!="")
			{
				$file = $_FILES['image']['name'];
				$file_temp = $_FILES['image']['tmp_name'];
				$path = 'uploads/banner/'.$file;				
				move_uploaded_file($file_temp,$path);				
			}
			else
			{
				$file = "";
			}			
					
			$data['image'] = $file; 
			$data['email'] = $email;
			$data['gender'] = $this->input->post('gender');
		    $data['mobile'] = $this->input->post('contact_no');
			$data['address'] = $this->input->post('address');
			$data['city'] = $this->input->post('city');
			$data['password']=$this->input->post('password');
			$data['state'] = $this->input->post('state');
			$data['vender_type']=$this->input->post('usertype');
			$data['status'] = $this->input->post('status');
	
			$insertedData = $this->admin_model->insertData($data);
			redirect('index.php/admin/userList');
			
		}	
		$this->load->view('admin/user/add-user');
	}	
	
	
	public function edituser()
	{
		$args = func_get_args();
		if(isset($_POST['saveuser']))
		{
			$email = $this->input->post('email');
			$data['fname'] = $this->input->post('username');
			if($_FILES['image']['name']=="")
			{
				$image= $_POST['oldimage'];
			}
			else
			{
				$file_allow=array('jpg','png','gif','jpeg');
				$path = 'uploads/banner/';
				$image= $this->upload('image',$path,$file_allow);
			}
			$data['image'] = $image;
			$data['email'] = $this->input->post('email');
			$data['password']=$this->input->post('password');
			$data['gender'] = $this->input->post('gender');
		    $data['mobile'] = $this->input->post('contact_no');
			$data['address'] = $this->input->post('address');
			$data['city'] = $this->input->post('city');
			$data['vender_type']=$this->input->post('usertype');
			$data['state'] = $this->input->post('state');
			$data['status'] = $this->input->post('status');
			$data['edit_status'] = $this->input->post('edit_status');
			$data['bank_status'] = $this->input->post('bank_status');
			$data['bussiness_status'] = $this->input->post('bussiness_status');
			
			$insertedData = $this->admin_model->updateAdminById($data,$args[0]);
			redirect('index.php/admin/userlist');
				
		}	
		
		$data['USERDATA'] = $this->admin_model->selectUserByID($args[0]);
		$this->load->view('admin/user/edit-user',$data);
	}
	
	
	function userList()
	{
		
		$data['USERLIST'] = $this->admin_model->getAllUsersvender();
		$this->load->view('admin/user/list',$data);
	}
	
	
	public function viewuser()
	{	
		$args = func_get_args();
		$data['USERDATA'] = $this->admin_model->selectUserByID($args[0]);
		$this->load->view('admin/user/view-user',$data);
	}
	
	function logout()
	{
		$this->session->sess_destroy();
		$this->session->set_userdata(array('admin_id' => '', 'admin_username' => '','admin_type'=>'', 'logged_in' => ''));
		$this->session->set_flashdata('message','<div class="alert alert-success">You have been successsully lougout.</div>');
		redirect('index.php/admin');	
	}
	
	function user_ligin()
	{
		$uid= func_get_args();		
		if(count($uid)>0)
		{
			$userdata = $this->user_model->selectuserby_id($uid[0]);
			if(count($userdata)>0)
			{
				$user_id = $userdata[0]->id;
				$login_data=array('id'=>$userdata[0]->id,'USER_ID'=>$userdata[0]->id,'email'=>$userdata[0]->email,'user_type'=>$userdata[0]->vender_type,'logged_in'=>true);
					$this->session->set_userdata($login_data);			
				redirect('index.php/user/profile');
			}
			else
			{
				redirect('index.php/admin/list_user');
			}	
		}
		else
		{
			redirect('index.php/admin/list_user');
		}
	}
	
	public function edit_blogbanner()
	{
		if(isset($_POST['updatedata']))
		{			
			if($_FILES['blogimage']['name']=="")
			{
				$file = $_POST['oldimage'];
			}
			else
			{
				$file = createUniqueFile($_FILES['blogimage']['name']);
				$file_temp = $_FILES['blogimage']['tmp_name'];
				$path = 'uploads/blog/'.$file;				
				move_uploaded_file($file_temp,$path);	
			}
			$data['blogimage'] = $file;
			$data['blogimageurl'] = $this->input->post('blogimageurl');			
			$this->admin_model->updateAdminBy_Id($data,1);			
			$this->session->set_flashdata('message','<div class="alert alert-success">blog has been successfully updated.</div>');
			redirect('index.php/admin/edit_blogbanner');			
		}
		$catdata['EDITBLOG']= $this->admin_model->checkAdminByID(1);
		$this->load->view('admin/blog/edit-blog-banner',$catdata);
	}
	
	function vandor_income()
	{
		if(isset($_POST['vendor_id']) && !empty($_POST['vendor_id']))
		{
			$this->db->where('vendor_id',$_POST['vendor_id']);
		}
		if(isset($_POST['product_id']) && !empty($_POST['product_id']))
		{
			$this->db->where('product_id',$_POST['product_id']);
		}
		if(isset($_POST['order_id']) && !empty($_POST['order_id']))
		{
			$this->db->where('order_id',$_POST['order_id']);
		}
		if(isset($_POST['sdate']) && isset($_POST['edate']))
		{
			$this->db->where('inv_date >=', $_POST['sdate']);
			$this->db->where('inv_date <=', $_POST['edate']);
		}
		
		$data['income'] = $this->admin_model->select_vendor_income();
		$this->load->view('admin/vedor-invoice/vendor-income',$data);
	}
	
	function edit_vendor_income()
	{
		$args = func_get_args();
		if(isset($_POST['updatedata']))
		{
		
			$status = $this->input->post('status');
			if($status=='Approved')
			{
				$ready_to_pay ='Yes';
			}
			else
			{
				if($status=='Paid')
				{
					$ready_to_pay ='Paid';
				}	
				else
				{
					$ready_to_pay ='No';
				}
				
			}	
			
			$update_data['transaction_id'] = $this->input->post('transaction_id');
			$update_data['status'] = $this->input->post('status');
			$update_data['ready_to_pay'] = $ready_to_pay;			
			$this->admin_model->update_vandor_income_by_id($update_data,$args[0]);
			$this->session->set_flashdata('message','<div class="alert alert-success">record has been successsully updated.</div>');
			redirect('index.php/admin/vandor_income');
		}	
		
		$data['income'] = $this->admin_model->select_vendor_income_by_id($args[0]);
		$this->load->view('admin/vedor-invoice/edit-invoice',$data);
	}
	
	public function distribute_vendor_money()
	{
			 $last_command_number= $this->admin_model->get_last_command_no();
			//print_r($last_command_number);
			if(count($last_command_number)>0){
			 $next_command_number=$last_command_number[0]['command_number']+1;
			}else{
				$next_command_number=1;
			}
			 $all = $this->db->query('select * from sellting')->result();
		$commission = count($all)>0?$all[0]->commission:'0';
		$admin_charge = count($all)>0?$all[0]->admin_charge:'3';
		$promotion_charge = count($all)>0?$all[0]->promotion_charge:'0';
		$total_charge = $commission+$admin_charge+$promotion_charge;
		$servicetax_gst = count($all)>0?$all[0]->servicetax_gst:'18';	
		$tcs = count($all)>0?$all[0]->tcs:'1';
		
		$total_amount = 0;
		$refund_amount = 0;
			
		$chk123456 = 0;
		
		$order_list = $this->orderdetail_model->select_order_status_success();
		//$total_seller_amount = 0;
		foreach($order_list as $order)
		{
			
			$items = $this->shoppingdetail_model->prodcutdetail($order->id); 			
			foreach($items as $item)
			{    $refund_amount=0;
			     //$incomfor=0; 
				 
 $get_income = $this->admin_model->check_vandor_income($item->vendor,$order->id,$item->pro_id);
				if(count($get_income)==0)
				{
					
					
					if($item->vendor!='0')
					{
						
						$chk123456=1;
						$sell_amount = $item->total_amount;						
						//$total_seller_amount += $item->total_amount;						
						$insdata['sell_amount'] = $sell_amount;
						$insdata['commission'] = $this->apply_charge($sell_amount,$commission);
						$insdata['admin_charge'] = $this->apply_charge($sell_amount,$admin_charge);
						$insdata['promotion_charge'] = $this->apply_charge($sell_amount,$promotion_charge);
						$insdata['taxable_amount'] = $this->apply_charge($sell_amount,$total_charge);
						$insdata['servicetax_gst'] = $this->apply_charge($this->apply_charge($sell_amount,$total_charge),$servicetax_gst);
						$insdata['final_deduction'] = $this->apply_charge($sell_amount,$total_charge)+$this->apply_charge($this->apply_charge($sell_amount,$total_charge),$servicetax_gst);
						$insdata['remittance'] = $sell_amount - $insdata['final_deduction'];
						$insdata['tcs_deducted'] =	$this->apply_charge($sell_amount,$tcs);
						$insdata['final_remittance'] =	$insdata['remittance']-$insdata['tcs_deducted'];
						$insdata['product_id'] = $item->pro_id;
						$insdata['order_id'] =	$item->order_id;
						$insdata['vendor_id'] =	$item->vendor;
						$insdata['inv_date'] =	date('Y-m-d');
						$insdata['inv_time'] = time(); 
						$insdata['status'] = 'unpaid';
						$insdata['command_number'] = $next_command_number;
						$insdata['ready_to_pay'] =	'no';					
						$insdata['command_date'] =	date("Y-m-d");					
						$this->admin_model->insert_vendor_income($insdata);		
						//$incomfor = $insdata['final_remittance'];				
						//$vendor_id = $item->vendor;
						
					}
				}
                $get_income = $this->admin_model->check_cancel_report($item->vendor,$order->id,$item->pro_id);
				if(count($get_income)=='0')
				{
					if($item->vendor!='0' && $item->refund=='Refund Approved')
					{
						$sell_amount = $item->total_amount;
						$insdata['sell_amount'] = $sell_amount;
						$insdata['commission'] = $this->apply_charge($sell_amount,$commission);
						$insdata['admin_charge'] = $this->apply_charge($sell_amount,$admin_charge);
						$insdata['promotion_charge'] = $this->apply_charge($sell_amount,$promotion_charge);
						$insdata['taxable_amount'] = $this->apply_charge($sell_amount,$total_charge);
						$insdata['servicetax_gst'] = $this->apply_charge($this->apply_charge($sell_amount,$total_charge),$servicetax_gst);
						$insdata['final_deduction'] = $this->apply_charge($sell_amount,$total_charge)+$this->apply_charge($this->apply_charge($sell_amount,$total_charge),$servicetax_gst);
						$insdata['remittance'] = $sell_amount - $insdata['final_deduction'];
						$insdata['tcs_deducted'] =	$this->apply_charge($sell_amount,$tcs);
						$insdata['final_remittance'] =	$insdata['remittance']-$insdata['tcs_deducted'];
						$insdata['product_id'] = $item->pro_id;
						$insdata['order_id'] =	$item->order_id;
						$insdata['vendor_id'] =	$item->vendor;
						$insdata['inv_date'] =	date('Y-m-d');
						$insdata['inv_time'] = time();
						$insdata['status'] = 'unpaid';
					//	$insdata['command_number'] = $next_command_number;

						$insdata['ready_to_pay'] =	$insdata['remittance']-$insdata['tcs_deducted'];						
						$this->admin_model->insert_cancel_report($insdata);
						$refund_amount = $insdata['remittance'] - $insdata['tcs_deducted'];
						
					}
				}
				
				
			}
		
			/*$finalcheck=$this->db->query("select count(*) as num from tbl_all_income where product_id='$item->pro_id' and order_id='$item->order_id' and vendor_id='$item->vendor'")->row();
				if($finalcheck->num<1)
				{	
						$incomfor=$item->total_amount-($this->apply_charge($item->total_amount,$total_charge)+$this->apply_charge($this->apply_charge($item->total_amount,$total_charge),$servicetax_gst));
						$incomfor=$incomfor - $this->apply_charge($item->total_amount,$tcs);
						$final_report['vendor_id'] =$item->vendor;
						$final_report['seller_amoount'] = $item->total_amount;
						$final_report['total_income'] = $incomfor;	
						$final_report['total_deducation'] = $refund_amount;
						$final_report['product_id'] = $item->pro_id;	
						$final_report['order_id'] = $item->order_id;
						$final_report['final_income'] = $incomfor - $refund_amount;
						$final_report['date'] = date('Y-m-d');
						$final_report['status'] = 'unpaid';
						$this->admin_model->insert_final_report($final_report);
						//print_r($final_report);
						//die;
                } */

			
			
			/*if($total_amount>0 && $total_amount!='0')
			//{
//$vendor_id
				$final_report['vendor_id'] =10;
				$final_report['seller_amoount'] = $total_seller_amount;
				$final_report['total_income'] = $total_amount;	
				$final_report['total_deducation'] = $refund_amount;
				$final_report['final_income'] = $total_amount - $refund_amount;
				$final_report['date'] = date('Y-m-d');
				$final_report['status'] = 'unpaid';
				$this->admin_model->insert_final_report($final_report);			
			//}*/
			
		}
		
		//final payout query	
		if($chk123456==1){
		$selamount ="";
		$income ="";
		$dudction ="";
		$vid = array();
		
		$finalcheck=$this->db->query("select * from tbl_income where command_date='".date('Y-m-d')."'")->result();
		foreach($finalcheck as $amo){
			array_push($vid,$amo->vendor_id);
		}
		
		$looparray = array_unique($vid);
		
		foreach($looparray as $vid){
			
			
			$samount = $this->db->query("select order_id, sum(sell_amount) as amount, sum(final_remittance) as income from tbl_income where command_number='".$next_command_number."' and vendor_id='".$vid."'")->row();
			
			$dedection = $this->db->query("select sum(final_remittance) as amount from tbl_return_deducation where command_number='".$next_command_number."' and vendor_id='".$vid."'")->row()->amount;
			
			            $final_report['vendor_id'] =$vid;
						$final_report['seller_amoount'] = $samount->amount;
						$final_report['total_income'] = $samount->income;	
						$final_report['total_deducation'] = $dedection;
						$final_report['product_id'] = '';	
						$final_report['order_id'] = $samount->order_id;
						$final_report['final_income'] =$samount->income-$dedection;
						$final_report['date'] = date('Y-m-d');
						$final_report['status'] = 'unpaid';
						$this->admin_model->insert_final_report($final_report);
						
		}  
		
						
		 
		//
	//	echo "<pre>";
		//print_r($final_report); 
		//die;
	} 
		//echo $chk123456;
		$this->load->view('admin/vedor-invoice/distribute-success');
	}
	
	public function apply_charge($amount,$charge)
	{
		return $amount*($charge/100);
	}
	
	function vandor_return_deductions()
	{
		if(isset($_POST['vendor_id']) && !empty($_POST['vendor_id']))
		{
			$this->db->where('vendor_id',$_POST['vendor_id']);
		}
		if(isset($_POST['product_id']) && !empty($_POST['product_id']))
		{
			$this->db->where('product_id',$_POST['product_id']);
		}
		if(isset($_POST['order_id']) && !empty($_POST['order_id']))
		{
			$this->db->where('order_id',$_POST['order_id']);
		}
		if(isset($_POST['sdate']) && isset($_POST['edate']))
		{
			$this->db->where('inv_date >=', $_POST['sdate']);
			$this->db->where('inv_date <=', $_POST['edate']);
		}
		
		$data['income'] = $this->admin_model->select_vendor_return_deducation();
		$this->load->view('admin/vedor-invoice/return-deducation',$data);
	}	
	function edit_vendor_return_deducation()
	{
		$args = func_get_args();
		if(isset($_POST['updatedata']))
		{
		
			$status = $this->input->post('status');
			if($status=='Approved')
			{
				$ready_to_pay ='Return Deducted from sell payout';
			}
			else
			{
				$ready_to_pay ='Under Dispute';
			}	
			
			$update_data['transaction_id'] = $this->input->post('transaction_id');
			$update_data['status'] = $this->input->post('status');
			$update_data['ready_to_pay'] = $ready_to_pay;			
			$this->admin_model->update_vandor_return_deducation_by_id($update_data,$args[0]);
			$this->session->set_flashdata('message','<div class="alert alert-success">record has been successsully updated.</div>');
			redirect('index.php/admin/vandor_return_deductions');
		}	
		
		$data['income'] = $this->admin_model->select_vendor_return_deducation_by_id($args[0]);
		$this->load->view('admin/vedor-invoice/edit-return-deducation',$data);
	}
	
	
	public function refund_report()
	{
		$order_list = $this->orderdetail_model->select_order_refund();		
		//print_r($order_list); die;		
		$commission = 1;
		$admin_charge = 2;
		$promotion_charge = 1;
		$total_charge = $commission+$admin_charge+$promotion_charge;
		$servicetax_gst = 15;	
		$tcs = 1;
		foreach($order_list as $order)
		{
			$items = $this->shoppingdetail_model->prodcutdetail($order->id);			
			foreach($items as $item)
			{
				$get_income = $this->admin_model->check_cancel_report($item->vendor,$order->id,$item->pro_id);
				if(count($get_income)=='0')
				{
					if($item->vendor!='0')
					{
						$sell_amount = $item->total_amount;
						$insdata['sell_amount'] = $sell_amount;
						$insdata['commission'] = $this->apply_charge($sell_amount,$commission);
						$insdata['admin_charge'] = $this->apply_charge($sell_amount,$admin_charge);
						$insdata['promotion_charge'] = $this->apply_charge($sell_amount,$promotion_charge);
						$insdata['taxable_amount'] = $this->apply_charge($sell_amount,$total_charge);
						$insdata['servicetax_gst'] = $this->apply_charge($this->apply_charge($sell_amount,$total_charge),$servicetax_gst);
						$insdata['final_deduction'] = $this->apply_charge($sell_amount,$total_charge)+$this->apply_charge($this->apply_charge($sell_amount,$total_charge),$servicetax_gst);
						$insdata['remittance'] = $sell_amount - $insdata['final_deduction'];
						$insdata['tcs_deducted'] =	$this->apply_charge($sell_amount,$tcs);
						$insdata['final_remittance'] =	$insdata['remittance']-$insdata['tcs_deducted'];
						$insdata['product_id'] = $item->pro_id;
						$insdata['order_id'] =	$item->order_id;
						$insdata['vendor_id'] =	$item->vendor;
						$insdata['inv_date'] =	date('Y-m-d');
						$insdata['inv_time'] = time();
						$insdata['status'] = 'unpaid';
						$insdata['ready_to_pay'] =	'no';						
						$this->admin_model->insert_cancel_report($insdata);
					}
				}	
			}
		}
		$this->load->view('admin/vedor-invoice/distribute-success');
	}
	
	function edit_credit_settlment()
	{
		$args = func_get_args();
		if(isset($_POST['updatedata']))
		{
			$update_data['transaction_id'] = $this->input->post('transaction_id');
			$update_data['status'] = $this->input->post('status');
			$this->admin_model->update_final_income($update_data,$args[0]);
			$this->session->set_flashdata('message','<div class="alert alert-success">record has been successsully updated.</div>');
			redirect('index.php/admin/seller_credit_settlment');
		}		
		$data['income'] = $this->admin_model->final_income_by_id($args[0]);
		$this->load->view('admin/vedor-invoice/edit-seller-credit-settlement',$data);
	}
	function seller_credit_settlment()
	{
		$data['income'] = $this->admin_model->vendor_credit_final();
		$this->load->view('admin/vedor-invoice/seller-credit-settlement',$data);
	}
	
	// admin helpdesk
	
	function order_product()
	{
		  $order_id = $_REQUEST['order_id'];
		
		$data['items'] = $this->shoppingdetail_model->prodcutdetail($order_id);
		$this->load->view('admin/order_product',$data);
	}
	function get_order()
	{
		  $booking_id = $_REQUEST['booking_id'];
		
		$data['items'] = $this->admin_model->getHelpdeskById($booking_id);
		
		$data['getcomment'] = $this->admin_model->getcommentById($booking_id);
		
		$this->load->view('admin/get_order',$data);
	} 
	function help_desk()
	{
		$loginUserId = $this->admin_model->getLoginUserVar('admin_id');
		//print_r($loginUserId); die;
		if(isset($_POST['checkHelpDesk']))
		{
			$sdate = $this->input->post('startDate');
			$edate = $this->input->post('endDate');
			if(!empty($sdate) && !empty($edate))
			{						
			   //$date_1 = 
				$this->db->where('queryDate >=',date('Y-m-d',strtotime($sdate)));
				$this->db->where('queryDate <=',date('Y-m-d',strtotime($edate)));
			}	
		}
		$data['myorder']=$this->admin_model->getHelpdeskByUserId($loginUserId);
		$data['myorder2']=$this->admin_model->getHelpdeskByUserId2($loginUserId);
		$data['myorder3']=$this->admin_model->getHelpdeskByUserId3($loginUserId);
		$data['myorder4']=$this->admin_model->getHelpdeskByUserId4($loginUserId);
		$data['myorder1']=$this->admin_model->getOrderByUserId($loginUserId);	
		$data['Depart']=$this->admin_model->getHelpDepart();
		//echo "<pre/>"; print_r($data); die;
 
		$this->load->view('admin/help_desk',$data);
	}
	
	function manage_department()
	{
			
		$data['Depart']=$this->admin_model->getHelpDepart();
		
		$this->load->view('admin/manage_department',$data);
	}
	
	function edit_department()
	{
		$args = func_get_args();
		//print_r($data['Depart']); die;
		//$this->load->model('helpdesk_model');		
		if(isset($_POST['updateData']))
		{
			$data['title'] = $this->input->post('title');
			$data['name'] = $this->input->post('name');
			$data['email'] = $this->input->post('email');
			$data['status'] = $this->input->post('status');
			$this->admin_model->updateDepartmentData($args[0],$data);
			redirect('admin/manage_department');
		}	
		
		$data['helpdesk']= $this->admin_model->selectDepartmentByID($args[0]);
		//print_r($data['Depart']); die;
		$this->load->view('admin/edit_department',$data);
	}
	
	function add_department()
	{
		//$dept_id=$this->uri->segment(3);	
		//$data['Depart']=$this->admin_model->edit_department($dept_id);
		
		$this->load->view('admin/add_department');
	}
	
	function insert_depart()
	{
		if(isset($_POST['updateData']))
		{
			$data['title'] = $this->input->post('title');
			$data['name'] = $this->input->post('name');
			$data['email'] = $this->input->post('email');
			$data['status'] = $this->input->post('status');
			$this->admin_model->insertDepartmentData($data);

			$this->session->set_flashdata('sms','Your Department has been submitted!'); 	
			redirect('admin/manage_department');
		}		
		
		
	}
	
	function insert_help_desk()
	{
		$data=$this->input->post();
		
		$pid=explode("DI-",$data['product_id']); 
		$test=$this->admin_model->select_vendor_by_productid($pid[1]);
		
		$data['myorder']=$this->admin_model->insert_help_desk($data, $test[0]->id, $pid[1]);	
		
		$this->session->set_flashdata('sms','Your Query has been submitted!'); 	
		redirect('admin/help_desk');
	}
	
	function comment_submit()
	{
		ob_start();
		$data=$this->input->post();
		
		
		$data['myorder']=$this->admin_model->insert_comment($data);	
		$this->session->set_flashdata('sms','Your comment has been submitted!');
		redirect(base_url().'admin/help_desk');
	}
	
	function download_all_helpdesk_query_mis()

	{					
		//$this->load->helper(array('dompdf', 'file'));
		$this->load->model('admin_model');

		$data['helpdesk']= $this->admin_model->getAllQuery();		

		$this->load->view('admin/all-query-download',$data);

		

		$html = $this->output->get_output();		

		$this->load->library('dompdf_gen');		

		$this->dompdf->load_html($html);

		$this->dompdf->render();

		$this->dompdf->stream(date('d_m_Y_').time()."help_desk_report.pdf");			

	}
	
	function test()
	{
			$config = Array(
								'protocol' => 'smtp',
								'smtp_host' => 'ssl://cs7.hostgator.in',
								'smtp_port' => 465,
								'smtp_user' => 'info@discountidea.com',
								'smtp_pass' => 'vikas1234',
                                 'mailtype'  => 'html',
                                'newline' =>"\r\n"
							);
            $subject="sdds";
			$message="dsdd";
             $this->email->initialize($config);
            $this->email->from('info@discountidea.com');
            //$to= 'indiandiscountidea@gmail.com';
            $this->email->to('ashish.kumar@zaptas.com');
			//$this->email->cc($cc);
			//$this->email->bcc($bcc);
            
            $this->email->subject($subject);
            
			
            $this->email->message($message);
			$this->email->send();

	}
	
	function manage_subadmin()
	{		
		$data['adminData'] = $this->admin_model->getAllSubAdmin();
		$this->load->view('admin/sub-admin-listing',$data);
	}
	
	function add_admin()
	{
		if(isset($_POST['suubmitData']))
		{ 
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			$email = $this->input->post('email');
			$status = $this->input->post('status');
			
			$adminData = $this->admin_model->checkAdminByUsername($username);
			if(count($adminData)>0)
			{
				$this->session->set_flashdata('message','<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>Username is already used please choose another.</strong></div>');
				redirect('index.php/admin/add_admin');
			}
			else
			{
				if(count($_POST['module'])>0)
				{
					$module = implode(",",$_POST['module']);
				}
				else{
					$module = "";
				}	
				
				$adata['username'] = $username;
				$adata['module'] = $module;
				$adata['password'] = $password;
				$adata['email'] = $email;
				$adata['type'] = 2;
				$adata['cdate'] = time();	
				$adata['status'] = $status;			
				$ins = $this->admin_model->insertAdmin($adata);
				if($ins)
				{
					$this->session->set_flashdata('message','<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>your password has been changed successfully.</strong></div>');
					redirect('index.php/admin/manage_subadmin');
				}
			}	
		}
		
		$this->load->view('admin/add-sub-admin');
	}
	function edit_admin()
	{
		$args = func_get_args();
		if(isset($_POST['suubmitData']))
		{
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			$email = $this->input->post('email');
			$status = $this->input->post('status');
			//print_r($_POST['module']); die();
			if(count($_POST['module'])>0)
			{
				$module = implode(",",$_POST['module']);
			}
			else{
				$module = "";
			}	
			
			$adata['username'] = $username;
			$adata['module'] = $module;
			$adata['password'] = $password;
			$adata['email'] = $email;
			$adata['type'] = 2;
			$adata['cdate'] = time();	
			$adata['status'] = $status;		
//print_r($args[0]); die;			
			$ins = $this->admin_model->updateAdminById1($args[0],$adata);
			if($ins)
			{
				$this->session->set_flashdata('message','<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>admin successfully updated.</strong></div>');
				redirect('index.php/admin/manage_subadmin');
			}
			
		}
		$data['adminData'] = $this->admin_model->selectAdminById($args[0]);
		$this->load->view('admin/add-sub-admin',$data);
	}
	function admin_delete()
	{
		$args = func_get_args();
		$this->admin_model->deleteAdmin($args[0]);
		$this->session->set_flashdata('message','<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>admin has been successfully deleted.</strong></div>');
		redirect('index.php/admin/manage_subadmin');
	}
	
	public function setting(){
		
		if(!empty($_POST)){
			//print_r($_POST);
			$this->db->query("delete from sellting");
			$this->db->insert("sellting",$_POST);
		redirect(base_url('index.php/admin/setting/'));
		}else{
			$data['all'] = $this->db->query('select * from sellting')->result();
			
			$this->load->view('admin/settingvendor',$data);
		}
		
	}
	function manage_pincode()
	{		
		//$data['adminData'] = $this->admin_model->getAllSubAdmin();
		$this->load->view('admin/manage_pincode');
	}
	function uploadpincode()
       {		
		
	if(isset($_POST['sub']))
		{						

		//print_r($_FILES['uploadFile']);
		$a = $_FILES['uploadFile'];
		$csv = array_map('str_getcsv', file($a['tmp_name']));
		$csv_pin = array();
		foreach ($csv as $row) {
			$n = $row[0];
			$d = $row[1];
			     array_push($csv_pin,array('pincode'=>$n ,'days'=>$d));

		}
		// print_r($csv_pin);
		// exit;
			$this->admin_model->insert_pincode($csv_pin);			
			$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully saved.</div>');
			redirect('index.php/admin/manage_pincode');			
		}
		//$this->load->view('admin/pincode/uploadcsvview');	
		
	}
}

