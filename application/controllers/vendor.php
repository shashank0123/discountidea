<?php
if ( !defined('BASEPATH')) exit('No direct script access allowed');
class Vendor extends CI_Controller
{
	
	public function __construct()
        {
                parent::__construct();
                $this->load->model('vendor_model');
				$this->load->helper('url');
        }	
	
	function create_account()
	{		
		if(isset($_POST['createbutn']))
		{			
			$email=$this->input->post('email');
			$user_data=$this->user_model->selectuserby_email($email);
			$checkfile= count($user_data);
			if($checkfile >0)
			{				
				$this->session->set_flashdata("message1","<br><div class='alert alert-danger'><strong>".$email."</strong> Email address already exists choose a different one.</div>");
				redirect('user/create_account');
			}
			$token=md5(time().$email);
			$password=$this->input->post('password');
            $data['fname']=$this->input->post('fname');
			$data['mobile']=$this->input->post('mobile');
         
			$data['email']= $email;
			$data['password']= $password;
			$data['status']= 1;
			$data['vender_type']= 1;
			$data['activate_token']=$token; 
                        $name = $this->input->post('fname');
			$insertdata=$this->user_model->insert_user($data);
			if($insertdata)
			{
				/*$this->email->set_newline("\r\n");
				$this->email->set_mailtype("html");
				$this->email->to($email);
				$this->email->from(ADMIN_EMAIL,'Discount Idea');
			    $this->email->subject('Mail Confirmation');*/
				$search  = array('{NAME}','{EMAIL}','{PASSWORD}');
				$replace = array($name,$email,$password);		
				$block_body = $this->block_model->selectblockbyid(8);
				$message =  str_replace($search,$replace,$block_body[0]->description);
				//$this->email->message($message);
				//$this->email->send();
		$this->user_model->sendmail('info@discountidea.com', $email,'rahulpal7623@yahoo.com', 'Discount Idea Vendor Registration Mail Confirmation', $message);
		
				
			}
			$this->session->set_flashdata("message1","<br><div class='alert alert-success'><h5>Thanks For Registering With us! </h5></div>");
			redirect('vendor/loginuser');
			
		}
		
		$this->load->view('front/vandor-login-page');
	}
	
	function loginuser()
	{
		$user_login_id = $this->session->userdata('USER_ID');
		if(!empty($user_login_id))
		{
			redirect('index.php/user/profile');
		}
		
		if(isset($_POST['buttonlogin']))
		{
			//echo "dsfdsfdsjfhdskj";die;
			$email=$this->input->post('email1');
			$password=$this->input->post('password1');
			
			if(!empty($email) && !empty($password))
			{
				$data=$this->user_model->user_login($email,$password);
				if(count($data)>0)
				{
					//echo $data[0]->status; die();
					if($data[0]->status==1)
					{
						$login_data=array('id'=>$data[0]->id,'USER_ID'=>$data[0]->id,'email'=>$data[0]->email,'user_type'=>$data[0]->vender_type,'logged_in'=>true);
						$this->session->set_userdata($login_data);
						if(isset($_GET['backurl']) && $_GET['backurl']!="")
						{
                                                       redirect('index.php/user/profile');
							//redirect($_GET['backurl']);
						}
						else
						{
							redirect('index.php/user/profile');
						}
					}
					else
					{
						$this->session->set_flashdata('message1','<div class="alert alert-danger">This user is inactive. Please contact us regarding this account.</div>');
						redirect('index.php/vendor/loginuser');
					}
				}
				else
				{				
					$this->session->set_flashdata('message1','<div class="alert alert-danger">Invalid username or password !</div>');
					redirect('index.php/vendor/loginuser');
				}			
			}
		}	
		$this->load->view('front/vandor-login-page');
	}
	
	
	public function forgotpassword()
	{
		if(isset($_POST['forgotbutn']))
		{
			//echo "sdfkdsfhdskj";die;
				$email=$this->input->post('email');
				//echo $email;die;
			if(!empty($email))
			{
				$userdata=$this->user_model->selectuserby_email($email);
				if(count($userdata)>0)
				{
					$password_token=sha1(time().$email);
					$data['activate_token']=$password_token;
					$upd_token = $this->user_model->update($userdata[0]->id,$data);
					if($upd_token)
					{
						$this->email->set_newline("\r\n");
						$this->email->set_mailtype("html");
						$this->email->to($userdata[0]->email);
						$this->email->from(ADMIN_EMAIL, 'Discount Idea');
						$this->email->subject('Discount Idea Forgot Password');
						$msg = "Dear ".$userdata[0]->name.",<br>Please click bellow link to reset your password.<br>"; 
						//$msg .="Please click on the following link: <br>";
						$msg .= "<a href='".base_url('vendor/reset_password/'.$password_token)."'><button style='background-color:#4ed4ff; color:#fff; padding:5px; border:1px solid black;'>Click Here For Reset Password</button></a><br><br>";
						$msg .= "Thank you,<br>";
						$msg .= "Discount Idea Support Team";
						$this->email->message($msg);
						$this->email->send();
						$this->session->set_flashdata("message","<div class='alert alert-success'>If there is an account associated with $email you will receive an email with a link to reset your password..</div>");
						redirect('vendor/forgotpassword');			
					}
				}
				else
				{
					$this->session->set_flashdata("message","<div class='alert alert-danger'>Please enter a valid email address.</div>");
					redirect('vendor/forgotpassword');
				}


			}
			else
			{
				$this->session->set_flashdata("message","<div class='alert alert-danger'>Please enter a valid email address.</div>");
				redirect('vendor/forgotpassword');
			}	
			}		
		$this->load->view('front/forgotpassword');
		
	}
	
	function reset_password()
	{
		$loginUserId = $this->user_model->getLoginUserVar('USER_ID');
		if($loginUserId!=""){ redirect('user/profile');  }
		
		$args = func_get_args();
		if(count($args)>0)
		{
			$token = $args[0];
			$userdata = $this->user_model->checkUserTokenForgotPassword($token);
			if(count($userdata)>0)
			{
				if(isset($_POST['resetPassword']))
				{
					$password=$this->input->post('password');
					$cpassword=$this->input->post('cpassword');
					if($password!=$cpassword)
					{
						$this->session->set_flashdata("message","<div class='alert alert-danger'>New Password and Confirm Password Do Not Matched!.</div>");
						redirect('vendor/reset_password/'.$args[0]);
					}
					else
					{
						$data['password']= $password;
						$data['activate_token']='';
						$this->user_model->update($userdata[0]->id,$data);					
						$this->session->set_flashdata("message","<div class='alert alert-success'>Password successfully changed.</div>");
						redirect('vendor/login');
					}	
				}		
				
			}
			else
			{
				$this->session->set_flashdata("message","<div class='alert alert-danger'>The token is invalid or expired!.</div>");
				redirect('vendor/forgot_password');
			}		
		}
		else
		{
				$this->session->set_flashdata("message","<div class='alert alert-danger'>The token is invalid or expired!.</div>");
				redirect('vendor/forgot_password');
		}
		$this->load->view('front/respassword');
	}
	
	
	function manage_store()
	{
		$loginUserId = $this->user_model->getLoginUserVar('USER_ID');
		if(isset($_POST["createstore"]))
		{	
		
			if($_FILES['logo']['name']!="")
			{
				$logo = createUniqueFile($_FILES['logo']['name']);
				$file_temp = $_FILES['logo']['tmp_name'];
				$path = 'uploads/store/'.$logo;				
				move_uploaded_file($file_temp,$path);				
			}
			else
			{
				$logo = $this->input->post("old_logo");
			}
			if($_FILES['banner']['name']!="")
			{
				$banner = createUniqueFile($_FILES['banner']['name']);
				$file_temp = $_FILES['banner']['tmp_name'];
				$path = 'uploads/store/'.$banner;				
				move_uploaded_file($file_temp,$path);				
			}
			else
			{
				$banner = $this->input->post("old_banner");
			}
			$store_id = $this->input->post("store_id");
			$data1["title"]=$this->input->post("title");
			$data1["logo"]=$logo;
			$data1["banner"]=$banner;
			$data1["content"]=$this->input->post("content");
			$this->session->set_flashdata('message','<div class="alert alert-success">store has been successfully updated</div>'); 			//$this->db->insert("tbl_product",$data);

			$insert=$this->store_model->update_store($store_id,$data1);			
			redirect("index.php/vendor/manage_store/");
		}
		$data['store']=$this->store_model->selectstoreby_uid($loginUserId);		
		$this->load->view('front/vendor/edit-store',$data);
	}
	
	
	function bussiness()
	{
		$loginUserId = $this->user_model->getLoginUserVar('USER_ID');
		if(isset($_POST["bussinessdetails"]))
		{	
	
			if($_FILES['tin_vat_upload']['name']!="")
			{
				$tin_vat_upload = createUniqueFile($_FILES['tin_vat_upload']['name']);
				$file_temp = $_FILES['tin_vat_upload']['tmp_name'];
				$path = 'uploads/vendor/'.$tin_vat_upload;				
				move_uploaded_file($file_temp,$path);				
			}
			else{ $tin_vat_upload = $this->input->post("tin_vat_upload_old"); 	}
			
			if($_FILES['tan_upload']['name']!="")
			{
				$tan_upload = createUniqueFile($_FILES['tan_upload']['name']);
				$file_temp = $_FILES['tan_upload']['tmp_name'];
				$path = 'uploads/vendor/'.$tan_upload;				
				move_uploaded_file($file_temp,$path);				
			}
			else{ $tan_upload = $this->input->post("tan_upload_old"); 	}
			
			if($_FILES['company_pan_upload']['name']!="")
			{
				$company_pan_upload = createUniqueFile($_FILES['company_pan_upload']['name']);
				$file_temp = $_FILES['company_pan_upload']['tmp_name'];
				$path = 'uploads/vendor/'.$company_pan_upload;				
				move_uploaded_file($file_temp,$path);				
			}
			else{ $company_pan_upload = $this->input->post("company_pan_upload_old"); 	}
			
			if($_FILES['gstin_upload']['name']!="")
			{
				$gstin_upload = createUniqueFile($_FILES['gstin_upload']['name']);
				$file_temp = $_FILES['gstin_upload']['tmp_name'];
				$path = 'uploads/vendor/'.$gstin_upload;				
				move_uploaded_file($file_temp,$path);				
			}
			else{ $gstin_upload = $this->input->post("gstin_upload_old"); 	}

            if($_FILES['trademark_upload']['name']!="")
			{
				$trademark_upload= createUniqueFile($_FILES['trademark_upload']['name']);
				$file_temp = $_FILES['trademark_upload']['tmp_name'];
				$path = 'uploads/vendor/'.$trademark_upload;				
				move_uploaded_file($file_temp,$path);				
			}
			else{ $trademark_upload = $this->input->post("trademark_upload_old"); 	}

			if($_FILES['company_address_upload']['name']!="")
			{
				$company_address_upload= createUniqueFile($_FILES['company_address_upload']['name']);
				$file_temp = $_FILES['company_address_upload']['tmp_name'];
				$path = 'uploads/vendor/'.$company_address_upload;				
				move_uploaded_file($file_temp,$path);				
			}
			else{ $company_address_upload = $this->input->post("company_address_upload_old"); 	}
			
			$data1["tin_vat_upload"]=$tin_vat_upload;
			$data1["tan_upload"]=$tan_upload;
			$data1["company_pan_upload"]=$company_pan_upload;
			$data1["gstin_upload"]=$gstin_upload;
            $data1["trademark_upload "] = $trademark_upload;
			
			$data1["company_address"]=$this->input->post("company_address");
			$data1["company_address_upload"]=$company_address_upload;
			
			$data1["company_type"]=$this->input->post("company_type");
			$data1["company_name"]=$this->input->post("company_name");
			$data1["tin_vat"]=$this->input->post("tin_vat");			
			$data1["tan"]=$this->input->post("tan");
			$data1["company_pan"]=$this->input->post("company_pan");
			$data1["gstin"]=$this->input->post("gstin");			
			
			$this->session->set_flashdata('message','<div class="alert alert-success">bussiness details has been successfully updated</div>'); 			//$this->db->insert("tbl_product",$data);

			$insert = $this->user_model->update($loginUserId,$data1);			
			redirect("index.php/vendor/bussiness/");
		}
		$data['user']=$this->user_model->selectuserby_id($loginUserId);		
		$this->load->view('front/vendor/bussiness-details',$data);
	}
	
	function bank()
	{
		$loginUserId = $this->user_model->getLoginUserVar('USER_ID');
		if(isset($_POST["bankdetails"]))
		{	
			if($_FILES['passbook']['name']!="")
			{
				$passbook = createUniqueFile($_FILES['passbook']['name']);
				$file_temp = $_FILES['passbook']['tmp_name'];
				$path = 'uploads/vendor/'.$passbook;				
				move_uploaded_file($file_temp,$path);				
			}
			else
			{
				$passbook = $this->input->post("old_passbook");
			}
			$data1["passbook"]=$passbook;
			$data1["account_no"]=$this->input->post("account_no");			
			$data1["account_holder"]=$this->input->post("account_holder");
			$data1["ifsc_code"]=$this->input->post("ifsc_code");
			$data1["bank_name"]=$this->input->post("bank_name");
			$data1["bank_branch"]=$this->input->post("bank_branch");
			$data1["bank_city"]=$this->input->post("bank_city");
			$data1['status'] = $this->input->post('status');
			$data1["edit_status"]=$this->input->post("edit_status");			
			$data1["bank_status"]=$this->input->post("bank_status");
			$data1["bussiness_status"]=$this->input->post("bussiness_status");
			
			$this->session->set_flashdata('message','<div class="alert alert-success">bank details has been successfully updated</div>'); 			//$this->db->insert("tbl_product",$data);

			$insert = $this->user_model->update($loginUserId,$data1);			
			redirect("index.php/vendor/bank/");
		}
		$data['user']=$this->user_model->selectuserby_id($loginUserId);
		$this->load->view('front/vendor/bank-details',$data);
	}
	
	function store()
	{
		$args = func_get_args(); 		
		$data['categoryimages']=$this->store_model->select_store_image($args[1]);
		$storedata = $this->store_model->selectstorebyid($args[1]);		
		$data['productdata']=$this->product_model->selectProductByVedorID($storedata[0]->uid);		
		$data['store']= $storedata; //$this->store_model->selectstorebyid($args[1]);		
		$this->load->view('front/vendor/store',$data);
	}
	
	function allstore()
	{
		$data['store']= $this->store_model->selectAllStore();		
		$this->load->view('admin/store/list',$data);
	}
	
	function store_edit_admin()
	{
		$args = func_get_args(); 
		
		if(isset($_POST["createstore"]))
		{	
		
			if($_FILES['logo']['name']!="")
			{
				$logo = createUniqueFile($_FILES['logo']['name']);
				$file_temp = $_FILES['logo']['tmp_name'];
				$path = 'uploads/store/'.$logo;				
				move_uploaded_file($file_temp,$path);				
			}
			else
			{
				$logo = $this->input->post("old_logo");
			}
			//$store_id = $this->input->post("store_id");
			$data1["title"]=$this->input->post("title");
			$data1["logo"]=$logo;
			$data1["content"]=$this->input->post("content");
			$data1["status"]=$this->input->post("status");
			$this->session->set_flashdata('message','<div class="alert alert-success">store has been successfully updated</div>'); 			//$this->db->insert("tbl_product",$data);

			$insert=$this->store_model->update_store($args[0],$data1);			
			redirect("index.php/vendor/allstore/");
		}
		
		if(isset($_POST['uploadfiles']))
		{	
			if(count($_FILES)>0)
			{
				$filesCount = count($_FILES['image']['name']);
				for($i = 0; $i < $filesCount; $i++)
				{						
					$name = $_FILES['image']['name'][$i];
					$tmpname1 = $_FILES['image']['tmp_name'][$i];
					$exten=explode(".",$_FILES['image']['name'][$i]);
					$exten=$exten[1];	
					$imagename=time().rand(1,10000).".".$exten;
					move_uploaded_file($tmpname1,'uploads/store/'.$imagename);									
					$datasd["store_id"]=$args[0];	  					
					$datasd["image"] = $imagename;
					$datasd["image_url"] = $_POST['image_url'][$i];
					$this->store_model->insert_store_image($datasd);
				}
			}
			$this->session->set_flashdata('message1','<div class="alert alert-success">image has been successfully uploaded</div>');
			redirect("index.php/vendor/store_edit_admin/".$args[0]);
		}			
		
		$data['LISTCATEGORYIMAGE']=$this->store_model->select_store_image($args[0]);
		$data['storedata']= $this->store_model->selectstorebyid($args[0]);			
		$this->load->view('admin/store/edit-store',$data);
	}
	
	function store_active()
	{
		$args = func_get_args(); 
		if($args[0]=='inactive')
		{
			$status = '0';
		}
		else
		{
			$status = '1';
		}
		//echo $status; die;
		$data1["status"]= $status;		 
		$insert=$this->store_model->update_store($args[1],$data1) ;	
        $this->session->set_flashdata('message','<div class="alert alert-success">store has been successfully updated</div>');		
		redirect("index.php/vendor/allstore/");
	}
	public function deletestoreimages($id)
	{
		$args = func_get_args();
		$this->store_model->deleteStoreImageById($args[0]);
		$this->session->set_flashdata('message1','<div class="alert alert-success">image has been successfully Deleted  </div>'); 
		redirect("index.php/vendor/store_edit_admin/".$args[1]);
    }
	
	public function addfilteroption()
	{
		$this->load->model('filters_model');   
		$data['p_id'] = $this->input->post('filter_cat');
		$data['title'] = $this->input->post('filter_val');
		$data['type'] = 'option';
		$this->filters_model->insert_filters($data);
		//print_r($_POST);
		//$args = func_get_args();
    }
	
	function manage_blog()
	{
		$args = func_get_args(); 		
		$loginUserId = $this->user_model->getLoginUserVar('USER_ID');
		$data['blogdata']=$this->blog_model->select_blog_by_uid($loginUserId);		
		$this->load->view('front/vendor/blog',$data);
	}
	
	function add_blog()
	{
		$args = func_get_args(); 		
		$loginUserId = $this->user_model->getLoginUserVar('USER_ID');
		if(isset($_POST['savedata']))
		{			
			if($_FILES['image']['name']!="")
			{
				$file = createUniqueFile($_FILES['image']['name']);
				$file_temp = $_FILES['image']['tmp_name'];
				$path = 'uploads/blog/'.$file;				
				move_uploaded_file($file_temp,$path);				
			}
			else
			{
				$file = "";
			}
			$data['image'] = $file;
			$data['cat_id'] = $this->input->post('cat_id');
			$data['uid'] = $loginUserId;
			$data['title'] = $this->input->post('title');
			$data['content'] = $this->input->post('content');	
			$data['status'] = 1;//$this->input->post('status');
			$data['create_date'] = date('Y-m-d');
			
			$this->blog_model->insert($data);			
			$this->session->set_flashdata('message','<div class="alert alert-success">blog has been successfully saved.</div>');
			redirect('index.php/vendor/manage_blog');			
		}		
		$catdata['CATEGORY'] = $this->blog_model->select_all_active_blogcat();		
		$this->load->view('front/vendor/add-blog',$catdata);
	}
	
	function edit_blog()
	{
		$args = func_get_args(); 		
		$loginUserId = $this->user_model->getLoginUserVar('USER_ID');
		if(isset($_POST['updatedata']))
		{
			if($_FILES['image']['name']=="")
			{
				$file = $_POST['oldimage'];
			}
			else
			{
				$file = createUniqueFile($_FILES['image']['name']);
				$file_temp = $_FILES['image']['tmp_name'];
				$path = 'uploads/blog/'.$file;				
				move_uploaded_file($file_temp,$path);	
			}
			$data['image'] = $file;
			$data['cat_id'] = $this->input->post('cat_id');
			$data['uid'] = $loginUserId;
			$data['title'] = $this->input->post('title');
			$data['content'] = $this->input->post('content');	
			$this->blog_model->update($args[0],$data);		
			$this->session->set_flashdata('message','<div class="alert alert-success">blog has been successfully updated.</div>');
			redirect('index.php/vendor/manage_blog');			
		}		
		$catdata['CATEGORY'] = $this->blog_model->select_all_active_blogcat();	
		$catdata['EDITBLOG']= $this->blog_model->select_blog_byid($args[0]);	
		$this->load->view('front/vendor/edit-blog',$catdata);
	}
	
	function order_report()
	{
		$args = func_get_args(); 		
		$loginUserId = $this->user_model->getLoginUserVar('USER_ID');
		$data['orderdata'] = $this->orderdetail_model->selectorderitme_by_vandor($loginUserId);
		$this->load->view('front/vendor/order-report',$data);
	}
	
	/**
	*	order_shipment() select delivery option
	*/
	function order_shipment()
	{		
		$data=array();
		$args=func_get_args(); 
		$data['message']=''; 

		if($this->input->post('status') == "success")
		{
			//$orderID=($this->uri->segment(3))?$this->uri->segment(3):'';
			$txnid 						= ($this->input->post('txnid')) ? $this->input->post('txnid') : '';
			$get_order_id				= @explode('_',$txnid);
			$resp['txnid'] 			    = ($get_order_id[0]) ? $get_order_id[0] : 0;
			$resp['order_id'] 		    = ($get_order_id[1]) ? $get_order_id[1] : $txnid;
			$resp['vendor_id'] 			= $this->user_model->getLoginUserVar('USER_ID');
			$resp['mihpayid'] 			= ($this->input->post('mihpayid')) ? $this->input->post('mihpayid') : '';
			$resp['mode'] 				= ($this->input->post('mode')) ? $this->input->post('mode') : '';
			$resp['status'] 			= ($this->input->post('status')) ? $this->input->post('status') : '';
			$resp['unmappedstatus'] 	= ($this->input->post('unmappedstatus')) ? $this->input->post('unmappedstatus') : '';
			$resp['key'] 				= ($this->input->post('key')) ? $this->input->post('key') : '';
			$resp['amount'] 			= ($this->input->post('amount')) ? $this->input->post('amount') : '';
			$resp['cardCategory'] 		= ($this->input->post('cardCategory')) ? $this->input->post('cardCategory') : '';
			$resp['discount'] 			= ($this->input->post('discount')) ? $this->input->post('discount') : '';
			$resp['net_amount_debit'] 	= ($this->input->post('net_amount_debit')) ? $this->input->post('net_amount_debit') : '';
			$resp['addedon'] 			= ($this->input->post('addedon')) ? $this->input->post('addedon') : '';
			$resp['payment_source'] 	= ($this->input->post('payment_source')) ? $this->input->post('payment_source') : '';
			$resp['pg_type'] 			= ($this->input->post('PG_TYPE')) ? $this->input->post('PG_TYPE') : '';
			$resp['bank_ref_num'] 		= ($this->input->post('bank_ref_num')) ? $this->input->post('bank_ref_num') : '';
			$resp['bankcode'] 			= ($this->input->post('bankcode')) ? $this->input->post('bankcode') : '';
			$resp['error'] 				= ($this->input->post('error')) ? $this->input->post('error') : '';
			$resp['error_message'] 		= ($this->input->post('error_Message')) ? $this->input->post('error_Message') : '';
			$resp['name_on_card'] 		= ($this->input->post('name_on_card')) ? $this->input->post('name_on_card') : '';
			$resp['cardnum'] 			= ($this->input->post('cardnum')) ? $this->input->post('cardnum') : '';
			$resp['cardhash'] 			= ($this->input->post('cardhash')) ? $this->input->post('cardhash') : '';
			$resp['issuing_bank'] 		= ($this->input->post('issuing_bank')) ? $this->input->post('issuing_bank') : '';
			$resp['card_type'] 			= ($this->input->post('card_type')) ? $this->input->post('card_type') : '';

			//echo "<pre>";print_r($resp);die;
			if($this->vendor_model->vendor_shipping_payu_response($resp))
			{
				$data['message']='<center><p>Shipping charge paid successfully.<br><strong> Transection Id :- '.$resp['mihpayid'] .'</strong><p></center>';

			}
		}

		if($this->input->post('status') == "failure")
		{

			$data['message']='<center><p>Shipping Charge Transection Failed.<br><strong> Transection Id :- '.$this->input->post('mihpayid') .'</strong><p></center>';
		}

		if(isset($_POST['updateshipment']))
		{
			$shippingdeatail = '';
			$detail  	= '';			
			$orderID 	= $args[0]; 
			$curl_responsee = $this->order_delivery($orderID); 
			$result 	= json_decode($curl_responsee);			
			$result1 	= $result->packages[0];
			$waybill 	= $result1->waybill;
			$upload_wbn = $result->upload_wbn; //echo "<pre>";print_r($curl_responsee);die;
			if($result->success==1)
			{
				$shippingdeatail['order_id'] 		= $orderID;
				$shippingdeatail['shipping_order'] 	= $curl_responsee;
				$this->orderdetail_model->store_shipping_detail($shippingdeatail);

				$detail['deleviry_company_id']	= $this->input->post('shipping_id');
				$detail['waybill'] 				= $waybill;
				$detail['upload_wbn'] 			= $upload_wbn;
				$this->orderdetail_model->update_shipping_detail($detail,$orderID);

				$shipdetail_json = $this->Packaging_Slip($waybill);//echo "<pre>";print_r($shipdetail_json);die;
				$this->orderdetail_model->updateOrderPackingSlip($orderID,array('shipping_slip' => $shipdetail_json));
				$shipdetail = json_decode($shipdetail_json);
				//if($shipdetail->packages_found>=1)
				$this->session->set_flashdata('message','Order shipped successfully!!');
				redirect('vendor/order_shipment/'.$orderID);
			}
		}	
		
		$data['orderID'] = $args[0];
		$data['orderdata']= $this->shoppingdetail_model->selectorder_detail($args[0]);
		$shippings = $this->shoppingdetail_model->shipping_companies(); //echo "<pre>";print_r($data['orderdata']);die;
		$data['shippings'][] = 'Select Courier Company';
		if(count($shippings)>0) {
			foreach($shippings as $shipping){
				$data['shippings'][$shipping->id] = $shipping->name;
			}
		}
		$data['items'] = $this->orderdetail_model->select_order_item($args[0]);
		$data['ships'] = $this->orderdetail_model->getOrderShippingDetail($args[0]);
		// check for shipping_charge_status
		$data['shipping_charge_status'] = $this->orderdetail_model->shipping_charge_status($args[0]);


		$selectorderitme   = $this->orderdetail_model->selectorderitme($args[0]);
	    $data['dimension'] = $this->orderdetail_model->clculate_product_weight($selectorderitme[0]->pro_id);
	    $this->load->view('front/vendor/order_shipment',$data);	
	}

	function order_delivery($orderID)
	{
		$select_order 	  	  = $this->orderdetail_model->select_order($orderID);
		$select_order_item 	  = $this->orderdetail_model->select_order_item($orderID);
		$select_order_item 	  = $select_order_item['0'];
		$select_deleviry_address = $this->orderdetail_model->select_deleviry_address($orderID);
		 
		$delhiveryAPIMode = 1; //1=>Live Mode, 2=>Test Mode
		if($delhiveryAPIMode==2){
		 	$API = '82d0098db7af369dd5d858d77b2b4dabb752067d';
		 	$url = 'https://test.delhivery.com/api/cmu/create.json';
		}
		else {
		 	$API = '2e58d4137f1c55002ae47e6ed9cbc78d99867f04';
		 	$url = 'https://track.delhivery.com/api/cmu/create.json';
		}

		$payment_mode = 'Prepaid';
		$cod_amount   = 0;
		if($select_order->payment_method=='cashondelivery'){
			$payment_mode = 'COD';
			$cod_amount = $select_order->total_amount;
		}//print_r($payment_mode);die;

		$name='vikas';
		$unqOrder = $orderID;//time(); //
		$data = 'format=json&data={
			"shipments":[{
			"waybill":"",
			"client":"SOCIALDISCOUNTIDEA SURFACE",
			"name":"'.$select_deleviry_address->firstname.'",
			"order":"'.$unqOrder.'",
			"products_desc":"'.$select_order_item->pro_name.'",
			"order_date":"'.$select_order->ord_date.'",
			"payment_mode":"'.$payment_mode.'",
			"total_amount":"'.$select_order->total_amount.'",
			"cod_amount":"'.$cod_amount.'",
			"add":"'.$select_deleviry_address->address.'",
			"city":"'.$select_deleviry_address->city.'",
			"state":"uttar pradesh",
			"country":"india",
			"phone":"9874586957",
			"pin":"'.$select_deleviry_address->pincode.'",
			"return_add":"",
			"return_city":"",
			"return_country":"",
			"return_name":"Test",
			"return_phone":"",
			"return_pin":"",
			"return_state":"",
			"supplier":"",
			"extra_parameters":"",
			"shipment_width":"",
			"shipment_height":"",
			"weight":"'.$select_order->total_amount.'",
			"quantity":"'.$select_order_item->qty.'",
			"seller_inv":"",
			"seller_inv_date":"",
			"seller_name":"abcd",
			"seller_add":"abcd",
			"seller_cst":"abcd",
			"seller_tin":"abcd",
			"consignee_tin":"",
			"commodity_value":"",
			"tax_value":"",
			"sales_tax_form_ack_no":"",
			"category_of_goods":"Non Document",
			"seller_gst_tin":"abcd",
			"client_gst_tin":"",
			"consignee_gst_tin":"",
			"hsn_code":"",
			"invoice_reference":""
			}
			],
			"pickup_location":{
			"city":"'.$select_deleviry_address->city.'",
			"name":"SOCIALDISCOUNTIDEA SURFACE",
			"pin":"'.$select_deleviry_address->pincode.'",
			"country":"india",
			"phone":"02525688766",
			"add":"'.$select_deleviry_address->address.'"
			}
			}
		';
			// print $data; die;
			$curll = curl_init($url);						
			$headr = array();
			$headr[] = 'Authorization: Token '.$API;
			$headr[] = 'Content-Type: application/json';			
			curl_setopt($curll, CURLOPT_FAILONERROR, 1);
			curl_setopt($curll, CURLOPT_FOLLOWLOCATION, 1);
			curl_setopt($curll, CURLOPT_TIMEOUT, 60);
			curl_setopt($curll, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($curll, CURLOPT_POST, true);			
			curl_setopt($curll, CURLOPT_POSTFIELDS,$data);
			curl_setopt($curll, CURLOPT_HTTPHEADER, $headr);	
			$curl_responsee = curl_exec($curll);
			$httpcode = curl_getinfo($curll, CURLINFO_HTTP_CODE);
			curl_close($curll);
			return $curl_responsee;			
	}

	function Packaging_Slip($baybill='')
	{
		$API = '2e58d4137f1c55002ae47e6ed9cbc78d99867f04';//'82d0098db7af369dd5d858d77b2b4dabb752067d';
		$url = 'https://track.delhivery.com/api/p/packing_slip/?wbns='.$baybill;//'https://test.delhivery.com/api/p/packing_slip/?wbns='.$baybill;//.$this->baybill;
		$curll = curl_init($url);						
		$headr = array();
		$headr[] = 'Authorization: Token '.$API;
		$headr[] = 'Content-Type: application/json';	
		curl_setopt($curll, CURLOPT_FAILONERROR, 1);
		curl_setopt($curll, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($curll, CURLOPT_TIMEOUT, 60);
		curl_setopt($curll, CURLOPT_RETURNTRANSFER, 1);				
		curl_setopt($curll, CURLOPT_HTTPHEADER, $headr);
		$curl_responsee = curl_exec($curll);
		$httpcode = curl_getinfo($curll, CURLINFO_HTTP_CODE);	
		curl_close($curll);	
		return 	$curl_responsee;
	}


	public function shipment_price()
	{
		$o_pin='';
		$d_pin='';
		$weight=0;
		$data='';
		if($this->input->post())
	 	{
		 	$comapany_id=$this->input->post('comapany_id');
		    $orderid=$this->input->post('orderid');
		    $vendorID=$this->input->post('vendorID');
		    $select_deleviry_address=$this->orderdetail_model->select_deleviry_address($orderid);
		    $vendorDetail=$this->orderdetail_model->selectusertable($vendorID);
		    $selectorderitme=$this->orderdetail_model->selectorderitme($orderid);
		    $d_pin= $select_deleviry_address->pincode;
		    $o_pin=$vendorDetail[0]->pincode;
		   	$data=$this->orderdetail_model->clculate_product_weight($selectorderitme[0]->pro_id);

		}  
			
		
			$API='2e58d4137f1c55002ae47e6ed9cbc78d99867f04';
			$dataurl = array(
					    'zn' => 'A',
					    'ss' => 'Delivered',
					    'pt' => 'Pre-paid',
					    'gm' => $data->weight,
					    'md' => 'S',
					    'o_pin' => $o_pin,
					    'd_pin' => $d_pin,
					    'cl' => 'SOCIALDISCOUNTIDEA SURFACE'
					);
			$data=(array) $data;
			

			$dataurl1=http_build_query($dataurl);
			$url ='http://track.delhivery.com/api/kinko/v1/invoice/charges/.json?'.$dataurl1;		
			$curll = curl_init($url);						
			$headr = array();
			$headr[] = 'Authorization: Token '.$API;
			$headr[] = 'Accept: application/json';	
			$headr[] = 'Content-Type: application/json';	
			curl_setopt($curll, CURLOPT_FAILONERROR, 1);
			curl_setopt($curll, CURLOPT_FOLLOWLOCATION, 1);
			curl_setopt($curll, CURLOPT_TIMEOUT, 120);
			curl_setopt($curll, CURLOPT_RETURNTRANSFER, 1);				
			curl_setopt($curll, CURLOPT_HTTPHEADER, $headr);
			$curl_responsee = curl_exec($curll);
			$httpcode = curl_getinfo($curll, CURLINFO_HTTP_CODE);	
			 $price_detail=json_decode($curl_responsee);
			 if($httpcode=='200')
			 {
			 	//$data['shippingprice'] =
			 	print $price_detail[0]->total_amount;exit;
			 	//print json_encode($data);
				/*$resp = '<strong>Shipping Price:- '.price_detail[0]->total_amount.'  </strong><br>';
				$resp = '<strong>Package Weight:- '.price_detail[0]->total_amount.'  (in gms)</strong><br>';
				$resp = '<strong>Package Height:- '.price_detail[0]->total_amount.'  </strong><br>';
				$resp = '<strong>Package Length:- '.price_detail[0]->total_amount.'  </strong><br>';
				$resp = '<strong>Package Width:- '.price_detail[0]->total_amount.'  </strong><br>';
			 	echo $resp;die;*/
			 }
			 else
			 {
			 	 print "Price not define for this Pincode";
			 }
			curl_close($curll);		
 			die;
 	}

 	/*
 	* shipment charge payed by vendor
 	*/
 	
 	public function pay_shipment_charge()
 	{
		
 		if($this->input->post())
		{

			$orderID=($this->uri->segment(3))?$this->uri->segment(3):'';
			$loginUserId = $this->user_model->getLoginUserVar('USER_ID');
			$data =$this->vendor_model->getVendorDetail($loginUserId);
			$amount=$this->input->post('shippingprice');
			$shipping_id=$this->input->post('shipping_id');
			$fname=$data->fname.' '.$data->lname;
			$email=$data->email;
			$mobile=$data->mobile;
			$this->session->set_userdata("order_id",$order_id);
			// Live detail
			$PAYU_BASE_URL = "https://secure.payu.in/_payment";
			$key ='2qxDnu';
			$SALT='3wWE5TWB';
			// testing detail
			// $PAYU_BASE_URL = "https://test.payu.in";
			// $key ='gtKFFx';
			// $SALT='eCwWELxi';
			$posted = array(
				"key"=>$key,
				"txnid"=>uniqid().'_'.$orderID,
				"amount"=>$amount,
				"firstname"=>$fname,
				"email"=>$email,
				"phone"=>$mobile,
				"productinfo"=>'payubiz',
				"surl"=>base_url().'vendor/order_shipment/'.$orderID,
				"furl"=>base_url().'vendor/order_shipment/'.$orderID
				); //echo "<pre>";print_r($posted);die;
				$hashSequence ="key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
			if(empty($posted['hash']) && sizeof($posted) > 0) {
				if(empty($posted['key'])
					|| empty($posted['txnid'])
					|| empty($posted['amount'])
					|| empty($posted['firstname'])
					|| empty($posted['email'])
					|| empty($posted['phone'])
					|| empty($posted['productinfo'])
					|| empty($posted['surl'])
					|| empty($posted['furl']))
				{
					$formError = 1;
				}
				else
				{

					$hashVarsSeq = explode('|', $hashSequence);
					$hash_string = '';
					foreach($hashVarsSeq as $hash_var) {
						$hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
						$hash_string .= '|';
					}
					$hash_string .= $SALT;
					$hash = strtolower(hash('sha512', $hash_string));
					$action = $PAYU_BASE_URL . '/_payment';
				}
			} elseif(!empty($posted['hash'])) {
				$hash = $posted['hash'];
				$action = $PAYU_BASE_URL . '/_payment'; 
			} 
			$html ='<form action="'.$action.'" method="post" id="pauform" >
			<input type="hidden" name="key" value="'.$posted["key"].'">
			<input type="hidden" name="txnid" value="'.$posted["txnid"].'">
			<input type="hidden" name="amount" value="'.$posted["amount"].'">
			<input type="hidden" name="firstname" value="'.$posted["firstname"].'">
			<input type="hidden" name="email" value="'.$posted["email"].'">
			<input type="hidden" name="phone" value="'.$posted["phone"].'">
			<input type="hidden" name="productinfo" value="'.$posted["productinfo"].'">
			<input type="hidden" name="surl" value="'.$posted["surl"].'">
			<input type="hidden" name="furl" value="'.$posted["furl"].'">
			<input type="hidden" name="hash" value="'.$hash.'">
			</form>'; //echo "<pre>";print_r($html);die;
			if($hash_string!="")
			{
				echo $html;
			    echo "<script> document.getElementById('pauform').submit();</script>";
					//redirect('index.php/order/success');
			}else{
				echo $html;
			}
		}else{
			redirect('index.php/order/paymentstatus');
		}
	}

	/*
 	* shipment Report payed by vendor
 	*/
	public function shipping_report()
	{
		$result=$this->orderdetail_model->selectorderitme($id=218);
				$args = func_get_args(); 		
	  	$loginUserId = $this->user_model->getLoginUserVar('USER_ID'); 
		$data['orderdata'] = $this->orderdetail_model->shipping_payment_detail($loginUserId);
		$this->load->view('front/vendor/shipping-report',$data);
	}

	function cancellation_report()
	{
		$args = func_get_args(); 		
		$loginUserId = $this->user_model->getLoginUserVar('USER_ID');
		$data['orderdata'] = $this->orderdetail_model->selectorderitme_by_vandor($loginUserId);
		$this->load->view('front/vendor/cancellation-report',$data);
	}

    function store_list()
    {
       $data['storedata'] = $this->store_model->selectAllActiveStore();
	   $this->load->view('front/vendor/all-store-list',$data);
    }
	
	function terms_condition()
    {
       $data['pagedata'] = $this->cms_model->selectCmsById(14);
	   $this->load->view('front/vendor/cms',$data);
    }
	
	public function edit_order_report()
    {
		$args=func_get_args();  
		if(isset($_POST['updatedata']))
		{
			$odata['awb_id'] = $this->input->post('awb_id');
			$odata['courier_company'] = $this->input->post('courier_company');
			$odata['status'] = $this->input->post('status');
			$odata['statusdata'] = date('Y-m-d');
			$this->orderdetail_model->update_order($args[0],$odata);
			$this->session->set_flashdata('message','<div class="alert alert-success">order has been successfully updated.</div>');
			redirect('vendor/order_report');
		}	
		
		$data['order']= $this->shoppingdetail_model->selectorder($args[0]);
		$this->load->view('front/vendor/edit-order-report',$data);
	}	
	
	public function addpincode()
	{
		$pincode = $this->input->post('pincode');
		if(!empty($pincode))
		{	
			$pindata = $this->pincode_model->checkpincodeby_pincode($pincode);
			if(count($pindata)==0)
			{	
				$data['pincode'] = $this->input->post('pincode');
				$data['day'] = 5; //$this->input->post('day');
				$data['uid'] = $this->input->post('uid');
				$data['days'] = $this->input->post('days');
				
				$adddata = $this->pincode_model->insert($data);
                                $last_id = $this->db->insert_id();

                                echo "<label><input type='checkbox' name='pincode_ids[]' value='".$last_id."'>".$pincode."<label> ";   
			}
                        else
                        {
                            echo '0';
                        }
        }
	}
	
	function invoice_listing()
	{
		$args = func_get_args(); 		
		$loginUserId = $this->user_model->getLoginUserVar('USER_ID');
		$data['orderdata'] = $this->orderdetail_model->selectorderitme_by_vandor($loginUserId);
		$this->load->view('front/vendor/invoice_listing',$data);
	}
	
	function invoice_details()
	{
		$args = func_get_args(); 		
		$loginUserId = $this->user_model->getLoginUserVar('USER_ID');
		//$data['orderdata'] = $this->orderdetail_model->selectorderitme_by_vandor($loginUserId);
		//$data['order'] = $this->user_model->getOrderByOrderId($args[0]);
		$data['items'] = $this->orderdetail_model->select_order_item($args[0]); //echo "<pre>";print_r($data['items']);die;
		$this->load->view('front/vendor/invoice-details',$data);
	}
	
	public function income()
	{
		$this->load->view('front/vendor/vendor_income');
	}
	
	public function distribute_vendor_money()
	{
		$order_list = $this->orderdetail_model->select_order_status_success();
		foreach($order_list as $order)
		{
			$items = $this->shoppingdetail_model->prodcutdetail($order->id); 
			foreach($items as $item)
			{
				if($item->vendor!='0')
				{	
					//echo $item->id.'<br>';
				}
			}
		}
		//print_r($order_list);
	}
	
	public function my_sell_payout()
	{
		$loginUserId = $this->user_model->getLoginUserVar('USER_ID');
		$data['income'] = $this->admin_model->get_vendor_payout_by_id($loginUserId);
		$this->load->view('front/vendor/my-sell-payout',$data);
	}
	
	public function my_deducation()
	{
		$loginUserId = $this->user_model->getLoginUserVar('USER_ID');
		$data['income'] = $this->admin_model->get_vendor_refund_by_id($loginUserId);
		$this->load->view('front/vendor/my-deducation',$data);
		
	}	
	
	public function final_payout()
	{
		$loginUserId = $this->user_model->getLoginUserVar('USER_ID');
		if(!empty($loginUserId)){
$data['final_payout'] = $this->db->query("select *, date as adate from tbl_all_income where vendor_id='".$loginUserId."' order by date DESC")->result();

		$this->load->view('front/vendor/my-final-payout',$data);
		}else{
			redirect(base_url()."user/loginuser");
		}
	
	}	
	
	// vendor helpdesk
	
	function order_product()
	{
		  $order_id = $_REQUEST['order_id'];
		
		$data['items'] = $this->shoppingdetail_model->prodcutdetail($order_id);
		$this->load->view('front/vendor/order_product',$data);
	}
	function get_order()
	{
		  $booking_id = $_REQUEST['booking_id'];
		
		$data['items'] = $this->vendor_model->getHelpdeskById($booking_id);
		
		$data['getcomment'] = $this->vendor_model->getcommentById($booking_id);
		
		$this->load->view('front/vendor/get_order',$data);
	} 
	function help_desk()
	{
		$loginUserId = $this->vendor_model->getLoginUserVar('USER_ID');
		$data['myorder']=$this->vendor_model->getHelpdeskByUserId($loginUserId);
		$data['myorder1']=$this->vendor_model->getOrderByUserId($loginUserId);	
		$data['Depart']=$this->vendor_model->getHelpDepart();
		
		$this->load->view('front/vendor/help_desk',$data);
	}
	
	function insert_help_desk()
	{
		$data=$this->input->post();
		
		$pid=explode("DI-",$data['product_id']); 
		$test=$this->vendor_model->select_vendor_by_productid($pid[1]);
		
		$data['myorder']=$this->vendor_model->insert_help_desk($data, $test[0]->id, $pid[1]);	
		
		$this->session->set_flashdata('sms','Your Query has been submitted!'); 	
		redirect('vendor/help_desk');
	}
	
	function comment_submit()
	{
		ob_start();
		$data=$this->input->post();
		
		
		$data['myorder']=$this->vendor_model->insert_comment($data);	
		$this->session->set_flashdata('sms','Your comment has been submitted!');
		redirect(base_url().'vendor/help_desk');
	}
	
	public function earnyourdiscount(){				

		$loginUser = $this->user_model->getLoginUserVar('USER_ID');

		$data['walletData'] = $this->user_model->getWalletStatementHistory($loginUser);
		$data['creditamounttotal'] ="0";
		$cdata  = $this->user_model->get_query(" select sum(credit_amount) as amount from tbl_walletstatement where user_id='".$loginUser."'");
		if(count($cdata)>0){
			$data['creditamounttotal'] =$cdata[0]->amount;
		}
		
		$data['creditamount'] ="0";
		$walletcurrent= $this->user_model->get_query("select credit_amount from tbl_walletstatement where user_id='".$loginUser."' and tranType='credit' order by invID DESC limit 1");
		
		if(count($walletcurrent)>0){
			$data['creditamount'] =$walletcurrent[0]->credit_amount;
		}
		
		$this->load->view("front/earnyourdiscount",$data);
	}
	
	
	public function getemail($id){
	
	return $this->db->get_where("table_user",["id"=>$id])->row();
	} 
	
	
	public function getwalletbalance($id=''){
	
	if($id=="1"){
	$loginUser = $this->user_model->getLoginUserVar('USER_ID');
	$amount = $this->input->post('amount');
	$userid= $this->input->post('userid');
	$famount= ($this->input->post('famount')-$this->input->post('amount'));
	$url= "http://publicfunda.com/index.php/apicontroler/transferamount/".$amount."/".$userid."/".$famount;
	$data = file_get_contents($url);
	$data = json_decode($data);
	if($data->msg='success'){
$indata['neftNo']="N/a";
$indata['statementId']=uniqid();
$indata['user_id']=$loginUser ;
$indata['another_user']="";
$indata['debit_amount']="";
$indata['credit_amount']=$amount ;
$indata['finalAmount']="";
$indata['information']="Discount Value Received from Public Funda";
$indata['trDate']=date("y-m-d");
$indata['trTime']=time();
$indata['tranType']="credit";
$indata['status']="complete";
$this->vendor_model->insert("tbl_walletstatement",$indata);
//print_r($indata);
echo json_encode(array("tramount"=>$amount,"famount"=>$famount));
	}

	}else{
	$email = $_POST['email'];
	$password = $_POST['password'];
	
$url= "http://publicfunda.com/index.php/apicontroler/getuserandwaller?email=".$email."&password=".$password;
$data = file_get_contents($url);
 print_r($data);

	}
	}
public function sendotp(){
$email = $this->input->post("email");
$otp = rand(1000,9999);
$subject = "Public Funda wallet OTP (Do not Reply, it is a system generated Mail)";
$msg = "Dear Sir/Madam, <br> <br> Here is your One Time Password (OTP) : ".$otp."<br><br> Please do not share your OTP and E-mail ID Password with anyone, It result to theft of your Discount Idea Value or Other kind misuse of your Public Funda Wallet and Discount Idea Wallet.<br> No Complaint will be entertained related to your wallet as it is a free facility to you. <br><br><br> Thanks and Regards Public funda Team. ";

$data = $this->db->query("delete from otp where email='".$email."'");
$indata['email']=$email;
$indata['otp']=$otp;
$indata['sdate']=date('Y-m-d');
$this->vendor_model->insert("otp",$indata);
$this->sendmail("info@publicfunda.com",$email,$subject,$msg);
echo "1";
}
	
public function verifyotp(){
$email = $this->input->post("email");
$otp = $this->input->post("otp");

$data = $this->db->query("select * from otp where email='".$email."' and otp='".$otp."'")->result();
//print_r($data ); die;
if(count($data)>0){
echo "1";

}else{
echo "0";
}

}	
	
	
	function sendmail($from, $to, $subject, $message)
	{
		//echo ' from: '.$from.' to: '.$to.' cc: '.$cc.' subject: '.$subject.' message: '.$message; die;
		$config = Array(
								'protocol' => 'smtp',
								'smtp_host' => 'ssl://cs7.hostgator.in',
								'smtp_port' => 465,
								'smtp_user' => 'info@discountidea.com',
								'smtp_pass' => 'vikas1234',
                                 'mailtype'  => 'html',
                                'newline' =>"\r\n"
							);
            
             $this->email->initialize($config);
            $this->email->from($from);
            //$to= 'indiandiscountidea@gmail.com';
            $this->email->to($to);
            $this->email->subject($subject);
            
			
            $this->email->message($message);
			$this->email->send();
	}
	
	
}
 


?>