<?php

class Add_pages extends CI_controller

{

function __construct() {
        parent::__construct();
        $this->load->model("Pagesmodel");
    }
	

		function index()

		{

			$data['listingmeta']=$this->Pagesmodel->selectallpage();

			$this->load->view('admin/pages/list',$data);

		}

	
	function addpage()

	{
		

		if(isset($_POST['addpage']))

		{
			$data['title']=$this->input->post('title');
$data['desc']=$this->input->post('content');
$data['created_by']=$this->session->userdata('admin_id');
$data['url']=strtolower($this->input->post('url'));
$data['created_date']=date('Y-m-d H:i:s');

			if($_FILES['fimage']!=''){
				 $config['upload_path']          = 'pageimage/';
                $config['allowed_types']        = 'gif|jpg|png';
                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('fimage'))
                {
                       echo $this->upload->display_errors(); die;
                }
                else
                {
                        $data['featured_image']=$config['upload_path'].$this->upload->data()['file_name'];
                }
			}
			$id= $this->Pagesmodel->insertdata($data);
			$urldata = "$"."route['".$data['url']."'] = 'pages/mypage/".$id."';";

			file_put_contents("route.php","\n".$urldata."\n",FILE_APPEND);
			
			redirect('index.php/add_pages');

			

		}

		

		$this->load->view('admin/pages/add');   

		

	}

	function editpage()

	{

		$args=func_get_args();

		

		if(isset($_POST['edit']))

		{

			$data['title']=$this->input->post('title');
$data['desc']=$this->input->post('content');
$data['created_by']=$this->session->userdata('admin_id');
//$data['url']=strtolower($this->input->post('url'));
$data['modify_date']=date('Y-m-d H:i:s');

			if($_FILES['fimage']!='' && $_FILES['fimage']['name']!=''){
				 $config['upload_path']          = 'pageimage/';
                $config['allowed_types']        = 'gif|jpg|png';
                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('fimage'))
                {
                       echo $this->upload->display_errors(); die;
                }
                else
                {
                        $data['featured_image']=$config['upload_path'].$this->upload->data()['file_name'];
                }
			}
			

			$this->Pagesmodel->updatemetabyid($args[0],$data);

			$this->session->set_flashdata('message','<div class="alert alert-success">Meta Tag Update successsully</div>');

			redirect('index.php/add_pages');

			

			

		}

		

		

		$data['editmetabyid']=$this->Pagesmodel->selectclintpagebyid($args[0]);

		$this->load->view('admin/pages/edit',$data);

		

	}

	function deletemeta($id)

	{

		$this->db->where('id',$id);

		$this->db->delete('tbl_pages');

		

		$this->session->set_flashdata('message','<div class="alert alert-success">Page Delete successsully</div>');

			redirect('index.php/add_pages');

		

		

	}

	

}



?>