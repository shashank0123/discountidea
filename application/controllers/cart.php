<?php 
error_reporting(1);
class cart extends CI_Controller
{
	function index()
	{
		

		if(isset($_POST['applycounponcode']))
        {
			$code = $this->input->post('coupon_code');
			if(!empty($code))
			{
				$counpondata = $this->offer_model->selectCouponCode($code);
				if(count($counpondata)==1)
				{					
					$code_data=array('coupon' => array($counpondata[0]->couponcode,$counpondata[0]->discount));
					$this->session->set_userdata($code_data);
					
					$this->session->set_flashdata("cmessage","<div class='alert alert-success'>coupon code successfully apply.</div>");
					redirect('index.php/cart#couponcode1');
				}
				else
				{
					$this->session->set_flashdata("cmessage","<div class='alert alert-danger'>Invalid coupon code.</div>");
					redirect('index.php/cart#couponcode1');
				}
			}
			else
			{
				$this->session->set_flashdata("cmessage","<div class='alert alert-danger'>Please enter coupon code.</div>");
				redirect('index.php/cart#couponcode1');
			}	
		}
		
		$data['cart_data'] = $this->cart->contents();
		$this->load->view('front/cart',$data);
	}
	
	function addtocart()
	{
		
		if(isset($_POST['addtocartbutton']))
		{	
			$product_id = $this->input->post('pid');	
			$qty = $this->input->post('qty');
			if(isset($_POST['size_id'])){ $size = $this->input->post('size_id'); }else{ $size=0; }	
            if(isset($_POST['color_id'])){ $color = $this->input->post('color_id'); }else{ $color=0; }	
			$product = $this->product_model->selectProductById($product_id);
			$product_qty = 1;//$product[0]->product_stock;
			if($product_qty>=$qty)
			{	
				if($product[0]->spacel_price!="")
				{
					$price = $product[0]->spacel_price;
				}
				else
				{
					$price = $product[0]->price;
				}
				unset($_POST['pid']);
				unset($_POST['qty']);
				unset($_POST['pincode']);
				unset($_POST['pro_pin']);
				unset($_POST['addtocartbutton']);
				$data = array('id'=> $product[0]->id, 'qty'=> $qty, 'options'=> $_POST, 'price'=> $price, 'name' => $product[0]->name,'vendor'=>$product[0]->vender_type);
				$this->cart->insert($data);
				$this->session->set_flashdata("message","<div class='alert alert-success'><strong>".$product[0]->name."</strong> successfully added to your cart.</div>");
				redirect('cart/');
			}
			else
			{
				$this->session->set_flashdata("stockmessage","<div class='alert alert-danger'><strong></strong> Only ".$product_qty." Quantity in stock.</div>");
				redirect($_SERVER['HTTP_REFERER'].'#prostock');
			}	
			
		}
	}

	function addtocart_ajax()
	{
		

		error_reporting(0);
		if(isset($_POST['product_id']))
		{	
			$product_id = $_POST['product_id'];
			$qty = 1;
			$product = $this->product_model->selectProductById($product_id);
			if($product[0]->spacel_price!="")
			{
				$price = $product[0]->spacel_price;
			}
			else
			{
				$price = $product[0]->price;
			}			
			$cart_data = array('id'=> $product[0]->id, 'qty'=> $qty, 'options'=>0, 'price'=> $price, 'name' => $product[0]->name,);
			$this->cart->insert($cart_data);
		}	
		$this->load->view('front/layout/add-to-cart-ajax');
		//print_r($this->cart->contents());
	}
		
	function remove($id)
	{
		$args = func_get_args($id);
		$data= array('rowid' =>$args[0], 'qty' =>0);
		$this->cart->update($data);
		$this->session->set_flashdata("message","<div class='alert alert-success'>item successfully removed to your cart.</div>");
		redirect('cart/');
	}

	function update()
	{
		if(isset($_POST['updatecart']))
		{
			if(count($_POST['rowid'])>0)
			{
				foreach($_POST['rowid'] as $key => $value)
				{
					if($_POST['qty'][$key]!="0")
					{	
						$data= array('rowid' =>$_POST['rowid'][$key], 'qty' =>$_POST['qty'][$key]);
						$this->cart->update($data);
						$this->session->set_flashdata("message","<div class='alert alert-success'>Cart has been successfully updated.</div>");
					}	
				}	
			}
			redirect('cart/');
		}
		else
		{
			redirect('');
		}		
	}
			
	function emptycart($data="")
	{
		$this->session->unset_userdata('SUBSIDOLD');
		$this->session->unset_userdata('coupon');
		$this->cart->destroy();
		redirect('cart/');
	}
	
	function getcartitempincode()
	{
		$this->load->model('pincode_model');
		$cart_data = $this->cart->contents();
		
		//	print_r($cart_data); die();
		$available_pins = "";
		foreach($cart_data as $items)
		{			
			$pid = $items['id'];
			//print($pid); die();
			$product = $this->product_model->selectProductById($pid);
			
			//print_r($product); die();
			if(!empty($product[0]->pincode_ids))
			{				
				$a =	$product[0]->pincode_ids;
				$a = json_decode($a);
				foreach($a as $row){
					//print_r($row);
					$available_pins .= $row->id.",";
				}
				//print_r($available_pins);

				//die();
				//$available_pins .= $product[0]->pincode_ids;
				
			}				
		}
		if($available_pins!="")
		{	
			return explode(",",$available_pins);
		}
		else
		{
			return array();
		}	
	}

	function checkpincodeoncheckout($pincode)
	{
		$this->load->model('pincode_model');
		$vailable_pins = $this->getcartitempincode();
		$data = $this->pincode_model->checkpincode($vailable_pins,$pincode); //echo "<pre>";print_r($data);die;
		if(count($data)>0)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	
	function checkout()
	{
	 	$prodata = $this->session->userdata("discountvalue"); 
	 	$prodataon=array();
	 	if(count($prodata)>0 && !empty($prodata)){
	 		foreach($prodata  as $key=>$val){
	 			$prodataon[$val[0]]=$val[1];
	 		}
	 	}
	 	//print_r($prodataon);
	 	//die;
	 	//$this->session->set_userdata("discountvalue",$prodataon); 
		$user_id = $this->user_model->getLoginUserVar('USER_ID');
		$cart_data = $this->cart->contents();
		
		$coupon_data = $this->session->userdata('coupon');
		if(!empty($coupon_data) && count($coupon_data)>0)
		{
			$coupon_title = $coupon_data[0];
			$coupon_discount = $coupon_data[1];
		}
		else
		{
			$coupon_title = 0;
			$coupon_discount = 0;
		}
		
		if(isset($_POST['checkout'])) 
		{
			$pincode_data = $this->input->post('pincodes');
			$pin_data = $this->checkpincodeoncheckout($pincode_data);
			if($pin_data==0)
			{
				$this->session->set_flashdata("message","<div class='alert alert-danger'>Any of the following product is not available in this Pin code.</div>");
				redirect('cart/checkout?pincode=error');
			}
			
			$order['user_id'] 			= $this->session->userdata('id');
			$order['payment_method']	= $this->input->post('paymentmethod');
			$order['ord_date']			= date('Y-m-d H:i:s');
			$order['ord_time']			= time();
			$order['coupon_title']		= $coupon_title;
			$order['coupon_discount']	= $coupon_discount;
			$order['total_amount']		= $this->cart->total();
			$order['ip_address']		= $this->input->post('ipaddress');
			$order 						= $this->checkout_model->insertorder($order);
			// var_dump($order);
			// die();
			$ordr_id 					= $order;
			$payamount = 0;
			//$ordr_id=1;
			if(!empty($ordr_id))
			{
				$payMode = $this->checkout_model->insertpaymenttype(array('order_id'=>$ordr_id,'payment_method'=>$this->input->post('paymentmethod')));
				foreach($cart_data as $items)
				{
					$prodDetail			= $this->checkout_model->getProductDetail($items['id']); 
					// echo "<pre>";print_r($prodDetail);die;
					$discountval 			= count($prodataon)>0?$prodataon[$items['id']]:"0";
					$item['order_id']		= $ordr_id;
					$item['pro_id']			= $items['id'];
					$item['user_id']		= $this->session->userdata('id');
					$item['pro_name']		= $items['name'];
					$item['gst_percentage']	= $prodDetail->gst_percentage;
					$item['unit_price']		= $items['price'];  
					$item['total_amount']	= $items['subtotal'];
					$item['discountvalue']	= $discountval;
					$item['finaltopay']		= $items['subtotal']-$discountval ;
					$qty = $item['qty']		= $items['qty'];  
					$item['vendor']			= $items['vendor'];
					$item['options']		= $this->get_option_httml($items['options']);
					$item['order_date']		= date('Y-m-d');
					//print_r($item);
					$this->checkout_model->inseritem($item);

					$sizeID = ($items['options']['customoption_7']) ? ($items['options']['customoption_7']) : 0;
					//echo "<pre>";print_r("UPDATE tbl_product_size_stock SET stock_quantity=stock_quantity - $qty where product_id='".$items['id']."' AND size_filter_id='".$sizeID."'");die;
					$this->db->query("UPDATE tbl_product_size_stock SET stock_quantity=stock_quantity - $qty where product_id='".$items['id']."' AND size_filter_id='".$sizeID."'");

					if(count($prodata)>0 && !empty($prodata)){
						$indatawallet['neftNo']="";
						$indatawallet['statementId']="";
						$indatawallet['user_id']=$item['user_id'];
						$indatawallet['product_id']=$items['id'];
						$indatawallet['another_user']="";
						$indatawallet['debit_amount']=$discountval;
						$indatawallet['credit_amount']="";
						$indatawallet['finalAmount']="";
						$indatawallet['information']="Product Purchase";
						$indatawallet['trDate']=date("Y-m-d H:i:s");
						$indatawallet['trTime']=time();
						$indatawallet['tranType']="debit";
						$indatawallet['status']="Paid";
						$this->checkout_model->insertdata("tbl_walletstatement",$indatawallet);	
						// print_r($indatawallet);die;
					}					

					//$this->db->query("UPDATE tbl_product SET product_stock=product_stock - $qty where id='".$items['id']."'");
					 $payamount+=$item['finaltopay'];
				}
				
				$data['ord_id']=$ordr_id;
				$data['user_id']=$this->session->userdata('id');
				$data['country']=$this->input->post('countryb');
				$data['firstname']=$this->input->post('firstnameb');
				$data['lastname']=$this->input->post('lastnameb');
				$data['address']=$this->input->post('addb');
				$data['city']=$this->input->post('cityb');
				$data['state']=$this->input->post('stateb');
				$data['pincode']=$this->input->post('pincodeb');
				$data['email']=$this->input->post('emailb');
				$data['mobile']=$this->input->post('phoneb');
				$data['add_date']=date('Y-m-d H:i:s');
				$billing=$this->checkout_model->insert_data($data);
				$id=$this->db->insert_id();
				// echo $id; die;
				if(!empty($id))
				{
					$data1['ord_id']=$ordr_id;
					$data1['user_id']=$this->session->userdata('id');
					$data1['country']=$this->input->post('countrys');
					$data1['firstname']=$this->input->post('firstnames');
					$data1['lastname']=$this->input->post('lastnames');
					$data1['email']=$this->input->post('emails');
					$data1['address']=$this->input->post('adds');
					$data1['city']=$this->input->post('citys');
					$data1['state']=$this->input->post('states');
					$data1['pincode']=$this->input->post('pincodes');
					$data1['add_date']=date('Y-m-d H:i:s');
					$this->checkout_model->insertshipping($data1);
				}
                
                $this->send_order_mail($ordr_id);
				$this->session->unset_userdata("discountvalue");
			}
			$this->emptycartoncheckoutpage('',$ordr_id,$payamount,$data['firstname'],$data['email'],$data['mobile']);			
		}
		
		$data['LOGIN_USER'] = $this->user_model->selectuserby_id($user_id);
		$data['cart_data'] = $cart_data;
		$this->load->view('front/checkout',$data);
	} 
	
	function emptycartoncheckoutpage($data="",$order_id,$amount,$fname,$email,$mobile)
	{
		//echo "<pre>";print_r($this->input->post());die;
		if($this->input->post("paymentmethod")=='payubiz'){
			$this->session->set_userdata("order_id",$order_id);
			//$PAYU_BASE_URL = "https://test.payu.in";
			$PAYU_BASE_URL = "https://secure.payu.in/_payment";
			$key ='2qxDnu';
			$SALT='3wWE5TWB';
			$posted = array(
				"key"=>$key,
				"txnid"=>uniqid(),
				"amount"=>$amount,
				"firstname"=>$fname,
				"email"=>$email,
				"phone"=>$mobile,
				"productinfo"=>'payubiz',
				"surl"=>base_url().'index.php/order/paymentstatus',
				"furl"=>base_url().'index.php/order/paymentstatus'
				); //echo "<pre>";print_r($posted);die;
			$hashSequence ="key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
			if(empty($posted['hash']) && sizeof($posted) > 0) {
				if(empty($posted['key'])
					|| empty($posted['txnid'])
					|| empty($posted['amount'])
					|| empty($posted['firstname'])
					|| empty($posted['email'])
					|| empty($posted['phone'])
					|| empty($posted['productinfo'])
					|| empty($posted['surl'])
					|| empty($posted['furl']))
				{
					$formError = 1;
				}
				else
				{
					$hashVarsSeq = explode('|', $hashSequence);
					$hash_string = '';
					foreach($hashVarsSeq as $hash_var) {
						$hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
						$hash_string .= '|';
					}
					$hash_string .= $SALT;
					$hash = strtolower(hash('sha512', $hash_string));
					$action = $PAYU_BASE_URL . '/_payment';
				}
			} elseif(!empty($posted['hash'])) {
				$hash = $posted['hash'];
				$action = $PAYU_BASE_URL . '/_payment'; 
			} 
			$html ='<form action="'.$action.'" method="post" id="pauform" >
			<input type="hidden" name="key" value="'.$posted["key"].'">
			<input type="hidden" name="txnid" value="'.$posted["txnid"].'">
			<input type="hidden" name="amount" value="'.$posted["amount"].'">
			<input type="hidden" name="firstname" value="'.$posted["firstname"].'">
			<input type="hidden" name="email" value="'.$posted["email"].'">
			<input type="hidden" name="phone" value="'.$posted["phone"].'">
			<input type="hidden" name="productinfo" value="'.$posted["productinfo"].'">
			<input type="hidden" name="surl" value="'.$posted["surl"].'">
			<input type="hidden" name="furl" value="'.$posted["furl"].'">
			<input type="hidden" name="hash" value="'.$hash.'">
			</form>'; //echo "<pre>";print_r($html);die;
			if($hash_string!="")
			{
				echo $html;
			    echo "<script> document.getElementById('pauform').submit();</script>";
					//redirect('index.php/order/success');
			}else{
				echo $html;
			}
		}else{
			redirect('index.php/order/paymentstatus');
		}
	}
	
	function get_option_httml($option_array)
	{
		$html = "";
		if(!empty($option_array) && count($option_array)>0)
		{
			foreach($option_array as $option => $key)
			{
				$html .= $this->filters_model->getSelectedOption($key);
			}
		}
		
		return $html;		
	}	
	
	function send_order_mail($orderID)
	{
		$loginUserId =  $this->session->userdata('USER_ID');
		if($loginUserId==""){ redirect('user/login'); }
		$user = $this->user_model->selectuserby_id($loginUserId);
        $shipping =$this->shoppingdetail_model->billingdetail($orderID);
		//$cart_data = $this->cart->contents();
		$cart_data = $this->db->query("select * from tbl_item where order_id='".$orderID."'")->result_array();
		//print_r($cart_data); die;
		$item_data = $this->orderdetail_model->select_order_item($orderID);	       
		 
		$pro_id =0;
		foreach ($cart_data as $item){	$pro_id = $item['pro_id'];	}	
		$vandor = $this->user_model->select_vendor_by_productid($pro_id);
		
		//print_r($vandor); die;
		
		$curent_date = date('F d, Y h:m:s A');
		$html_body = "<div style='background:#f6f6f6;font-family:Verdana,Arial,Helvetica,sans-serif;font-size:12px;margin:0;padding:0'>
						<table cellspacing='0' cellpadding='0' border='0' width='100%'>
							<tbody>
								<tr>
									<td align='center' valign='top' style='padding:20px 0 20px 0'>
										<table bgcolor='#FFFFFF' cellspacing='0' cellpadding='10' border='0' width='650' style='border:1px solid #e0e0e0'>
											<tbody><tr style='background: green;'>
												<td valign='top'><a href='https://www.discountidea.com/' target='_blank'><img src='".base_url()."assets/front/images/stick-logo.png' alt='discountidea.com' style='margin-bottom:10px' border='0'></a></td>
											</tr>
											<tr>
												<td valign='top'>
													<h1 style='font-size:22px;font-weight:normal;line-height:22px;margin:0 0 11px 0'>Hello, ".$user[0]->fname.' '.$user[0]->lname."</h1>
													<p style='font-size:12px;line-height:16px;margin:0'>
														Thank you for your <span class='il'>Shopping</span> from <a href='http://discountidea.com' target='_blank'>discountidea.com</a>.                        
														If you have any questions about your <span class='il'>order</span> please contact us at <a href='mailto:info@discountidea.com' style='color:#ff5200' target='_blank'>info@discountidea.com</a>.
													</p>
													<p style='font-size:12px;line-height:16px;margin:0'>Your <span class='il'>order</span> confirmation is below. Thank you again for your business.</p>
												</td>
											</tr>
											<tr>
												<td>
													<h2 style='font-size:18px;font-weight:normal;margin:0'>Your <span class='il'>Order</span> #".$orderID." <small>(placed on $curent_date)</small></h2>
												</td>
											</tr>
											<tr>
												<td>
													<table cellspacing='0' cellpadding='0' border='0' width='650'>
														<thead>
														<tr>
															<th align='left' width='325' bgcolor='#EAEAEA' style='font-size:13px;padding:5px 9px 6px 9px;line-height:1em'>Customer Information:</th>
															<th width='10'></th>
															<th align='left' width='325' bgcolor='#EAEAEA' style='font-size:13px;padding:5px 9px 6px 9px;line-height:1em'>Vendor Information:</th>
														</tr>
														</thead>
														<tbody>
														<tr>
															<td valign='top' style='font-size:12px;padding:7px 9px 9px 9px;border-left:1px solid #eaeaea;border-bottom:1px solid #eaeaea;border-right:1px solid #eaeaea'>
																<p style='margin: 2px 0px 3px 0px;'>".$shipping[0]->firstname."</p>
																<p style='margin: 2px 0px 3px 0px;'>".$shipping[0]->address."</p>
																<p style='margin: 2px 0px 3px 0px;'>".$shipping[0]->pincode.",&nbsp;&nbsp;".$shipping[0]->city.",&nbsp;&nbsp;".$shipping[0]->state."</p>
																<p style='margin: 2px 0px 3px 0px;'>".$shipping[0]->country."</p>
																<p style='margin: 2px 0px 3px 0px;'>".$shipping[0]->mobile."</p>
															</td>
															<td>&nbsp;</td>
															<td valign='top' style='font-size:12px;padding:7px 9px 9px 9px;border-left:1px solid #eaeaea;border-bottom:1px solid #eaeaea;border-right:1px solid #eaeaea'>
																<p style='margin: 2px 0px 3px 0px;'><b>Business Name: </b>".$vandor[0]->company_name."</p>
																<p style='margin: 2px 0px 3px 0px;'><b>Business Address : </b>".$vandor[0]->company_address."</p>
																<p style='margin: 2px 0px 3px 0px;'><b>Vendor Name : </b>".$vandor[0]->fname."</p>
															</td>
														</tr>
														</tbody>
													</table>
													<br>
													<table cellspacing='0' cellpadding='0' border='0' width='650' style='border:1px solid #eaeaea'>
														<thead>
															<tr>
																<th align='left' bgcolor='#EAEAEA' style='font-size:13px;padding:3px 9px'>Item</th>
																<th align='center' bgcolor='#EAEAEA' style='font-size:13px;padding:3px 9px'>Qty</th>
																<th align='left' bgcolor='#EAEAEA' style='font-size:13px;padding:3px 9px'>Gross Amount</th>
																<th align='right' bgcolor='#EAEAEA' style='font-size:13px;padding:3px 9px'>Discount</th>
																<th align='right' bgcolor='#EAEAEA' style='font-size:13px;padding:3px 9px'>Taxable Value</th>
																<th align='right' bgcolor='#EAEAEA' style='font-size:13px;padding:3px 9px'>IGST OR (CGST+SGST)</th>
																<th align='right' bgcolor='#EAEAEA' style='font-size:13px;padding:3px 9px'>Total</th>
															</tr>
														</thead>
														<tbody bgcolor='#F6F6F6'>";
															$row="";
															$subtotal = "";
															$disocunttotal = "";
															$finaltopay = "";

															$totalTaxable = 0;
														   	$totalTax = 0;
														   	$totalAmount = 0;

															foreach($cart_data as $items)
															{

																$unitPrice = $items['unit_price'];
																$discountValue = $items['discountvalue'];
																$taxableValue = $this->get_net_amount($items['total_amount'],$items['gst_percentage']);
																$tax = ($unitPrice-$taxableValue);
																$total = $items['finaltopay'];
														   		$totalTaxable += $taxableValue;
																$totalTax += $tax;
														   		$totalAmount += $total;

																$subtotal+=$items['total_amount'];
																$disocunttotal+=$items['discountvalue'];
																$finaltopay+=$items['finaltopay'];
																$row .="<tr><td align='left' valign='top' style='padding:3px 9px'>".$items['pro_name']."</td>
																<td align='center' valign='top' style='padding:3px 9px'>".$items['qty']."</td>
																<td align='left' valign='top' style='padding:3px 9px'>Rs ".$this->cart->format_number($unitPrice)."</td>
																<td align='right' valign='top' style='padding:3px 9px'><span>Rs ".$this->cart->format_number($discountValue)."</span></td>
																<td align='right' valign='top' style='padding:3px 9px'><span>Rs ".$this->cart->format_number($taxableValue)."</span></td>
																<td align='right' valign='top' style='padding:3px 9px'><span>Rs ".$this->cart->format_number($tax)."</span></td>
																<td align='right' valign='top' style='padding:3px 9px'><span>Rs ".$this->cart->format_number($total)."</span></td></tr>";
															}
															
																
										 	$html_body .=$row;
											$html_body .="
														</tbody>
														<tbody>
															<tr>
																<td colspan='5' align='right' style='padding:3px 9px'>Total</td>
																<td colspan='2' align='right' style='padding:3px 9px'><span>Rs ".$this->cart->format_number($totalTaxable)."</span></td>
															</tr>
															<tr>
																<td colspan='5' align='right' style='padding:3px 9px'><div>Tax</div></td>
																<td colspan='2' align='right' style='padding:3px 9px'><span>Rs ".$this->cart->format_number($totalTax)."</span></td>
															</tr>
															<tr>
																<td colspan='5' align='right' style='padding:3px 9px'><strong>Final to Pay</strong></td>
																<td colspan='2' align='right' style='padding:3px 9px'><strong><span>Rs ".$this->cart->format_number($totalAmount)."</span></strong></td> 
															</tr>
														</tbody>
													</table>
													<p style='font-size:12px;margin:0 0 10px 0'></p>
												</td>
											</tr>
											<tr>
												<td bgcolor='#EAEAEA' align='center' style='background:#eaeaea;text-align:center'><center><p style='font-size:12px;margin:0'>Thank you, <strong><a href='http://discountidea.com' target='_blank'>discountidea.com</a></strong></p></center></td>
											</tr>
										</tbody></table>
									</td>
								</tr>
							</tbody>
						</table>
					</div>";


		//echo $html_body;
		$to = $user[0]->email;
		$from = 'info@discountidea.com';
		/*$this->load->library('email');
		$this->email->set_newline("\r\n");
        $this->email->set_mailtype("html");
		$this->email->from($from , 'discountidea.com');
		$this->email->to($to);
		$this->email->subject('discountidea.com - New Order #'.$orderID);
		$this->email->message($html_body);
		$mail= $this->email->send();*/	

		$this->user_model->sendmail('info@discountidea.com', $to, 'rahulpal7623@yahoo.com', 'discountidea.com - New Order #'.$orderID, $html_body);
			
	}
	
	public function get_net_amount($amount,$tax)
	{
		return $amount/(1+($tax/100));
	}
	
	public function settosession()
	{
		$data = $_POST['data'];
		$discountvalue = array();
		foreach($data as $value){
		array_push($discountvalue, $value);
	}

	
	$this->session->set_userdata("discountvalue",$discountvalue);
	print_r($this->session->userdata("discountvalue"));
	}
} 
?>