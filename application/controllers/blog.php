<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Blog extends CI_Controller 

{

	

	public function myblog()

	{

		$loginUserId = $this->user_model->getLoginUserVar('USER_ID');

		$data['blod_data'] = $this->blog_model->select_blog_by_uid($loginUserId);

		$this->load->view('blog/my-blog',$data);

	}

	

	public function post_blog()
	{

		$args = func_get_args(); 		

		$loginUserId = $this->user_model->getLoginUserVar('USER_ID');

		if(isset($_POST['savedata']))
		{			
			if($_FILES['image']['name']!="")
			{
				$file = createUniqueFile($_FILES['image']['name']);
				$file_temp = $_FILES['image']['tmp_name'];
				$path = 'uploads/blog/'.$file;				
				move_uploaded_file($file_temp,$path);				
			}
			else
			{
				$file = "";
			}
			$data['image'] = $file;
			$data['cat_id'] = $this->input->post('cat_id');
			$data['uid'] = $loginUserId;
			$data['title'] = $this->input->post('title');
			$data['content'] = $this->input->post('content');	
			$data['status'] = 1;//$this->input->post('status');
			$data['create_date'] = date('Y-m-d');
			
			$this->blog_model->insert($data);			
			$this->session->set_flashdata('message','<div class="alert alert-success">blog has been successfully saved.</div>');
			redirect('index.php/vendor/manage_blog');			
		}		
		$catdata['CATEGORY'] = $this->blog_model->select_all_active_blogcat();	
		$this->load->view('blog/post-blog',$catdata);

	}

	

	public function edit_myblog()

	{

		$args = func_get_args(); 		

		$loginUserId = $this->user_model->getLoginUserVar('USER_ID');

		if(isset($_POST['savedata']))

		{			

			if($_FILES['image']['name']=="")

			{

				$file = $_POST['oldimage'];

			}

			else

			{

				$file = createUniqueFile($_FILES['image']['name']);

				$file_temp = $_FILES['image']['tmp_name'];

				$path = 'uploads/blog/'.$file;				

				move_uploaded_file($file_temp,$path);	

			}

			$data['image'] = $file;

			$data['cat_id'] = $this->input->post('cat_id');

			$data['uid'] = $loginUserId;

			$data['title'] = $this->input->post('title');

			$data['content'] = $this->input->post('content');	

			$data['status'] = 1;

			$data['create_date'] = date('Y-m-d');

			

			$this->blog_model->insert($data);			

			$this->session->set_flashdata('message','<div class="alert alert-success">blog has been successfully saved.</div>');

			redirect('index.php/blog/myblog');			

		}

		$catdata['CATEGORY'] = $this->blog_model->select_all_active_blogcat();

		$catdata['EDITBLOG']= $this->blog_model->select_blog_byid($args[0]);

		$this->load->view('blog/edit-blog',$catdata);

	}

	

	function delete_myblog()

	{

		$this->load->model('blog_model');

		$args=func_get_args();		

		$this->blog_model->delete_blog($args[0]);

		$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully deleted.</div>');

		redirect('index.php/blog/myblog');

	}

	

	public function index()

	{

		$this->load->view('blog/index');

	}

	function display()

	{

		$args=func_get_args();		

		$catdata['blogdata'] = $this->blog_model->select_blog_by_catid($args[0]);

		$this->load->view('blog/blog-listing',$catdata);

	}

	

	function details()

	{

		$args=func_get_args();		

		$catdata['blogdata'] = $this->blog_model->select_blog_byid($args[0]);

		$this->load->view('blog/blog-details',$catdata);

	}

	

	function listing()

	{		

	    $this->load->model('blog_model');

		$data['BLOGDATA']= $this->blog_model->select_all_blog();

		$this->load->view('admin/blog/listing',$data);

	}

	function add()

	{	

		$this->load->model('blog_model');

		if(isset($_POST['savedata']))

		{			

			if($_FILES['image']['name']!="")

			{

				$file = createUniqueFile($_FILES['image']['name']);

				$file_temp = $_FILES['image']['tmp_name'];

				$path = 'uploads/blog/'.$file;				

				move_uploaded_file($file_temp,$path);				

			}

			else

			{

				$file = "";

			}

			$data['image'] = $file;

			$data['cat_id'] = $this->input->post('cat_id');

			$data['title'] = $this->input->post('title');

			$data['content'] = $this->input->post('content');	

			$data['status'] = $this->input->post('status');

			$data['create_date'] = date('Y-m-d');

			

			$this->blog_model->insert($data);			

			$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully saved.</div>');

			redirect('index.php/blog/listing');			

		}		

		$catdata['CATEGORY'] = $this->blog_model->select_all_active_blogcat();

		$this->load->view('admin/blog/add',$catdata);

	}

	function edit()

	{	

		$this->load->model('blog_model');

		$args=func_get_args();

		if(isset($_POST['updatedata']))

		{

			if($_FILES['image']['name']=="")

			{

				$file = $_POST['oldimage'];

			}

			else

			{

				$file = createUniqueFile($_FILES['image']['name']);

				$file_temp = $_FILES['image']['tmp_name'];

				$path = 'uploads/blog/'.$file;				

				move_uploaded_file($file_temp,$path);	

			}

			$data['image'] = $file;

			$data['cat_id'] = $this->input->post('cat_id');

			$data['title'] = $this->input->post('title');

			$data['content'] = $this->input->post('content');	

			$data['status'] = $this->input->post('status');

			//$data['create_date'] = date('Y-m-d');

			

			$this->blog_model->update($args[0],$data);

			$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully updated.</div>');

			redirect('index.php/blog/listing');

		}

		$catdata['EDITBLOG']= $this->blog_model->select_blog_byid($args[0]);

		$catdata['CATEGORY'] = $this->blog_model->select_all_active_blogcat();

		$this->load->view('admin/blog/edit',$catdata);

	}

	

	function delete()

	{

		$this->load->model('blog_model');

		$args=func_get_args();		

		$this->blog_model->delete_blog($args[0]);

		$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully deleted.</div>');

		redirect('index.php/blog/listing');

	}



}