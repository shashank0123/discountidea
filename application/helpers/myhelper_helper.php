<?php

function createUniqueFile($file)

{

	$data = time().'_'.strtolower($file);

	return $data;

}



function geturl($key,$content)

{

	return str_replace($key.'/',base_url(),$content);

}



function slugurl($title, $separator = '-')

{

    // convert String to Utf-8 Ascii

    $title = iconv(mb_detect_encoding($title, mb_detect_order(), true), "UTF-8", $title);

 

    // Convert all dashes/underscores into separator

    $flip = $separator == '-' ? '_' : '-';

 

    $title = preg_replace('!['.preg_quote($flip).']+!u', $separator, $title);

 

    // Remove all characters that are not the separator, letters, numbers, or whitespace.

    $title = preg_replace('![^'.preg_quote($separator).'\pL\pN\s]+!u', '', mb_strtolower($title));

 

    // Replace all separator characters and whitespace by a single separator

    $title = preg_replace('!['.preg_quote($separator).'\s]+!u', $separator, $title);

 

    return trim($title, $separator);

}



function encodeurlval($val)

{

	//return urlencode(base64_encode($val));
return $val;

}



function decodeurlval($val)

{

	//return base64_decode(urldecode($val));
return $val;

}

function getadminmodule()
{
	$array = array('homepagepopup/listing'=>'homepagepopup',
	'banner/listing'=>'banner',
	'categorycontroller/home_category'=>'categorycontroller',
	'blogcategory/cat_listing'=>'blogcategory',
	'blog/listing'=>'blog',
	'admin/edit_blogbanner'=>'edit_blogbanner',
	'user/listing'=>'user',
	'admin/userList'=>'userList',
	'admin/adduser'=>'adduser',
	'vendor/allstore'=>'allstore',
	'categorycontroller/listing'=>'categorycontroller',
	'adminproducts/listing'=>'adminproducts',
	'csv/index'=>'csv',
	'pincode/listing'=>'pincode',
	'filters/listing'=>'filters',
	'offer/listing'=>'offer',
	'faq/listing'=>'faq',
	'cms/listing'=>'cms',
	'newsletter/listing'=>'newsletter',
	'our_client/listing'=>'our client',
	'order/listing'=>'listing',
	'order/report'=>'report',
	'admin_meta_tag/index'=>'admin_meta_tag',
	'block/listing'=>'block',
	'admin/distribute_vendor_money'=>'distribute_vendor_money',
	'admin/seller_credit_settlment'=>'seller_credit_settlment',
	'admin/vandor_income'=>'vandor_income',
	'admin/vandor_return_deductions'=>'vandor_return_deductions',
	'admin/help_desk'=>'help_desk',
	'admin/dashboard'=>'dashboard',
	
	);
	
	return $array;
}

?>