<?php 

$admin_id = $this->session->userdata('admin_id'); 

if(empty($admin_id))

{

	redirect('index.php/admin');

}	

?>

<!DOCTYPE html>

<html lang="en">

<?php $this->load->view('admin/layout/head'); ?>

<body>



    <div id="wrapper">

        <!-- Navigation -->

        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">

            <!-- Brand and toggle get grouped for better mobile display -->

            <?php $this->load->view('admin/layout/header'); ?>

            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->

            <?php $this->load->view('admin/layout/left-menu'); ?>

            <!-- /.navbar-collapse -->

        </nav>



        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->

                <div class="row">

                    <div class="col-lg-12">

						<div class="table-responsive">

							<h1>Vendor Calculation Setting</h1>

                           <div class="col-md-12">
						   <form method="post" action="">
  <div class="form-group">
    <label for="commission">Commission:</label>
    <input type="text" class="form-control" name="commission" value="<?=count($all)>0?$all[0]->commission:''?>" id="commission">
  </div>
  <div class="form-group">
    <label for="admin_charge">Admin Charge:</label>
    <input type="text" class="form-control" name="admin_charge" value="<?=count($all)>0?$all[0]->admin_charge:''?>" id="admin_charge">
  </div>
  <div class="form-group">
    <label for="promotion_charge">Promotion Charge:</label>
    <input type="text" class="form-control" name="promotion_charge" value="<?=count($all)>0?$all[0]->promotion_charge:''?>" id="promotion_charge">
  </div>
  <div class="form-group">
    <label for="servicetax_gst">Servicetax GST:</label>
    <input type="text" class="form-control" name="servicetax_gst" value="<?=count($all)>0?$all[0]->servicetax_gst:''?>" id="servicetax_gst">
  </div>
  <div class="form-group">
    <label for="tsc">TCS:</label>
    <input type="text" class="form-control" name="tcs" value="<?=count($all)>0?$all[0]->tcs:''?>" id="tcs">
  </div>
  
  <button type="submit" class="btn btn-default">Submit</button>
</form>
						   </div>

                        </div>

                    </div>

                </div>

                <!-- /.row -->



            </div>

            <!-- /.container-fluid -->



        </div>

        <!-- /#page-wrapper -->



    </div>

    <!-- /#wrapper -->

<?php $this->load->view('admin/layout/footer-js'); ?>

</body>



</html>

