<?php 

$admin_id = $this->session->userdata('admin_id'); 

if(empty($admin_id))

{

	redirect('index.php/admin');

}	

?>

<!DOCTYPE html>

<html lang="en">

<?php $this->load->view('admin/layout/head'); ?>

<body>



    <div id="wrapper">

        <!-- Navigation -->

        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">

            <!-- Brand and toggle get grouped for better mobile display -->

            <?php $this->load->view('admin/layout/header'); ?>

            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->

            <?php $this->load->view('admin/layout/left-menu'); ?>

            <!-- /.navbar-collapse -->

        </nav>



        <div id="page-wrapper">



            <div class="container-fluid">



                <!-- Page Heading -->

                <div class="row">

                    <div class="col-lg-12">

                        <h1 class="page-header">Welcome to Helpdesk</h1>

                       <div class="my_order_list table-responsive" style="padding:3px;">

            <?php if(count($myorder)>0){ ?>

			<table class="table table-bordered table-hover">

				<thead>

					<tr>

						<th>S. No.</th>
						<th>Ticket ID</th>
						<th>User ID</th>
						<th>Order ID</th>
						<th>Product ID</th>
						<th>Vendor Name</th>
						<th>User Contact</th>
						<th>Vendor Contact</th>
						<th>Query Type</th>

						<th>Executive</th>

						<th>Date</th>

						<th>Status</th>
						<th></th>

					</tr>

				</thead>

				<tbody>

					<?php $i=1; foreach($myorder as $order){ ?>

					<?php $items = $this->user_model->getOrderItemsByOrderId($order->id); ?>

					<?php $user = $this->user_model->selectuserby_id($order->uid); ?>

					<tr>
						<td><?php echo $i; ?></td>
						<td>#<?php echo $order->id; ?></td>

						<td><?php echo $user[0]->fname.' '.$user[0]->lname; ?></td>

						

						
						<td><?php echo $order->order_product; ?></td>
						<td><?php echo $order->product_id; ?></td>
						<?php $vender_id = $this->user_model->selectuserby_id($order->vender_id); ?>
						<td><?php echo $vender_id[0]-> company_name ; ?></td>
						<td><?php echo $user[0]->mobile; ?></td>
						<td><?php echo $vender_id[0]->mobile; ?></td>
						<?php $select_depart = $this->user_model->select_depart($order->type); ?>
						<td><?php echo $select_depart[0]->title; ?></td>
						
						<td><?php echo $select_depart[0]->name; ?></td>
						

						<td><?php echo $order->queryTime; ?></td>
						<td><?php echo $order->status; ?></td>
						<td><button data-booking2="myBtn<?php echo $order->id ?>" id="myBtn2" class="myBtn3"><i class="fa fa-eye"></i> View</button><input type="hidden"  id="myBtn<?php echo $order->id ?>" value="<?php echo $order->id  ?>"></td>

					</tr>

					<?php } ?>

				</tbody>

			</table> 		

			<?php $i++; } ?> 		 		    								    				<div  id="myModal2" class="modal2">

                      
                          <div class="modal-content1">
                            <div class="modal-header1">
                              <span class="Close2">&times;</span>
                              <h2>Submit Your Query</h2>
                            </div>
                            <div class="modal-body1"  id="booking_val2"></div>
                          </div>

                    </div>	

	    </div>
  </div>

                    </div>

                </div>

                <!-- /.row -->



            </div>

            <!-- /.container-fluid -->



        </div>

        <!-- /#page-wrapper -->



    </div>

    <!-- /#wrapper -->

<?php $this->load->view('admin/layout/footer-js'); ?>





</body>



</html>

