 <?php 
$admin_id = $this->session->userdata('admin_id'); 
if(empty($admin_id))
{
	redirect('index.php/admin');
}	
?>
<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/layout/head'); ?>
<style>
.naseemdiv
{
	height: 210px;
    overflow: scroll;
    border: 2px solid rgba(34, 34, 34, 0.41);
    padding: 10px;
}
.naseemdiv label{padding: 3px;}
</style>
<body>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <?php $this->load->view('admin/layout/header'); ?>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <?php $this->load->view('admin/layout/left-menu'); ?>
            <!-- /.navbar-collapse -->
        </nav>

				
					 <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Add Product</h1>
						<div class="col-md-6 col-lg-12 col-xl-6">
						    <div class="panel panel-sign">
								<div class="panel-body">
									<form  enctype="multipart/form-data" method="POST">
									<table class="table table-bordered table-striped table-condensed" style="">	
									<input type="hidden" id="cat_pos" name="cat_pos" value="0">
									<tbody>
								    <tr>
										<td> Category</td>
										<td>
											<?php echo form_dropdown('category_id[0]',$listCat,set_value('category_id[0]'),'id="category_id0" required="required" class="form-control" onchange="get_subcat(this.value,0)"')?>
										</td>
									</tr>
									
									<tr id="vendorRow">
										<td> Vender Name</td>
										<td>
											<select  name="vendername" class="form-control">
											<option value="0">Select Vender</option>
											<?php 
												$query = $this->admin_model->selectvendername();												
												foreach ($query as $row)
												{
												echo "<option value='" . $row->id. "'>" . $row->fname.  "</option>";
												} 
											?>									
											</select>
										</td>
									</tr>
									
									
									<tr>
										<td>Product Name</td>
										<td>
		<input type="text" name="name"  class="form-control" autocomplete="off" required>
										</td>
									</tr>
								
									<tr>
										<td> Sort Description</td>
										<td>
<textarea name="sortdesc" value="" class="form-control" rows="3"></textarea>
										</td>
									</tr>
									<tr>
										<td> Description</td>
										<td>
<textarea name="description"  class="form-control" rows="4"></textarea>
										</td>
									</tr>
									<tr>
										<td>Price</td>
										<td>
<input type="text" name="price"  class="form-control" id="price" autocomplete="off" required>
										</td>
									</tr>
									<tr>
										<td>Spacel Price</td>
										<td>
<input type="text" name="spacel_price" id="spacel_price" class="form-control" autocomplete="off">
										</td>
									</tr>
									<tr>
										<td> Descount</td>
										<td>
<input type="text" name="descount"  class="form-control" autocomplete="off" onkeyup="return calculatespecialpeice(this.value);" >
										</td>
									</tr>
									<tr>
										<td>Releted Product</td>
										<td>
											<div class="naseemdiv">
											<?php 
											$query = $this->product_model->selectAllProduct();
											foreach ($query as $row){ ?>
												<label><input type="checkbox"  name="reletedproduct[]"  value="<?php echo $row->id;?>" /> <?php echo $row->name; ?></label><br>
											<?php } ?>
											</div>
										</td>
									</tr>
									<tr>
							<td>Pincode</td>
							<td>
								<div class="naseemdiv">
								<?php 
								$pincodequery = $this->pincode_model->selectAllPincode();								
								 ?>
								 //ajay script 
											           <section>
                <input class="magicsearch" id="basic" name="value" placeholder="search names...">
                <div id="daysp"></div>
            </section>
											<?php
											$newpin= array();
								foreach ($pincodequery as $row){ 


									if(in_array($row->id,$newpin)){
									?>

		                            <label><input type="checkbox"  name="pincode_ids[<?php echo $row->id;?>][id]"  value="<?php echo $row->id;?>"  /> <?php echo $row->pincode; ?></label>
		                            	
		                            <input type="text" name="pincode_ids[<?php echo $row->id;?>][days]" id="mypin" value="<?php if(!empty($newarray[$row->id])){echo $newarray[$row->id];} ?>" placeholder="">
	
								<?php } 
										}
									 ?>
								</div>
							</td>
						</tr>
									
									<tr>
										<td>Product Stock</td>
										<td>
                                            <input type="text" name="product_stock"  class="form-control" autocomplete="off">
										</td>
									</tr>
									
									
									<tr>
										<td>Meta Title</td>
										<td>
                                            <input type="text" name="producttitle"  class="form-control" autocomplete="off">
										</td>
									</tr>
									
									<tr>
										<td>Meta Keyword</td>
										<td>
                                            <input type="text" name="productkeyword"  class="form-control" autocomplete="off">
										</td>
									</tr>
									
									<tr>
										<td>Meta Description</td>
										<td>
                                            <input type="text" name="productdescription"  class="form-control" autocomplete="off">
										</td>
									</tr>
									

									<tr>
										<td>Status</td>
										
										<td><select  name="status" class="form-control">
										<option value="1">Active</option>
										<option value="0">Inactive</option>
										</select>
										</td>
									</tr>
									
																	
									<tr>
			<td colspan="2"><button type="submit" class="btn btn-info" name="add_product">Submit</button></td>
									</tr>
								</tbody>
							</table>

						</form>
				</div>
				</div>
						</div>
					<!-- end: page -->
			</div>

 </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
	 </div>
	 
    <!-- /#wrapper -->
<?php $this->load->view('admin/layout/footer-js'); ?>
<script>
function calculatespecialpeice(discount)
{
	var spacel_price = document.getElementById('spacel_price');
	var price = document.getElementById('price').value;
	if(price>0  && discount>0)
	{
		var discount_amount = price * (discount/100);
		spacel_price.value = price - discount_amount 
	}	
}
</script>
<link href="<?php echo base_url('assets/admin');?>/css/jquery.magicsearch.css">

<script  src="http://code.jquery.com/jquery-2.2.3.min.js"></script>
<script src="<?php echo base_url('assets/admin'); ?>/js/jquery.magicsearch.js"></script>
    <script>
        $(function() {
        	$.ajax({ method: "POST", url: "http://www.discountidea.com/index.php/adminproducts/pincode_ajax"
			}).done(function( msg ) {
			    //alert( "Data Saved: " + msg );
			    var dataSource = msg;
			    //console.log(dataSource);
			    $('#basic').magicsearch({
                dataSource: dataSource,
                fields: ['pincode'],
                id: 'id',
                format: '%pincode%',
                multiple: true,
                multiField: 'pincode',
                multiStyle: {
                    space: 5,
                    width: 80
                },
                success: function($input, data) {
                	console.log(data);
                	$("#daysp").append('<div class="form-group" id="autofill_'+data['id']+'"><label><input type="checkbox"  name="pincode_ids['+data['id']+'][id]"  value="'+data['id']+'"/>'+data['pincode']+'</label><input type="text" name="pincode_ids['+data['id']+'][days]" id="mypin" value="" placeholder=""></div>'); //add input box
      				return true;
  		},
  		afterDelete: function($input, data) {
  			  $("#autofill_"+data['id']+"").remove();
		      return true;		
		}

            });
			  });
           /* var dataSource = [
                {id: 1, pincode: 'Tim', days: 'Cook'},
                {id: 2, pincode: 'Eric', days: 'Baker'},
                {id: 3, pincode: 'Victor', days: 'Brown'},
                {id: 4, pincode: 'Lisa', days: 'White'},
                {id: 5, pincode: 'Oliver', days: 'Bull'},
                {id: 6, pincode: 'Zade', days: 'Stock'},
                {id: 7, pincode: 'David', days: 'Reed'},
                {id: 8, pincode: 'George', days: 'Hand'},
                {id: 9, pincode: 'Tony', days: 'Well'},
                {id: 10, pincode: 'Bruce', days: 'Wayne'},
            ];
            */
            
             

        });

        function get_subcat(mCat,nextcat=0)
		{
			var incrVal = $('#cat_pos').val();
			var lastr = incrVal;
			incrVal++;
			$('#cat_pos').val(incrVal);

			for(r=incrVal;r>nextcat;r--){
				$('#catrow_'+r).remove(); console.log($('#catrow_'+r).remove());
			}

			if(mCat>0) {
				$.ajax({
					type: "POST",
					url: "http://www.discountidea.com/index.php/categorycontroller/getSubCategory/"+mCat+'/'+incrVal,
					success: function(datas) {
			        	$('#vendorRow').before(datas);
					},
				});
			}
		}
        function get_subcat_limit(mCat,nextcat)
        {
			$('#root'+nextcat+' > option').remove();
			$.ajax({
				type: "POST",
				url: "http://www.discountidea.com/index.php/categorycontroller/getSubCategory/"+mCat,
				success: function(datas) {
                	console.log(datas);
                	$.each(datas,function(optVal,optName) {
						var opt = $('<option />');
						opt.val(optVal);
						opt.text(optName);							
						$('#root'+nextcat).append(opt);
					});
  				},
			});
		}
    </script>
</body>

</html>