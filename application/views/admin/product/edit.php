<?php 
$admin_id = $this->session->userdata('admin_id'); 
if(empty($admin_id))
{
	redirect('index.php/admin');
}	
?>
<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/layout/head'); ?>
<style>
.naseemdiv
{
	height: 210px;
    overflow: scroll;
    border: 2px solid rgba(34, 34, 34, 0.41);
    padding: 10px;
}
.naseemdiv label{padding: 3px;}
.naseemdiv ul {padding-left:5px; list-style:none;}
</style>
<body>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <?php $this->load->view('admin/layout/header'); ?>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <?php $this->load->view('admin/layout/left-menu'); ?>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Edit Product</h1>
						<div class="col-md-6 col-lg-12 col-xl-6">
						 <div class="panel panel-sign">
					
					<div class="panel-body">
 
					<form enctype="multipart/form-data"  action="" method="POST">
						<?php //print_r($EDITPRODUCT);?>
						<table class="table table-bordered table-striped table-condensed" style="">	
						<input type="hidden" id="cat_pos" name="cat_pos" value="<?php echo (count($cats)-1);?>">
						<tbody>
						
						<?php
						if(count($cats)>0) {
							foreach($cats as $key=>$cat) {
								$lab = ($key==0) ? 'Category' : 'Sub Category';
								$req = ($key==0) ? 'required="required"' : '';
						?>
						<tr id="catrow_<?php echo $key;?>">
							<td><?php echo $lab; ?></td>
							<td>
								<?php echo form_dropdown("category_id[$key]",$cat['lists'],$cat['selected'],'id="category_id'.$key.'" '.$req.' class="form-control" onchange="get_subcat(this.value,'.$key.')"')?>
							</td>
						</tr>
						<?php } } ?>

						<tr id="vendorRow">
							 <td>Vendor Name</td>
							<td>
								<select  name="vendername" class="form-control">
								<option value="0">select Vender</option>
								<?php 
								$query = $this->admin_model->selectvendername();	
								foreach ($query as $row)
								{
								?>
									<option <?php echo ($row->id==$EDITPRODUCT[0]->vender_type)?"selected" :"" ?> value="<?php echo $row->id; ?>" > 
										<?php echo $row->fname.' '.$row->lname; ?>
									</option>
								<?php
								} 
								?>
								</select>
							</td>
						</tr>
						
						<tr>
							<td>Product Name</td>
							<td>
								<input type="text" name="name" value="<?php echo  $EDITPRODUCT[0]->name; ?>" class="form-control" autocomplete="off">
							</td>
						</tr>

						<tr>
							<td>Sort Description</td>
						<td>
							<textarea name="sortdesc" value="" class="form-control" rows="3" autocomplete="off"><?php echo $EDITPRODUCT[0]->sortdesc; ?></textarea>
						</td>
						</tr>
						<tr>
							<td>Description</td>
						<td>
							<textarea name="description" class="form-control" rows="4"><?php echo $EDITPRODUCT[0]->description; ?></textarea>
						</td>
						</tr>
						<tr>
							<td> Price</td>
						<td>
							<input type="text" name="price" id="price" value="<?php echo $EDITPRODUCT[0]->price; ?>" class="form-control" autocomplete="off">
						</td>
						</tr>
						<tr>
							<td>Special Price</td>
						<td>
							<input type="text" name="spacel_price" id="spacel_price" value="<?php echo $EDITPRODUCT[0]->spacel_price; ?>" class="form-control" autocomplete="off" >
						</td>
						</tr>
						<tr>
							<td>Discount</td>
						<td>
							<input type="text" name="descount" value="<?php echo $EDITPRODUCT[0]->descount; ?>" class="form-control" autocomplete="off" onkeyup="return calculatespecialpeice(this.value);">
						</td>
						</tr>
						<?php /* ?>
						<tr>
							<td>Pincode</td>
							<td>
								<div class="naseemdiv">
								<?php 
								$pincodequery = $this->pincode_model->selectAllPincode();								
								$pincode= $EDITPRODUCT[0]->pincode_ids;

								?>
								//ajay script 
								<section>
					                <input class="magicsearch" id="basic" name="value" placeholder="search names...">
					                <div id="daysp"></div>
					            </section>
								<?php
								if(!empty($pincode)){
								$pincode_array = json_decode($pincode); 
								
								$newpin=array();
											foreach ($pincode_array as $row) {
												if(!empty($row->id)){
													array_push($newpin,$row->id);
												}												
											}
											//print_r($pincode_array);

								$newarray = array();			
									foreach ($pincode_array as $row) {
												if(!empty($row->id)){
													$newarray[$row->id] = $row->days;
												}												
											}
											//print_r($newarray);
											
								foreach ($pincodequery as $row){ 


									if(in_array($row->id,$newpin)){
									?>

		                            <label><input type="checkbox"  name="pincode_ids[<?php echo $row->id;?>][id]"  value="<?php echo $row->id;?>" <?php echo(in_array($row->id,$newpin))?'checked':''; ?> /> <?php echo $row->pincode; ?></label>
		                            	<?php $id = $row->id; 

		                            	?>
		                            <input type="text" name="pincode_ids[<?php echo $row->id;?>][days]" id="mypin" value="<?php if(!empty($newarray[$row->id])){echo $newarray[$row->id];} ?>" placeholder="">
	
								<?php } 
										}
									}
									
									else{  

									// foreach ($pincodequery as $row){ ?>

		                            <label><input type="checkbox"  name="pincode_ids[<?php echo $row->id;?>][id]"  value="<?php echo $row->id;?>"/> <?php echo $row->pincode; ?></label>
		                            	
		                            <input type="text" name="pincode_ids[<?php echo $row->id;?>][days]" id="mypin" value="" placeholder="">
	
								<?php } // }  ?>
								</div>
							</td>
						</tr>
						<?php */ ?>
						
						<tr>
							<td>Releted Product</td>
							<td>
								<div class="naseemdiv">
								<?php 
								$query = $this->product_model->selectAllProduct();								
								$arr= $EDITPRODUCT[0]->reletedproduct;
								$val_array=explode(',', $arr); 
								foreach ($query as $row){ ?>
		<label><input type="checkbox"  name="reletedproduct[]"  value="<?php echo $row->id;?>" <?php echo(in_array($row->id,$val_array))?'checked':''; ?> /> <?php echo $row->name; ?></label><br>
								<?php } ?>
								</div>
							</td>
						</tr>
						
						<tr>
							<td>Product Filters</td>
							<td>
								<div class="naseemdiv" style="height: 300px;">
									<?php $filters_array =(!empty($EDITPRODUCT[0]->filter_ids))? explode(",",$EDITPRODUCT[0]->filter_ids): array(); ?>
									<?php $filter_data = $this->filters_model->selectAllFilters(); ?>
									<?php $pro_option_array = $this->filters_model->selectOptionByProductID($EDITPRODUCT[0]->id); ?>
									<?php foreach($filter_data as $filter){ ?>
									<?php $option_data = $this->filters_model->selectAllFiltersOptions($filter->id); ?>
									<div class="col-md-4" style="border:1px solid #ddd; overflow: scroll; height: 180px;">
										<h4><?php echo $filter->title; ?></h4>
										<small><label><input <?php echo (in_array($filter->id, $filters_array))?"checked" :"" ?> type='checkbox' name="filter[]" value="<?php echo $filter->id; ?>"> Dispay In product details page<label></small>
										<ul>  
											<?php if(count($option_data)>0){ ?>
											<?php foreach($option_data as $option){ ?>
											<li><label><input <?php echo (in_array($option->id, $pro_option_array))?"checked" :"" ?> type='checkbox' name="options[]" value="<?php echo $option->id; ?>"> <?php echo $option->title; ?></label> </li>
											<?php } ?>
											<?php } ?>
										</ul>
									</div>
									<?php } ?>
								</div>
							</td>
						</tr>
						
						<tr>
							<td>Product Stock</td>
							<td>
								<input type="text" name="product_stock" value="<?php echo $EDITPRODUCT[0]->product_stock; ?>" class="form-control" autocomplete="off">
							</td>
						</tr>		

						
						
						<tr>
							<td>Meta Title</td>
							<td>
								<input type="text" name="producttitle" value="<?php echo $EDITPRODUCT[0]->meta_title; ?>" class="form-control" autocomplete="off">
							</td>
						</tr>	

							<tr>
							<td>Meta Keyword</td>
							<td>
								<input type="text" name="productkeyword" value="<?php echo $EDITPRODUCT[0]->meta_keyword; ?>" class="form-control" autocomplete="off">
							</td>
						</tr>					
						<tr>
							<td>Meta Description</td>
							<td>
								<input type="text" name="productdescription" value="<?php echo $EDITPRODUCT[0]->meta_description; ?>" class="form-control" autocomplete="off">
							</td>
						</tr>					
						
						
						
						<tr>
							<td>Status</td>
							<td>
								<select  name="status" class="form-control">
								<option <?php echo ($EDITPRODUCT[0]->status==1)?"selected" :"" ?> value="1">Active</option>
								<option <?php echo ($EDITPRODUCT[0]->status==0)?"selected" :"" ?> value="0">Inactive</option>
								</select>
							</td>
						</tr>


						<tr>
						<td colspan="2"><button type="submit" class="btn btn-info" name="edit">Submit</button></td>
						</tr>
						</tbody>
						</table>
					</form>
					
					   </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
<?php $this->load->view('admin/layout/footer-js'); ?>
<script>
function calculatespecialpeice(discount)
{
	var spacel_price = document.getElementById('spacel_price');
	var price = document.getElementById('price').value;
	if(price>0  && discount>0)
	{
		var discount_amount = price * (discount/100);
		spacel_price.value = price - discount_amount 
	}
}

function get_subcat(mCat,nextcat=0)
{
	var incrVal = $('#cat_pos').val();
	var lastr = incrVal;
	incrVal++;
	$('#cat_pos').val(incrVal);

	for(r=incrVal;r>nextcat;r--){
		$('#catrow_'+r).remove(); console.log($('#catrow_'+r).remove());
	}

	if(mCat>0) {
		$.ajax({
			type: "POST",
			url: "http://www.discountidea.com/index.php/categorycontroller/getSubCategory/"+mCat+'/'+incrVal,
			success: function(datas) {
	        	$('#vendorRow').before(datas);
			},
		});
	}
}
</script>

<link href="<?php echo base_url('assets/admin');?>/css/jquery.magicsearch.css">

<script  src="http://code.jquery.com/jquery-2.2.3.min.js"></script>
<script src="<?php echo base_url('assets/admin'); ?>/js/jquery.magicsearch.js"></script>
    <script>
        $(function() {
        	$.ajax({ method: "POST", url: "http://www.discountidea.com/index.php/adminproducts/pincode_ajax",data: {id: <?php echo $this->uri->segment(3);?>}
			}).done(function( msg ) {
			    //alert( "Data Saved: " + msg );
			    var dataSource = msg;
			    //console.log(dataSource);
			    $('#basic').magicsearch({
                dataSource: dataSource,
                fields: ['pincode'],
                id: 'id',
                format: '%pincode%',
                multiple: true,
                multiField: 'pincode',
                multiStyle: {
                    space: 5,
                    width: 80
                },
                success: function($input, data) {
                	console.log(data);
                	$("#daysp").append('<div class="form-group" id="autofill_'+data['id']+'"><label><input type="checkbox"  name="pincode_ids['+data['id']+'][id]"  value="'+data['id']+'"/>'+data['pincode']+'</label><input type="text" name="pincode_ids['+data['id']+'][days]" id="mypin" value="" placeholder=""></div>'); //add input box
      				return true;
  				},
  				afterDelete: function($input, data) {
  			  		$("#autofill_"+data['id']+"").remove();
		      		return true;		
				}
        	});
		});
    });
    </script>
</body>
</html>