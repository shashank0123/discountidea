<?php 

$admin_id = $this->session->userdata('admin_id'); 

if(empty($admin_id))

{

	redirect('index.php/admin');

}	

?>

<!DOCTYPE html>

<html lang="en">

<?php $this->load->view('admin/layout/head'); ?>

<body>



    <div id="wrapper">

        <!-- Navigation -->

        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">

            <!-- Brand and toggle get grouped for better mobile display -->

            <?php $this->load->view('admin/layout/header'); ?>

            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->

            <?php $this->load->view('admin/layout/left-menu'); ?>

            <!-- /.navbar-collapse -->

        </nav>



        <div id="page-wrapper">



            <div class="container-fluid">



                <!-- Page Heading -->

                <div class="row">

                    <div class="col-lg-12">

                        <h1 class="page-header">Admin Listing</h1>

                       <div class="row"><?php echo $this->session->flashdata('message'); ?>  

						<header class="panel-heading">							

							<a href="<?php echo base_url('index.php/admin/add_admin') ?>" class="btn btn-info pull-right" name="changePassword">Add New Admin</a>

							<br>

						</header><br>
					<div class="table-responsive">
						<table class="table table-bordered table-striped" id="datatable-default" width="100%">

							<thead>

								<tr>

									<th>SNo.</th>

									<th>Username</th>

									<th>Password</th>

									<th>E-mail address</th>

									<th>Status</th>

									<th>Action</th>

								</tr>

							</thead>

							<tbody>

							

								<?php $j=0; foreach($adminData as $admin){ $j++; ?>

								<tr class="gradeX">

									<td><?php echo $j; ?></td>

									<td><?php echo $admin->username; ?></td>

									<td><?php echo $admin->password; ?></td>

									<td><?php echo $admin->email; ?></td>

									<td><i class="status_checks btn <?php echo ($admin->status==1)? 'btn-success' : 'btn-danger'?>"><?php echo ($admin->status==1)? 'Active' : 'Inactive'?></i></td>

									<td class="actions">

										<a class="on-default edit-row" href="<?php echo base_url('index.php/admin/edit_admin/'.$admin->id); ?>" title="edit record"><i class="fa fa-pencil"></i></a>

										<a onclick="return confirm('Are you sure? you want to delete this record');" class="on-default remove-row" href="<?php echo base_url('index.php/admin/admin_delete/'.$admin->id); ?>" title="delete record"><i class="fa fa-trash-o"></i></a>

									</td>

								</tr>

								<?php } ?>	

							</tbody>

						</table>

					</div>
					</div>		

                    </div>

                </div>

                <!-- /.row -->



            </div>

            <!-- /.container-fluid -->



        </div>

        <!-- /#page-wrapper -->



    </div>

    <!-- /#wrapper -->

<?php $this->load->view('admin/layout/footer-js'); ?>





</body>



</html>

