<?php 
$admin_id = $this->session->userdata('admin_id'); 
if(empty($admin_id))
{
	redirect('index.php/admin');
}	
?>
<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/layout/head'); ?>
<body>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <?php $this->load->view('admin/layout/header'); ?>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <?php $this->load->view('admin/layout/left-menu'); ?>
            <!-- /.navbar-collapse -->
        </nav>
        <div id="page-wrapper">
            <div class="container-fluid">
                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h2 class="page-header">Edit Invoice</h2>
                         <form role="form" method="post" enctype="multipart/form-data">
							<div class="form-group">
                               
                            </div>
						 
                            <div class="form-group">
                                <label>Transaction ID</label>
                                <input type="text" class="form-control" placeholder="Transaction ID" value="<?php echo $income[0]->transaction_id; ?>" name="transaction_id">
                            </div>
							
                            <div class="form-group">
                                <label>Status</label>
                                <select name="status" class="form-control">
                                    <option <?php echo($income[0]->status=='Unpaid')?'selected':''; ?> value="Unpaid">Unpaid</option>
									<option <?php echo($income[0]->status=='Paid')?'selected':''; ?> value="Paid">Paid</option>
									<option <?php echo($income[0]->status=='Reffered to Approval')?'selected':''; ?> value="Reffered to Approval">Reffered to Approval</option>
									<option <?php echo($income[0]->status=='Pendding to Approval')?'selected':''; ?> value="Pendding to Approval">Pendding to Approval</option>
									<option <?php echo($income[0]->status=='Unapproved')?'selected':''; ?> value="Unapproved">Unapproved</option>
									<option <?php echo($income[0]->status=='Approved')?'selected':''; ?> value="Approved">Approved</option>
                                </select>
                            </div>                            
							
                            <button type="submit" name="updatedata" class="btn btn-default">Submit Button</button>
                            <button type="reset" class="btn btn-default">Reset Button</button>

                        </form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->		
    </div>
    <!-- /#wrapper -->
<?php $this->load->view('admin/layout/footer-js'); ?>
</body>

</html>
