<?php 
$admin_id = $this->session->userdata('admin_id'); 
if(empty($admin_id))
{
	redirect('index.php/admin');
}	
?>
<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/layout/head'); ?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<body>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <?php $this->load->view('admin/layout/header'); ?>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <?php $this->load->view('admin/layout/left-menu'); ?>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Seller Credit Settlement</h1>
                        <?php echo $this->session->flashdata('message'); ?>
						<div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
										<th>S.No.</th>
                                        <th>Vendor ID</th>
										<th>Selling Amount</th>
										<th>Income</th>
										<th>Deducation</th>
										<th>Final Income</th>										
										<th>Date</th>
										<th>Transaction ID</th>
										<th>Status</th>
										<th></th>		
                                    </tr>
                                </thead>
                                <tbody>
								    <?php if(count($income)>0){ ?>
									<?php $i=0; foreach($income as $data){ $i++; ?>	
										<tr>
											<td><?php echo $i; ?></td>
											<td><?php echo $data->vendor_id; ?></td>
											<td><?php echo $data->seller_amoount; ?></td>
											<td><?php echo $data->total_income; ?></td>
											<td><?php echo $data->total_deducation; ?></td>
											<td><?php echo $data->final_income; ?></td>											
											<td><?php echo $data->date; ?></td>
											<td><?php echo $data->transaction_id; ?></td>
											<td><?php echo $data->status; ?></td>
											<td>
												<a href="<?php echo base_url('admin/edit_credit_settlment/'.$data->id); ?>" title="edit record"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
											</td>	
										</tr>
									<?php }} ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
<script>
function update_status(VALUE)
{
	alert(VALUE);
}
</script>	
<?php $this->load->view('admin/layout/footer-js'); ?>

  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
   <script>
  jQuery( function() {
    jQuery( "#datepicker1, #datepicker2" ).datepicker({ dateFormat: 'yy-mm-dd' });
    
  } );
  </script>
</body>

</html>
