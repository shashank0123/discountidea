<?php 
$admin_id = $this->session->userdata('admin_id'); 
if(empty($admin_id))
{
	redirect('index.php/admin');
}	
?>
<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/layout/head'); ?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<body>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <?php $this->load->view('admin/layout/header'); ?>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <?php $this->load->view('admin/layout/left-menu'); ?>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Return Deductions</h1>
                        <?php echo $this->session->flashdata('message'); ?>
						<div class="table-responsive">
						    <form method="post">
						    <table class="table1 table-bordered table-hover" width="70%" >
								<tr>
									<td width="15%"><input type="number" name="vendor_id" value="<?php echo (isset($_POST['vendor_id']))?$_POST['vendor_id']:''; ?>" class="form-control" placeholder="Vendor ID"></td>
									<td width="15%"><input type="number" name="product_id" value="<?php echo (isset($_POST['product_id']))?$_POST['product_id']:''; ?>" class="form-control" placeholder="Product ID"></td>
									<td width="13%"><input type="number" name="order_id" value="<?php echo (isset($_POST['order_id']))?$_POST['order_id']:''; ?>" class="form-control" placeholder="Order ID"></td>
									<td><input type="text" name="sdate" id="datepicker1" value="<?php echo (isset($_POST['sdate']))?$_POST['sdate']:''; ?>" class="form-control" placeholder="start date"></td>
									<td><input type="text" name="edate" id="datepicker2" value="<?php echo (isset($_POST['edate']))?$_POST['edate']:''; ?>" class="form-control" placeholder="start date"></td>
									<td><button type="submit"  class="form-control">Search</button></td>
								</tr>
							</table><br>
							</form>
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
										<th>S.No.</th>
                                        <th>Vendor ID</th>
										<th>Order ID</th>
										<th>Product ID</th>
										<th>Transaction ID</th>
										<th>Sell Amount</th>
                                        <th>Commission</th>
                                        <th>Admin Charge</th>
										<th>Promotional Charge</th>
										<th>Taxable Amount</th>
										<th>Service Tax / GTS</th>
										<th>Final Deduction</th>
										<th>Remittance</th>
										<th>TCS Deducted</th>
										<th>Refund Deduction</th>
										<th>Invoice Date</th>
										<th>Status</th>
										<th>Deduction status</th>
										<th></th>	
                                    </tr>
                                </thead>
                                <tbody>
								    <?php if(count($income)>0){ ?>
									<?php $i=0; foreach($income as $data){ $i++; ?>	
										<tr>
											<td><?php echo $i; ?></td>
											<td><?php echo $data->vendor_id; ?></td>
											<td><?php echo $data->order_id; ?></td>
											<td><?php echo $data->product_id; ?></td>
											<td><?php echo $data->transaction_id; ?></td>
											<td><?php echo $data->sell_amount; ?></td>
											<td><?php echo $data->commission; ?></td>
											<td><?php echo $data->admin_charge; ?></td>
											<td><?php echo $data->promotion_charge; ?></td>
											<td><?php echo $data->taxable_amount; ?></td>
											<td><?php echo $data->servicetax_gst; ?></td>
											<td><?php echo $data->final_deduction; ?></td>
											<td><?php echo $data->remittance; ?></td>
											<td><?php echo $data->tcs_deducted; ?></td>
											<td><?php echo $data->final_remittance; ?></td>
											<td><?php echo $data->inv_date; ?></td>
											<td>
												<select name="status">
													<option <?php echo($data->status=='Unpaid')?'selected':''; ?> value="Unpaid">Inspection under Process</option>
													<option <?php echo($data->status=='Paid')?'selected':''; ?> value="Paid">Vendor Communication under process</option>
													<option <?php echo($data->status=='Reffered to Approval')?'selected':''; ?> value="Reffered to Approval">Reffered to Approval</option>
													<option <?php echo($data->status=='Pendding to Approval')?'selected':''; ?> value="Pendding to Approval">Pendding to Approval</option>
													<option <?php echo($data->status=='Unapproved')?'selected':''; ?> value="Unapproved">Request Not Approved</option>
													<option <?php echo($data->status=='Approved')?'selected':''; ?> value="Approved">Request Approved</option>
												</select>
											</td>
											<td><?php echo $data->ready_to_pay; ?></td>
											<td>
												<a href="<?php echo base_url('admin/edit_vendor_return_deducation/'.$data->id); ?>" title="edit record"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
											</td>
										</tr>
									<?php }} ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
<script>
function update_status(VALUE)
{
	alert(VALUE);
}
</script>	
<?php $this->load->view('admin/layout/footer-js'); ?>

  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
   <script>
  jQuery( function() {
    jQuery( "#datepicker1, #datepicker2" ).datepicker({ dateFormat: 'yy-mm-dd' });
    
  } );
  </script>
</body>

</html>
