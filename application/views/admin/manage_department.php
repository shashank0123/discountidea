<?php 

$admin_id = $this->session->userdata('admin_id'); 

if(empty($admin_id))

{

	redirect('index.php/admin');

}	

?>

<!DOCTYPE html>

<html lang="en">

<?php $this->load->view('admin/layout/head'); ?>

<body>



    <div id="wrapper">

        <!-- Navigation -->

        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">

            <!-- Brand and toggle get grouped for better mobile display -->

            <?php $this->load->view('admin/layout/header'); ?>

            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->

            <?php $this->load->view('admin/layout/left-menu'); ?>

            <!-- /.navbar-collapse -->

        </nav>



        <div id="page-wrapper">



            <div class="container-fluid">



                <!-- Page Heading -->

                <div class="row">

                    <div class="col-lg-12">

                        <h1 class="page-header">Welcome to Dashboard</h1>
	<?php if(!empty($this->session->flashdata('item'))){ echo "<h4>".$this->session->flashdata('item')."</h4>"; } ?>
 <div class="col-md-12"> <br>

							 							

							<section class="panel">								

							<div class="panel-body">

                                <a href="<?php echo base_url().'admin/add_department'; ?>" class="btn btn-info pull-right">Add Department</a>							

								<div class="table-responsive">                                 

									<table class="table table-bordered table-striped" id="datatable-default">									

								<thead>

                                    <tr>

										<th width="5%">S.No.</th>

                                        <th>Title</th>

										<th>Email</th>	

                                        <th width="20%">Action</th>

                                    </tr>

                                </thead>

                                <tbody>

								<?php //echo "<pre/>"; print_r($Depart);
									$i=1;
									foreach($Depart as $departs){
								?>
                                    <tr>

										<td><?php echo $i;  ?></td>

                                        <td><?=$departs->title?></td>
										 
										<td><?=$departs->email?></td>	

                                        <td>

											<a href="<?php echo base_url().'admin/edit_department/'.$departs->id; ?>" title="edit record"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>										

										</td>

                                    </tr>

									<?php } ?>
                                    

                                                                       

                                </tbody>

						</table>

								</div>

							</div>

						</section>

                         

						</div>                      

                    </div>

                </div>

                <!-- /.row -->



            </div>

            <!-- /.container-fluid -->



        </div>

        <!-- /#page-wrapper -->



    </div>

    <!-- /#wrapper -->

<?php $this->load->view('admin/layout/footer-js'); ?>





</body>



</html>

