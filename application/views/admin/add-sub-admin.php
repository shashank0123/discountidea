<?php 

$admin_id = $this->session->userdata('admin_id'); 

if(empty($admin_id))

{

	redirect('index.php/admin');

}	

?>

<!DOCTYPE html>

<html lang="en">

<?php $this->load->view('admin/layout/head'); ?>

<body>
<?php 

if(isset($adminData[0]->module) && $adminData[0]->module!="")

{

	$module_array = explode(",",$adminData[0]->module);

}

else

{

	$module_array =array();

} 

 ?>


    <div id="wrapper">

        <!-- Navigation -->

        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">

            <!-- Brand and toggle get grouped for better mobile display -->

            <?php $this->load->view('admin/layout/header'); ?>

            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->

            <?php $this->load->view('admin/layout/left-menu'); ?>

            <!-- /.navbar-collapse -->

        </nav>



        <div id="page-wrapper">



            <div class="container-fluid">



                <!-- Page Heading -->

                <div class="row">

                    <div class="col-lg-12">

                        <h1 class="page-header">Welcome to Dashboard</h1>

                       <div class="row">						

						<div class="col-md-12"> <br><?php echo $this->session->flashdata('message'); ?>                           

							<section class="panel">

							<header class="panel-heading">

								<div class="panel-actions">									

								</div>						

								<h2 class="panel-title pull-left">Add Sub Admin</h2><br>

							</header>

							<div class="panel-body">								

								<div class="table-responsive">

									<form method="post">

										<table class="table table-bordered table-striped table-condensed" style="">										

											<tbody>

												<tr>

													<td>Username</td>

													<td><input type="text" name="username" value="<?php echo(isset($adminData[0]->username))?$adminData[0]->username:''; ?>" class="form-control" data-validation="required"></td>

												</tr>

												<tr>

													<td>Password</td>

													<td><input type="text" name="password" value="<?php echo(isset($adminData[0]->username))?$adminData[0]->password:''; ?>" class="form-control" data-validation="required"></td>

												</tr>

												<tr>

													<td>Email address</td>

													<td><input type="text" name="email" value="<?php echo(isset($adminData[0]->email))?$adminData[0]->email:''; ?>" class="form-control" data-validation="required"></td>

												</tr>

												<tr>

													<td>Admin module</td>

													<td>

														<ul style="float:left;">

															<?php foreach(getadminmodule() as $key=>$value){ ?>

																<li><label><input <?php echo(in_array($key, $module_array))?'checked':''; ?> type="checkbox" name="module[]" value="<?php echo $key; ?>"><?php echo $value; ?><label></li>

															<?php } ?>

														</ul>

													</td>

												</tr>

												<tr>

													<td>Status</td>

													<td>

														<select name="status" class="form-control">

															<option value="1" <?php echo(isset($adminData[0]->status) && $adminData[0]->status==1)?'selected':''; ?>>Active</option>

															<option value="0" <?php echo(isset($adminData[0]->status)  && $adminData[0]->status==0)?'selected':''; ?>>Inactive</option>

														</select>

													</td>

												</tr>

												<tr>

													<td colspan="2"><button type="submit" class="btn btn-info" name="suubmitData">Submit</button></td>

												</tr>

											</tbody>

										</table>

									</form>

								</div>

							</div>

						</section>

                        

						</div>

						



					</div>

                    </div>

                </div>

                <!-- /.row -->



            </div>

            <!-- /.container-fluid -->



        </div>

        <!-- /#page-wrapper -->



    </div>

    <!-- /#wrapper -->

<?php $this->load->view('admin/layout/footer-js'); ?>





</body>



</html>

