 <?php 
$admin_id = $this->session->userdata('admin_id'); 
if(empty($admin_id))
{
	redirect('index.php/admin');
}	
?>
<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/layout/head'); ?>
<body>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <?php $this->load->view('admin/layout/header'); ?>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <?php $this->load->view('admin/layout/left-menu'); ?>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">

<a class="btn btn-success" href="<?php echo base_url('size/add');?>" style="float: right;">Add Offer</a>
					<!-- start: page -->
					<div class="row">
					<div class="col-md-12"> <br><?php echo $this->session->flashdata('message'); ?>
						<div class="col-md-6 col-lg-12 col-xl-6">
						    <div class="panel panel-sign">
					
					<div class="panel-body">

			<form enctype="multipart/form-data" action="<?php echo base_url();?>index.php/size/listing/" method="POST">

									<table class="table table-bordered table-striped mb-none">
									<?php
								//print_r ($LISTINGSIZE);die("fff");
									?>
									
							<thead>
								<tr class="gradeX">
								    <th>Sr No</th>
									<th>Name</th>
									<th>Status</th>
									<th>action</th>
								</tr>
							</thead>
							<tbody>
							
							<?php if(count($LISTINGSIZE)>0){ ?>
							<?php $i=0; foreach($LISTINGSIZE as $LISTINGSIZE){ $i++;?>
							<tr class="gradeX">
							<td>
							<?php echo $i;?>	
							</td>
							<td>
							<?php echo $LISTINGSIZE->size;?>
							</td>
							<td><i data="<?php echo $LISTINGSIZE->id;?>" class="status_checks btn <?php echo ($LISTINGSIZE->status)? 'btn-success' : 'btn-danger'?>"><?php echo ($LISTINGSIZE->status)? 'Active' : 'Inactive'?></i>

							</td>
							<td><a href="<?php echo site_url('index.php/size/edit/'.$LISTINGSIZE->id.''); ?>" title="edit record"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a><?php echo "&nbsp;&nbsp;&nbsp;&nbsp;" ?>
							<a href="<?php echo site_url('index.php/size/delete/'.$LISTINGSIZE->id.''); ?>" title="delete record"><i class="fa fa-trash" aria-hidden="true"></i></a>
							</td>
							</tr>
							<?php
							}
							}
							?>																				
							</tbody>
						</table>

						</form>
					</div>
				</div>
						</div>
					<!-- end: page -->
			</div>

 </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
	 </div>
	  </div>
	   </div>
    <!-- /#wrapper -->
<?php $this->load->view('admin/layout/footer-js'); ?>
</body>

</html>