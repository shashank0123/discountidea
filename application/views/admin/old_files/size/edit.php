<?php 
$admin_id = $this->session->userdata('admin_id'); 
if(empty($admin_id))
{
	redirect('index.php/admin');
}	
?>
<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/layout/head'); ?>
<body>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <?php $this->load->view('admin/layout/header'); ?>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <?php $this->load->view('admin/layout/left-menu'); ?>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Edit Size</h1>
						<div class="col-md-6 col-lg-12 col-xl-6">
						    <div class="panel panel-sign">
					
					<div class="panel-body">
 
				<form enctype="multipart/form-data"  action="" method="POST">
                                   <?php //print_r($EDITSIZE);?>
							<table class="table table-bordered table-striped table-condensed" style="">										
								<tbody>
								  
									<tr>
										<td>Size</td>
										<td>
		<input type="text" name="size" value="<?php echo  $EDITSIZE[0]->size; ?>" class="form-control">
										</td>
									</tr>
                                    <tr>
                                        <td> Status</td>
                                        <td>
                                        <select  name="status" class="form-control">
                                        <option <?php echo ($EDITSIZE[0]->status==1)?"selected" :"" ?> value="1">Active</option>
                                        <option <?php echo ($EDITSIZE[0]->status==0)?"selected" :"" ?> value="0">Inactive</option>
                                        </select>
                                        </td>
                                    </tr>							
									<tr>
			<td colspan="2"><button type="submit" class="btn btn-info" name="edit">Submit</button></td>
									</tr>
								</tbody>
							</table>

						</form>
					
					   </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    </div>
    </div>
    </div>
    <
    <!-- /#wrapper -->
<?php $this->load->view('admin/layout/footer-js'); ?>
</body>

</html>
