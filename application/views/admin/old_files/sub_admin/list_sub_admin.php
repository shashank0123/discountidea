 <?php $this->load->view('admin/layout/header'); ?>
			<!-- end: header -->

			<div class="inner-wrapper">
				<!-- start: sidebar -->
				<aside id="sidebar-left" class="sidebar-left">
				
					 <?php $this->load->view('admin/layout/nave'); ?>
				
				</aside>
				<!-- end: sidebar -->

				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Create Sub Admin</h2>
					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="index-2.html">
										<i class="fa fa-home"></i>
									</a>
								</li>
								<li><span>Create Sub Admin</span></li>
							</ol>
					
							<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
						</div>

					</header>

					<!-- start: page -->
					<div class="row">
					<div class="col-md-12"> <br><?php echo $this->session->flashdata('message'); ?>
						<div class="col-md-6 col-lg-12 col-xl-6">
						    <div class="panel panel-sign">
					
					<div class="panel-body">

				<form action="<?php echo base_url();?>index.php/admin/list_sub_admin/" method="POST">

									<table class="table table-bordered table-striped mb-none">
									<?php
									//print_r($LISTADMIN);
									?>
							<thead>
								<tr class="gradeX">
								    <th>ID</th>
									<th>name</th>
									<th>username</th>
									<th>email</th>
									<th>phoneno</th>
									<th>type</th>
									<th>Status</th>
									<th>action</th>
								</tr>
							</thead>
							<tbody>
							
							<?php if(count($LISTADMIN)>0){ ?>
							 <?php foreach($LISTADMIN as $LISTADMIN){?>

								<tr class="gradeX">
								
									<td><?php echo $LISTADMIN->id;?></td>
									<td><?php echo $LISTADMIN->name;?></td>
									<td><?php echo $LISTADMIN->username;?></td>
									<td><?php echo $LISTADMIN->email;?></td>
									<td><?php echo $LISTADMIN->phoneno;?></td>
									<td><?php echo $LISTADMIN->type;?></td>
									<td><i data="<?php echo $LISTADMIN->id;?>" class="status_checks btn <?php echo ($LISTADMIN->status)? 'btn-success' : 'btn-danger'?>"><?php echo ($LISTADMIN->status)? 'Active' : 'Inactive'?></i>
									</td>
      <td><a href="<?php echo site_url('index.php/admin/edit_list_sub_admin/'.$LISTADMIN->id.''); ?>">Edit</a>
                                   </td>
								</tr>
								<?php
							}
						}
							?>																				
							</tbody>
						</table>

						</form>
					</div>
				</div>
						</div>
					<!-- end: page -->
				</section>
			</div>

			
		</section>

		<!-- Vendor -->
		<script src="<?php echo base_url(); ?>assets/vendor/jquery/jquery.js"></script>		
		<script src="<?php echo base_url(); ?>assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>	
		<script src="<?php echo base_url(); ?>assets/vendor/jquery-cookie/jquery.cookie.js">
		</script>		
	<script src="<?php echo base_url(); ?>assets/vendor/style-switcher/style.switcher.js"></script>	
		<script src="<?php echo base_url(); ?>assets/vendor/bootstrap/js/bootstrap.js"></script>
		<script src="<?php echo base_url(); ?>assets/vendor/nanoscroller/nanoscroller.js"></script>	
		<script src="<?php echo base_url(); ?>assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>		
		<script src="<?php echo base_url(); ?>assets/vendor/magnific-popup/magnific-popup.js"></script>	
		<script src="<?php echo base_url(); ?>assets/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		
		<!-- Specific Page Vendor -->		
		
				
	
		
		<!-- Theme Base, Components and Settings -->
		<script src="<?php echo base_url(); ?>assets/javascripts/theme.js"></script>
		
		<!-- Theme Custom -->
		<script src="<?php echo base_url(); ?>assets/javascripts/theme.custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="<?php echo base_url(); ?>assets/javascripts/theme.init.js"></script>
		<!-- Analytics to Track Preview Website -->		<script>		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)		  })(window,document,'script','../../../www.google-analytics.com/analytics.js','ga');		  ga('create', 'UA-42715764-8', 'auto');		  ga('send', 'pageview');		</script>

		<!-- Examples -->
		<script src="<?php echo base_url(); ?>assets/javascripts/dashboard/examples.dashboard.js"></script>
		
	</body>

</html>