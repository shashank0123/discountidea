 <?php 
$admin_id = $this->session->userdata('admin_id'); 
if(empty($admin_id))
{
	redirect('index.php/admin');
}	
?>
<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/layout/head'); ?>
<style>
.naseemdiv
{
	height: 210px;
    overflow: scroll;
    border: 2px solid rgba(34, 34, 34, 0.41);
    padding: 10px;
}
.naseemdiv label{padding: 3px;}
.naseemdiv ul {padding-left:5px; list-style:none;}
</style>
<body>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <?php $this->load->view('admin/layout/header'); ?>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <?php $this->load->view('admin/layout/left-menu'); ?>
            <!-- /.navbar-collapse -->
        </nav>

				<div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Add Category</h1>
						<div class="col-md-6 col-lg-12 col-xl-6">
						    <div class="panel panel-sign">
					
					<div class="panel-body">

				<form  enctype="multipart/form-data" action="<?php echo base_url();?>index.php/categorycontroller/add/" method="POST">

							<table class="table table-bordered table-striped table-condensed" style="">										
								<tbody>
								    <tr>
										<td>Parent Category</td>
										<td>
										<select  name="parent_id" class="form-control">
										<option value="0">Root</option>
										<?php 
											$query = $this->category_model->selectAllCategory();
											foreach ($query as $row)
											{
												echo "<option value='" . $row->id. "'>" . $row->title .  "</option>";										
											} 
										?>									
										</select>
										</td>
									</tr>
									<tr>
										<td>Title</td>
										<td><input type="text" name="name" class="form-control" required></td>
									</tr>
									
									<!--<tr>
										<td>Image</td>
										<td><input type="file" name="image" class="form-control">
                                             <input type="hidden" name="oldImage" class="form-control">
										</td>
									</tr>-->
																		
								    <tr>
										<td>Display On Home</td>
										<td><select  name="display_on_home" class="form-control">
										<option value="0">No</option>
										<option value="1">Yes</option>
										</select>
										</td>
									</tr>
									  <tr>
										<td>Display On Main Category</td>
										<td><select  name="display_on_mcategory" class="form-control">
										<option value="0">No</option>
										<option value="1">Yes</option>
										</select>
										</td>
									</tr>
									
									<tr>
										<td>Display Filters</td>
										<td>
											<div class="naseemdiv">
												<?php $filter_data = $this->filters_model->selectAllFilters(); ?>
												<?php foreach($filter_data as $filter){ ?>
												<label><input type="checkbox"  name="filters_id[]"  value="<?php echo $filter->id; ?>" /> <?php echo $filter->title; ?> </label><br>
												<?php } ?>
											</div>
										</td>
									</tr>
									<tr>
										<td>Meta Title</td>
										<td><input type="text" name="metatile" class="form-control" required></td>
									</tr>
									
									<tr>
										<td>Meta Keyword</td>
										<td><input type="text" name="metakeyword" class="form-control" required></td>
									</tr>
									
									<tr>
										<td>Meta Description</td>
										<td><input type="text" name="metadescription" class="form-control" ></td>
									</tr>
									
									<tr>
										<td>status</td>
										<td><select  name="status" class="form-control">
										<option value="1">Active</option>
										<option value="0">Inactive</option>
										</select>
										</td>
									</tr>
									<tr>
			<td colspan="2"><button type="submit" class="btn btn-info" name="add">Submit</button></td>
									</tr>
								</tbody>
							</table>

						</form>
				</div>
				</div>
						</div>
					<!-- end: page -->
			</div>

 </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
	 </div>
	  </div>
	   </div>
    <!-- /#wrapper -->
<?php $this->load->view('admin/layout/footer-js'); ?>
</body>

</html>