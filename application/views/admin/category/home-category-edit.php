<?php 
$admin_id = $this->session->userdata('admin_id'); 
if(empty($admin_id))
{
	redirect('index.php/admin');
}	
?>
<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/layout/head'); ?>
<style>
.naseemdiv
{
	height: 310px;
    overflow: scroll;
    border: 2px solid rgba(34, 34, 34, 0.41);
    padding: 10px;
}
.naseemdiv label{padding: 3px;}
.naseemdiv ul {padding-left:5px; list-style:none;}
</style>
<body>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <?php $this->load->view('admin/layout/header'); ?>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <?php $this->load->view('admin/layout/left-menu'); ?>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Edit Category</h1>
						<div class="col-md-6 col-lg-12 col-xl-6">
						    <div class="panel panel-sign">
					
					<div class="panel-body">
 
				<form enctype="multipart/form-data"  action="" method="POST">
                                   <?php //print_r($EDITCATEGORY);?>
							<table class="table table-bordered table-striped table-condensed" style="">										
								<tbody>
								  	<tr>
										<td>Title</td>
										<td>
		<input type="text" name="title" value="<?php echo  $EDITCATEGORY[0]->title; ?>" class="form-control">
										</td>
									</tr>
																					
									
									<?php 
										if(!empty($EDITCATEGORY[0]->product_ids))
										{
											$product_array = explode(",",$EDITCATEGORY[0]->product_ids);
										}
										else
										{
											$product_array = array();
										}	
									?>
									<tr>
										<td>Display Products</td>
										<td>
											<div class="naseemdiv">
												<?php $product_data = $this->product_model->select_active_products(); ?>
												<?php foreach($product_data as $product){ ?>
												<label><input type="checkbox"  name="product_ids[]"  value="<?php echo $product->id; ?>" <?php echo (in_array($product->id, $product_array))?"checked" :"" ?> /> <?php echo $product->name; ?> </label><br>
												<?php } ?>
											</div>
										</td>
									</tr>
									
									<tr>
										<td>Status</td>
										<td>
										<select  name="status" class="form-control">
										<option <?php echo ($EDITCATEGORY[0]->status==1)?"selected" :"" ?> value="1">Active</option>
										<option <?php echo ($EDITCATEGORY[0]->status==0)?"selected" :"" ?> value="0">Inactive</option>
										</select>
										</td>
									</tr>
									
																	
									<tr>
			<td colspan="2"><button type="submit" class="btn btn-info" name="edit">Submit</button></td>
									</tr>
								</tbody>
							</table>

						</form>
					
					   </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
<?php $this->load->view('admin/layout/footer-js'); ?>
</body>

</html>
