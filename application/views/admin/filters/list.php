<?php 
$admin_id = $this->session->userdata('admin_id'); 
if(empty($admin_id))
{
	redirect('index.php/admin');
}	
?>
<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/layout/head'); ?>
<body>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <?php $this->load->view('admin/layout/header'); ?>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <?php $this->load->view('admin/layout/left-menu'); ?>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h2 class="page-header">Manage Filters 						
						<a href="<?php echo base_url('index.php/filters/add/0'); ?>" class="btn btn-primary pull-right">Add New</a></h2>
						<?php echo $this->session->flashdata('message'); ?>
                         <div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>Title</th>
										<th width="8%"></th>
                                        <th width="20%">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
								<?php $i=0; foreach($FILTERDATA as $data){ $i++; ?>
								<?php $option_data = $this->filters_model->selectAllFiltersOptions($data->id); ?>
                                    <tr>
                                        <td><b><?php echo $data->title; ?></b></td>
										<td><a href="<?php echo base_url('index.php/filters/add/'.$data->id); ?>" class="btn btn-warning pull-right">Add Option</a></td>
                                        <td>
											<a href="<?php echo base_url('index.php/filters/edit/'.$data->id); ?>" title="edit record"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
											<?php if(count($option_data)<=0){ ?>
											<a href="<?php echo base_url('index.php/filters/deletefilter/'.$data->id); ?>" onclick="return deleteConfirm();" title="delete record"><i class="fa fa-trash" aria-hidden="true"></i></a>																				
											<?php } ?>
										</td>
                                    </tr>
								<?php if(count($option_data)>0){ ?>
								<?php foreach($option_data as $option){ ?>
								 <tr>
                                        <td><b>--------></b> <?php echo $option->title; ?></td>
										<td></td>
                                        <td>
											<a href="<?php echo base_url('index.php/filters/edit/'.$option->id); ?>" title="edit record"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
											<a href="<?php echo base_url('index.php/filters/deletefilter/'.$option->id); ?>" onclick="return deleteConfirm();" title="delete record"><i class="fa fa-trash" aria-hidden="true"></i></a>																				
										</td>
                                  </tr>
								<?php } ?>
								<?php } ?>	
                                <?php } ?>                                       
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
<?php $this->load->view('admin/layout/footer-js'); ?>
</body>

</html>
