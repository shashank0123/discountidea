<?php 
$admin_login_id = $this->session->userdata('admin_id'); 
//
$admin_data = $this->admin_model->selectAdminById($admin_login_id);
//print_r($admin_data); die;
$uri = $this->uri->segment(1)."/".$this->uri->segment(2);
//$module_array1 = explode(",",$admin_data[0]->module);
//print_r($uri);
if($admin_data[0]->type==2)
{
	$module_array = explode(",",$admin_data[0]->module);
	//print_r($module_array); die;
	if(!in_array($uri, $module_array)){ 
	redirect(base_url()."index.php/admin/dashboard");
	
	}
	$homepagepopup = (in_array('homepagepopup/listing', $module_array))?'1':'0';
	$banner = (in_array('banner/listing', $module_array))?'1':'0';
	//$module_array = explode(",",$admin_data[0]->module);
	$categorycontroller = (in_array('categorycontroller/home_category', $module_array))?'1':'0';
	$blogcategory = (in_array('blogcategory/cat_listing', $module_array))?'1':'0';
	$blog = (in_array('blog/listing', $module_array))?'1':'0';
	$edit_blogbanner = (in_array('admin/edit_blogbanner', $module_array))?'1':'0';
	$user = (in_array('user/listing', $module_array))?'1':'0';
	$userList = (in_array('admin/userList', $module_array))?'1':'0';
	$adduser = (in_array('admin/adduser', $module_array))?'1':'0';
	$allstore = (in_array('vendor/allstore', $module_array))?'1':'0';
	$categorycontroller1 = (in_array('categorycontroller/listing', $module_array))?'1':'0';
	$adminproducts = (in_array('adminproducts/listing', $module_array))?'1':'0';
	$csv = (in_array('csv/index', $module_array))?'1':'0';
	$pincode = (in_array('pincode/listing', $module_array))?'1':'0';
	$filters = (in_array('filters/listing', $module_array))?'1':'0';
	$offer = (in_array('offer/listing', $module_array))?'1':'0';
	$faq = (in_array('faq/listing', $module_array))?'1':'0';
	$cms = (in_array('cms/listing', $module_array))?'1':'0';
	$newsletter = (in_array('newsletter/listing', $module_array))?'1':'0';
	$our_client = (in_array('our_client/listing', $module_array))?'1':'0';
	$listing = (in_array('order/listing', $module_array))?'1':'0';
    $report= (in_array('order/report', $module_array))?'1':'0';
	$admin_meta_tag= (in_array('admin_meta_tag/index', $module_array))?'1':'0';
	$block = (in_array('block/listing', $module_array))?'1':'0'; 
	$distribute_vendor_money = (in_array('admin/distribute_vendor_money', $module_array))?'1':'0';
	$seller_credit_settlment = (in_array('admin/seller_credit_settlment', $module_array))?'1':'0';
	$vandor_income = (in_array('admin/vandor_income', $module_array))?'1':'0';
	$vandor_return_deductions = (in_array('admin/vandor_return_deductions', $module_array))?'1':'0';
	$help_desk = (in_array('admin/help_desk', $module_array))?'1':'0';
	$dashboard = (in_array('admin/dashboard', $module_array))?'1':'0';
	//echo $dashboard; die;
}
else
{
	$module_array = 1;
	$homepagepopup = 1;
	$banner = 1;
	$categorycontroller = 1;
	$blogcategory = 1;
	$blog = 1;
	$edit_blogbanner = 1;
	$user = 1;
	$userList = 1;
	$adduser = 1;
	$allstore = 1;
	$categorycontroller1 = 1;
	$adminproducts = 1;
	$csv = 1;
	$pincode = 1;
	$filters = 1;
	$offer = 1;
	$faq = 1;
	$cms = 1;
	$newsletter = 1;
	$our_client = 1;
	$listing = 1;
    $report= 1;
	$admin_meta_tag= 1;
	$block = 1; 
	$distribute_vendor_money = 1;
	$seller_credit_settlment = 1;
	$vandor_income = 1;
	$vandor_return_deductions = 1;
	$help_desk = 1;
	$dashboard = 1;
}
//print_r($admin_data);
?>
<div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
				<?php if($dashboard==1){ ?>
                    <li class="active">
                        <a href="<?php echo base_url('index.php/admin/dashboard'); ?>"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                    </li>
				<?php } ?>					
					<li>
					<?php if($homepagepopup==1 || $categorycontroller==1 || $banner==1){ ?>
                        <a href="javascript:;" data-toggle="collapse" data-target="#popup">
							<i class="fa fa-fw fa-arrows-v"></i>Home Page<i class="fa fa-fw fa-caret-down"></i>
						</a>
					<?php } ?>	
                        <ul id="popup" class="collapse">
                            <?php if($homepagepopup==1){ ?>
							<li>
                                <a href="<?php echo base_url('index.php/homepagepopup/listing'); ?>">
									<i class="fa fa-arrow-circle-right"></i> Manage Popup
								</a>
                            </li>
							<?php } ?>	
							<?php if($banner==1){ ?>
							<li>
								<a href="<?php echo base_url('index.php/banner/listing/'); ?>">
									<i class="fa fa-arrow-circle-right"></i> Manage Banner
								</a>
							</li>
							<?php } ?>
							<?php if($categorycontroller==1){ ?>
							<li>
								<a href="<?php echo base_url('index.php/categorycontroller/home_category/'); ?>">
									<i class="fa fa-arrow-circle-right"></i> Home Category
								</a>
							</li>
							<?php } ?>
                        </ul>
                    </li>
					<li><?php if($blogcategory==1 || $blog==1 || $edit_blogbanner==1){ ?>
                        <a href="javascript:;" data-toggle="collapse" data-target="#blog">
							<i class="fa fa-fw fa-arrows-v"></i>Manage Blog<i class="fa fa-fw fa-caret-down"></i>
						</a>
					<?php } ?>
                        <ul id="blog" class="collapse">
						<?php if($blogcategory==1){ ?>
                            <li>
                                <a href="<?php echo base_url('index.php/blogcategory/cat_listing'); ?>">
									<i class="fa fa-arrow-circle-right"></i> Manage Category
								</a>
                            </li>
							<?php } ?>
							<?php if($blog==1){ ?>
							<li>
								<a href="<?php echo base_url('index.php/blog/listing/'); ?>">
									<i class="fa fa-arrow-circle-right"></i> Manage Blog
								</a>
							</li>
							<?php } ?>
							<?php if($edit_blogbanner==1){ ?>
							<li>
								<a href="<?php echo base_url('index.php/admin/edit_blogbanner/'); ?>">
									<i class="fa fa-arrow-circle-right"></i> Blog Banner
								</a>
							</li>
							<?php } ?>
                        </ul>
                    </li>
					<li>
					<?php if($user==1 || $userList==1 || $adduser==1){ ?>
                        <a href="javascript:;" data-toggle="collapse" data-target="#demo">
							<i class="fa fa-fw fa-arrows-v"></i>Manage Users<i class="fa fa-fw fa-caret-down"></i>
						</a>
					<?php } ?>	
                        <ul id="demo" class="collapse">
						<?php if($user==1){ ?>
							<li>
                                <a href="<?php echo base_url('index.php/user/listing'); ?>">
								<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
								Normal Users</a>
                            </li>
							<?php } ?>
						<?php if($userList==1){ ?>
                            <li>
                                <a href="<?php echo base_url('index.php/admin/userList'); ?>">
								<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
								Vender List</a>
                            </li>
							<?php } ?>
						<?php if($adduser==1){ ?>
                            <li>
                                <a href="<?php echo base_url('index.php/admin/adduser'); ?>">
								<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
								Add Vender</a>
                            </li>
							<?php } ?>
						<?php if($allstore==1){ ?>
							<li>
								<a href="<?php echo base_url('index.php/vendor/allstore'); ?>">
								<i class="fa fa-arrow-circle-right" aria-hidden="true"></i> Manage Vendor Store
								</a>                        
							</li>
							<?php } ?>
                        </ul>
                    </li>   
						
					<li>
					<?php if($categorycontroller1==1 ){ ?>
                        <a href="javascript:;" data-toggle="collapse" data-target="#cat">
							<i class="fa fa-fw fa-arrows-v"></i>Manage Category<i class="fa fa-fw fa-caret-down"></i>
						</a>
					<?php } ?>	
                        <ul id="cat" class="collapse">
						<?php if($categorycontroller1==1){ ?>
                            <li>
                                <a href="<?php echo base_url('index.php/categorycontroller/listing'); ?>">
								<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
								Category Listing</a>
                            </li>
							<?php } ?>
                        </ul>
                    </li>
					
					<li>
					<?php if($adminproducts==1 || $csv==1 || $pincode==1){ ?>
                        <a href="javascript:;" data-toggle="collapse" data-target="#product">
							<i class="fa fa-fw fa-arrows-v"></i>Manage Products<i class="fa fa-fw fa-caret-down"></i>
						</a>
						
					<?php } ?>	
                        <ul id="product" class="collapse">
						<?php if($adminproducts==1){ ?>
                            <li>
                                <a href="<?php echo base_url('index.php/adminproducts/listing'); ?>">
								<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
								Product  Listing</a>
                            </li>
							<?php } ?>
							<?php if($csv==1){ ?>
							 <li>
                                <a href="<?php echo base_url('index.php/csv/'); ?>">
								<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
								Upload Csv</a>
                            </li>
							<?php } ?>
							<?php if($pincode==1){ ?>
							<li>
                                <a href="<?php echo base_url('index.php/pincode/listing'); ?>">
								<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
								Pincode  Listing</a>
                            </li>
							<?php } ?>

							<?php if($admin_data[0]->id==1){ ?>
							<li>
                                <a href="<?php echo base_url('index.php/pincode/hsn_list'); ?>">
								<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
								HSN Listing</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('index.php/pincode/taxcode_list'); ?>">
								<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
								Tax Codes Listing</a>
                            </li>
							<?php } ?>
                        </ul>
                    </li>
					<li>
					<?php if($filters==1){ ?>
                        <a href="javascript:;" data-toggle="collapse" data-target="#filters">
							<i class="fa fa-fw fa-arrows-v"></i>Manage Filters<i class="fa fa-fw fa-caret-down"></i>
						</a>
					<?php } ?>	
                        <ul id="filters" class="collapse">
						<?php if($filters==1){ ?>
                            <li>
                                <a href="<?php echo base_url('index.php/filters/listing'); ?>">
								<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
								Filters Master</a>
                            </li>
							<?php } ?>
                        </ul>
                    </li>
					
					<li>
					<?php if($offer==1){ ?>
                        <a href="javascript:;" data-toggle="collapse" data-target="#coupon">
                            <i class="fa fa-fw fa-arrows-v"></i>Manage Coupon Code<i class="fa fa-fw fa-caret-down"></i>
                        </a>
					<?php } ?>	
                        <ul id="coupon" class="collapse">
						<?php if($offer==1){ ?>
                            <li>
                                <a href="<?php echo base_url('index.php/offer/listing'); ?>">
                                    <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
                                    Manage Coupon Listing
                                </a>
                            </li>
                            <?php } ?>
                        </ul>
                    </li>
					
					<li>
					<?php if($faq==1){ ?>
                        <a href="javascript:;" data-toggle="collapse" data-target="#faq">
							<i class="fa fa-fw fa-arrows-v"></i>Manage Faq<i class="fa fa-fw fa-caret-down"></i>
						</a>
					<?php } ?>	
                        <ul id="faq" class="collapse">
						<?php if($faq==1){ ?>
                            <li>
                                <a href="<?php echo base_url('index.php/faq/listing'); ?>">
									<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
									FAQ Listing
								</a>
                            </li>	
						<?php } ?>
								
                        </ul>
                    </li>
					
					<li>
					<?php if($cms==1){ ?>
                        <a href="javascript:;" data-toggle="collapse" data-target="#cms">
							<i class="fa fa-fw fa-arrows-v"></i>Manage Cms<i class="fa fa-fw fa-caret-down"></i>
						</a>
					<?php } ?>	
                        <ul id="cms" class="collapse">
						<?php if($cms==1){ ?>
                            <li>
                                <a href="<?php echo base_url('index.php/cms/listing'); ?>">
									<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
									Page Listing
								</a>
                            </li>
							<?php } ?>
                        </ul>
                    </li>
					
					<li>
					<?php if($newsletter==1){ ?>
                        <a href="javascript:;" data-toggle="collapse" data-target="#newsletter">
							<i class="fa fa-fw fa-arrows-v"></i>News Letter<i class="fa fa-fw fa-caret-down"></i>
						</a>
					<?php } ?>	
                        <ul id="newsletter" class="collapse">
						<?php if($newsletter==1){ ?>
                            <li>
                                <a href="<?php echo base_url('index.php/newsletter/listing'); ?>">
									<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
									News Letter
								</a>
                            </li>
							<?php } ?>
                        </ul>
                    </li>
					
					<li>
					<?php if($our_client==1){ ?>
                        <a href="javascript:;" data-toggle="collapse" data-target="#clint">
							<i class="fa fa-fw fa-arrows-v"></i>Our Clients<i class="fa fa-fw fa-caret-down"></i>
						</a>
					<?php } ?>	
                        <ul id="clint" class="collapse">
							<?php if($our_client==1){ ?>
                            <li>
                                <a href="<?php echo base_url('index.php/our_client/listing'); ?>">
									<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
									Our Clients
								</a>
                            </li>
							<?php } ?>
                        </ul>
                    </li>
					
					<li>
					<?php if($listing==1 || $report==1){ ?>
                        <a href="javascript:;" data-toggle="collapse" data-target="#order">
							<i class="fa fa-fw fa-arrows-v"></i>Manage Orders<i class="fa fa-fw fa-caret-down"></i>
						</a>
					<?php } ?>
                        <ul id="order" class="collapse">
						<?php if($listing==1){ ?>
                            <li>
                                <a href="<?php echo base_url('index.php/order/listing/'); ?>">
								<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
								Orders Listing</a>
                            </li>
							<?php } ?>
							<?php if($report==1){ ?>
							<li>
                                <a href="<?php echo base_url('index.php/order/report/'); ?>">
								<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
								Orders Report</a>
                            </li>
							<?php } ?>
							<?php if($report==1){ ?>
							<li>
                                <a href="<?php echo base_url('index.php/order/shipment_payment_report/'); ?>">
								<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
								Shipment Payment Report</a>
                            </li>
							<?php } ?>
                        </ul>
                    </li>
					<li>
					<?php if($admin_meta_tag==1 || $block==1){ ?>
                        <a href="javascript:;" data-toggle="collapse" data-target="#metatg">
							<i class="fa fa-fw fa-arrows-v"></i>Manage Meta Tag<i class="fa fa-fw fa-caret-down"></i>
						</a>
					<?php } ?>	
                        <ul id="metatg" class="collapse">
						<?php if($admin_meta_tag==1){ ?>
                            <li>
                                <a href="<?php echo base_url('index.php/admin_meta_tag/'); ?>">
								<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
								Meta Tag</a>
                            </li>
                             <li>
                                <a href="<?php echo base_url('index.php/add_pages/'); ?>">
								<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
								Page Creation</a>
                            </li>
							<?php } ?>
                        </ul>
                    </li>
					<?php if($block==1){ ?>
					<li>
                        <a href="<?php echo base_url('index.php/block/listing/'); ?>">
							<i class="fa fa-fw fa-arrows-v"></i>Manage Static Block</i>
						</a>
                    </li>
					<?php } ?>	
					<li>
					<?php if($distribute_vendor_money==1 || $seller_credit_settlment==1 || $vandor_income==1 || $vandor_return_deductions==1){ ?>
                        <a href="javascript:;" data-toggle="collapse" data-target="#vendor">
							<i class="fa fa-fw fa-arrows-v"></i>Vendor Income<i class="fa fa-fw fa-caret-down"></i>
						</a>
					<?php } ?>
                        <ul id="vendor" class="collapse">
						
						<?php if($distribute_vendor_money==1){ ?>
							<li>
                                <a href="<?php echo base_url('index.php/admin/setting/'); ?>">
								<i class="fa fa-arrow-circle-right" aria-hidden="true"></i> Calculation Setting</a>
                            </li>
							<li>
                                <a href="<?php echo base_url('index.php/admin/distribute_vendor_money/'); ?>">
								<i class="fa fa-arrow-circle-right" aria-hidden="true"></i> Distribute Income</a>
                            </li>
							<?php } ?>
							<?php if($seller_credit_settlment==1){ ?>
							<li>
                                <a href="<?php echo base_url('index.php/admin/seller_credit_settlment/'); ?>">
								<i class="fa fa-arrow-circle-right" aria-hidden="true"></i> Seller Credit Settlement</a>
                            </li>
							<?php } ?>
							<?php if($vandor_income==1){ ?>
                            <li>
                                <a href="<?php echo base_url('index.php/admin/vandor_income/'); ?>">
								<i class="fa fa-arrow-circle-right" aria-hidden="true"></i> Income</a>
                            </li>	
							<?php } ?>
							<?php if($vandor_return_deductions==1){ ?>							
							<li>
                                <a href="<?php echo base_url('index.php/admin/vandor_return_deductions/'); ?>">
								<i class="fa fa-arrow-circle-right" aria-hidden="true"></i> Return Deductions</a>
                            </li>
							<?php } ?>
                        </ul>
                    </li>
                    <li>
					<?php //if($distribute_vendor_money==1 || $seller_credit_settlment==1 || $vandor_income==1 || $vandor_return_deductions==1){ ?>
                        <a href="javascript:;" data-toggle="collapse" data-target="#pincode_settings">
							<i class="fa fa-fw fa-arrows-v"></i>PinCode Settings<i class="fa fa-fw fa-caret-down"></i>
						</a>
					<?php //} ?>
                        <ul id="pincode_settings" class="collapse">
						
						<?php //if($distribute_vendor_money==1){ ?>
							<li>
                                <a href="<?php echo base_url('index.php/admin/manage_pincode/'); ?>">
								<i class="fa fa-arrow-circle-right" aria-hidden="true"></i> Manage PinCode</a>
                            </li>
                        </ul>
                    </li>        	

							<?php if($help_desk==1){ ?>
							<li class="active">
								<a href="<?php echo base_url('index.php/admin/help_desk'); ?>"><i class="fa fa-fw fa-dashboard"></i> Admin Helpdesk</a>
							</li>
							<?php } ?>
            </div>