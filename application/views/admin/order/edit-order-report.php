<?php 
$admin_id = $this->session->userdata('admin_id'); 
if(empty($admin_id))
{
	redirect('index.php/admin');
}	
?>
<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/layout/head'); ?>
<body>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <?php $this->load->view('admin/layout/header'); ?>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <?php $this->load->view('admin/layout/left-menu'); ?>
            <!-- /.navbar-collapse -->
        </nav>
        <div id="page-wrapper">
            <div class="container-fluid">
                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h2 class="page-header">Edit Order Report</h2>
                         <form role="form" method="post" enctype="multipart/form-data">		 
                            <div class="form-group">
                                <label>Order Cancellation Date</label>
                                <input type="text" class="form-control" placeholder="Order Cancellation Date" name="orderdatecancel" value="<?php  echo $order[0]->orderdatecancel ?>">
                            </div>							
							<div class="form-group">
                                <label>Requested for</label>
                                <select name="requestfor" class="form-control">
                                    <option <?php echo ($order[0]->requestfor=='Replacement')?'selected':''; ?>>Replacement</option>
                                    <option <?php echo ($order[0]->requestfor=='Refund')?'selected':''; ?>>Refund</option>
									<option <?php echo ($order[0]->requestfor=='Cancel')?'selected':''; ?>>Cancel</option>
                                </select>
                            </div>  <!---ALTER TABLE `tbl_order` ADD `request_for_comment` VARCHAR(250) NOT NULL AFTER `requestfor`;--->
							<div class="form-group">
                                <label>Requested for Comment</label>
                                <input type="text" class="form-control"  name="qqqqqq" value="<?php echo $order[0]->request_for_comment; ?>">
                            </div>   
							
							<div class="form-group">
                                <label>Request Date</label>
                                <input type="text" class="form-control" placeholder="Request Date" name="requestdate" value="<?php  echo $order[0]->requestdate ?>">
                            </div>
							<div class="form-group">
                                <label>Request Approval Status</label>
                                <select name="requestapprovalstatus" class="form-control">
                                    <option <?php echo ($order[0]->requestapprovalstatus=='Approved')?'selected':''; ?>>Approved</option>
                                    <option <?php echo ($order[0]->requestapprovalstatus=='Disapproved' ||$order[0]->requestapprovalstatus=='')?'selected':''; ?>>Disapproved</option>
                                </select>
                            </div>
							<div class="form-group">
                                <label>Status Approval Date</label>
                                <input type="text" class="form-control" placeholder="Status Approval Date" name="statusapprovaldate" value="<?php  echo $order[0]->statusapprovaldate ?>">
                            </div>
							<div class="form-group">
                                <label>Payment Status</label>
                                <select name="payment_status" class="form-control">
                                    <option <?php echo ($order[0]->payment_status=='paid')?'selected':''; ?>>paid</option>
                                    <option <?php echo ($order[0]->payment_status=='unpaid')?'selected':''; ?>>unpaid</option>
									<option <?php echo ($order[0]->payment_status=='refunded')?'selected':''; ?>>refunded</option>
<option <?php echo ($order[0]->payment_status=='pending for approval')?'selected':''; ?>>pending for approval</option>
<option <?php echo ($order[0]->payment_status=='reffer to approval')?'selected':''; ?>>reffer to approval</option>
<option <?php echo ($order[0]->payment_status=='unapproved')?'selected':''; ?>>unapproved</option>
<option <?php echo ($order[0]->payment_status=='approved')?'selected':''; ?>>approved</option>
                                </select>
                            </div>
							<div class="form-group">
                                <label> Status</label>
                                <select name="status" class="form-control">
									<option value="">Select Status</option>
									<option <?php echo ($order[0]->status=='ready to pack')?'selected':''; ?> value="ready to pack">Ready to pack</option>
									<option <?php echo ($order[0]->status=='pending to dispatch')?'selected':''; ?> value="pending to dispatch">Pending to dispatch</option>
									<option <?php echo ($order[0]->status=='shipment ready to dispatch')?'selected':''; ?> value="shipment ready to dispatch">Shipment ready to dispatch</option>
									<option <?php echo ($order[0]->status=='shipment from vendor point')?'selected':''; ?> value="shipment from vendor point">Shipment from vendor point</option>
									<option <?php echo ($order[0]->status=='delivery under proccess')?'selected':''; ?> value="delivery under proccess">Delivery under proccess</option>  
									<option <?php echo ($order[0]->status=='delivered successfully')?'selected':''; ?> value="delivered successfully">Delivered successfully</option>
									<option <?php echo ($order[0]->status=='cancelled')?'selected':''; ?> value="cancelled">Cancelled</option>
									<option <?php echo ($order[0]->status=='disputed')?'selected':''; ?> value="disputed">Disputed</option>
									<option <?php echo ($order[0]->status=='pending')?'selected':''; ?> value="pending">Pending</option>
									<option <?php echo ($order[0]->status=='under process')?'selected':''; ?> value="under process">Under Process</option>
								</select>
                            </div>
							<div class="form-group">
                                <label>Admin Comment</label>
                                <input type="text" class="form-control" placeholder="Admin Comment" name="admin_comment" value="<?php  echo $order[0]->admin_comment ?>">
                            </div>
                            <button type="submit" name="updatedata" class="btn btn-default">Submit Button</button>
                            <button type="reset" class="btn btn-default">Reset Button</button>
                        </form>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
<?php $this->load->view('admin/layout/footer-js'); ?>
</body>

</html>
