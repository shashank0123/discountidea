<?php 
$admin_id = $this->session->userdata('admin_id'); 
if(empty($admin_id))
{
	redirect('index.php/admin');
}	//
?>
<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/layout/head'); ?>
<body>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <?php $this->load->view('admin/layout/header'); ?>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <?php $this->load->view('admin/layout/left-menu'); ?>
            <!-- /.navbar-collapse -->
        </nav>
        <div id="page-wrapper">
            <div class="container-fluid">
                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h2 class="page-header">Edit Order Report</h2>
                         <form role="form" method="post" enctype="multipart/form-data">		 
                            						
							<div class="form-group">
                                <label>Requested for</label>
                                <select name="requestfor" class="form-control" disabled>
                                    <option <?php echo ($order[0]->requestfor=='Replacement')?'selected':''; ?>>Replacement</option>
                                    <option <?php echo ($order[0]->requestfor=='Refund')?'selected':''; ?>>Refund</option>
									<option <?php echo ($order[0]->requestfor=='Cancel')?'selected':''; ?>>Cancel</option>
                                </select>
                            </div>  <!---ALTER TABLE `tbl_order` ADD `request_for_comment` VARCHAR(250) NOT NULL AFTER `requestfor`;--->
							<div class="form-group"style="display:none;">
                                <label>Requested for Comment</label>
                                <input type="text" class="form-control"  name="qqqqqq" value="<?php echo $order[0]->request_for_comment; ?>">
                            </div>   
							<div class="form-group">
                                <label>Cancellation Transaction ID</label>
                                <input type="text" class="form-control"  name="cancellation_transaction_id" value="<?php echo $order[0]->cancellation_transaction_id; ?>">
                            </div>  
							<div class="form-group">
                                <label>Action Date</label>
                                <input readonly type="text" class="form-control" placeholder="Request Date" name="requestdate" value="<?php  echo $order[0]->requestdate ?>">
                            </div>
							<div class="form-group">
                                <label>Request Approval Status</label>
                                <select name="requestapprovalstatus" class="form-control">
                                    <option <?php echo ($order[0]->requestapprovalstatus=='Refund Pending')?'selected':''; ?> value="Refund Pending">Refund Pending</option>
                                    <option <?php echo ($order[0]->requestapprovalstatus=='Refund to Buyer done')?'selected':''; ?> value="Refund to Buyer done">Refund to Buyer done</option>
									<option <?php echo($order[0]->requestapprovalstatus=='Cancellation Approvedoved')?'selected':''; ?> value="Cancellation Approved">Cancellation Approved</option>
									<option <?php echo($order[0]->requestapprovalstatus=='Inspection under Process')?'selected':''; ?> value="Inspection under Process">Inspection under Process</option>
									<option <?php echo($order[0]->requestapprovalstatus=='Vendor Communication under process')?'selected':''; ?> value="Vendor Communication under process">Vendor Communication under process</option>
									<option <?php echo($order[0]->requestapprovalstatus=='Replacement Approved')?'selected':''; ?> value="Replacement Approved">Replacement Approved</option>
									<option <?php echo($order[0]->requestapprovalstatus=='Reffered to Approval')?'selected':''; ?> value="Reffered to Approval">Reffered to Approval</option>
									<option <?php echo($order[0]->requestapprovalstatus=='Pendding to Approval')?'selected':''; ?> value="Pendding to Approval">Pendding to Approval</option>
									<option <?php echo($order[0]->requestapprovalstatus=='Request Not Approved')?'selected':''; ?> value="Request Not Approved">Request Not Approved</option>
									<option <?php echo($order[0]->requestapprovalstatus=='Refund Approved')?'selected':''; ?> value="Refund Approved">Refund Approved</option>
									<option <?php echo($order[0]->requestapprovalstatus=='Pre-Payment Cancellation Approved')?'selected':''; ?> value="Pre-Payment Cancellation Approved">Pre-Payment Cancellation Approved</option>
                                </select>
                            </div>
							<div class="form-group">
                                <label> Status</label>
                                <select name="status" class="form-control">
									<option value="">Select Status</option>
									<option <?php echo ($order[0]->status=='ready to pack')?'selected':''; ?> value="ready to pack">Ready to pack</option>
									<option <?php echo ($order[0]->status=='pending to dispatch')?'selected':''; ?> value="pending to dispatch">Pending to dispatch</option>
									<option <?php echo ($order[0]->status=='shipment ready to dispatch')?'selected':''; ?> value="shipment ready to dispatch">Shipment ready to dispatch</option>
									<option <?php echo ($order[0]->status=='shipment from vendor point')?'selected':''; ?> value="shipment from vendor point">Shipment from vendor point</option>
									<option <?php echo ($order[0]->status=='delivery under proccess')?'selected':''; ?> value="delivery under proccess">Delivery under proccess</option>  
									<option <?php echo ($order[0]->status=='delivered successfully')?'selected':''; ?> value="delivered successfully">Delivered successfully</option>
									<option <?php echo ($order[0]->status=='cancelled')?'selected':''; ?> value="cancelled">Cancelled</option>
									<option <?php echo ($order[0]->status=='disputed')?'selected':''; ?> value="disputed">Disputed</option>
									<option <?php echo ($order[0]->status=='pending')?'selected':''; ?> value="pending">Pending</option>
									<option <?php echo ($order[0]->status=='under process')?'selected':''; ?> value="under process">Under Process</option>
								</select>
                            </div>
							<div class="form-group">
                                <label>Admin Comment</label>
                                <input type="text" class="form-control" placeholder="Admin Comment" name="admin_comment" value="">
                            </div>
                            <button type="submit" name="updatedata" class="btn btn-default">Submit Button</button>
                            <button type="reset" class="btn btn-default">Reset Button</button>
							<br><br>
							<?php $comments =   $this->user_model->select_order_comment($order[0]->id); ?>
							<?php foreach($comments as $comment){ ?>
								<b>By <?php echo($comment->user=='admin')?'admin':'user'; ?>, <?php echo date('d-m-Y h:s a',$comment->comment_time); ?></b><br>
								<?php echo $comment->comment; ?><br><br>
							<?php } ?>
							
							
                        </form>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
<?php $this->load->view('admin/layout/footer-js'); ?>
</body>

</html>
