<?php 
$admin_id = $this->session->userdata('admin_id'); 
if(empty($admin_id))
{
	redirect('index.php/admin');
}	
?>
<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/layout/head'); ?>
<body>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <?php $this->load->view('admin/layout/header'); ?>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <?php $this->load->view('admin/layout/left-menu'); ?>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h2 class="page-header">Orders Report</h2>
						<?php echo $this->session->flashdata('message'); ?>						
							<div class="table-responsive">
								<table class="table table-bordered table-hover">
									<thead>
										<tr>
											<th>Order ID</th>
											<th>Product Name</th>
											<th>Product ID</th>
											<th>QTY</th>
											<th>Item Amount</th>
											<th>Discount Points</th>
											<th>Paid By Customer</th>
											<th>Order Time</th>
											<th>Vendor ID</th>
											<th>Vendor Name</th>
											<th>Vendor Email</th>
											<th>Vendor Mobile</th>
											<th>Vendor Address</th>
											<th>AWB ID</th>		
											<th>Courier Company</th>
											<th>Delivery Address</th>
											<th>Expected Dispatch Date</th>
											<th>Expected Delivery Date</th>	
                                            <th>Customer Name</th>	
											<th>Customer Email</th>
											<th>Customer Mobile</th>											
											<th>Customer Address</th>
											<th>Customer Pincode</th>											
											<th>Order Cancellation Date</th>
											<th>Requested for</th>
											<th>Request Date</th>
											<th>Request Approval Status</th>
                                            <th>Status Approval Date</th>											
											<th>Transaction ID</th>
											<th>Payment Status</th>
<th>Payment Stage</th>
											<th>Status</th>
											<th>Status Date</th>
											<th>Sub Admin Comments</th>
											<th>Sub Admin ID</th>
											<th></th>
										</tr>
									</thead>
									<tbody>								
										<?php foreach($orderdetail as $data){ $id=$data->id; $user_id=$data->user_id;
										$customer=$this->orderdetail_model->selectusertable($user_id);									
										?>
										<?php $items = $this->shoppingdetail_model->prodcutdetail($data->id); ?>
										<?php $vendor = $this->user_model->select_vendor_by_product_id($data->id); ?>
										<?php  $shipping = $this->shoppingdetail_model->shippingdetail($data->id) ?>
										<?php if(count($items)>0){ ?>
										<?php foreach($items as $item){ ?>
										<tr>
											<td><?php echo $data->id; ?></td>
											<td><?php echo $item->pro_name; ?></td>
											<td>DI-<?php echo $item->pro_id; ?></td>
											<td><?php echo $item->qty; ?></td>											
											<td>Rs. <?php echo $item->total_amount; ?></td>
											<td>Rs. <?php echo $item->discountvalue; ?></td>
											<td>Rs. <?php echo $item->finaltopay; ?></td>
											<td><?php echo date('Y-m-d h:i a',$data->ord_time); ?></td>
											<td><?php echo $vendor['id']; ?></td>
											<td><?php echo $vendor['name']; ?></td>
											<td><?php echo $vendor['email']; ?></td>
											<td><?php echo $vendor['mobile']; ?></td>
											<td><?php echo $vendor['address']; ?></td>
											<td>AWD ID</td>
											<td>CP</td>
											<td><?php echo $shipping[0]->address; ?><br>							
											<?php echo $shipping[0]->city;?><br>
											<?php echo $shipping[0]->state;?><br>
											<?php echo $shipping[0]->country?></td>
											<td><?php echo date('Y-m-d', strtotime('+3 day',$data->ord_time)) ?></td>	
											<td><?php echo date('Y-m-d', strtotime('+2 day',$data->ord_time)) ?></td>
											<td><?php echo $customer[0]->fname; ?></td>
											<td><?php echo $customer[0]->email; ?></td>
											<td><?php echo $customer[0]->mobile; ?></td>
											<td><?php echo $customer[0]->address; ?></td>
											<td><?php echo $customer[0]->pincode; ?></td>
											<td><?php echo $data->orderdatecancel; ?></td>
											<td><?php echo $item->user_cancel!='0' ? $item->user_cancel:" "; ?></td>
											<td><?php echo $data->requestdate; ?></td>
											<td><?php echo $item->refund; ?></td>
											<td><?php echo $data->statusapprovaldate; ?></td>
											<td><?php echo $data->transaction_id; ?></td>
<td><?php echo $data->payment_status; ?></td>
<td><?php if($data->payment_status=='paid'){echo 'paid to bank';}else{echo ($data->payment_status=='approved')?'ready to bank credit':'approval under process';} ?></td>
											<td><?php echo ucfirst($data->status); ?></td>
                                                                                        
											<td><?php echo $data->statusdata; ?></td>
											<td><?php echo $data->admin_comment; ?></td>
											<td><?php echo $data->adminid; ?></td>
											<td>
												<a href="<?php echo base_url('index.php/order/edit_order/'.$data->id.'/'.$item->pro_id); ?>">Edit</a>
											</td>
										</tr>
										<?php } } } ?>                                       
									</tbody>
								</table>
							</div>
						</div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
<?php $this->load->view('admin/layout/footer-js'); ?>
</body>

</html>
