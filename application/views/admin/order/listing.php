      <?php 
$admin_id = $this->session->userdata('admin_id'); 
if(empty($admin_id))
{
	redirect('index.php/admin');
}	
?>
<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/layout/head'); ?>
<body>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <?php $this->load->view('admin/layout/header'); ?>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <?php $this->load->view('admin/layout/left-menu'); ?>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h2 class="page-header">Order Detail
							<a href="<?php echo base_url('order/revers_orders'); ?>"><button class="btn pull-right" ><b>Revers Order Request</b></button></a>
						</h2>
						<?php echo $this->session->flashdata('message'); ?>	
						<form method="post">
							<div style="text-align:right; width:100%; padding-bottom:10px;">
								<select name="status" class="form-control" style="display: inline; width: 25%;" required>
									<option value="">Select Status</option>
									<option value="ready to pack">Ready to pack</option>
									<option value="pending to dispatch">Pending to dispatch</option>
									<option value="shipment ready to dispatch">Shipment ready to dispatch</option>
									<option value="shipment from vendor point">Shipment from vendor point</option>
									<option value="delivery under proccess">Delivery under proccess</option>  
									<option value="delivered successfully">Delivered successfully</option>
									<option value="cancelled">Cancelled</option>
									<option value="disputed">Disputed</option>
									<option value="pending">Pending</option>
									<option value="under process">Under Process</option>
								</select>	
								<button class="btn" type="submit" name="submitstatus">Submit</button>
							</div>
							<div class="table-responsive">
								<table class="table table-bordered table-hover">
									<thead>
										<tr>
											<th width="5%">S.No.</th>
											<th>Order ID</th>
											<th>Name</th>
											<th>Email</th>
											<th>Total Amount</th>
											<th>Status</th>
											<th></th>
										</tr>
									</thead>
									<tbody>								
										<?php $i=0; foreach($orderdetail as $data){ $i++; ?>                                    
										<?php
										$id=$data->id;
										$id1=$data->user_id;
										$qry=$this->orderdetail_model->selectusertable($id1);									
										?>
										<tr>
											<td><input type="checkbox" name="order_ids[]" value="<?php echo $data->id; ?>"></td>
											<td>#<?php echo $data->id; ?></td>
											<td><?php echo $qry[0]->fname; ?></td>
											<td><?php echo $qry[0]->email; ?></td>
											<td><?php echo $data->total_amount; ?></td>
											<td><?php echo ucfirst($data->status); ?></td>		
											<td>
												<a href="<?php echo base_url('index.php/order/details/'.$data->id);?>"><i class="fa fa-eye" aria-hidden="true"></i></a>
											</td>
											</td>
										</tr>
									<?php } ?>                                       
									</tbody>
								</table>
							</div>
						</form>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
<?php $this->load->view('admin/layout/footer-js'); ?>
</body>

</html>
