<?php 
$admin_id = $this->session->userdata('admin_id'); 
if(empty($admin_id))
{
	redirect('index.php/admin');
}	
?>
<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/layout/head'); ?>
<body>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <?php $this->load->view('admin/layout/header'); ?>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <?php $this->load->view('admin/layout/left-menu'); ?>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h2 class="page-header">Shipment Payment  Report</h2>
						<?php echo $this->session->flashdata('message'); ?>						
							<div class="table-responsive">
								<table class="table table-bordered table-hover">
									<thead>
			                <tr>
								<th>S.No.</th>
								<th>Order ID</th>
								<th>Vendor Name</th>
								<th>Mobile</th>
								<th>Delivery Company</th>
								<th>Transaction ID</th>
								<th>Shipment Amount</th>
								<th>Product id</th>
								<th>Product Name</th>
								<th>Order Amount</th>
								<th>Status</th>
								<th>Date</th>
			                </tr>
            			</thead>
			            <tbody>
						    <?php if(count($orderdata)>0){ ?>
							<?php $i=0; foreach($orderdata as $data){ $i++; ?>	
								<tr>
									<td><?php echo $i; ?></td>
									<td><?php echo $data->order_id; ?></td>
									<td><?php echo ucfirst($data->fname.' '.$data->lname);  ?></td>
									<td><?php echo $data->mobile; ?></td>
									<td><?php echo ucfirst($data->name); ?></td>
									<td><?php echo $data->mihpayid; ?></td>
									<td><?php echo $data->amount; ?></td>

									 <?php
									 $result=$this->orderdetail_model->selectorderitme($data->order_id);   
									 print "<td>"	;	
									 foreach ($result as $key ) {
									 	
									 	echo ucfirst($key->pro_id.'<br>');
									 }
									  print "</td>"	;
									 print "<td>"	;	
									 foreach ($result as $key ) {
									 	
									 	echo ucfirst($key->pro_name.'<br>');
									 }
									  print "</td>";					
									 ?>

									<td><?php echo $data->total_amount; ?></td>
									<td><?php echo $data->status; ?></td>
									<td><?php echo $data->added_date; ?></td>
								</tr>

								<?php }} ?>
			            	</tbody>
							</div>
						</div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
<?php $this->load->view('admin/layout/footer-js'); ?>
</body>

</html>
