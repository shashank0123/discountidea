<?php 
$admin_id = $this->session->userdata('admin_id'); 
if(empty($admin_id))
{
	redirect('index.php/admin');
}	
?>
<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/layout/head'); ?>
<body>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <?php $this->load->view('admin/layout/header'); ?>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <?php $this->load->view('admin/layout/left-menu'); ?>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        
						<?php echo $this->session->flashdata('message'); ?>	
						<div class="table-responsive">
                             <table class="table table-bordered table-hover">
				<thead>
					<tr>
						<td class="text-left" colspan="2"><h4 class="page-header">Order Information</h4></td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="text-left"> 
							<b>Order ID:</b> #<?php echo $order[0]->id; ?><br>
							<b>Date Added:</b> <?php echo date('d/m/Y',$order[0]->ord_time); ?>
						</td>
						<td class="text-left"> 
							<b>Payment Method:</b> <?php echo $order[0]->payment_method; ?><br>
						</td>
					</tr>
				</tbody>
			</table>
			<?php $billing = $this->shoppingdetail_model->billingdetail($order[0]->id); ?>
			<?php $shipping = $this->shoppingdetail_model->shippingdetail($order[0]->id);

//print_r($shipping);
			 ?>
			<table class="table table-bordered table-hover">
				<thead>
					<tr>
						<td class="text-left"><b>Billing Address</b></td>
						<td class="text-left"><b>Shipping Address</b></td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="text-left">
							<?php echo $billing[0]->firstname.' '.$billing[0]->lastname; ?><br>
							<?php echo $billing[0]->mobile;?><br>
							<?php echo $billing[0]->address; ?><br>	
                            <?php echo $billing[0]->pincode?><br> 							
							<?php echo $billing[0]->city;?><br>
							<?php echo $billing[0]->state;?><br>
							<?php echo $billing[0]->country?>							
						</td>
						<td class="text-left">
							<?php echo $shipping[0]['firstname'].' '.$shipping[0]['lastname']; ?><br>
							<?php //echo $shipping[0]->mobile;?>
							<?php echo $shipping[0]['address']; ?><br>	
                            <?php echo $shipping[0]['pincode']?><br> 							
							<?php echo $shipping[0]['city'];?><br>
							<?php echo $shipping[0]['state'];?><br>
							<?php echo $shipping[0]['country']?>
						</td>
					</tr>
				</tbody>
			</table>
			<table class="table table-bordered table-hover">
				<thead>
					<tr>
						<td class="text-left">Product Name</td>
						<td class="text-right">Quantity</td>
						<td class="text-right">Gross Amount</td>
						<td class="text-right">Discount</td>
						<td class="text-right">Taxable Value</td>
						<td class="text-right">IGST OR (CGST+SGST)</td>
						<td class="text-right">Total</td>
					</tr>
				</thead>
				<tbody>
				   <?php
				   	$items = $this->shoppingdetail_model->prodcutdetail($order[0]->id);
				   	function get_net_amount($amount,$tax)
					{
						return $amount/(1+($tax/100));
					}
					if(count($items)>0){ 
				   		$discountv = "";
					   	$totalTaxable = 0;
					   	$totalTax = 0;
					   	$totalAmount = 0;
				   		
				   		foreach($items as $item)
				   		{
				   			$discountv +=$item->discountvalue;
							$unitPrice = $item->unit_price;
							$discountValue = $item->discountvalue;
							$taxableValue = get_net_amount($item->total_amount,$item->gst_percentage);
							$tax = ($unitPrice-$taxableValue);
							$total = $item->finaltopay;
					   		$totalTaxable += $taxableValue;
							$totalTax += $tax;
					   		$totalAmount += $total;
				   	?>
					<tr>
						<td class="text-left"><?php echo $item->pro_name; ?>
						<?php if(!empty($item->color) && $item->color!='0'){ ?><br>
						&nbsp;<small><b>Color :</b> <?php echo $item->color; ?></small>
						<?php } ?>
						<?php if(!empty($item->size) && $item->size!='0'){ ?><br>
						&nbsp;<small><b>Size :</b> <?php echo $item->size; ?></small>
						<?php } ?>
						</td>
						<td class="text-right"><?php echo $item->qty; ?></td>
						<td class="text-right"><?php echo CURRENCY.number_format($unitPrice,2); ?></td>
						<td class="text-right"><?php echo CURRENCY.number_format($discountValue,2); ?></td>
						<td class="text-right"><?php echo CURRENCY.number_format($taxableValue,2); ?></td>
						<td class="text-right"><?php echo CURRENCY.number_format($tax,2); ?></td>
						<td class="text-right"><?php echo CURRENCY.number_format($total,2); ?></td>
					</tr>
				   <?php } ?> 
				   <?php } ?>
				</tbody>
				<tfoot>
					<tr>
						<td class="text-right" colspan="6"><b>Grand Total</b></td>						
						<td class="text-right"><?php echo CURRENCY.number_format($totalTaxable,2); ?></td>
					</tr>
					<tr>
						<td class="text-right" colspan="6"><b>Shipping</b></td>
						<td class="text-right">Free</td>
					</tr>
					<tr>
						<td colspan="6" class="text-right"><b>Tax</b></td>
						<td class="text-right"><?php echo CURRENCY.number_format($totalTax,2); ?></td>
					</tr>
					<tr>
						<td class="text-right" colspan="6"><b>Total</b></td>
						<td class="text-right"><?php echo CURRENCY.number_format($totalAmount,2); ?></td>
					</tr>
				</tfoot>
			</table>
                        </div>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
<?php $this->load->view('admin/layout/footer-js'); ?>
</body>

</html>
