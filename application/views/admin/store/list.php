<?php 
$admin_id = $this->session->userdata('admin_id'); 
if(empty($admin_id))
{
	redirect('index.php/admin');
}	
?>
<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/layout/head'); ?>
<body>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <?php $this->load->view('admin/layout/header'); ?>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <?php $this->load->view('admin/layout/left-menu'); ?>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Store List</h1>
                         <?php echo $this->session->flashdata('message'); ?>
						 <div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
										<th>S.No.</th>
                                        <th>Store Name</th>
										<th>Vendor ID</th>
										<th>Status</th>										
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
								<?php $i=0; foreach($store as $data){ $i++; ?>
                                    <tr>
										<td><?php echo $i; ?></td>
                                        <td><?php echo $data->title; ?></td>
										<td><?php echo $data->uid; ?></td>
										<td>
										<?php if($data->status=='0'){ ?>
										<a href="<?php echo base_url('index.php/vendor/store_active/active/'.$data->id); ?>" onclick="return confirm('are you sure do you want to active this record!');"><img src="<?php echo base_url('assets/admin/image/inactive.png'); ?>"></a>
										<?php }else{ ?>
										<a href="<?php echo base_url('index.php/vendor/store_active/inactive/'.$data->id); ?>" onclick="return confirm('are you sure do you want to inactive this record!');"><img src="<?php echo base_url('assets/admin/image/active.png'); ?>"></a>
										<?php } ?>
										</td>
                                        <td>
											<a href="<?php echo base_url('index.php/vendor/store/'.slugurl($data->title).'/'.$data->id); ?>" title="view store" target="_blank"><i class="fa fa-eye" aria-hidden="true"></i></a>
											<a href="<?php echo site_url('index.php/vendor/store_edit_admin/'.$data->id.''); ?>" title="edit record"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;
										</td>
                                    </tr>
                                <?php } ?>                                       
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
<?php $this->load->view('admin/layout/footer-js'); ?>
</body>

</html>
