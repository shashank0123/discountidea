<?php 
$admin_id = $this->session->userdata('admin_id'); 
if(empty($admin_id))
{
    redirect('index.php/admin');
}   
?>
<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/layout/head'); ?>
<body>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <?php $this->load->view('admin/layout/header'); ?>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <?php $this->load->view('admin/layout/left-menu'); ?>
            <!-- /.navbar-collapse -->
        </nav>

                <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Upload Pincode Using Csv File<br><small>
                        
                        Please Enter Only CSV(ms-dos) File and Don't Enter Duplicate Data. 
                        </small></h1>
                        <div class="col-md-6  col-xl-6">
                            <div class="panel panel-sign">
                    
                    <div class="panel-body">
     
                 <?php if (isset($error)): ?>
                    <div class="alert alert-error"><?php echo $error; ?></div>
                <?php endif; ?>
                <?php if ($this->session->flashdata('success') == TRUE): ?>
                    <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
                <?php endif; ?>
                    <form method="post" action="<?php echo site_url("admin/uploadpincode") ?>" enctype="multipart/form-data">
                        <input type="file" name="uploadFile" ><br><br>
                        <input type="submit" name="sub" value="UPLOAD" class="btn btn-primary">
                    </form>
                </div>
                </div>
                        </div>
                    <!-- end: page -->
            </div>    

 </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
     </div>
    
    <!-- /#wrapper -->
<?php $this->load->view('admin/layout/footer-js'); ?>
</body>

</html>