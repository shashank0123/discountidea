 <?php 

$admin_id = $this->session->userdata('admin_id'); 

if(empty($admin_id))

{

	redirect('index.php/admin');

}	

?>

<!DOCTYPE html>

<html lang="en">

<?php $this->load->view('admin/layout/head'); ?>

<body>



    <div id="wrapper">

        <!-- Navigation -->

        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">

            <!-- Brand and toggle get grouped for better mobile display -->

            <?php $this->load->view('admin/layout/header'); ?>

            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->

            <?php $this->load->view('admin/layout/left-menu'); ?>

            <!-- /.navbar-collapse -->

        </nav>



				

					 <div id="page-wrapper">



            <div class="container-fluid">



                <!-- Page Heading -->

                <div class="row">

                    <div class="col-lg-12">

                        <h1 class="page-header">Edit Page</h1>

						<div class="col-md-6 col-lg-12 col-xl-6">

						    <div class="panel panel-sign">

					

					<div class="panel-body">



				<form  enctype="multipart/form-data" method="POST">



							<table class="table table-bordered table-striped table-condensed" style="">										

								<tbody>

								   
<tr>

										<td> Title</td>

										<td>

		<input type="text" name="title" value="<?=$editmetabyid->title?>"  class="form-control" autocomplete="off" required>

										</td>

									</tr>

									

									<tr>

										<td> Url</td>

										<td>

		<span><?php echo base_url();?></span>
		<input type="text" name="url" value="<?=$editmetabyid->url?>" disabled id="urltextbox" class="form-control" autocomplete="off" required>

										</td>

									</tr>

									<tr>

										<td>Page Content</td>

										<td>
											<textarea name="content" id="pagecontent" class="form-control" autocomplete="off" required >
												<?=$editmetabyid->desc?>
											 </textarea>

										</td>

									</tr>


																		<tr>

										<td>Featured Image</td>

										<td>

		<input type="file" name="fimage"  class="form-control">
		<?php
		if($editmetabyid->featured_image!="")
		{?>
			<img src='<?=base_url().$editmetabyid->featured_image?>' width="100" height="100" alt="<?=$editmetabyid->title?>">

			<?php

		}

		?>

										</td>

									</tr>

									

								<!--	<tr>

										<td> Meta keyword</td>

										<td>

		<input type="text" name="keyword"  class="form-control" autocomplete="off" >

										</td>

									</tr>
									
									
									
									</tr>
									
									        <td> Meta Description</td>

										<td>

			<input type="text" name="description"  class="form-control" autocomplete="off" >

										</td>
										
										
													        
										
									</tr>
									
									
									
									</tr>
									
									        <td> Meta Author</td>

										<td>

			<input type="text" name="author"  class="form-control" autocomplete="off" >

										</td>
										
										
									</tr>
									
									
									
									</tr>
									
									        <td> Meta Charset</td>

										<td>

			<input type="text" name="charset"  class="form-control" autocomplete="off" >

										</td>
										
										
																			
										
										
									</tr>-->

																						

									<tr>

			<td colspan="2"><button type="submit" class="btn btn-info" name="edit">Submit</button></td>

									</tr>

								</tbody>

							</table>



						</form>

				</div>

				</div>

						</div>

					<!-- end: page -->

			</div>



 </div>

            <!-- /.container-fluid -->



        </div>

        <!-- /#page-wrapper -->



    </div>

	 </div>

	 

    <!-- /#wrapper -->
     <script>

     	function makeurl(data){
     		var title = $(data).val().replace(/ /g,'-');
     		$("#urltextbox").val(title);
     	}
                CKEDITOR.replace( 'pagecontent' );
            </script>

<?php $this->load->view('admin/layout/footer-js'); ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

<script>/*

$(document).ready(function(){

    $("#btn1").click(function(){

    	var i=0;

    	i++;

$("p").append("<span id="rmdiv'+ i +'"><input type='file' name='image[]' multiple><input type='text' name='image_title[]' multiple><button onclick="return removefile('+ i +')" type="button" class="btn btn-xs btn-danger imgremove"> <b>X</b></button>");

     

        //$("p1").append("<input type='text' name='image_title[]' multiple>");

        //alert("sssssssssss");

    });



  

});*/

</script>

<script>

	var i = 0;

function addimage()

{

	i++;

	var fileHtml = '<span id="rmdiv'+ i +'"><div class="col-md-12" style="padding: 9px;"><div class="col-md-4"><input type="file" class="form-control" name="image[]" id="image" required></div><div class="col-md-7"> <input type="text" class="form-control" name="image_title[]" id="image_title" placeholder="enter image title" required></div><div class="col-md-1"><button onclick="return removefile('+ i +')" type="button" class="btn btn-xs btn-danger imgremove"> <b>X</b></button></div></div></span>';

	$('#appendimage').append(fileHtml);

}

function removefile(ID)

{

	$('#rmdiv'+ ID).remove();

}

</script>

</body>



</html>