<?php 

$admin_id = $this->session->userdata('admin_id'); 

if(empty($admin_id))

{

	redirect('index.php/admin');

}	

?>

<!DOCTYPE html>

<html lang="en">

<?php $this->load->view('admin/layout/head'); ?>

<body>



    <div id="wrapper">

        <!-- Navigation -->

        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">

            <!-- Brand and toggle get grouped for better mobile display -->

            <?php $this->load->view('admin/layout/header'); ?>

            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->

            <?php $this->load->view('admin/layout/left-menu'); ?>

            <!-- /.navbar-collapse -->

        </nav>



        <div id="page-wrapper">



            <div class="container-fluid">



                <!-- Page Heading -->

                <div class="row">

                    <div class="col-lg-12">

                        <h2 class="page-header">Add Page</h2>

                         <form role="form" method="post" enctype="multipart/form-data">

                            <div class="form-group">

                                <label>Page Title</label>

                                <input type="text" class="form-control" placeholder="Page Title" name="title">

                            </div>
<div class="form-group">

                                <label>Page Url</label>

                                <input type="text" class="form-control" placeholder="Page URL" name="url">

                            </div>

							<!--<div class="form-group">

                                <label>Youtube</label>

                                <input type="text" class="form-control" placeholder="youtube" name="youtube">

                            </div>-->

							<div class="form-group">

                                <label>Content</label>

                                <!--<input type="text" class="form-control" placeholder="Content" name="slide_text">-->
                                <textarea id="editor1" class="form-control" placeholder="Slide Content" name="slide_text"></textarea>

                            </div>

							<div class="form-group">

                                <label>Image</label>

                                <input type="file" class="form-control"  name="image">

                            </div>	

							

							<!--<div class="form-group">

                                <label>Advertisement 1 </label>

                                <input type="file" class="form-control"  name="image2">

                            </div>

							

							<div class="form-group">

                                <label>Advertisement 2</label>

                                <input type="file" class="form-control"  name="image3">

                            </div>-->

                            

                            <div name="status" class="form-group">

                                <label>Status</label>

                                <select class="form-control">

                                    <option value="1">Active</option>

                                    <option value="0">Inactive</option>

                                </select>

                            </div>                            



                            <button type="submit" name="savedata" class="btn btn-default">Submit Button</button>

                            <button type="reset" class="btn btn-default">Reset Button</button>



                        </form>

                    </div>

                </div>

                <!-- /.row -->



            </div>

            <!-- /.container-fluid -->



        </div>

        <!-- /#page-wrapper -->



    </div>

    <!-- /#wrapper -->

<?php $this->load->view('admin/layout/footer-js'); ?>

<script>

function checkpancard()

{

	if(document.getElementById("nopancard").checked==true)

	{

		alert('Kindly Note you need to submit PAN Card in 30 days else your home based work will be stopped & previous work will be washed out.');

		document.getElementById("pancard").style.backgroundColor = "#ccc";

		document.getElementById("pancard").readOnly = true;

	}

	else

	{

		document.getElementById("pancard").style.backgroundColor = "#fff";

		document.getElementById("pancard").readOnly = false;

	}	

}



</script>
  <script src="<?php echo HTTP_JS_PATH; ?>jquery.min.js"></script> 	
  <script src="http://cdnjs.cloudflare.com/ajax/libs/ckeditor/4.0.1/ckeditor.js"></script>

<script src="http://4u2u.in/pt-22-feb/assets/js/nicEdit.js" type="text/javascript"></script>

<script type="text/javascript">

	bkLib.onDomLoaded(function() { nicEditors.allTextAreas() });

</script>
<script src="https://cdn.ckeditor.com/4.6.2/standard/ckeditor.js"> </script>
        <script src="http://cdn.ckeditor.com/4.6.2/standard-all/ckeditor.js"></script>
             <script>
                CKEDITOR.replace( 'editor1' );
            </script>


</body>



</html>

