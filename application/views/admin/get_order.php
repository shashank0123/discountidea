<?php foreach($items as $order){ ?>
<form method="post" action="<?php echo base_url()."admin/comment_submit"; ?>">
<table class="table table-bordered table-striped table-condensed mb-none">
										<tbody><tr>
											<td><strong>Department</strong></td>
											<td><?php $select_depart = $this->user_model->select_depart($order->type); ?><?php echo $select_depart[0]->title; ?></td>
										</tr>

										<tr>
											<td><strong>Ticket ID</strong></td>
											<td><?php echo $order->id; ?></td>
										</tr>
										<tr>
											<td><strong>Subject</strong></td>
											<td><?php echo $order->subject; ?></td>
										</tr>	
										
											<tr>
												<td>Attachment</td>
												<td><a class="btn btn-warning" target="_blank" href="<?php echo base_url().'uploads/helpdesk/'.$order->file; ?>" data-plugin-lightbox="" data-plugin-options="{ &quot;type&quot;:&quot;image&quot; }"><i class="fa fa-search"></i> View</a></td>										
											 </tr>										
										
										<tr>
											<td><strong>Executive</strong></td>
											<td><?php echo $select_depart[0]->name; ?></td>
										</tr>
										<tr>
											<td><strong>Date</strong></td>
											<td><?php echo $order->queryTime; ?></td>
										</tr>
										<tr>
											<td><strong>Query</strong></td>
											<td><?php echo $order->query; ?></td>
										</tr>
										<tr>
											<td><strong>Status</strong></td>
											<td><select name="status">
													<option value="pending" <?php if($order->status=='pending'){ echo 'selected';} ?>>pending</option>
													<option value="open" <?php if($order->status=='open'){ echo 'selected';} ?>>open</option>
													<option value="close" <?php if($order->status=='close'){ echo 'selected';} ?>>close</option>
												</select></td>
										</tr>
										
													<tr>
														<td><strong>Comment</strong></td>
														<td><textarea required="" class="form-control" name="comment"></textarea>
														<input type="hidden" name="uid" value="<?php echo $order->uid; ?>">
														<input type="hidden" name="query_id" value="<?php echo $order->id; ?>">
														</td>										
													 </tr>
													 <tr>
														<td colspan="2">
															<button name="submitComment" type="submit" class="btn btn-info pull-right">
																Submit
															</button>
														</td>
													 </tr>
										
									</tbody>
</table></form>
<?php } ?>
<div class="my-flow">
									<ul class="list-unstyled search-results-list">
																								<?php foreach($getcomment as $getcomments){ ?>									
																													<li>												
							<span>
								<i class="fa fa-user"></i><strong> By <?php echo $getcomments->comment_type; ?></strong>&nbsp;&nbsp;
								<i class="fa fa fa-clock-o"></i> <?php echo $getcomments->comment_date; ?></span>
									<div class="result-data">
										<p class="description"><?php echo $getcomments->comment; ?>											</p>
									</div>
						</li>
																								<?php } ?>																												</ul>
																								</div>
																								<style>
			.my-flow{																					    height: 300px;
			overflow: scroll;}
			
			.btn-warning {
    color: #fff;
    background-color: #ff3366;
    border-color: #ff3366;
}
	</style>