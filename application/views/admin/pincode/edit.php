<?php 

$admin_id = $this->session->userdata('admin_id'); 

if(empty($admin_id))

{

	redirect('index.php/admin');

}	

?>

<!DOCTYPE html>

<html lang="en">

<?php $this->load->view('admin/layout/head'); ?>

<body>



    <div id="wrapper">

        <!-- Navigation -->

        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">

            <!-- Brand and toggle get grouped for better mobile display -->

            <?php $this->load->view('admin/layout/header'); ?>

            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->

            <?php $this->load->view('admin/layout/left-menu'); ?>

            <!-- /.navbar-collapse -->

        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->

                <div class="row">

                    <div class="col-lg-12">

                        <h2 class="page-header">Edit Pincode</h2>

                         <form role="form" method="post" enctype="multipart/form-data">

						 

                            <div class="form-group">

                                <label>Pincode</label>

                                <input type="text" class="form-control" placeholder="Pincode" value="<?php echo $EDITPINCODE[0]->pincode; ?>" name="pincode">

                            </div>

                            

							<div class="form-group">

                                <label>Delivery Day</label>

                                <input type="text" class="form-control" placeholder="Delivery Day" value="<?php echo $EDITPINCODE[0]->day; ?>" name="day">

                            </div>                          



                            <button type="submit" name="updatedata" class="btn btn-default">Submit Button</button>

                            <button type="reset" class="btn btn-default">Reset Button</button>



                        </form>

                    </div>

                </div>

                <!-- /.row -->



            </div>

            <!-- /.container-fluid -->



        </div>

        <!-- /#page-wrapper -->



    </div>

    <!-- /#wrapper -->

<?php $this->load->view('admin/layout/footer-js'); ?>

</body>



</html>

