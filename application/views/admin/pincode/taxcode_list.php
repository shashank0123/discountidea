<?php 
$admin_id = $this->session->userdata('admin_id'); 
if(empty($admin_id))
{
	redirect('index.php/admin');
}	
?>
<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/layout/head'); ?>
<body>
    <div id="wrapper">
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <?php $this->load->view('admin/layout/header'); ?>
            <?php $this->load->view('admin/layout/left-menu'); ?>
        </nav>

        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                            <h2 class="page-header">Tax Code Listing 						
							<div class="pull-right"><a href="<?php echo base_url('index.php/pincode/taxcode_add'); ?>" class="btn btn-primary">Add New</a> &nbsp; 
						<a href="<?php echo base_url('index.php/pincode/taxcode_uploadcsv'); ?>" class="btn btn-primary ">Upload CSV</a></h2>
						<?php echo $this->session->flashdata('message'); ?>
                         <div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
										<th width="4%">S.No.</th>
                                        <th width="8%">Tax Code</th>
										<th width="80%">Tax (%)</th>
                                        <th width="8%">Action</th>
                                    </tr>
                                </thead>
                                <tbody>

								<?php 
                                $off = $this->uri->segment(3);
                                if(!empty($off)){$i=$off;}else{$i=0;}
                                 foreach($rows as $data){ $i++; ?>
                                    <tr>
										<td><?php echo $i; ?>.</td>
                                        <td><?php echo $data->tax_code; ?></td>
										<td><?php echo ($data->tax_percentage>0) ? $data->tax_percentage.'%' : ''; ?></td>
                                        <td>
											<a href="<?php echo base_url('index.php/pincode/taxcode_edit/'.$data->taxcode_id); ?>" title="edit record"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
											<a href="<?php echo base_url('index.php/pincode/taxcode_delete/'.$data->taxcode_id); ?>" onclick="return deleteConfirm();" title="delete record"><i class="fa fa-trash" aria-hidden="true"></i></a>
										</td>
                                    </tr>
                                <?php } ?>                                       
                                </tbody>
                            </table>
                        </div>
                        <?php
                        $off = $this->uri->segment(3);
                        if(!empty($off)){
                            $n = $off+200;
                        
                        }else{
                            $n = 200;
                        }
                        if(!empty($off) && $off >= 200){
                            $p = $off-200;
                        }else{
                            $p = "";
                        }
                        if(count($rows)>0) {
                        ?>
                        <a href="http://www.discountidea.com/index.php/pincode/listing/<?php echo $p ?>" class="btn btn-primary">Prev</a>
                        <a href="http://www.discountidea.com/index.php/pincode/listing/<?php echo $n ?>" class="btn btn-primary">Next</a>
                        <?php } ?>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
    </div>
<!-- /#wrapper -->
<?php $this->load->view('admin/layout/footer-js'); ?>

<script>
function checkpancard()
{
	if(document.getElementById("nopancard").checked==true)
	{
		alert('Kindly Note you need to submit PAN Card in 30 days else your home based work will be stopped & previous work will be washed out.');
		document.getElementById("pancard").style.backgroundColor = "#ccc";
		document.getElementById("pancard").readOnly = true;
	}
	else
	{
		document.getElementById("pancard").style.backgroundColor = "#fff";
		document.getElementById("pancard").readOnly = false;
	}	
}
</script>
</body>
</html>