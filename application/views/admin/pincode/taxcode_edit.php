<?php 
$admin_id = $this->session->userdata('admin_id'); 
if(empty($admin_id))
{
	redirect('index.php/admin');
}
?>

<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/layout/head'); ?>
<body>
    <div id="wrapper">
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <?php $this->load->view('admin/layout/header'); ?>
            <?php $this->load->view('admin/layout/left-menu'); ?>
        </nav>

        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h2 class="page-header">Edit Tax Code Detail</h2>
                        <form role="form" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <label>Tax Code</label>
                                <input type="text" class="form-control" placeholder="Tax Code" name="tax_code" value="<?php echo ($detail->tax_code) ? $detail->tax_code : '';?>" required>
                            </div>

							<div class="form-group">
                                <label>Tax Percentage</label>
                                <input type="text" class="form-control" placeholder="Tax Percentage" name="tax_percentage" value="<?php echo ($detail->tax_percentage) ? $detail->tax_percentage : '';?>" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');" required>
                            </div>
                            <button type="submit" name="savedata" class="btn btn-success">SAVE</button>
                            <a href="<?php echo base_url('index.php/pincode/taxcode_list'); ?>" class="btn btn-primary">Back to List</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $this->load->view('admin/layout/footer-js'); ?>
</body>
</html>