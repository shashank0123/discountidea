<?php 
$admin_id = $this->session->userdata('admin_id'); 
if(empty($admin_id))
{
    redirect('index.php/admin');
}   
?>

<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/layout/head'); ?>
<body>
    <div id="wrapper">
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <?php $this->load->view('admin/layout/header'); ?>
            <?php $this->load->view('admin/layout/left-menu'); ?>
        </nav>

        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Upload Tax Code Using CSV File<br><small>Please Enter Only CSV(ms-dos) File and Don't Enter Duplicate Data.</small></h1>
                        <div class="col-md-6  col-xl-6">
                            <div class="panel panel-sign">
                                <div class="panel-body">
                                    <?php if (isset($error)): ?>
                                    <div class="alert alert-error"><?php echo $error; ?></div>
                                    <?php endif; ?>
                                    <?php if ($this->session->flashdata('success') == TRUE): ?>
                                    <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
                                    <?php endif; ?>
                                    <form method="post" action="<?php echo site_url("pincode/taxcode_uploadcsv") ?>" enctype="multipart/form-data">
                                        <input type="file" name="uploadFile" required><br><br>
                                        <input type="submit" name="sub" value="UPLOAD" class="btn btn-success">
                                        <a href="<?php echo base_url('index.php/pincode/taxcode_list'); ?>" class="btn btn-primary">Back to List</a>
                                    </form>
                                </div>
                            </div>
                        </div>
						<div class="col-md-6 col-xl-6">
						<a href="<?php echo base_url('GST_code.csv');?>"><img src="<?php echo base_url('uploads/Logo Images.jpg');?>" style="    height: 51px;
    width: 61px;"><strong>Download Here</strong></a>
						</div>
            </div>
 </div>
        </div>
    </div>
     </div>
    
<?php $this->load->view('admin/layout/footer-js'); ?>
</body>
</html>