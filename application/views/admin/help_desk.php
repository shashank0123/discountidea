<?php 

$admin_id = $this->session->userdata('admin_id'); 

if(empty($admin_id))

{

	redirect('index.php/admin');

}	

?>

<!DOCTYPE html>

<html lang="en">

<?php $this->load->view('admin/layout/head'); ?>
<style>
 .panel-default>.panel-heading {
    color: #fff;
    background-color: #1fa67a;
    border-color: #1fa67a;
}
h1.page-header {
	font-size: 22px;
    color: #f68245;
    border-bottom: 1px solid #f56c23;
}


/* The Modal (background) */
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 9999; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content */
.modal-content {
    position: relative;
    background-color: #fefefe;
    margin: auto;
    padding: 0;
    border: 1px solid #888;
    width: 92%;
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);
    -webkit-animation-name: animatetop;
    -webkit-animation-duration: 0.4s;
    animation-name: animatetop;
    animation-duration: 0.4s
}

/* Add Animation */
@-webkit-keyframes animatetop {
    from {top:-300px; opacity:0} 
    to {top:0; opacity:1}
}

@keyframes animatetop {
    from {top:-300px; opacity:0}
    to {top:0; opacity:1}
}

/* The Close Button */
.close {
    color: black;
    float: right;
    font-size: 28px;
    font-weight: bold;
}

.close:hover,
.close:focus {
    color: #000;
    text-decoration: none;
    cursor: pointer;
}

/* The Close1 Button */
.Close1 {
    color: black !important;
    float: right !important;
    font-size: 28px !important;
    font-weight: bold !important;
}

.Close1:hover,
.Close1:focus {
    color: #000;
    text-decoration: none;
    cursor: pointer;
}

/* The Close2 Button */
.Close2 {
    color: black;
    float: right;
    font-size: 28px;
    font-weight: bold;
}

.Close2:hover,
.Close2:focus {
    color: #000;
    text-decoration: none;
    cursor: pointer;
}

.modal-header {
    padding: 2px 16px;
    background-color: #5cb85c;
    color: black;
}

.modal-body1 {padding: 2px 16px;}



    /* The Modal (background) */
.modal1 {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 9999; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}


.modal2 {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 9999; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content */
.modal-content1 {
    position: relative;
    background-color: #fefefe;
    margin: auto;
    padding: 0;
    border: 1px solid #888;
    width: 92%;
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);
    -webkit-animation-name: animatetop;
    -webkit-animation-duration: 0.4s;
    animation-name: animatetop;
    animation-duration: 0.4s
}
.modal-header1 {
    padding: 2px 16px;
    background-color: #ff3366;
    color: black;
}
/* Add Animation */
@-webkit-keyframes animatetop {
    from {top:-300px; opacity:0} 
    to {top:0; opacity:1}
}

@keyframes animatetop {
    from {top:-300px; opacity:0}
    to {top:0; opacity:1}
}

.panel {
    margin-bottom: 0 !important;
    }
button
                {
                        padding: 3px 8px 4px 5px !important;
                        color: blue;
                        background: -webkit-linear-gradient(top, rgb(144, 195, 86) 0%, rgb(100, 154, 39) 100%);
                        border-width: 1px !important;
                        color: #ffffff;
                        border-radius: 10px;
                        color: white;
                        background: #ff3366;
                }
  .aj-form {
	  padding:1em;
  }
  .aj-button {
	  padding:5px 10px;
	  background:#ccc;
	  color:#000;
	  
  }
  
  .aj-button:hover {
	  background:#000;
	  color:#fff;
	  
  }
  
  .search-results-list li {
    border-bottom: 1px solid #eee;
    margin-bottom: 15px;
    padding-bottom: 15px;
    position: relative;
}
.btn-warning {
    color: #fff;
    background-color: #ff3366;
    border-color: #ff3366;
}
  </style>

<body>



    <div id="wrapper">

        <!-- Navigation -->

        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">

            <!-- Brand and toggle get grouped for better mobile display -->

            <?php $this->load->view('admin/layout/header'); ?>

            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->

            <?php $this->load->view('admin/layout/left-menu'); ?>

            <!-- /.navbar-collapse -->

        </nav>



        <div id="page-wrapper">



            <div class="container-fluid">



                <!-- Page Heading -->

                <div class="row">

                    <div class="col-lg-12">

                        <h1 class="page-header">Welcome to Helpdesk</h1>
					
					<div class="panel-body">

								<form class="form-horizontal" method="post">

									<h4 class="mb-xlg"></h4>

									<fieldset>												

										<div class="form-group">													

											<div class="col-md-6">

												<div class="input-daterange input-group" data-plugin-datepicker="">

													<span class="input-group-addon">

														<i class="fa fa-calendar"></i>

													</span>

													<input class="form-control" name="startDate" placeholder="start Date" value="" required="" type="date">

													<span class="input-group-addon">to</span>

													<input class="form-control" name="endDate" placeholder="end Date" value="" required="" type="date">                                                        

												</div>

											</div>
											<div class="col-md-3">

												<button type="submit" name="checkHelpDesk" class="btn btn-warning"><i class="fa fa-search"></i> Search</button>

											</div>

										</div>                                                  

									</fieldset>

								</form>

							</div>
<div class="panel-body">

								<a data-toggle="tab" href="#home1" class="btn btn-info">Total  Query (<?php echo count($myorder); ?>)</a>
								<a data-toggle="tab" href="#home2" class="btn btn-info">Total Pending Query -(<?php echo count($myorder2); ?>)</a>

								<a data-toggle="tab" href="#home3" class="btn btn-info">Total Close Query -(<?php echo count($myorder3); ?>)</a>

								<a data-toggle="tab" href="#home4" class="btn btn-info">Total Open Query -(<?php echo count($myorder4); ?>)</a>

								

                                <a href="<?php echo base_url().'admin/manage_department'; ?>" class="btn btn-info pull-right">Manage Department</a>

								<a href="<?php echo base_url().'index.php/admin/download_all_helpdesk_query_mis'; ?>" target="_blank" class="btn btn-warning ">Download All Query</a>	
</div>



<div class="tab-content">
<div class="row datatables-header form-inline"><div class="col-sm-12 col-md-6" style="float:right;"><div id="datatable-default_filter"  style="float:right;" class="dataTables_filter"><label><form action="#" method="post" style="    float: right;">
              <input class="search1" style="outline: none; padding: 10px; color: #888; font-size: 15px;  width: 85%; border: ridge 2px #ffffff; background: #ffffff; float: left; height: 41px; letter-spacing: 1px; font-weight: 400;" type="search" id="myInput" name="search" placeholder="Search..." >
              
            </form></label></div></div>
</div>
  <div id="home1" class="tab-pane fade in active">
  <div class="row">
    
								
                       <?php if(count($myorder)>0){ ?>

			<table class="table table-bordered table-hover">

				<thead>

					<tr>

						<th>S. No.</th>
						<th>Ticket ID</th>
						<th>User ID</th>
						<th>Order ID</th>
						<th>Product ID</th>
						<th>Vendor Name</th>
						<th>User Contact</th>
						<th>Vendor Contact</th>
						<th>Department</th>

						<th>Executive</th>

						<th>Date</th>

						<th>Status</th>
						<th></th>

					</tr>

				</thead>

				<tbody>

					<?php $i=1; foreach($myorder as $order){ ?>

					<?php $items = $this->user_model->getOrderItemsByOrderId($order->id); ?>

					<?php $user = $this->user_model->selectuserby_id($order->uid); ?>

					<tr>
						<td><?php echo $i; ?></td>
						<td>#<?php echo $order->id; ?></td>

						<td><?php echo $user[0]->fname.' '.$user[0]->lname; ?></td>

						

						
						<td><?php echo $order->order_product; ?></td>
						<td><?php echo $order->product_id; ?></td>
						<?php $vender_id = $this->user_model->selectuserby_id($order->vender_id); ?>
						<td><?php echo $vender_id[0]-> company_name ; ?></td>
						<td><?php echo $user[0]->mobile; ?></td>
						<td><?php echo $vender_id[0]->mobile; ?></td>
						<?php $select_depart = $this->user_model->select_depart($order->type); ?>
						<td><?php echo $select_depart[0]->title; ?></td>
						
						<td><?php echo $select_depart[0]->name; ?></td>
						

						<td><?php echo $order->queryTime; ?></td>
						<td><?php echo $order->status; ?></td>
						<td><button data-booking2="myBtn<?php echo $order->id ?>" id="myBtn2" class="myBtn2"><i class="fa fa-eye"></i> View</button><input type="hidden"  id="myBtn<?php echo $order->id ?>" value="<?php echo $order->id  ?>"></td>

					</tr>

					<?php } ?>

				</tbody>

			</table> 		

			<?php $i++; } ?> 
							<div  id="myModal2" class="modal2">

                      
								  <div class="modal-content1">
									<div class="modal-header1">
									  <span class="Close2">&times;</span>
									  <h2>Submit Your Comment</h2>
									</div>
									<div class="modal-body1"  id="booking_val2"></div>
								  </div>

							</div>
							</div>
  </div>
  <div id="home2" class="tab-pane fade">
    <div class="row">
    
								
                       <?php if(count($myorder2)>0){ ?>

			<table class="table table-bordered table-hover">

				<thead>

					<tr>

						<th>S. No.</th>
						<th>Ticket ID</th>
						<th>User ID</th>
						<th>Order ID</th>
						<th>Product ID</th>
						<th>Vendor Name</th>
						<th>User Contact</th>
						<th>Vendor Contact</th>
						<th>Department</th>

						<th>Executive</th>

						<th>Date</th>

						<th>Status</th>
						<th></th>

					</tr>

				</thead>

				<tbody>

					<?php $i=1; foreach($myorder2 as $order){ ?>

					<?php $items = $this->user_model->getOrderItemsByOrderId($order->id); ?>

					<?php $user = $this->user_model->selectuserby_id($order->uid); ?>

					<tr>
						<td><?php echo $i; ?></td>
						<td>#<?php echo $order->id; ?></td>

						<td><?php echo $user[0]->fname.' '.$user[0]->lname; ?></td>

						

						
						<td><?php echo $order->order_product; ?></td>
						<td><?php echo $order->product_id; ?></td>
						<?php $vender_id = $this->user_model->selectuserby_id($order->vender_id); ?>
						<td><?php echo $vender_id[0]-> company_name ; ?></td>
						<td><?php echo $user[0]->mobile; ?></td>
						<td><?php echo $vender_id[0]->mobile; ?></td>
						<?php $select_depart = $this->user_model->select_depart($order->type); ?>
						<td><?php echo $select_depart[0]->title; ?></td>
						
						<td><?php echo $select_depart[0]->name; ?></td>
						

						<td><?php echo $order->queryTime; ?></td>
						<td><?php echo $order->status; ?></td>
						<td><button data-booking2="myBtn<?php echo $order->id ?>" id="myBtn2" class="myBtn2"><i class="fa fa-eye"></i> View</button><input type="hidden"  id="myBtn<?php echo $order->id ?>" value="<?php echo $order->id  ?>"></td>

					</tr>

					<?php } ?>

				</tbody>

			</table> 		

			<?php $i++; } ?> 
							<div  id="myModal2" class="modal2">

                      
								  <div class="modal-content1">
									<div class="modal-header1">
									  <span class="Close2">&times;</span>
									  <h2>Submit Your Comment</h2>
									</div>
									<div class="modal-body1"  id="booking_val2"></div>
								  </div>

							</div>
							</div>
  </div>
  <div id="home3" class="tab-pane fade">
     <div class="row">
    
								
                       <?php if(count($myorder3)>0){ ?>

			<table class="table table-bordered table-hover">

				<thead>

					<tr>

						<th>S. No.</th>
						<th>Ticket ID</th>
						<th>User ID</th>
						<th>Order ID</th>
						<th>Product ID</th>
						<th>Vendor Name</th>
						<th>User Contact</th>
						<th>Vendor Contact</th>
						<th>Department</th>

						<th>Executive</th>

						<th>Date</th>

						<th>Status</th>
						<th></th>

					</tr>

				</thead>

				<tbody>

					<?php $i=1; foreach($myorder3 as $order){ ?>

					<?php $items = $this->user_model->getOrderItemsByOrderId($order->id); ?>

					<?php $user = $this->user_model->selectuserby_id($order->uid); ?>

					<tr>
						<td><?php echo $i; ?></td>
						<td>#<?php echo $order->id; ?></td>

						<td><?php echo $user[0]->fname.' '.$user[0]->lname; ?></td>

						

						
						<td><?php echo $order->order_product; ?></td>
						<td><?php echo $order->product_id; ?></td>
						<?php $vender_id = $this->user_model->selectuserby_id($order->vender_id); ?>
						<td><?php echo $vender_id[0]-> company_name ; ?></td>
						<td><?php echo $user[0]->mobile; ?></td>
						<td><?php echo $vender_id[0]->mobile; ?></td>
						<?php $select_depart = $this->user_model->select_depart($order->type); ?>
						<td><?php echo $select_depart[0]->title; ?></td>
						
						<td><?php echo $select_depart[0]->name; ?></td>
						

						<td><?php echo $order->queryTime; ?></td>
						<td><?php echo $order->status; ?></td>
						<td><button data-booking2="myBtn<?php echo $order->id ?>" id="myBtn2" class="myBtn2"><i class="fa fa-eye"></i> View</button><input type="hidden"  id="myBtn<?php echo $order->id ?>" value="<?php echo $order->id  ?>"></td>

					</tr>

					<?php } ?>

				</tbody>

			</table> 		

			<?php $i++; } ?> 
							<div  id="myModal2" class="modal2">

                      
								  <div class="modal-content1">
									<div class="modal-header1">
									  <span class="Close2">&times;</span>
									  <h2>Submit Your Comment</h2>
									</div>
									<div class="modal-body1"  id="booking_val2"></div>
								  </div>

							</div>
							</div>
  </div>
  <div id="home4" class="tab-pane fade">
     <div class="row">
    
								
                       <?php if(count($myorder4)>0){ ?>

			<table class="table table-bordered table-hover">

				<thead>

					<tr>

						<th>S. No.</th>
						<th>Ticket ID</th>
						<th>User ID</th>
						<th>Order ID</th>
						<th>Product ID</th>
						<th>Vendor Name</th>
						<th>User Contact</th>
						<th>Vendor Contact</th>
						<th>Department</th>

						<th>Executive</th>

						<th>Date</th>

						<th>Status</th>
						<th></th>

					</tr>

				</thead>

				<tbody>

					<?php $i=1; foreach($myorder4 as $order){ ?>

					<?php $items = $this->user_model->getOrderItemsByOrderId($order->id); ?>

					<?php $user = $this->user_model->selectuserby_id($order->uid); ?>

					<tr>
						<td><?php echo $i; ?></td>
						<td>#<?php echo $order->id; ?></td>

						<td><?php echo $user[0]->fname.' '.$user[0]->lname; ?></td>

						

						
						<td><?php echo $order->order_product; ?></td>
						<td><?php echo $order->product_id; ?></td>
						<?php $vender_id = $this->user_model->selectuserby_id($order->vender_id); ?>
						<td><?php echo $vender_id[0]-> company_name ; ?></td>
						<td><?php echo $user[0]->mobile; ?></td>
						<td><?php echo $vender_id[0]->mobile; ?></td>
						<?php $select_depart = $this->user_model->select_depart($order->type); ?>
						<td><?php echo $select_depart[0]->title; ?></td>
						
						<td><?php echo $select_depart[0]->name; ?></td>
						

						<td><?php echo $order->queryTime; ?></td>
						<td><?php echo $order->status; ?></td>
						<td><button data-booking2="myBtn<?php echo $order->id ?>" id="myBtn2" class="myBtn2"><i class="fa fa-eye"></i> View</button><input type="hidden"  id="myBtn<?php echo $order->id ?>" value="<?php echo $order->id  ?>"></td>

					</tr>

					<?php } ?>

				</tbody>

			</table> 		

			<?php $i++; } ?> 
							<div  id="myModal2" class="modal2">

                      
								  <div class="modal-content1">
									<div class="modal-header1">
									  <span class="Close2">&times;</span>
									  <h2>Submit Your Comment</h2>
									</div>
									<div class="modal-body1"  id="booking_val2"></div>
								  </div>

							</div>
							</div>
  </div>
</div>


                    </div>

                </div>

                <!-- /.row -->



            </div>

            <!-- /.container-fluid -->



        </div>

        <!-- /#page-wrapper -->



    </div>

    <!-- /#wrapper -->

<?php $this->load->view('admin/layout/footer-js'); ?>





</body>
<script>
$(".myBtn2").click(function(){
				//alert("s");
				var booking2 = $(this).attr('data-booking2');
			   // var book =$(".booking_id").val();
				//var booking_id =$(this).val(book);
				var booking_val2 = $("#"+booking2).val();
				//alert(booking_val);
				var modal = document.getElementById('myModal2');

			// Get the button that opens the modal


			// Get the <span> element that closes the modal
			var span = document.getElementsByClassName("Close2")[0];
				var btn = document.getElementById("myBtn2");
				modal.style.display = "block";

				$.get("http://www.discountidea.com/admin/get_order?booking_id="+booking_val2, function(data){
				   // alert(data);
					$("#booking_val2").html(data);
					});
			   // });


			// When the user clicks on <span> (x), close the modal
			span.onclick = function() {
				modal.style.display = "none";
			}

			// When the user clicks anywhere outside of the modal, close it
			window.onclick = function(event) {
				if (event.target == modal) {
					modal.style.display = "none";
				}
			} 
			});
</script>
<script type="text/javascript">
  $("#myInput").keyup(function () {
        var value = this.value.toLowerCase().trim();

        $("table tr").each(function (index) {
            if (!index) return;
            $(this).find("td").each(function () {
                var id = $(this).text().toLowerCase().trim();
                var not_found = (id.indexOf(value) == -1);
                $(this).closest('tr').toggle(!not_found);
                return not_found;
            });
        });
    });

 /* $(".w3_inner_tittle").click(function(){
    alert("The paragraph was clicked.");
});*/
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>


</html>

