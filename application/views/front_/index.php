  <!-- end header --> 
<?php $this->load->view('front/layout/header-home.php');?>  
  <!-- Navbar -->
<script>
public function getbannerurl(URL)
{
  if(URL!="")
  {
    //location.href=URL;
   // window.open( URL, '_blank' );
  }
}
</script>
  <!-- end nav -->
  <div class="slider">
    <div class="container">
      <div class="row">
        <div class="col-md-9 col-md-offset-3">
          <div class="flexslider ma-nivoslider">
                  <div id="ma-inivoslider-banner7" class="slides">
  
  <?php
  $bannerimage=$this->banner_model->selectAllBanner();
  foreach($bannerimage as $banner)
  {
  ?>
  <a href="<?php echo($banner->url!='')?$banner->url:'#';?>" target="_blank" ><img src="<?php echo base_url('uploads/banner');?>/<?php echo $banner->image;?>" class="dn" onclick="getbannerurl('<?php echo($banner->url!='')?$banner->url:'#';?>')" /></a>
  <?php
  }
  ?>
  
  
  
 </div>
          
          </div>
          <!-- /.flexslider --> 
        </div>
      </div>
    </div>
  </div>
  <!-- .fullwidth -->
 <div class="main-container" style="min-height: 20px; background:#fd6602;">
    <div class="container">
      <div class="row" style="padding:5px;">
          <center><a href="<?php echo base_url('vendor/store_list') ?>" target="_blank"><button style="background-color:green;" class="view-btn">Choose Your Brand From Famous Vendor Stores </button><center>
      </div>
     </div>
</div>
 

  
  <!-- main container -->
  <div class="main-container col1-layout">
    <div class="container">
      <div class="row"> 
      <div class="col-md-2 col-sm-4 hot-deal">

          <ul class="products-grid">
            <li class="item">
            <div class="product-item">
                        <div class="item-inner fadeInUp">
                          <div class="product-thumbnail right-hadding-sec">
                            <?php //$dis_data = $this->product_model->getproductdiscountwise(70); ?>
							
                            <div class="haddin-sale">
                              <h2 class="text-center">Deals of the Day</h2>
                              <div class="timer-sale"><i class="fa fa-clock-o"></i> <span id="timer"></span></div>                              
                              <div class="view-all">							
								<a href="<?php echo base_url('home/discount/deals-of-the-day');?>" class="view-btn"> <span> View All</span> </a>
                              </div>
                            </div>
                            
                             
                          </div>
                          
                        </div>
                      </div>
                      
              
            </li>
          </ul>
        </div>
        <!-- Home Tabs  -->
        <div class="home-tab col-sm-8 col-md-10 col-xs-12">
          
          <div id="productTabContent" class="tab-content">
            <div class="tab-pane active in" id="new-arrivals">
              <div class="new-arrivals-pro">
                <div class="slider-items-products">
                  <div id="new-arrivals-slider" class="product-flexslider hidden-buttons">
                    <div class="slider-items slider-width-col4">
                      <?php
						$deal="dealday";
					$data=$this->product_model->getproductdiscountwise(50); //$this->product_model->homeproductall($deal);
					foreach($data  as $dealday)
					{

						 $product_image = $this->product_model->selectProductDoubleImage($dealday->id);
						?>	
                      <div class="product-item">
                        <div class="item-inner fadeInUp">
                          <div class="product-thumbnail">
                            <div class="pr-img-area">
							<a title="<?php echo $dealday->name; ?>" href="<?php echo base_url('product/'.slugurl($dealday->name)).'/'.encodeurlval($dealday->id); ?>">
								<figure> <?php if(count($product_image)>0){ ?>
									<img class="first-img" src="<?php echo base_url('uploads/product/thumbs'); ?>/<?php echo $product_image[0]->image; ?>" alt="<?php echo $dealday->name; ?>"> 
									<?php if(isset($product_image[1]->image)){ ?><img class="hover-img" src="<?php echo base_url('uploads/product/thumbs'); ?>/<?php echo $product_image[1]->image; ?>" alt="<?php echo $dealday->name; ?>"><?php } ?>
									<?php }else{ ?>
									<img class="first-img" src="http://placehold.it/264x264?text=Image not found" alt="<?php echo $dealday->name; ?>"> 
									<img class="hover-img" src="http://placehold.it/264x264?text=Image not found" alt="<?php echo $dealday->name; ?>">
									<?php } ?>
								</figure>
							</a>	
                            <form method="post" action="<?php echo base_url('index.php/cart/addtocart'); ?>">
								<input type="hidden" name="pid" value="<?php echo $dealday->id; ?>" >
								<input type="hidden" name="qty" value="1" >						   
								<button type="button" name="addtocartbutton1" onclick="return addtocart(<?php echo $dealday->id; ?>);" class="add-to-cart-mt"> <i class="fa fa-shopping-cart"></i><span> Add to Cart</span> </button>
								<button type="submit" name="addtocartbutton" class="add-to-cart-mt1"> <i class="fa fa-bolt"></i><span> Buy Now</span> </button>
							</form>
                            </div>
                            
                          </div>
                          <div class="item-info">
                      <div class="info-inner">
                        <div class="item-title"> 
							<a title="<?php echo $dealday->name; ?>" href="<?php echo base_url('product/'.slugurl($dealday->name)).'/'.encodeurlval($dealday->id); ?>">
								<?php echo $dealday->name; ?>
							</a>
						</div>
                        <div class="item-content">
                          <div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                          <div class="price-box">
							  <?php if($dealday->spacel_price!=""){ ?>
                              <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> <?php echo CURRENCY.$dealday->spacel_price; ?> </span> </p>
                              <p class="old-price"> <span class="price-label">Regular Price:</span> <span class="price"> <?php echo CURRENCY.$dealday->price; ?> </span> </p>
							  <?php }else{ ?>
							  <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> <?php echo CURRENCY.$dealday->price; ?> </span> </p>
							  <?php } ?>
                          </div>
                        </div>
                      </div>
                    </div>
                        </div>
                      </div>
					  
					  <?php
					}
					?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
            
          </div>
        </div>
      </div>
    </div>
  </div>
  
  
    <div class="main-container col1-layout">
    <div class="container">
      <div class="row"> 
      <div class="col-md-2 col-sm-4 hot-deal">
     
          <ul class="products-grid">
            <li class="item">
            <div class="product-item">
                        <div class="item-inner fadeInUp">
                          <div class="product-thumbnail right-hadding-sec">
                         
                            <div class="haddin-sale">
                              <h2 class="text-center">Discounts for You</h2>
                              
                              
                              <div class="view-all">							  
                              	<a href="<?php echo base_url('home/discount/discounts-for-you');?>" class="view-btn"> <span> View All</span> </a>
                              </div>
                            </div>
                            
                             
                          </div>
                          
                        </div>
                      </div>
                      
              
            </li>
          </ul>
        </div>
        <!-- Home Tabs  -->
        <div class="home-tab col-sm-8 col-md-10 col-xs-12">
          
          <div id="productTabContent" class="tab-content">
            <div class="tab-pane active in" id="new-arrivals">
              <div class="new-arrivals-pro">
                <div class="slider-items-products">
                  <div id="new-arrivals-slider" class="product-flexslider hidden-buttons">
                    <div class="slider-items slider-width-col4">
                    
                              
                      
                      <?php
						$discount="discount_you";
					$datadiscount= $this->product_model->getproductdiscountwise(30);
					foreach($datadiscount  as $discountre)
					{

						 $product_image = $this->product_model->selectProductDoubleImage($discountre->id);
						?>	
                      <div class="product-item">
                        <div class="item-inner fadeInUp">
                          <div class="product-thumbnail">
                            <div class="pr-img-area">
								<a title="<?php echo $discountre->name; ?>" href="<?php echo base_url('product/'.slugurl($discountre->name)).'/'.encodeurlval($discountre->id); ?>">
									<figure> 
										<?php if(count($product_image)>0){ ?>
										<img class="first-img" src="<?php echo base_url('uploads/product/thumbs'); ?>/<?php echo $product_image[0]->image; ?>" alt="<?php echo $discountre->name; ?>"> 
										<?php if(isset($product_image[1]->image)){ ?><img class="hover-img" src="<?php echo base_url('uploads/product/thumbs'); ?>/<?php echo $product_image[1]->image; ?>" alt="<?php echo $discountre->name; ?>"><?php } ?>
										<?php }else{ ?>
										<img class="first-img" src="http://placehold.it/264x264?text=Image not found" alt="<?php echo $discountre->name; ?>"> 
										<img class="hover-img" src="http://placehold.it/264x264?text=Image not found" alt="<?php echo $discountre->name; ?>">
										<?php } ?>
									</figure>
								</a>
								<form method="post" action="<?php echo base_url('index.php/cart/addtocart'); ?>">
									<input type="hidden" name="pid" value="<?php echo $discountre->id; ?>" >
									<input type="hidden" name="qty" value="1" >
									<button type="button" name="addtocartbutton1" onclick="return addtocart(<?php echo $discountre->id; ?>);" class="add-to-cart-mt"> <i class="fa fa-shopping-cart"></i><span> Add to Cart</span> </button>
									<button type="submit" name="addtocartbutton" class="add-to-cart-mt1"> <i class="fa fa-bolt"></i><span> Buy Now</span> </button>
								</form>
                            </div>                            
                          </div>
                        <div class="item-info">
							<div class="info-inner">
								<div class="item-title"> 
									<a title="<?php echo $discountre->name; ?>" href="<?php echo base_url('product/'.slugurl($discountre->name)).'/'.encodeurlval($discountre->id); ?>">
										<?php echo $discountre->name; ?>
									</a>
								</div>
								<div class="item-content">
								  <div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
								  <div class="price-box">
									  <?php if($discountre->spacel_price!=""){ ?>
									  <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> <?php echo CURRENCY.$discountre->spacel_price; ?> </span> </p>
									  <p class="old-price"> <span class="price-label">Regular Price:</span> <span class="price"> <?php echo CURRENCY.$discountre->price; ?> </span> </p>
									  <?php }else{ ?>
									  <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> <?php echo CURRENCY.$discountre->price; ?> </span> </p>
									  <?php } ?>
								  </div>
								</div>
							</div>
						</div>
                        </div>
                      </div>					  
					  <?php } ?>					  
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
            
          </div>
        </div>
      </div>
    </div>
  </div>
  
  <div class="main-container col1-layout">
    <div class="container">
      <div class="row"> 
      <div class="col-md-2 col-sm-4 hot-deal">
     
          <ul class="products-grid">
            <li class="item">
            <div class="product-item">
                        <div class="item-inner fadeInUp">
                          <div class="product-thumbnail right-hadding-sec">
                         
                            <div class="haddin-sale">
                              <h2 class="text-center">Offers for You</h2>
                              
                              
                              <div class="view-all">							 
                              	<a href="<?php echo base_url('home/discount/offers-for-you');?>" class="view-btn"> <span> View All</span> </a>
                              </div>
                            </div>
                            
                             
                          </div>
                          
                        </div>
                      </div>
                      
              
            </li>
          </ul>
        </div>
        <!-- Home Tabs  -->
        <div class="home-tab col-sm-8 col-md-10 col-xs-12">
          
          <div id="productTabContent" class="tab-content">
            <div class="tab-pane active in" id="new-arrivals">
              <div class="new-arrivals-pro">
                <div class="slider-items-products">
                  <div id="new-arrivals-slider" class="product-flexslider hidden-buttons">
                    <div class="slider-items slider-width-col4">
                    <?php
					$offer="offer_for_you";
					$datafooter= $this->product_model->getproductdiscountwise(1);//$this->product_model->homeproductall($offer);
					foreach($datafooter  as $dataofferre)
					{
						$product_image = $this->product_model->selectProductDoubleImage($dataofferre->id);
					?>	
                      <div class="product-item">
                        <div class="item-inner fadeInUp">
                          <div class="product-thumbnail">
                            <div class="pr-img-area">
								<a title="<?php echo $dataofferre->name; ?>" href="<?php echo base_url('product/'.slugurl($dataofferre->name)).'/'.encodeurlval($dataofferre->id); ?>">
									<figure> 
										<?php if(count($product_image)>0){ ?>
										<img class="first-img" src="<?php echo base_url('uploads/product/thumbs'); ?>/<?php echo $product_image[0]->image; ?>" alt="<?php echo $dataofferre->name; ?>"> 
										<?php if(isset($product_image[1]->image)){ ?><img class="hover-img" src="<?php echo base_url('uploads/product/thumbs'); ?>/<?php echo $product_image[1]->image; ?>" alt="<?php echo $dataofferre->name; ?>"><?php } ?>
										<?php }else{ ?>
										<img class="first-img" src="http://placehold.it/264x264?text=Image not found" alt="<?php echo $dataofferre->name; ?>"> 
										<img class="hover-img" src="http://placehold.it/264x264?text=Image not found" alt="<?php echo $dataofferre->name; ?>">
										<?php } ?>
									</figure>
								</a>
								<form method="post" action="<?php echo base_url('index.php/cart/addtocart'); ?>">
									<input type="hidden" name="pid" value="<?php echo $dataofferre->id; ?>" >
									<input type="hidden" name="qty" value="1" >
									<button type="button" name="addtocartbutton1" onclick="return addtocart(<?php echo $dataofferre->id; ?>);" class="add-to-cart-mt"> <i class="fa fa-shopping-cart"></i><span> Add to Cart</span> </button>
									<button type="submit" name="addtocartbutton" class="add-to-cart-mt1"> <i class="fa fa-bolt"></i><span> Buy Now</span> </button>
								</form>
                            </div>                            
                          </div>
                          <div class="item-info">
                      <div class="info-inner">
                        <div class="item-title"> 
							<a title="<?php echo $dataofferre->name; ?>" href="<?php echo base_url('product/'.slugurl($dataofferre->name)).'/'.encodeurlval($dataofferre->id); ?>">
								<?php echo $dataofferre->name; ?>
							</a>
						</div>
                        <div class="item-content">
                          <div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                          <div class="price-box">
							  <?php if($dataofferre->spacel_price!=""){ ?>
                              <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> <?php echo CURRENCY.$dataofferre->spacel_price; ?> </span> </p>
                              <p class="old-price"> <span class="price-label">Regular Price:</span> <span class="price"> <?php echo CURRENCY.$dataofferre->price; ?> </span> </p>
							  <?php }else{ ?>
							  <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> <?php echo CURRENCY.$dataofferre->price; ?> </span> </p>
							  <?php } ?>
                          </div>
                        </div>
                      </div>
                    </div>
                        </div>
                      </div>					  
					  <?php }  ?>
					  
					  
					  
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
            
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php $home_category = $this->category_model->select_active_home_category(); ?>
  <?php if(count($home_category)>0){ ?>
  <?php foreach($home_category as $catdata){ ?>
  <div class="main-container col1-layout">
    <div class="container">
      <div class="row"> 
      <div class="col-md-2 col-sm-4 hot-deal">
     
          <ul class="products-grid">
            <li class="item">
            <div class="product-item">
                        <div class="item-inner fadeInUp">
                          <div class="product-thumbnail right-hadding-sec">
                         
                            <div class="haddin-sale">
                              <h2 class="text-center"><?php echo $catdata->title; ?></h2>
                               <div class="view-all">
								  
                              	<a href="<?php echo base_url('index.php/home/products/'.slugurl($catdata->title).'/'.$catdata->id);?>" class="view-btn"> <span> View All</span> </a>
                              </div>
                            </div>
                            
                             
                          </div>
                          
                        </div>
                      </div>
                      
              
            </li>
          </ul>
        </div>
        <!-- Home Tabs  -->
        <div class="home-tab col-sm-8 col-md-10 col-xs-12">
          
          <div id="productTabContent" class="tab-content">
            <div class="tab-pane active in" id="new-arrivals">
              <div class="new-arrivals-pro">
                <div class="slider-items-products">
                  <div id="new-arrivals-slider" class="product-flexslider hidden-buttons">
                    <div class="slider-items slider-width-col4">
                    				
					<?php
					$products_str =  $catdata->product_ids; 
					$products_ids = explode(",",$products_str);
					$datalaptop=$this->product_model->selectAllReletedProduct($products_ids);
					foreach($datalaptop  as $laptodata)
					{
						$product_image = $this->product_model->selectProductDoubleImage($laptodata->id);
					?>	
                      <div class="product-item">
                        <div class="item-inner fadeInUp">
                          <div class="product-thumbnail">
                            <div class="pr-img-area">
								<a title="<?php echo $laptodata->name; ?>" href="<?php echo base_url('product/'.slugurl($laptodata->name)).'/'.encodeurlval($laptodata->id); ?>">
									<figure> 
										<?php if(count($product_image)>0){ ?>
										<img class="first-img" src="<?php echo base_url('uploads/product/thumbs'); ?>/<?php echo $product_image[0]->image; ?>" alt="<?php echo $laptodata->name; ?>"> 
										<?php if(isset($product_image[1]->image)){ ?><img class="hover-img" src="<?php echo base_url('uploads/product/thumbs'); ?>/<?php echo $product_image[1]->image; ?>" alt="<?php echo $laptodata->name; ?>"><?php } ?>
										<?php }else{ ?>
										<img class="first-img" src="http://placehold.it/264x264?text=Image not found" alt="<?php echo $laptodata->name; ?>"> 
										<img class="hover-img" src="http://placehold.it/264x264?text=Image not found" alt="<?php echo $laptodata->name; ?>">
										<?php } ?>
									</figure>
								</a>
								<form method="post" action="<?php echo base_url('index.php/cart/addtocart'); ?>">
									<input type="hidden" name="pid" value="<?php echo $laptodata->id; ?>" >
									<input type="hidden" name="qty" value="1" >
									<button type="button" name="addtocartbutton1" onclick="return addtocart(<?php echo $laptodata->id; ?>);" class="add-to-cart-mt"> <i class="fa fa-shopping-cart"></i><span> Add to Cart</span> </button>
									<button type="submit" name="addtocartbutton" class="add-to-cart-mt1"> <i class="fa fa-bolt"></i><span> Buy Now</span> </button>
								</form>
                            </div>                           
                          </div>
                          <div class="item-info">
                      <div class="info-inner">
                        <div class="item-title"> 
							<a title="<?php echo $laptodata->name; ?>" href="<?php echo base_url('product/'.slugurl($laptodata->name)).'/'.encodeurlval($laptodata->id); ?>">
								<?php echo $laptodata->name; ?>
							</a>
						</div>
                        <div class="item-content">
                          <div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                          <div class="price-box">
							  <?php if($laptodata->spacel_price!=""){ ?>
                              <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> <?php echo CURRENCY.$laptodata->spacel_price; ?> </span> </p>
                              <p class="old-price"> <span class="price-label">Regular Price:</span> <span class="price"> <?php echo CURRENCY.$laptodata->price; ?> </span> </p>
							  <?php }else{ ?>
							  <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> <?php echo CURRENCY.$laptodata->price; ?> </span> </p>
							  <?php } ?>
                          </div>
                        </div>
                      </div>
                    </div>
                        </div>
                      </div>
					  <?php } ?>
					  
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
            
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php }} ?>
  
  <?php $recently_product_ids = $this->product_model->getRecentlyViewedProduct(); ?>
  <?php if(count($recently_product_ids)>0){  ?>
  <div class="main-container col1-layout">
    <div class="container">
      <div class="row"> 
      <div class="col-md-2 col-sm-4 hot-deal">
     
          <ul class="products-grid">
            <li class="item">
            <div class="product-item">
                        <div class="item-inner fadeInUp">
                          <div class="product-thumbnail right-hadding-sec">
                         
                            <div class="haddin-sale">
                              <h2 class="text-center">Recently Viewed</h2>
                              
                              
                            </div>
                            
                             
                          </div>
                          
                        </div>
                      </div>
                      
              
            </li>
          </ul>
        </div>
        <!-- Home Tabs  -->
		
		<?php $recent_product = $this->product_model->selectAllReletedProduct($recently_product_ids); ?>
		<?php if(count($recent_product)>0){ ?>
        <div class="home-tab col-sm-8 col-md-10 col-xs-12">
          
          <div id="productTabContent" class="tab-content">
            <div class="tab-pane active in" id="new-arrivals">
              <div class="new-arrivals-pro">
                <div class="slider-items-products">
                  <div id="new-arrivals-slider" class="product-flexslider hidden-buttons">
                    <div class="slider-items slider-width-col4">
                     <?php foreach($recent_product  as $dataarival){
						 $product_image = $this->product_model->selectProductDoubleImage($dataarival->id);
						?>	
                      <div class="product-item">
                        <div class="item-inner fadeInUp">
                          <div class="product-thumbnail">
                            <div class="pr-img-area">
								<a title="<?php echo $dataarival->name; ?>" href="<?php echo base_url('product/'.slugurl($dataarival->name)).'/'.encodeurlval($dataarival->id); ?>">
									<figure> 
										<?php if(count($product_image)>0){ ?>
										<img class="first-img" src="<?php echo base_url('uploads/product/thumbs'); ?>/<?php echo $product_image[0]->image; ?>" alt="<?php echo $dataarival->name; ?>"> 
										<?php if(isset($product_image[1]->image)){ ?><img class="hover-img" src="<?php echo base_url('uploads/product/thumbs'); ?>/<?php echo $product_image[1]->image; ?>" alt="<?php echo $dataarival->name; ?>"><?php } ?>
										<?php }else{ ?>
										<img class="first-img" src="http://placehold.it/264x264?text=Image not found" alt="<?php echo $dataarival->name; ?>"> 
										<img class="hover-img" src="http://placehold.it/264x264?text=Image not found" alt="<?php echo $dataarival->name; ?>">
										<?php } ?>
									</figure>
								</a>
								<form method="post" action="<?php echo base_url('index.php/cart/addtocart'); ?>">
									<input type="hidden" name="pid" value="<?php echo $dataarival->id; ?>" >
									<input type="hidden" name="qty" value="1" >
									<button type="button" name="addtocartbutton1" onclick="return addtocart(<?php echo $dealday->id; ?>);" class="add-to-cart-mt"> <i class="fa fa-shopping-cart"></i><span> Add to Cart</span> </button>
									<button type="submit" name="addtocartbutton" class="add-to-cart-mt1"> <i class="fa fa-bolt"></i><span> Buy Now</span> </button>
								</form>
                            </div>                            
                          </div>
                          <div class="item-info">
                      <div class="info-inner">
                        <div class="item-title"> 
							<a title="<?php echo $dataarival->name; ?>" href="<?php echo base_url('product/'.slugurl($dataarival->name)).'/'.encodeurlval($dataarival->id); ?>">
								<?php echo $dataarival->name; ?>
							</a>
						</div>
                        <div class="item-content">
                          <div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                          <div class="price-box">
							  <?php if($dataarival->spacel_price!=""){ ?>
                              <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> <?php echo CURRENCY.$dataarival->spacel_price; ?> </span> </p>
                              <p class="old-price"> <span class="price-label">Regular Price:</span> <span class="price"> <?php echo CURRENCY.$dataarival->price; ?> </span> </p>
							  <?php }else{ ?>
							  <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> <?php echo CURRENCY.$dataarival->price; ?> </span> </p>
							  <?php } ?>
                          </div>
                        </div>
                      </div>
                    </div>
                        </div>
                      </div>					  
					  <?php } ?>					
                    </div>
                  </div>
                </div>
              </div>
            </div>            
            
          </div>
        </div>
		<?php } ?>
      </div>
    </div>
  </div>
  <?php } ?>
  
  <!-- end main container --> 
 <div class="bottom-banner-section">   
    <div class="container">
      
      <div class="row">
	  
	  <?php
	  error_reporting(0);
	  $categorydata=$this->category_model->selectCategoryForHome();
	  foreach($categorydata as $data)
	  {
		  $id=$data->id;
		  $categoryimage=$this->category_model->selectAllCategoryImages($id);
		  //print_r($categoryimage);
?>
      <div class="col-md-4 col-sm-6"> <a href="<?php echo base_url('products/'.slugurl($data->title)).'/'. encodeurlval($data->id);?>" class="bottom-banner-img">
	  <img src="<?php echo base_url('uploads/category');?>/<?php echo $categoryimage[0]->image;?>" alt="bottom banner"> <span class="banner-overly"></span>
          <div class="bottom-img-info">
            <h3><?php echo $data->title;?></h3>
           
            <span class="shop-now-btn">View more</span> </div>
          </a> 
		  </div>
		  <?php
	  }
	  ?>
		  
  
                  
  
        
        
      </div>
    </div></div>  
    
    <!--special-products-->
 <div class="special-products"> 
  <div class="container">  
  <div class="page-header">
                  <h2>special products</h2>
                  
                </div>
              <div class="special-products-pro">
                <div class="slider-items-products">
                  <div id="special-products-slider3" class="product-flexslider hidden-buttons">
                    <div class="slider-items slider-width-col4">
                    
                     <?php
					$randata=$this->product_model->selecproducthomerand();
					foreach($randata  as $homerandedataprod)
					{
						$product_image = $this->product_model->selectProductDoubleImage($homerandedataprod->id);
					?>	
                      <div class="product-item">
                        <div class="item-inner fadeInUp">
                          <div class="product-thumbnail">
                            <div class="pr-img-area">
							<a title="<?php echo $homerandedataprod->name; ?>" href="<?php echo base_url('product/'.slugurl($homerandedataprod->name)).'/'.encodeurlval($homerandedataprod->id); ?>">
								<figure> 
									<?php if(count($product_image)>0){ ?>
									<img class="first-img" src="<?php echo base_url('uploads/product/thumbs'); ?>/<?php echo $product_image[0]->image; ?>" alt="<?php echo $homerandedataprod->name; ?>"> 
									<img class="hover-img" src="<?php echo base_url('uploads/product/thumbs'); ?>/<?php echo $product_image[1]->image; ?>" alt="<?php echo $homerandedataprod->name; ?>">
									<?php }else{ ?>
									<img class="first-img" src="http://placehold.it/264x264?text=Image not found" alt="<?php echo $homerandedataprod->name; ?>"> 
									<img class="hover-img" src="http://placehold.it/264x264?text=Image not found" alt="<?php echo $homerandedataprod->name; ?>">
									<?php } ?>
								</figure>
							</a>
                            <form method="post" action="<?php echo base_url('index.php/cart/addtocart'); ?>">
								<input type="hidden" name="pid" value="<?php echo $homerandedataprod->id; ?>" >
								<input type="hidden" name="qty" value="1" >
								<button type="button" name="addtocartbutton1" onclick="return addtocart(<?php echo $homerandedataprod->id; ?>);" class="add-to-cart-mt"> <i class="fa fa-shopping-cart"></i><span> Add to Cart</span> </button>
								<button type="submit" name="addtocartbutton" class="add-to-cart-mt1"> <i class="fa fa-bolt"></i><span> Buy Now</span> </button>
							</form>
                            </div>
                            
                          </div>
                          <div class="item-info">
                      <div class="info-inner">
                        <div class="item-title"> 
							<a title="<?php echo $homerandedataprod->name; ?>" href="<?php echo base_url('product/'.slugurl($homerandedataprod->name)).'/'.encodeurlval($homerandedataprod->id); ?>">
								<?php echo $homerandedataprod->name; ?>
							</a>
						</div>
                        <div class="item-content">
                          <div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                          <div class="price-box">
							  <?php if($homerandedataprod->spacel_price!=""){ ?>
                              <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> <?php echo CURRENCY.$homerandedataprod->spacel_price; ?> </span> </p>
                              <p class="old-price"> <span class="price-label">Regular Price:</span> <span class="price"> <?php echo CURRENCY.$homerandedataprod->price; ?> </span> </p>
							  <?php }else{ ?>
							  <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> <?php echo CURRENCY.$homerandedataprod->price; ?> </span> </p>
							  <?php } ?>
                          </div>
                        </div>
                      </div>
                    </div>
                        </div>
                      </div>
					  
					  <?php
					}
					?>
					  
					  
                    </div>
                  </div>
                </div>
              </div>
   
            </div></div>
 


  <!-- our clients Slider -->
 <section class="our-clients">
    <div class="container">
      <div class="slider-items-products">
        <div id="our-clients-slider" class="product-flexslider hidden-buttons"> 
              <div class="slider-items slider-width-col6"> 
            <?php
			$data=$this->our_clint_model->selectallclint();
			foreach($data as $val)
			{
			?>
            <!-- Item -->
            <div class="item wow zoomIn"> <a href="#"><img src="<?php echo base_url('uploads/our_clint');?>/<?php echo $val->image;?>" alt="Image" class="grayscale"></a> </div>
            <!-- End Item --> 
            <?php
			}
			?>
             
            
          </div>
        </div>
      </div>
    </div>
  </section>	
<script>
// Set the date we're counting down to
var countDownDate = new Date("April 16, 2017").getTime();

// Update the count down every 1 second
var x = setInterval(function() {

    // Get todays date and time
    var now = new Date().getTime();
    
    // Find the distance between now an the count down date
    var distance = countDownDate - now;
    
    // Time calculations for days, hours, minutes and seconds
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    
    // Output the result in an element with id="demo"
    document.getElementById("timer").innerHTML =  + hours + ":"
    + minutes + ":" + seconds + "";
    
    // If the count down is over, write some text 
    if (distance < 0) {
        clearInterval(x);
        document.getElementById("timer").innerHTML = "EXPIRED";
    }
}, 1000);
	

function addtocart(pid)
{
	jQuery.ajax({
        	url: "<?php echo base_url('index.php/cart/addtocart_ajax'); ?>",
			type: "POST",
			data:   { product_id : pid},
			//beforeSend: function(){$(this).html('OKKKKKK')},
			success: function(data)
		    {
				jQuery('#cartresponsedata').html(data);
				jQuery(".mini-cart").attr("tabindex",-1).focus(500);
			},
		  	error: function() 
	    	{
				
	    	} 	        
	   });
}
</script>
  <!-- Footer -->
  <?php $this->load->view('front/layout/home-footer');?>