<?php $this->load->view('front/layout/header');?>
<!-- flexslider CSS -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/front/'); ?>/css/flexslider.css" >

  <!-- Breadcrumbs -->
  <div class="breadcrumbs">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
		 <?php
		  $cart=$this->category_model->selectCategoryByID($pagedata[0]->category_id);
		  ?>
          <ul>
            <li class="home"> <a title="Go to Home Page" href="<?php echo base_url(); ?>">Home</a><span>&raquo;</span></li>
             <li class=""> <a title="Go to Home Page" href="shop_grid.html"><?php echo $cart[0]->title;?></a><span>&raquo;</span></li>
            <li class="category13"><strong><?php echo $pagedata[0]->name; ?></strong></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <!-- Breadcrumbs End --> 
  <!-- Main Container -->
  <div class="main-container col1-layout">
  <div class="container">
    <div class="row">
      <div class="col-main">
        <div class="product-view-area">
          <div class="product-big-image col-xs-12 col-sm-5 col-lg-5 col-md-5"> 
            <?php $proimages = $this->product_model->selectProductSingleImage($pagedata[0]->id); ?>	
            <?php if(count($proimages)>0){ ?>			
            <div class="large-image"> <a href="<?php echo base_url('uploads/product/'.$proimages[0]->image); ?>" class="cloud-zoom" id="zoom1" rel="useWrapper: false, adjustY:0, adjustX:20"> <img class="zoom-img" src="<?php echo base_url('uploads/product/'.$proimages[0]->image); ?>" alt="products"> </a> </div>
            <?php } ?>
			<?php $pro_image = $this->product_model->selectProductImages($pagedata[0]->id); ?>
			<?php if(count($pro_image)>0){ ?>
			<div class="flexslider flexslider-thumb">
              <ul class="previews-list slides">
			    <?php foreach($pro_image as $p_image){ ?>
                <li><a href='<?php echo base_url('uploads/product/'.$p_image->image); ?>"' class='cloud-zoom-gallery' rel="useZoom: 'zoom1', smallImage: '<?php echo base_url('uploads/product/'.$p_image->image); ?>' "><img src="<?php echo base_url('uploads/product/thumbs/'.$p_image->image); ?>" alt = "Thumbnail 2"/></a></li>
                <?php } ?>
              </ul>
            </div>
            <?php } ?> 
            <!-- end: more-images --> 
            
          </div>
          <div class="col-xs-12 col-sm-7 col-lg-7 col-md-7">
            <div class="product-details-area">
              <div class="product-name">
                <h1><?php echo $pagedata[0]->name; ?></h1>
              </div>
              <div class="price-box">
			    <?php if($pagedata[0]->spacel_price!=""){ ?>
                <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> $<?php echo $pagedata[0]->spacel_price; ?> </span> </p>
                <p class="old-price"> <span class="price-label">Regular Price:</span> <span class="price"> $<?php echo $pagedata[0]->price; ?> </span> </p>
				<?php }else{ ?>
				<p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> $<?php echo $pagedata[0]->price; ?> </span> </p>
				<?php } ?>
              </div>
              <div class="ratings">
                <div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                <p class="rating-links"> <a href="#">1 Review(s)</a> <span class="separator">|</span> <a href="#">Add Your Review</a> </p>
                <p class="availability in-stock pull-right">Availability: <span><?php echo ($pagedata[0]->product_stock > 0)?'In Stock':'Out Of Stock'; ?></span></p>
              </div>
              <div class="short-description">
                <h2>Quick Overview</h2>
                <p><?php echo $pagedata[0]->sortdesc; ?></p>               
              </div>
              <div class="product-color-size-area">
				<?php $color_data = $this->color_model->getAvailableColors($pagedata[0]->id); ?>
				<?php if(count($color_data)>0){ ?>
                <div class="color-area">
                  <h2 class="saider-bar-title">Color</h2>
                  <div class="color" id="colordiv">
                    <ul >
					 <?php foreach($color_data as $color){ ?>	
                      <li><a style="background:<?php echo $color->color; ?>;" onclick="return setvalue('cid','<?php echo $color->color; ?>');"></a></li>
					 <?php } ?>
                    </ul>
                  </div>
                </div>
				<?php } ?>
				<?php $size_data = $this->size_model->getAvailableSizes($pagedata[0]->id); ?>
				<?php if(count($color_data)>0){ ?>
                <div class="size-area">
                  <h2 class="saider-bar-title">Size</h2>
                  <div class="size" id="sizediv">
                    <ul >                      
                     <?php foreach($size_data as $size){ ?>	
                      <li><a onclick="return setvalue('sid','<?php echo $size->size; ?>');"><?php echo $size->size; ?></a></li>
					 <?php } ?>
                    </ul>
                  </div>
                </div>
				<?php } ?>
              </div>
              <div class="product-variation">
                <form method="post" action="<?php echo base_url('index.php/cart/addtocart'); ?>">
				  <input type="hidden" name="color_id" id='cid' value="0" >
				  <input type="hidden" name="size_id" id='sid' value="0" >
				  <input type="hidden" name="pid" value="<?php echo $pagedata[0]->id; ?>" >
				  
                  <div class="cart-plus-minus">
                    <label for="qty">Quantity:</label>
                    <div class="numbers-row">
                      <div onClick="var result = document.getElementById('qty'); var qty = result.value; if( !isNaN( qty ) &amp;&amp; qty &gt; 0 ) result.value--;return false;" class="dec qtybutton"><i class="fa fa-minus">&nbsp;</i></div>
                      <input type="text" class="qty" title="Qty" value="1" maxlength="12" id="qty" name="qty">
                      <div onClick="var result = document.getElementById('qty'); var qty = result.value; if( !isNaN( qty )) result.value++;return false;" class="inc qtybutton"><i class="fa fa-plus">&nbsp;</i></div>
                    </div>
                  </div>
                  <button onclick="return checkoprion();"  class="button pro-add-to-cart" title="Add to Cart" type="submit" name="addtocartbutton" <?php echo ($pagedata[0]->product_stock > 0)?'':'disabled'; ?>><span><i class="fa fa-shopping-cart"></i> Add to Cart</span></button>
                </form>
              </div>
              <div class="product-cart-option">
                <ul>
				<?php
						$loginuserid=$this->user_model->getLoginUserVar('USER_ID');
						if(!empty($loginuserid))
						{
						?>
<li><a href="<?php echo base_url('index.php/user/addtowishlist/'.$loginuserid.'/'.$pagedata[0]->id);?>"><i class="fa fa-heart"></i><span>Add to Wishlist</span></a></li>
				  <?php
					}else{
						?>
<li><a href="<?php echo base_url('user/loginuser');?>"><i class="fa fa-heart"></i><span>Add to Wishlist</span></a></li>
						<?php
					}
					?>
						
                  
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="product-overview-tab wow fadeInUp">
        <div class="container">
          <div class="row">
            <div class="col-xs-12">
              <ul id="product-detail-tab" class="nav nav-tabs product-tabs">
                <li class="active"> <a href="#description" data-toggle="tab"> Description </a> </li>                
				<li> <a href="#reviews" data-toggle="tab">Reviews</a> </li>
              </ul>
              <div id="productTabContent" class="tab-content">
                <div class="tab-pane fade in active" id="description">
                  <div class="std">
                    <p><?php echo $pagedata[0]->description; ?></p>                    
                  </div>
                </div>
                
                  <div id="reviews" class="tab-pane fade">
							<div class="col-sm-5 col-lg-5 col-md-5">
								<div class="reviews-content-left">
									<h2>Customer Reviews</h2>
									<?php
									$pro_id=$pagedata[0]->id;
									$data=$this->user_model->selectreviewbyid($pro_id);
									foreach($data as $review)
									{
									
									?>
									<div class="review-ratting">
									
									<table>
										<tbody><tr>
											<th>Rating</th>
											<td>
												<div class="rating">
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star-o"></i>
													<i class="fa fa-star-o"></i>
													<i class="fa fa-star-o"></i>
												</div>
											</td>
										</tr>								
										
									</tbody></table>
									
									<p class="author">
										<?php echo $review->comment;?><small> (Posted on <?php echo  $review->comment_date;?>)</small>
									</p>
									</div>
									<?php
									}
									?>
                                    
                                    
                                    
                                    
                                    
                                    
                                    
								</div>
							</div>
							<div class="col-sm-7 col-lg-7 col-md-7">
								<div class="reviews-content-right">
									<h2>Write Your Own Review</h2>
									<form method="post" action="<?php echo base_url('home/selectprocutcoment');?>/<?php echo $pagedata[0]->id;?>">
										<h4>How do you rate this product?<em>*</em></h4>
                                        <div class="table-responsive">
										<table>
											<tbody>
											<tr>
												<th align="center">1 star</th>
												<th align="center">2 stars</th>
												<th align="center">3 stars</th>
												<th align="center">4 stars</th>
												<th align="center">5 stars</th>
											</tr>
											<tr>
												<td align="center"><input type="radio" name="start" value="1"></td>
												<td align="center"><input type="radio"name="start" value="2"></td>
												<td align="center"><input type="radio" name="start" value="3"></td>
												<td align="center"><input type="radio" name="start" value="4"></td>
												<td align="center"><input type="radio"  name="start" value="5"></td>
											</tr>
											
										</tbody></table></div>
										<div class="form-area">
											
											<div class="form-element">
												<label>Review <em>*</em></label>
												<textarea class="form-control input-sm" name="comment"></textarea>
											</div>
											<div class="buttons-set">
												<button class="button submit" name="submit" title="Submit Review" type="submit"><span><i class="fa fa-thumbs-up"></i> &nbsp;Review</span></button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>            
                
              </div>
            </div>
          </div>
        </div>
      </div>    
    </div>
  </div>
</div>
<!-- Main Container End --> 
<?php $releted_products =  $pagedata[0]->reletedproduct;  ?>
<?php if(!empty($releted_products)){ ?>
<?php $releted_products_ids = explode(",",$releted_products); ?>
<?php $rproduct = $this->product_model->selectAllReletedProduct($releted_products_ids); ?>
<?php if(count($rproduct)>0){ ?>
<!-- Related Product Slider -->
  <section class="related-product-area">
  <div class="container">
  <div class="row">
  <div class="col-xs-12">
          <div class="page-header-wrapper">
      <div class="container">
        <div class="page-header text-center wow fadeInUp">
          <h2>Related <span class="text-main">Products</span></h2>
          <div class="divider divider-icon divider-md">&#x268A;&#x268A; &#x2756; &#x268A;&#x268A;</div>
          
        </div>
      </div>
    </div>
 
                <div class="slider-items-products">
                  <div id="related-product-slider" class="product-flexslider hidden-buttons">
                    <div class="slider-items slider-width-col4 fadeInUp">
                      <?php foreach($rproduct as $product){ ?>
					  <div class="product-item">
                        <div class="item-inner fadeInUp">
                          <div class="product-thumbnail">                            
                            <div class="pr-img-area"> 
								<?php $product_image = $this->product_model->selectProductDoubleImage($product->id); ?>
								<?php if(count($product_image)>0){ ?>
								<img class="first-img" src="<?php echo base_url('uploads/product/thumbs'); ?>/<?php echo $product_image[0]->image; ?>" alt="<?php echo $product->name; ?>"> 
								<img class="hover-img" src="<?php echo base_url('uploads/product/thumbs'); ?>/<?php echo $product_image[1]->image; ?>" alt="<?php echo $product->name; ?>">
								<?php }else{ ?>
								<img class="first-img" src="http://placehold.it/264x264?text=Image not found" alt="<?php echo $product->name; ?>"> 
								<img class="hover-img" src="http://placehold.it/264x264?text=Image not found" alt="<?php echo $product->name; ?>">
								<?php } ?>
                                <form method="post" action="<?php echo base_url('index.php/cart/addtocart'); ?>">
								   <input type="hidden" name="pid" value="<?php echo $product->id; ?>" >
								   <input type="hidden" name="qty" value="1" >
								   <button type="submit" name="addtocartbutton" class="add-to-cart-mt"> <i class="fa fa-shopping-cart"></i><span> Add to Cart</span> </button>
								</form>
                            </div>
                            <div class="pr-info-area">
                              <div class="pr-button">
                                <div class="mt-button add_to_wishlist"> <a href="#"> <i class="fa fa-heart"></i> </a> </div>
                              </div>
                            </div>
                          </div>
                          <div class="item-info">
                            <div class="info-inner">
                              <div class="item-title">
								<a title="<?php echo $product->name; ?>" href="<?php echo base_url('product/'.slugurl($product->name)).'/'.encodeurlval($product->id); ?>">
									<?php echo $product->name; ?>
								</a>
							  </div>
                              <div class="item-content">
                                <div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                                <div class="item-price">
                                  <?php if($product->spacel_price!=""){ ?>
								  <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> $<?php echo $product->spacel_price; ?> </span> </p>
								  <p class="old-price"> <span class="price-label">Regular Price:</span> <span class="price"> $<?php echo $product->price; ?> </span> </p>
								  <?php }else{ ?>
								  <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> $<?php echo $product->price; ?> </span> </p>
								  <?php } ?>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <?php } ?>
                    </div>
                  </div>
                </div>
        
                </div>
                </div>
              </div>
              </section>
<!-- Related Product Slider End -->
<?php }} ?>		
<?php $this->load->view('front/layout/footer');?>
<script>
function setvalue(ID,FIELDVAL)
{
	document.getElementById(ID).value =''+FIELDVAL;
}

function checkoprion()
{
	var color = document.getElementById('cid').value;
	var size = document.getElementById('sid').value;
	if(color=="0")
	{
		document.getElementById("colordiv").style.border = "1px solid red";
		document.getElementById("colordiv").style.overflow = "hidden";
		document.getElementById("colordiv").style.padding = "2px";
		return false;
	}
	
	if(size=="0")
	{	
		document.getElementById("sizediv").style.border = "1px solid red";
		document.getElementById("sizediv").style.overflow = "hidden";
		document.getElementById("sizediv").style.padding = "2px";
		return false;
	}	
}
</script>
<!--cloud-zoom js --> 
<script type="text/javascript" src="<?php echo base_url('assets/front'); ?>/js/cloud-zoom.js"></script>
<!-- flexslider js --> 
<script type="text/javascript" src="<?php echo base_url('assets/front'); ?>/js/jquery.flexslider.js"></script> 


