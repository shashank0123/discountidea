<?php $this->load->view('front/layout/header');?>
  <!-- end header --> 
  
  <!-- Navbar -->
  <?php //$this->load->view('front/layout/other_page_left_menu');?>
  <!-- end nav --> 
  
  <!-- Breadcrumbs -->
  
  <div class="breadcrumbs">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <ul>
            <li class="home"> <a title="Go to Home Page" href="index.html">Home</a><span>&raquo;</span></li>
            <li class="category13"><strong>Forgot Password </strong></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <!-- Breadcrumbs End --> 
  
  <!-- Main Container -->
  <section class="main-container col1-layout">
    <div class="main container">
      <div class="account-login">
        <div class="page-title">
          <h2>Forgot Password</h2>
        </div>
        <div class="page-content">
          <div class="row">
		  <?php echo $this->session->flashdata('message');?>
									
		  <form method="post" action="<?php echo base_url('user/forgotpassword?q='.$_GET['q']);?>">
            <div class="col-sm-12">
              <div class="box-authentication">
                <h3>Create an account</h3>
                <p>Please enter your detail to email address.</p>
                <label for="emmail_register">Email address</label>
                <input id="emmail_register" type="email" name="email" class="form-control" required>
				
				
                <button class="button" name="forgotbutn"><i class="fa fa-user"></i>&nbsp; <span>Forgot Password</span></button>
              </div>
            </div>
			</form>
			
          </div>
        </div>
      </div>
      <br>
      <br>
      <br>
      <br>
      <br>
    </div>
  </section>
  <!-- Main Container End --> 
  <!-- our clients Slider -->
  
  <!-- home contact -->
 
  <!-- Footer -->
  <?php $this->load->view('front/layout/footer');?>