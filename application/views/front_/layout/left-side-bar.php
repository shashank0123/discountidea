
<style>
.color > ul > li > label > .active, .color > ul > li > label > a:hover, .color  > ul > li > label > a:visited 
{	
    border: 2px solid #000;
}
</style>

<?php 
$filters_array = (!empty($pagedata[0]->filters_id))?explode(",",$pagedata[0]->filters_id):array(0);
$filters_data = $this->filters_model->selectFiltersWhereId_in($filters_array); 
?>
<div class="category-sidebar">
            <div class="sidebar-title">
              <h3>Categories</h3>
            </div>
			<?php if(isset($pagedata[0]->id)){ ?>
            <ul class="product-categories">              
			  <?php 
				$categoty_data = $this->category_model->selectAllCategoryParentID($pagedata[0]->id);
			    foreach($categoty_data as $category){ 
				$child_data = $this->category_model->countChildNav($category->id);
				$has_child =(count($child_data)>0)?'cat-parent':'';
				?>
				<li class="cat-item <?php echo $has_child; ?>"><a  href="<?php echo base_url('products/'.slugurl($category->title)).'/'.encodeurlval($category->id); ?>"><?php echo $category->title; ?></a>
				 <?php echo $this->category_model->getLeftSideBarFilterMenus($category->id); ?>
				</li>
			  <?php } ?>
            </ul>
			<?php } ?>
          </div>
		  <form id="filterform" name="filterform" method="post">
		  <div class="product-price-range wow fadeInUp">
            <div class="sidebar-bar-title">
              <h3>Price</h3>
            </div>
            <div class="block-content">
              <div class="slider-range">
                <div data-label-reasult="Range:" data-min="0" data-max="1000" data-unit="<?php echo CURRENCY; ?>" class="slider-range-price" data-value-min="0" data-value-max="1000"></div>
                <div class="amount-range-price">Range: <?php echo CURRENCY; ?>0 - <?php echo CURRENCY; ?>1000</div>
				
					<input type="hidden" id="cat_id" name="category" value="<?php echo $pagedata[0]->id; ?>">
					<input type="hidden" id="minprice" name="minprice" value="0">
					<input type="hidden" id="maxprice" name="maxprice" value="1000">
              </div>
            </div>
          </div>
		  <?php if(count($filters_data)>0){ ?>
		  <div class="category-sidebar">
            <div class="sidebar-bar-title">
              <h3>Shop By</h3>
            </div>
            <ul class="product-categories">              
				<?php foreach($filters_data as $filters){ ?>
				<?php $option_data = $this->filters_model->selectAllFiltersOptions($filters->id); ?>
				<li class="cat-item cat-parent"><a href="#"><?php echo $filters->title; ?></a>
					<?php if(count($option_data)>0){ ?>
					<ul class="check-box-list">					    
						<?php foreach($option_data as $option){ ?>
						<li>
							<input type="checkbox" id="<?php echo md5($option->id); ?>" name="options[]" value="<?php echo $option->id; ?>" onclick="return filterdata();" />
							<label for="<?php echo md5($option->id); ?>"> <span class="button"></span> <?php echo $option->title; ?> </label>
						</li>
						<?php } ?>
					</ul>
					<?php } ?>
				</li>             
				<?php } ?>    
            </ul>
          </div>
		  <?php } ?>
		  
		  </form>
		   <?php if(count($this->cart->contents())>0){ ?>
		   <div class="sidebar-cart wow fadeInUp">
            <div class="sidebar-bar-title">
              <h3>My Cart</h3>
            </div>
            <div class="block-content">
              <p class="amount">There are <a href="#"><?php echo count($this->cart->contents()); ?> items</a> in your cart.</p>              
			  <ul>
				<?php foreach($this->cart->contents() as $items){ ?>
				<?php $proimages = $this->product_model->selectProductSingleImage($items['id']); ?>
				<?php $product = $this->product_model->selectProductById($items['id']);  ?>
                <li class="item"> 
				  <?php if(count($proimages)>0){ ?>
						<a  href="<?php echo base_url('product/'.slugurl($product[0]->name)).'/'.encodeurlval($product[0]->id); ?>" class="product-image"><img src="<?php echo base_url('uploads/product/'.$proimages[0]->image); ?>" ></a>
				  <?php }else{ ?>
						<a  href="<?php echo base_url('product/'.slugurl($product[0]->name)).'/'.encodeurlval($product[0]->id); ?>" class="product-image"><img  src="http://placehold.it/100x100?text=Image not found"></a>
				  <?php } ?>
                  <div class="product-details">
                    <div class="access"> <a href="<?php echo base_url('cart/remove/'.$items['rowid']); ?>" title="Remove This Item" class="remove-cart"><i class="icon-close"></i></a></div>
                    <p class="product-name"> <a href="<?php echo base_url('product/'.slugurl($product[0]->name)).'/'.encodeurlval($product[0]->id); ?>"><?php echo $product[0]->name; ?></a></p>
                     <strong><?php echo $items['qty']; ?></strong> x <span class="price"><?php echo CURRENCY.$items['price']; ?></span> </div>
                </li>
                <?php } ?> 
              </ul>
              <div class="summary">
                <p class="subtotal"> <span class="label">Cart Subtotal:</span> <span class="price"><?php echo CURRENCY.$this->cart->total(); ?></span> </p>
              </div>
              <div class="cart-checkout">
                <a href="<?php echo base_url('index.php/cart/'); ?>"><button class="button button-checkout" title="Submit" type="submit"><i class="fa fa-check"></i> <span>View Cart</span></button></a>
              </div>
            </div>
          </div>
		   <?php } ?>
<script>
function filterdata()
{
	jQuery.ajax({
				url: "<?php echo base_url('home/filterproduct') ?>",
				data: $("#filterform").serialize(), //'minprice='+MINPRICE+'&maxprice='+MAXPRICE+'&category='+CATEGORY,
				type: "POST",
				beforeSend: function(){
					$('#filterresponse').html("<center><img src='<?php echo base_url(); ?>/assets/front/images/loading.gif'></center>");
				},
				success:function(mydata)
				{		
				     //alert(mydata); 
					jQuery('#filterresponse').html(mydata);
				},
				 error: function(ts) { alert(ts.responseText) }
			});
}
</script>		   