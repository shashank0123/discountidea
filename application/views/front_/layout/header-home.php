<!DOCTYPE html>
<html lang="en">

<head>

<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Discount idea</title>
<meta name="description" content="">

<!-- Mobile specific metas  -->
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Favicon  -->
<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">

<!-- Google Fonts -->
<link href='https://fonts.googleapis.com/css?family=Karla:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,200italic,300,300italic,400,400italic,600,600italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Dancing+Script:400,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Source+Code+Pro:400,500,600,700,300' rel='stylesheet' type='text/css'>

<!-- CSS Style -->

<!-- Bootstrap CSS -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/front');?>/css/bootstrap.min.css">

<!-- font-awesome & simple line icons CSS -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/front');?>/css/font-awesome.css" media="all">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/front');?>/css/simple-line-icons.css" media="all">

<!-- owl.carousel CSS -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/front');?>/css/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/front');?>/css/owl.theme.css">

<!-- animate CSS  -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/front');?>/css/animate.css" media="all">

<!-- flexslider CSS -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/front');?>/css/flexslider.css" >

<!-- Nivo Slider CSS -->
<link href="<?php echo base_url('assets/front');?>/css/nivo-slider.css" rel="stylesheet">

<!-- style CSS -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/front');?>/css/style.css" media="all">

<!-- shortcodes -->
<link rel="stylesheet" media="screen" href="<?php echo base_url('assets/front');?>/css/shortcodes/shortcodes.css" type="text/css" />

<!-- accordion -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/front');?>/js/accordion/accordion.css" />

<!-- tooltips -->
<link rel="stylesheet" href="<?php echo base_url('assets/front');?>/js/tooltips/tooltip.css" />

<!-- popup newsletter css -->
<link rel="stylesheet" href="<?php echo base_url('assets/front');?>/css/popup-newsletter.css">

<!-- Home3 CSS -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/front');?>/css/home3.css" media="all">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/front/css/my_account.css" media="all">

<style>
.customlink a{    font-size: 14px;
    color: #fff;
    margin-right: 6px;
	margin-left:2px !important;
	font-size:13px;
	}
	
	.loaderpage {
	position: fixed;
	left: 0px;
	top: 0px;
	width: 100%;
	height: 100%;
	z-index: 9999;
	background: url('<?php echo base_url('assets/front');?>/images/loading-logo.png') 50% 50% no-repeat rgb(249,249,249);
}
</style>
<script>
window.onload = function(e) {
   jQuery(".loaderpage").fadeOut("slow");
   document.getElementById('mypopupaa').click();
};
</script>
</head>

<body class="cms-index-index cms-home-page home-3">
<div class="loaderpage"></div>
<!-- mobile menu -->
<?php $this->load->view('front/layout/mobile-menu'); ?>
<!-- end mobile menu -->
<div id="page"> 
  <!-- newsletter popup -->
  
  <div class="newsletter-popup" style="display:none;">
    <div class="newsletter-bg newsletter-ready"></div>
    <div class="newsletter-wrap newsletter-close-btn-in">
      <div class="newsletter-container">
        <div class="newsletter-content">
          <div class="news-inner">
            <div class="news-popup">
              <div class="popup-title">
                <h2>Newsletter <span class="text-main">Popup</span></h2>
                <div class="divider divider-icon divider-md">&#x268A;&#x268A; &#x2756; &#x268A;&#x268A;</div>
                <p class="notice">Sign up to our email newsletter to be the first to hear about great offers &amp; more</p>
              </div>
              <form class="form-subscribe">
                <input placeholder="Sign up your email..." type="text">
                <button class="button"><i class="fa fa-envelope"></i> SUBSCRIBE</button>
              </form>
              <div class="checkbox">
                <label>
                  <input value="" type="checkbox">
                  DON’T SHOW THIS POPUP AGAIN</label>
              </div>
            </div>
            <button title="Close (Esc)" type="button" class="newsletter-close">×</button>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  <!-- end newsletter popup --> 
  <!-- Header -->
  <header>
    <div class="header-container">
      <div class="container">
        <div class="row">
          <div class="col-sm-4 col-md-2 col-xs-12"> 
            <!-- Header Logo -->
            <div class="logo"><a title="Logo" href="<?php echo base_url(); ?>"><img alt="Logo" src="<?php echo base_url('assets/front');?>/images/logo-blue.png"></a> </div>
            <!-- End Header Logo --> 
          </div>
          <div class="col-xs-8 col-sm-4 col-md-7 hidden-xs">
            <div class="support-client">
              <div class="headerlinkmenu" style="text-align:left;">
            <div class="links">
			  <?php
			  $user_id=$this->user_model->getLoginUserVar('USER_ID');
			  if(empty($user_id)) {
			  ?> 
              <div class="myaccount"><a title="My Account" href="<?php echo base_url('vendor/loginuser'); ?>"><span class="hidden-xs">sale on discount idea</span></a></div>
              <?php } ?>
              <div class="blog hidden-xs"><a title="Blog" href="#"><span class="hidden-xs">track your order</span></a></div>
              <form method="get" action="<?php echo base_url('home/searchproduct');?>">
              <div class="search-box">
					<input name="q" placeholder="Search" class="form-control" type="text" required>
                    <button type="submit" class="btn btn-warning search-bth" ><i class="fa fa-search"></i></button>
                  
              </div>
			  </form>
            </div>
            
          </div>
            </div>
          </div>
          <!-- top cart -->
          
          <div class="col-lg-3 col-xs-12 top-cart">
            <div class="mm-toggle-wrap">
              <div class="mm-toggle"> <i class="fa fa-align-justify"></i><span class="mm-label">Menu</span> </div>
            </div>
            <div class="top-cart-contain">
              <div class="mini-cart">
                <div data-toggle="dropdown" data-hover="dropdown" class="basket dropdown-toggle"> <a href="#"><i class="fa fa-shopping-cart"></i><span class="cart-title">Shopping Cart (<?php echo count($this->cart->contents()); ?>)</span></a></div>
                <?php if(count($this->cart->contents())>0){ ?>
				<div>
                  <div class="top-cart-content" id="cartresponsedata">
                    <div class="block-subtitle">Recently added item(s)</div>
                    <ul id="cart-sidebar" class="mini-products-list">
					  <?php foreach($this->cart->contents() as $items){ ?>
					  <?php $proimages = $this->product_model->selectProductSingleImage($items['id']); ?>
					  <?php $product = $this->product_model->selectProductById($items['id']);  ?>
                      <li class="item odd"> 					    
						<?php if(count($proimages)>0){ ?>
							<a  href="<?php echo base_url('product/'.slugurl($product[0]->name)).'/'.encodeurlval($product[0]->id); ?>" class="product-image"><img src="<?php echo base_url('uploads/product/'.$proimages[0]->image); ?>" ></a>
					    <?php }else{ ?>
							<a  href="<?php echo base_url('product/'.slugurl($product[0]->name)).'/'.encodeurlval($product[0]->id); ?>" class="product-image"><img  src="http://placehold.it/100x100?text=Image not found"></a>
					    <?php } ?>
                        <div class="product-details"> <a href="<?php echo base_url('cart/remove/'.$items['rowid']); ?>" title="Remove This Item" class="remove-cart"><i class="icon-close"></i></a>
                          <p class="product-name"><a href="<?php echo base_url('product/'.slugurl($product[0]->name)).'/'.encodeurlval($product[0]->id); ?>"><?php echo $product[0]->name; ?></a> </p>
                          <strong><?php echo $items['qty']; ?></strong> x <span class="price"><?php echo CURRENCY.$items['price']; ?></span> </div>
                      </li>
                      <?php } ?>                      
                    </ul>
                    <div class="top-subtotal">Subtotal: <span class="price"><?php echo CURRENCY.$this->cart->total(); ?></span></div>
                    <div class="actions">
                      <a href="<?php echo base_url('index.php/cart/checkout');?>"><button class="btn-checkout" type="button"><i class="fa fa-check"></i><span>Checkout</span></button></a>
                      <a href="<?php echo base_url('index.php/cart/'); ?>"><button class="view-cart" type="button"><i class="fa fa-shopping-cart"></i> <span>View Cart</span></button></a>
                    </div>
                  </div>
                </div>
				<?php } ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </header>
  <!-- end header --> 
  
  <!-- Navbar -->
  <nav>
    <div class="container">
      <div class="row">
        <div class="col-md-3 col-sm-3">
          <div class="mega-container visible-lg visible-md visible-sm">
            <div class="navleft-container">
              <div class="mega-menu-title">
                <h3>Category</h3>
              </div>
              <div class="mega-menu-category">
                <ul class="nav">
                  <!-- <li> <a href="#">Home</a></li> -->
                  <?php $d=0; foreach($this->category_model->selectAllParentCategory() as $category){ $d++; ?>
				  <?php if($d<10){ ?>
                  <li> 				  
					<a href="<?php echo base_url('products/'.slugurl($category->title)).'/'.encodeurlval($category->id); ?>"><?php echo $category->title; ?></a>
					<?php  echo $this->category_model->getNavigation($category->id);  ?>                    
                  </li>
				  <?php }} ?>
				  <?php $e=0; foreach($this->category_model->selectAllParentCategory() as $category){ $e++; ?>
				  <?php if($e>9){ ?>
					<li class="more-menu"><a href="<?php echo base_url('products/'.slugurl($category->title)).'/'.encodeurlval($category->id); ?>"><?php echo $category->title; ?></a>
					<?php  echo $this->category_model->getNavigation($category->id);  ?> </li>
				  <?php }} ?>	
					 <li class="view-more"><a href="#">More category</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-9">
          <div class="language-currency-wrapper col-sm-3 col-md-5 col-lg-6">
           
            <!-- Default Welcome Message -->
            <div class="welcome-msg hidden-xs hidden-sm"> 
			    <div class="customlink">
					<a href="http://www.publicfunda.com/index.php/user/signup" target="_blank">Earn From Home </a>
					<a href="http://www.publicfunda.com/index.php/user/dashboard" target="_blank">Public-Funda-Network</a> 
					<a href="http://www.publicfunda.com">Create Business Page </a>
				</div>
			</div>
            <!-- End Default Welcome Message -->  
          </div>
          <!-- top links -->
          <div class="headerlinkmenu col-lg-6 col-md-7 col-sm-6 col-xs-6">
            <div class="links">
			  <div class="myaccount"><a title="My Account" href="<?php echo base_url('user/profile');?>"><i class="fa fa-user"></i><span class="hidden-xs">My Account</span></a></div>
                <div class="wishlist"><a title="My Wishlist" href="<?php echo base_url('user/wishlist');?>"><i class="fa fa-heart"></i><span class="hidden-xs">Wishlist</span></a></div>
			  
			  <div class="blog hidden-xs"><a title="Blog" target="_blank" href="<?php echo base_url('blog');?>"><i class="fa fa-rss"></i><span class="hidden-xs">Blog</span></a></div>
			  <?php
			  $user_id=$this->user_model->getLoginUserVar('USER_ID');
			  if(!empty($user_id))
			  {
			  ?>                
				<div class="login"><a href="<?php echo base_url('user/logoutuser');?>"><i class="fa fa-unlock-alt"></i><span class="hidden-xs">Logout</span></a></div>
				<?php
			  }else
			  {
			  ?>
                
                <div class="login"><a href="<?php echo base_url('user/loginuser');?>"><i class="fa fa-unlock-alt"></i><span class="hidden-xs">Log In</span></a></div>
				<?php
			  }
			  ?>
              </div>
			
          </div>
        </div>
      </div>
    </div>
  </nav>
  <!-- end nav -->