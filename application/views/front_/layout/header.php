<!DOCTYPE html>
<html lang="en">

<head>
<!-- Basic page needs -->
<meta charset="utf-8">
<!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <![endif]-->
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Discount Idea </title>
<meta name="description" content="">

<!-- Mobile specific metas  -->
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Favicon  -->
<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">

<!-- Google Fonts -->
<link href='https://fonts.googleapis.com/css?family=Karla:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,200italic,300,300italic,400,400italic,600,600italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Dancing+Script:400,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Source+Code+Pro:400,500,600,700,300' rel='stylesheet' type='text/css'>

<!-- CSS Style -->

<!-- Bootstrap CSS -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/front'); ?>/css/bootstrap.min.css">

<!-- font-awesome & simple line icons CSS -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/front'); ?>/css/font-awesome.css" media="all">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/front'); ?>/css/simple-line-icons.css" media="all">

<!-- owl.carousel CSS -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/front'); ?>/css/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/front'); ?>/css/owl.theme.css">

<!-- animate CSS  -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/front'); ?>/css/animate.css" media="all">

<!-- jquery-ui.min CSS  -->
<link rel="stylesheet" href="<?php echo base_url('assets/front'); ?>/css/jquery-ui.css">

<!-- main CSS -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/front'); ?>/css/main.css" media="all">

<!-- style CSS -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/front'); ?>/css/style.css" media="all">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/front/css/my_account.css" media="all">

</head>

<body class="shop_grid_page">
<!-- mobile menu -->

<!-- end mobile menu -->
<div id="page"> 
  <!-- Header -->
  <header>
    <div class="header-container">
      <div class="header-top">
        <div class="container">
          <div class="row">
            <div class="col-lg-4 col-md-5 col-sm-5 col-xs-6 language-currency-wrapper">
              <!--
              <div class="welcome-msg hidden-xs"><i class="fa fa-envelope" ></i> Email id : <a href="mailto:discountidea@info.com">Discountidea@info.com</a> </div>
				-->
			 <!-- End Default Welcome Message --> 
            </div>
            <!-- top links -->
            <div class="headerlinkmenu col-lg-8 col-md-7 col-sm-7 col-xs-6">
              <div class="links">
			  <div class="myaccount"><a title="My Account" href="<?php echo base_url('user/profile');?>"><i class="fa fa-user"></i><span class="hidden-xs">My Account</span></a></div>
                <div class="wishlist"><a title="My Wishlist" href="<?php echo base_url('user/wishlist');?>"><i class="fa fa-heart"></i><span class="hidden-xs">Wishlist</span></a></div>
			  <?php
			  $user_id=$this->user_model->getLoginUserVar('USER_ID');
			  if(!empty($user_id))
			  {
			  ?>                
				<div class="login"><a href="<?php echo base_url('index.php/user/logoutuser');?>"><i class="fa fa-unlock-alt"></i><span class="hidden-xs">Logout</span></a></div>
				<?php
			  }else
			  {
			  ?>                
                <div class="login"><a href="<?php echo base_url('user/loginuser');?>"><i class="fa fa-unlock-alt"></i><span class="hidden-xs">Log In</span></a></div>
				<?php
			  }
			  ?>
              </div>
              
              <!-- Search -->
              
              
              
              <!-- End Search --> 
            </div>
          </div>
        </div>
      </div>
      <div class="container">
        <div class="row">          
          <!--support client-->
         <div class="col-sm-4 col-md-2 col-xs-12"> 
            <!-- Header Logo -->
            <div class="logo"><a title="Logo" href="<?php echo base_url(); ?>"><img alt="Logo" src="<?php echo base_url('assets/front');?>/images/logo-blue.png"></a> </div>
            <!-- End Header Logo --> 
          </div>
          <div class="col-xs-8 col-sm-4 col-md-7 hidden-xs">
            <div class="support-client">
              <div class="headerlinkmenu" style="text-align:left;">
            <div class="links" >
			 <?php
			  $user_id=$this->user_model->getLoginUserVar('USER_ID');
			  if(empty($user_id)) {
			  ?> 
              <div class="myaccount"><a title="My Account" href="<?php echo base_url('vendor/loginuser'); ?>"><span class="hidden-xs">sale on discount idea</span></a></div>
              <?php } ?>
              <div class="blog hidden-xs"><a title="Blog" href="#"><span class="hidden-xs">track your order</span></a></div>
              <form method="get" action="<?php echo base_url('home/searchproduct');?>">
              <div class="search-box">
					<input name="q" placeholder="Search" class="form-control" type="text" required>
                    <button type="submit" class="btn btn-warning search-bth" ><i class="fa fa-search"></i></button>
                  
              </div>
			  </form>
            </div>
            
          </div>
            </div>
          </div>
		  
		  
          <div class="col-lg-3 col-xs-12 top-cart">
            <div class="mm-toggle-wrap">
              <div class="mm-toggle"> <i class="fa fa-align-justify"></i><span class="mm-label">Menu</span> </div>
            </div>
            <div class="top-cart-contain">
              <div class="mini-cart" id="cartresponsedata">
                <div data-toggle="dropdown" data-hover="dropdown" class="basket dropdown-toggle"> <a href="#"><i class="fa fa-shopping-cart"></i><span class="cart-title">Shopping Cart (<?php echo count($this->cart->contents()); ?>)</span></a></div>
                <?php if(count($this->cart->contents())>0){ ?>
				<div>
                  <div class="top-cart-content">
                    <div class="block-subtitle">Recently added item(s)</div>
                    <ul id="cart-sidebar" class="mini-products-list">
					  <?php foreach($this->cart->contents() as $items){ ?>
					  <?php $proimages = $this->product_model->selectProductSingleImage($items['id']); ?>
					  <?php $product = $this->product_model->selectProductById($items['id']);  ?>
                      <li class="item odd"> 					    
						<?php if(count($proimages)>0){ ?>
							<a  href="<?php echo base_url('product/'.slugurl($product[0]->name)).'/'.encodeurlval($product[0]->id); ?>" class="product-image"><img src="<?php echo base_url('uploads/product/'.$proimages[0]->image); ?>" ></a>
					    <?php }else{ ?>
							<a  href="<?php echo base_url('product/'.slugurl($product[0]->name)).'/'.encodeurlval($product[0]->id); ?>" class="product-image"><img  src="http://placehold.it/100x100?text=Image not found"></a>
					    <?php } ?>
                        <div class="product-details"> <a href="<?php echo base_url('cart/remove/'.$items['rowid']); ?>" title="Remove This Item" class="remove-cart"><i class="icon-close"></i></a>
                          <p class="product-name"><a href="<?php echo base_url('product/'.slugurl($product[0]->name)).'/'.encodeurlval($product[0]->id); ?>"><?php echo $product[0]->name; ?></a> </p>
                          <strong><?php echo $items['qty']; ?></strong> x <span class="price"><?php echo CURRENCY.$items['price']; ?></span> </div>
                      </li>
                      <?php } ?>                      
                    </ul>
                    <div class="top-subtotal">Subtotal: <span class="price"><?php echo CURRENCY.$this->cart->total(); ?></span></div>
                    <div class="actions">
                      <a href="<?php echo base_url('index.php/cart/checkout');?>"><button class="btn-checkout" type="button"><i class="fa fa-check"></i><span>Checkout</span></button></a>
                      <a href="<?php echo base_url('index.php/cart/'); ?>"><button class="view-cart" type="button"><i class="fa fa-shopping-cart"></i> <span>View Cart</span></button></a>
                    </div>
                  </div>
                </div>
				<?php } ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </header>
  <!-- end header --> 
  
  <!-- Navbar -->
  <nav>
    <div class="stick-logo"><a title="Home Page" href="<?php echo base_url(); ?>"><img alt="logo" src="<?php echo base_url('assets/front'); ?>/images/stick-logo.png" width="77%"></a> </div>
    <div class="container">
      <div class="row">
        <div class="mtmegamenu">
          <ul>
            <li class="mt-root demo_custom_link_cms">
              <div class="mt-root-item"><a href="<?php echo base_url(); ?>">
                <div class="title title_font"><span class="title-text">Home</span></div>
                </a></div>
              
            </li>
            
            <?php $r=0; foreach($this->category_model->selectAllParentCategory() as $category){ $r++;?>
			<?php if($r<=8){ ?>
			<li class="mt-root">
              <div class="mt-root-item">
				<a href="<?php echo base_url('products/'.slugurl($category->title)).'/'.encodeurlval($category->id); ?>">
					<div class="title title_font"><span class="title-text"><?php echo $category->title; ?></span></div>
                </a>
			  </div>
			 <?php $second_categoty = $this->category_model->selectAllCategoryParentID($category->id); ?>
			 <?php if(count($second_categoty>0)){ ?>
              <ul class="menu-items col-md-10 col-xs-12">
			    <?php foreach($second_categoty as $scategory){ ?>
                <li class="menu-item depth-1 category menucol-1-4 ">
                  <div class="title title_font"> <a href="<?php echo base_url('products/'.slugurl($scategory->title)).'/'.encodeurlval($scategory->id); ?>"> <?php echo $scategory->title; ?> </a></div>
                  <?php $third_categoty = $this->category_model->selectAllCategoryParentID($scategory->id); ?>
				  <?php if(count($third_categoty>0)){ ?>
				  <ul class="submenu">
					<?php foreach($third_categoty as $tcategory){ ?>
                    <li class="menu-item">
                      <div class="title"> <a href="<?php echo base_url('products/'.slugurl($tcategory->title)).'/'.encodeurlval($tcategory->id); ?>"> <?php echo $tcategory->title; ?>  </a></div>
                    </li>
					<?php } ?>
                  </ul>
				  <?php } ?> 
                </li>
				<?php } ?>         
              </ul>
			 <?php } ?>
            </li> 
			<?php } } ?>
                   
            <li class="mt-root demo_custom_link_cms">
              <div class="mt-root-item"><a href="<?php echo base_url('home/contact_us');?>">
                <div class="title title_font"><span class="title-text">Contact Us</span></div>
                </a>
			   </div>              
            </li>            
            
          </ul>
        </div>
      </div>
    </div>
  </nav>
  <!-- end nav --> 