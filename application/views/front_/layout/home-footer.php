
  <!-- Footer -->
  <div class="footer-newsletter">
    <div class="container">
      <div class="row">
        <div class="col-md-3 col-sm-3 hidden-sm hidden-xs">
          <h3>Sign up for newsletter</h3>
          <p>Duis autem vel eum iriureDuis autem</p>
        </div>
        <div class="col-md-5 col-sm-7">
          <form id="newsletter-validate-detail" method="post" action="#">
            <div class="newsletter-inner">
              <input class="newsletter-email" name='Email' placeholder='Enter Your Email'/>
              <button class="button subscribe" type="submit" title="Subscribe">Subscribe</button>
            </div>
          </form>
        </div>
        <div class="social col-md-4 col-sm-5">
          <ul class="inline-mode">
            <li class="social-network fb"><a title="Connect us on Facebook" target="_blank" href="https://www.facebook.com/"><i class="fa fa-facebook"></i></a></li>
            <li class="social-network googleplus"><a title="Connect us on Google+" target="_blank" href="https://plus.google.com/"><i class="fa fa-google-plus"></i></a></li>
            <li class="social-network tw"><a title="Connect us on Twitter" target="_blank" href="https://twitter.com/"><i class="fa fa-twitter"></i></a></li>
            <li class="social-network rss"><a title="Connect us on Instagram" target="_blank" href="https://instagram.com/"><i class="fa fa-rss"></i></a></li>
            <li class="social-network linkedin"><a title="Connect us on Linkedin" target="_blank" href="https://www.pinterest.com/"><i class="fa fa-linkedin"></i></a></li>
            <li class="social-network instagram"><a title="Connect us on Instagram" target="_blank" href="https://instagram.com/"><i class="fa fa-instagram"></i></a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <footer>
<?php
$block1 = $this->block_model->selectblockbyid(1);
$block2 = $this->block_model->selectblockbyid(2);
$block3 = $this->block_model->selectblockbyid(3);
$block4 = $this->block_model->selectblockbyid(4);
$block5 = $this->block_model->selectblockbyid(5);
$block6 = $this->block_model->selectblockbyid(6);
$block7 = $this->block_model->selectblockbyid(7);
 ?> 
    <div class="container">
        <div class="row">
        <div class="col-sm-12 col-xs-12 col-lg-4">
          <div class="footer-logo"><a href="#"><img src="<?php echo base_url('assets/front'); ?>/images/footer-logo.png" alt="fotter logo"></a></div>
          <p><?php echo geturl('{BASE_URL}',$block1[0]->description); ?></p>
          
        </div>
        <div class="col-sm-6 col-md-3 col-xs-12 col-lg-2 collapsed-block">
          <div class="footer-links">
            <?php echo geturl('{BASE_URL}',$block2[0]->description); ?>
          </div>
        </div>
        <div class="col-sm-6 col-md-3 col-xs-12 col-lg-2 collapsed-block">
          <div class="footer-links">
            <?php echo geturl('{BASE_URL}',$block3[0]->description); ?>
          </div>
        </div>
        <div class="col-sm-6 col-md-3 col-xs-12 col-lg-2 collapsed-block">
          <div class="footer-links">
            <?php echo geturl('{BASE_URL}',$block4[0]->description); ?>
          </div>
        </div>
        <div class="col-sm-6 col-md-3 col-xs-12 col-lg-2 collapsed-block">
          <div class="footer-links">
           <?php echo geturl('{BASE_URL}',$block5[0]->description); ?>
          </div>
        </div>
      </div>
    </div>
    <div class="footer-coppyright">
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-xs-12 coppyright"><?php echo $block6[0]->description; ?></div>
        </div>
      </div>
    </div>
  </footer>
  <a href="#" class="totop"> </a> </div>
<!-- End Footer --> 

<?php $data=$this->homepopup_model->selectallpopup();?>
<?php if($data[0]->status==1){ ?>
<button style="display:none;" id="mypopupaa" data-toggle="modal" data-target="#myModalPoppup">x</button>
<!-- Modal -->
  <div class="modal fade" id="myModalPoppup" role="dialog" style="top:11%;">
    <div class="modal-dialog modal-lg" style="text-align:center;">    
      <!-- Modal content-->
      <div class="modal-content">
        <img src="<?php echo base_url('uploads/cms/'.$data[0]->image); ?>">  
      </div>      
    </div>
  </div>
<?php } ?>
<!-- JS --> 

<!-- cookies js --> 
<script type="text/javascript" src="<?php echo base_url('assets/front');?>/js/cookies.js"></script> 

<!-- jquery js --> 
<script type="text/javascript" src="<?php echo base_url('assets/front');?>/js/jquery.min.js"></script> 

<!-- bootstrap js --> 
<script type="text/javascript" src="<?php echo base_url('assets/front');?>/js/bootstrap.min.js"></script> 

<!-- owl.carousel.min js --> 
<script type="text/javascript" src="<?php echo base_url('assets/front');?>/js/owl.carousel.min.js"></script> 

<!-- bxslider js --> 
<script type="text/javascript" src="<?php echo base_url('assets/front');?>/js/jquery.bxslider.js"></script> 

<!-- Nivo Slider Js --> 
<script type="text/javascript" src="<?php echo base_url('assets/front');?>/js/jquery.nivo.slider.pack.js"></script> 

<!-- megamenu js --> 

<script type="text/javascript">
        /* <![CDATA[ */   
        var mega_menu = '0';
        
        /* ]]> */
        </script> 

<!-- jquery.mobile-menu js --> 
<script type="text/javascript" src="<?php echo base_url('assets/front');?>/js/mobile-menu.js"></script> 

<!-- jquery.waypoints js --> 
<script type="text/javascript" src="<?php echo base_url('assets/front');?>/js/waypoints.js"></script>  

<!--jquery-ui.min js --> 
<script src="<?php echo base_url('assets/front');?>/js/jquery-ui.js"></script> 

<!-- main js --> 
<script type="text/javascript" src="<?php echo base_url('assets/front');?>/js/main.js"></script> 

<!-- infographic js --> 
<script type="text/javascript" src="<?php echo base_url('assets/front');?>/js/infographic.js"></script> 

<!-- jquery.waypoints js --> 
<script type="text/javascript" src="<?php echo base_url('assets/front');?>/js/waypoints.js"></script> 

<!-- tooltips js --> 
<script src="<?php echo base_url('assets/front');?>/js/tooltips/jquery.tooltip.js"></script> 

<!-- countdown js -->
<script type="text/javascript" src="<?php echo base_url('assets/front');?>/js/countdown.js"></script> 


<!-- popup newsletter js --> 
<script type="text/javascript" src="<?php echo base_url('assets/front');?>/js/popup-newsletter.js"></script> 

 
<script type="text/javascript">
			/* Main Slideshow */
			jQuery(window).load(function() {
				jQuery(document).off('mouseenter').on('mouseenter', '.pos-slideshow', function(e){
					jQuery('.ma-banner7-container .timethai').addClass('pos_hover');
				});
				jQuery(document).off('mouseleave').on('mouseleave', '.pos-slideshow', function(e){
					jQuery('.ma-banner7-container .timethai').removeClass('pos_hover');
				});
			});
			jQuery(window).load(function() {
				jQuery('#ma-inivoslider-banner7').nivoSlider({
					effect: 'random',
					slices: 15,
					boxCols: 8,
					boxRows: 4,
					animSpeed: 1000,
					pauseTime: 6000,
					startSlide: 0,
					controlNav: false,
					controlNavThumbs: false,
					pauseOnHover: true,
					manualAdvance: false,
					prevText: 'Prev',
					nextText: 'Next',
					afterLoad: function(){
						},     
					beforeChange: function(){ 
					}, 
					afterChange: function(){ 
					}
				});
			});
		</script> 
<!--cloud-zoom js --> 
<script type="text/javascript" src="<?php echo base_url('assets/front');?>/js/cloud-zoom.js"></script>
<!-- Hot Deals Timer 1--> 
<script type="text/javascript">
            var dthen1 = new Date("12/25/16 11:59:00 PM");
            start = "08/04/15 03:02:11 AM";
            start_date = Date.parse(start);
            var dnow1 = new Date(start_date);
            if(CountStepper>0)
                ddiff= new Date((dnow1)-(dthen1));
            else
                ddiff = new Date((dthen1)-(dnow1));
            gsecs1 = Math.floor(ddiff.valueOf()/1000);
            
            var iid1 = "countbox_1";
            CountBack_slider(gsecs1,"countbox_1", 1);
        </script>
		

		
		</body>

</html>