<?php $this->load->view('front/layout/header');?>  <!-- end nav --> 
   <style>
.naseemdiv
{
	height: 210px;
    overflow: scroll;
    border: 2px solid rgba(34, 34, 34, 0.41);
    padding: 10px;
}
.naseemdiv label{padding: 3px;}
.naseemdiv ul {padding-left:5px; list-style:none;}
</style> 
  <!-- Breadcrumbs -->
  
  <div class="breadcrumbs">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <ul>
            <li class="home"> <a title="Go to Home Page" href="<?php echo base_url(); ?>">Home</a><span>&raquo;</span></li>
            <li class="category13"><strong>My Products</strong></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <!-- Breadcrumbs End --> 
  
  <!-- Main Container -->
  <section class="main-container col1-layout">
    <div class="main container">
      <div class="account-login mb-30">
        	<div class="row">
            <div class="col-md-3">
              <div id="left-pnl" class="aside-site-left my_account_section   ">
                      
<div class="action_links">
     <?php $this->load->view('front/layout/my-account-left-sidebar'); ?>   
</div>


                <div class="sidebox-bottom"> <span> &nbsp; </span> </div>
                              </div>
            </div>
            <div class="col-md-8">
              <div class="aside-site-content my_account_section ">
                                            <div class="site-content ">
        
                 <div class="central-content"> 
        
        



<div class="cm-notification-container"></div>
            
                   
                          
    
    <div class="mainbox-container margin margin">
   <div class="mainbox-body"><div class="main_section" style="display: block;">
    <div class="my_order" style="display: block;">
	<div class="sub_action_links">
	    <h3>Add Product</h3>
	
	    	</div>
		    <div class="my_order_list table-responsive" style="padding:3px;">
			<form method="post">
			<table class="table table-bordered table-hover">
			
			
			<tr>
										<td>Category</td>
										<td><select name="category_id" class="form-control">
								  <?php $data = $this->category_model->getSelectOption(0); ?>
                                </select></td>
									</tr>
								
			
									<tr>
										<td>Product Name</td>
										<td><input type="text" name="name"  class="form-control" autocomplete="off" required></td>
									</tr>
									<tr>
										<td>Brand (Trade Mark)</td>
										<td><input type="text" name="brand"  class="form-control" autocomplete="off" required></td>
									</tr>
									<tr>
										<td> Sort Description</td>
										<td><textarea name="sortdesc" value="" class="form-control" rows="3"></textarea></td>
									</tr>
									<tr>
										<td> Description</td>
										<td><textarea name="description"  class="form-control" rows="4"></textarea></td>
									</tr>
									<tr>
										<td>Price</td>
										<td><input type="text" name="price"  class="form-control" autocomplete="off" required></td>
									</tr>
									
									<tr>
										<td>Special Price</td>
										<td><input type="text" name="spacel_price"  class="form-control" autocomplete="off"></td>
									</tr>
									<tr>
										<td>Discount</td>
										<td><input type="text" name="descount" class="form-control" autocomplete="off"></td>
									</tr>
									<tr>
										<td>Product Stock</td>
										<td>
                                            <input type="text" name="product_stock"  class="form-control" autocomplete="off">
										</td>
									</tr>

<tr>
							<td>Product Filters</td>
							<td>
								<div class="naseemdiv" style="height: 300px;">
									
									<?php $filter_data = $this->filters_model->selectAllFilters(); ?>
									<?php foreach($filter_data as $filter){ ?>
									<?php $option_data = $this->filters_model->selectAllFiltersOptions($filter->id); ?>
									<div class="col-md-4" style="border:1px solid #ddd; overflow: scroll; height: 200px;">
										<h4><?php echo $filter->title; ?></h4>
										<small><label><input type='checkbox' name="filter[]" value="<?php echo $filter->id; ?>"> Dispay In product details page<label></small>
										<ul>  
											<?php if(count($option_data)>0){ ?>
											<?php foreach($option_data as $option){ ?>
											<li><label><input  type='checkbox' name="options[]" value="<?php echo $option->id; ?>"> <?php echo $option->title; ?></label> </li>
											<?php } ?>
											<?php } ?>
										</ul>
										
									</div>
									<?php } ?>
								</div>
							</td>
						</tr>


									<tr>
							<td>Pincode</td>
							<td>
                        <span id="pinerrror"></span>
							    <input type="number" name="dp" id="mypin" value="" placeholder="add your pincode">
								<button type="button" onclick="return addpincode();">Add Pincode</button>
								<div class="naseemdiv">
								<?php 
								$pincodequery = $this->pincode_model->selectAllPincode();								
								 
								foreach ($pincodequery as $row){ ?>
		<label><input type="checkbox"  name="pincode_ids[]"  value="<?php echo $row->id;?>" /> <?php echo $row->pincode; ?></label>
								<?php } ?>
                                                                  <span id="pinresponse"></span>
								</div>
							</td>
						</tr>
									<tr>
										<td>Status</td>										
										<td>
											<select  name="status" class="form-control">
												<option value="1">Active</option>
												<option value="0">Inactive</option>
											</select>
										</td>
									</tr>
									<tr>
										<td colspan="2"><button type="submit" class="btn btn-info" name="add_product">Submit</button></td>
									</tr>
									
			</table> 	
			</form>	
	    </div></div>
		
    </div>
</div>

        
</div>
    </div>
           
                 </div> 
                   </div>
        
                    </div>
            </div>
          </div>
      </div>
     
    </div>
  </section>
  <!-- Main Container End --> 
  <!-- our clients Slider -->
  <?php $this->load->view('front/layout/footer');?>
    <?php  $user_id=$this->user_model->getLoginUserVar('USER_ID'); ?>
  <script>
   function addpincode()
  {	  
	var pincode = document.getElementById('mypin').value;
	if(pincode!="")
	{	
		jQuery.ajax({
				url: "<?php echo base_url('index.php/vendor/addpincode'); ?>",
				type: "POST",
				data:   { pincode : pincode,uid : <?php echo $user_id; ?>},
				success: function(data)
				{
	                                if(data==0)
                                        {
                                             $('#pinerrror').html(' <div class="alert alert-danger">this pincode allready exists.</div>');
                                         }
                                         else
                                         {
                                              $('#pinresponse').append(data);
                                         } 			
                                       
				},
				error: function() 
				{
					alert('error');
				} 	        
		   });
	}
  }
  </script>