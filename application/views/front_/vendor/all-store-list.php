<?php $this->load->view('front/layout/header');?>
<style>
.logo .gallery {
    margin: 5px;
    border: 1px solid #ccc;
    float: left;
    width: 180px;
}

.logo .gallery:hover {
    border: 1px solid #777;
}

.logo .gallery img {
    width: 100%;
    height: auto;
}

.logo .desc {
    padding: 15px;
    text-align: center;
}
</style>
  <!-- Breadcrumbs -->
  <div class="breadcrumbs">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
		
          <ul>
            <li class="home"> <a title="Go to Home Page" href="<?php echo base_url(); ?>">Home</a><span>&raquo;</span></li>
			<li class="home"> <a title="Store" href="<?php echo base_url(); ?>">Store</a><span></span></li>
		 </ul>
        </div>
      </div>
    </div>
  </div>
  <!-- Breadcrumbs End --> 
  <!-- Main Container -->
  <div class="main-container col2-left-layout">
    <div class="container">
      <div class="row">
        <div class="col-main col-sm-12 col-md-12">
          <div class="page-title">
            <?php foreach($storedata as $store){ ?>
             
             	 <div class="logo gallery">
  <a target="_blank" href="<?php echo base_url('vendor/store/my-store/'.$store->id) ?>">
    <img src="<?php echo base_url('uploads/store/'.$store->logo); ?>" width='100' height='auto'>
  </a>
  <div class="desc"><?php echo $store->title; ?></div>
</div>
                  
	          
              
            
            <?php } ?>
          </div>
        	  
        </div>
      </div>
    </div>
  </div>
  <!-- Main Container End -->   			
<?php $this->load->view('front/layout/footer');?>