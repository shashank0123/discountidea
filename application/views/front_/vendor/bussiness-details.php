<?php $this->load->view('front/layout/header');?>  <!-- end nav --> 
  
  <!-- Breadcrumbs -->
  
  <div class="breadcrumbs">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <ul>
            <li class="home"> <a title="Go to Home Page" href="<?php echo base_url(); ?>">Home</a><span>&raquo;</span></li>
            <li class="category13"><strong>My Products</strong></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <!-- Breadcrumbs End --> 
  
  <!-- Main Container -->
  <section class="main-container col1-layout">
    <div class="main container">
      <div class="account-login mb-30">
        	<div class="row">
            <div class="col-md-3">
              <div id="left-pnl" class="aside-site-left my_account_section   ">
                      
<div class="action_links">
     <?php $this->load->view('front/layout/my-account-left-sidebar'); ?>   
</div>


                <div class="sidebox-bottom"> <span> &nbsp; </span> </div>
                              </div>
            </div>
            <div class="col-md-9">
			<div class="aside-site-content my_account_section ">
				<a href="<?php echo base_url('index.php/vendor/order_report'); ?>"><button class="btn-warning orange" style="padding:5px;">Order Report</button></a>
				<a href="<?php echo base_url('index.php/vendor/cancellation_report'); ?>"><button class="btn-warning orange" style="padding:5px;">Cancellation Report</button></a>
			</div>
              <div class="aside-site-content my_account_section ">
                                            <div class="site-content ">
        
                 <div class="central-content"> 
        
        



<div class="cm-notification-container"></div>
            
                   
                          
    
    <div class="mainbox-container margin margin">
   <div class="mainbox-body"><div class="main_section" style="display: block;">
    <div class="my_order" style="display: block;">
	<div class="sub_action_links">
	    <h3>Bussiness Details</h3>
	    <?php echo $this->session->flashdata('message'); ?>
	    	
		    <div class="my_order_list table-responsive" style="padding:3px;">
			<form method="post" enctype='multipart/form-data'>
			<table class="table table-bordered table-hover">				
				<tr>
					<td>Bussiness Type</td>
					<td>
						<select name="company_type" class="form-control" onchange="return checkbussinesstype(this.value);">
							<option <?php echo ($user[0]->company_type=='proprietor')?'selected':''; ?> value="proprietor">Proprietor</option>
							<option <?php echo ($user[0]->company_type=='company')?'selected':'selected'; ?> value="company">Company</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>Bussiness  Name</td>
					<td><input type="text" name="company_name"  class="form-control" value="<?php echo $user[0]->company_name; ?>" autocomplete="off" required></td>
				</tr>
				<tr>
					<td>TIN / VAT</td>
					<td><input type="text" name="tin_vat"  class="form-control" value="<?php echo $user[0]->tin_vat; ?>" autocomplete="off" required>
						<input type="file" name="tin_vat_upload" class="form-control" required>
						<?php if($user[0]->tin_vat_upload!=""){ ?>
							<img src="<?php echo base_url('uploads/vendor/'.$user[0]->tin_vat_upload) ?>" width="150">
						<?php } ?>
						<input type="hidden" name="tin_vat_upload_old" value="<?php echo $user[0]->tin_vat_upload; ?>">
					</td>
				</tr>
				<tr>
					<td>TAN</td>
					<td><input type="text" name="tan"  class="form-control" value="<?php echo $user[0]->tan; ?>" autocomplete="off" required>
						<input type="file" name="tan_upload" class="form-control" required>
						<?php if($user[0]->tan_upload!=""){ ?>
							<img src="<?php echo base_url('uploads/vendor/'.$user[0]->tan_upload) ?>" width="150">
						<?php } ?>
						<input type="hidden" name="tan_upload_old" value="<?php echo $user[0]->tan_upload; ?>">
					</td>
				</tr>
				<tr id="cpan" style="display:<?php echo ($user[0]->company_type=='proprietor')?'none':''; ?>">
					<td>Company Pan</td>
					<td><input type="text" name="company_pan" id="companypancard"  class="form-control" value="<?php echo $user[0]->company_pan; ?>" autocomplete="off" >
						<input type="file" name="company_pan_upload" class="form-control" required>
						<?php if($user[0]->company_pan_upload!=""){ ?>
							<img src="<?php echo base_url('uploads/vendor/'.$user[0]->company_pan_upload) ?>" width="150">
						<?php } ?>
						<input type="hidden" name="company_pan_upload_old" value="<?php echo $user[0]->company_pan_upload; ?>">
					</td>
				</tr>
				<tr>
					<td>GSTIN / Provisional ID Number</td>
					<td><input type="text" name="gstin"  class="form-control" value="<?php echo $user[0]->gstin; ?>" autocomplete="off" required>
						<input type="file" name="gstin_upload" class="form-control" required>
						<?php if($user[0]->gstin_upload!=""){ ?>
							<img src="<?php echo base_url('uploads/vendor/'.$user[0]->gstin_upload) ?>" width="150">
						<?php } ?>
						<input type="hidden" name="gstin_upload_old" value="<?php echo $user[0]->gstin_upload; ?>">
					</td>
				</tr>

<tr>
					<td>Trade Mark / Brand uses Authority Letter / Agency Certificate </td>
					<td>
						<input type="file" name="trademark_upload" class="form-control" required>
						<?php if($user[0]->trademark_upload!=""){ ?>
							<img src="<?php echo base_url('uploads/vendor/'.$user[0]->trademark_upload) ?>" width="150">
						<?php } ?>
						<input type="hidden" name="trademark_upload_old" value="<?php echo $user[0]->trademark_upload; ?>">
					</td>
				</tr>
				<tr><?php if($user[0]->bank_status==0){ ?>
				<td colspan="2"><button type="submit" class="btn btn-info" name="bussinessdetails">Submit</button></td><?php }
				</tr>					
			</table> 	
			</form>	
	    </div></div>
    </div>
</div>

        
</div>
    </div>
           
                 </div> 
                   </div>
        
                    </div>
            </div>
          </div>
      </div>
     
    </div>
  </section>
  <!-- Main Container End --> 
  <!-- our clients Slider -->
  <script>
	function checkbussinesstype(VAL)
	{
		//alert(VAL);
		if(VAL=='company')
		{
                        //companypancard
                        $("#companypancard").attr("required", "true");
			$("#cpan").show();
		}
		else
		{
                        $("#companypancard").removeAttr('required');
			$("#cpan").hide();
		}	
		
	}
  </script>
  <?php $this->load->view('front/layout/footer');?>