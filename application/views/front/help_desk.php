<?php $this->load->view('front/layout/header');?>  <!-- end nav --> 

  

  <!-- Breadcrumbs -->

  <style>
 .panel-default>.panel-heading {
    color: #fff;
    background-color: #1fa67a;
    border-color: #1fa67a;
}
h1.page-header {
	font-size: 22px;
    color: #f68245;
    border-bottom: 1px solid #f56c23;
}


/* The Modal (background) */
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 9999; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content */
.modal-content {
    position: relative;
    background-color: #fefefe;
    margin: auto;
    padding: 0;
    border: 1px solid #888;
    width: 92%;
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);
    -webkit-animation-name: animatetop;
    -webkit-animation-duration: 0.4s;
    animation-name: animatetop;
    animation-duration: 0.4s
}

/* Add Animation */
@-webkit-keyframes animatetop {
    from {top:-300px; opacity:0} 
    to {top:0; opacity:1}
}

@keyframes animatetop {
    from {top:-300px; opacity:0}
    to {top:0; opacity:1}
}

/* The Close Button */
.close {
    color: black;
    float: right;
    font-size: 28px;
    font-weight: bold;
}

.close:hover,
.close:focus {
    color: #000;
    text-decoration: none;
    cursor: pointer;
}

/* The Close1 Button */
.Close1 {
    color: black !important;
    float: right !important;
    font-size: 28px !important;
    font-weight: bold !important;
}

.Close1:hover,
.Close1:focus {
    color: #000;
    text-decoration: none;
    cursor: pointer;
}

/* The Close2 Button */
.Close2 {
    color: black;
    float: right;
    font-size: 28px;
    font-weight: bold;
}

.Close2:hover,
.Close2:focus {
    color: #000;
    text-decoration: none;
    cursor: pointer;
}

.modal-header {
    padding: 2px 16px;
    background-color: #5cb85c;
    color: black;
}

.modal-body1 {padding: 2px 16px;}



    /* The Modal (background) */
.modal1 {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 9999; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}


.modal2 {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 9999; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content */
.modal-content1 {
    position: relative;
    background-color: #fefefe;
    margin: auto;
    padding: 0;
    border: 1px solid #888;
    width: 92%;
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);
    -webkit-animation-name: animatetop;
    -webkit-animation-duration: 0.4s;
    animation-name: animatetop;
    animation-duration: 0.4s
}
.modal-header1 {
    padding: 2px 16px;
    background-color: #ff3366;
    color: black;
}
/* Add Animation */
@-webkit-keyframes animatetop {
    from {top:-300px; opacity:0} 
    to {top:0; opacity:1}
}

@keyframes animatetop {
    from {top:-300px; opacity:0}
    to {top:0; opacity:1}
}

.panel {
    margin-bottom: 0 !important;
    }
button
                {
                        padding: 3px 8px 4px 5px !important;
                        color: blue;
                        background: -webkit-linear-gradient(top, rgb(144, 195, 86) 0%, rgb(100, 154, 39) 100%);
                        border-width: 1px !important;
                        color: #ffffff;
                        border-radius: 10px;
                        color: white;
                        background: #ff3366;
                }
  .aj-form {
	  padding:1em;
  }
  .aj-button {
	  padding:5px 10px;
	  background:#ccc;
	  color:#000;
	  
  }
  
  .aj-button:hover {
	  background:#000;
	  color:#fff;
	  
  }
  
  .search-results-list li {
    border-bottom: 1px solid #eee;
    margin-bottom: 15px;
    padding-bottom: 15px;
    position: relative;
}
  </style>

  <div class="breadcrumbs">

    <div class="container">

      <div class="row">

        <div class="col-xs-12">

          <ul>

            <li class="home"> <a title="Go to Home Page" href="index.html">Home</a><span>&raquo;</span></li>

            <li class="category13"><strong>My Orders</strong></li>

          </ul>

        </div>

      </div>

    </div>

  </div>

  <!-- Breadcrumbs End --> 

  

  <!-- Main Container -->

  <section class="main-container col1-layout">

    <div class="main container-fluid">

      <div class="account-login mb-30">

        	<div class="row">

            <div class="col-md-3">

              <div id="left-pnl" class="aside-site-left my_account_section   ">

                      

<div class="action_links">

     <?php $this->load->view('front/layout/my-account-left-sidebar'); ?>   

</div>





                <div class="sidebox-bottom"> <span> &nbsp; </span> </div>

                              </div>

            </div>

            <div class="col-md-9">

              <div class="aside-site-content my_account_section ">

                                            <div class="site-content ">

        

                 <div class="central-content"> 

        

        







<div class="cm-notification-container"></div>

            

                   

                          

    

    <div class="mainbox-container margin margin">

   <div class="mainbox-body"><div class="main_section" style="display: block;">

    <div class="my_order" style="display: block;">
<ul class="nav nav-tabs">
  <li class="active"><a data-toggle="tab" href="#home">Query Status</a></li>
  <li><a data-toggle="tab" href="#menu1">Submit Query</a></li>
  
</ul>

<div class="tab-content">
  <div id="home" class="tab-pane fade in active">
  <?php if(!empty($this->session->flashdata('sms'))){ ?>
  <h3 style="background: #ff3366; color: #fff;"><?php echo $this->session->flashdata('sms'); ?></h3><?php } ?>
    <div class="my_order_list table-responsive" style="padding:3px;">

            <?php if(count($myorder)>0){ ?>

			<table class="table table-bordered table-hover">

				<thead>

					<tr>

						<th>S. No.</th>
						<th>Ticket ID</th>
						<th>User ID</th>
						<th>Order ID</th>
						<th>Product ID</th>
						<th>Vendor Name</th>
						<th>User Contact</th>
						<th>Vendor Contact</th>
						<th>Department</th>

						<th>Executive</th>

						<th>Date</th>

						<th>Status</th>
						<th></th>

					</tr>

				</thead>

				<tbody>

					<?php $j=1; foreach($myorder as $order){ ?>

					<?php $items = $this->user_model->getOrderItemsByOrderId($order->id); ?>

					<?php $user = $this->user_model->selectuserby_id($order->uid); ?>

					<tr>
						<td><?php echo $j; ?></td>
						<td>#<?php echo $order->id; ?></td>

						<td><?php echo $user[0]->fname.' '.$user[0]->lname; ?></td>

						

						
						<td><?php echo $order->order_product; ?></td>
						<td><?php echo $order->product_id; ?></td>
						<?php $vender_id = $this->user_model->selectuserby_id($order->vender_id); ?>
						<td><?php echo $vender_id[0]-> company_name ; ?></td>
						<td><?php echo $user[0]->mobile; ?></td>
						<td><?php echo $vender_id[0]->mobile; ?></td>
						<?php $select_depart = $this->user_model->select_depart($order->type); ?>
						<td><?php echo $select_depart[0]->title; ?></td>
						
						<td><?php echo $select_depart[0]->name; ?></td>
						

						<td><?php echo $order->queryTime; ?></td>
						<td><?php echo $order->status; ?></td>
						<td><button data-booking2="myBtn<?php echo $order->id ?>" id="myBtn2" class="myBtn2"><i class="fa fa-eye"></i> View</button><input type="hidden"  id="myBtn<?php echo $order->id ?>" value="<?php echo $order->id  ?>"></td>

					</tr>

					<?php $j++; } ?>

				</tbody>

			</table> 		

			<?php  } ?> 		 		    								    				<div  id="myModal2" class="modal2">

                      
                          <div class="modal-content1">
                            <div class="modal-header1">
                              <span class="Close2">&times;</span>
                              <h2>Submit Your Query</h2>
                            </div>
                            <div class="modal-body1"  id="booking_val2"></div>
                          </div>

                    </div>	

	    </div>
  </div>
  <div id="menu1" class="tab-pane fade">
  
  <div class="aj-form">
    <form method="post" action="insert_help_desk"  enctype="multipart/form-data" >
	
	<div class="col-md-6">
  <div class="form-group">
    <label for="email">Select Department:</label>
	<input type="hidden" name="uid" value="<?=$this->session->userdata('id')?>">
    <select name="type" class="form-control" required>
	<option value="">---Select Department---</option>
	<?php foreach($Depart as $Departs){ ?>
		<option value="<?=$Departs->id?>"><?=$Departs->title?></option>
	<?php } ?>
	</select>
  </div>
  </div>
  <div class="col-md-6">
  <div class="form-group">
    <label for="email">Select Order ID:</label>
	<?php //print_r($getOrderByUserId); die; ?>
    <select name="order_product" id="order_product" class="form-control" required>
	<option value="">---Select Oder ID---</option>
	<?php foreach($myorder1 as $Order){ ?>
		<option value="<?=$Order->id?>"><?=$Order->id?></option>
	<?php } ?>
	</select>
  </div>
  </div>
  <div class="col-md-6">
  <div class="form-group">
    <label for="email">Select Product ID:</label>
	<?php //print_r($getOrderByUserId); die; ?>
    <select name="product_id" id="product_id" class="form-control" required>
	<option value="">---Select Product ID---</option>
	
	</select>
  </div>
  </div>
  <div class="col-md-6">
  <div class="form-group">
    <label for="pwd">Subject:</label>
    <input type="text" name="subject" class="form-control" id="pwd" required>
  </div>
  </div>
  <div class="col-md-12">
  <div class="form-group">
    <label for="pwd">Query:</label>
    <textarea name="query" class="form-control" rows="3" required></textarea>
  </div>
  </div> 
  <div class="col-md-6">
  <div class="form-group">
    <label for="pwd">File Upload:</label>
    <input type="file" name="file" class="form-control" id="pwd" required>
  </div>
  </div>
   <div class="col-md-6">
   <div class="form-group">
   <label for="pwd">&nbsp</label>
  <input type="submit" name="submit" class="btn btn-default" value="Submit">
  </div>
  </div>
  <div class="clearfix"></div>
</form>
  </div>
 </div>
</div>
	

		    

		

    </div>

</div>



        

</div>

    </div>

           

                 </div> 

                   </div>

        

                    </div>

            </div>

          </div>

      </div>

     

    </div>

  </section>

  <!-- Main Container End --> 

  <!-- our clients Slider -->

  <?php $this->load->view('front/layout/footer');?>