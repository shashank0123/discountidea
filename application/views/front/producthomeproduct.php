<?php $this->load->view('front/layout/header');?>
  
  <!-- Breadcrumbs End --> 
  <!-- Main Container -->
  <div class="main-container col2-left-layout">
    <div class="container">
      <div class="row">
      
        <div class="col-main col-sm-12">          
          <div class="toolbar123">
            <center><h2><?php echo $pagedata[0]->title; ?></h2></div></center>    
          </div>
		  <span id="filterresponse">
          <div class="product-grid-area" id="griddata">
            <ul class="products-grid">
			<!--<?php print_r($productdata); ?>-->
			  <?php if(count($productdata)>0){ ?>
			  <?php foreach($productdata as $product){ ?>
<?php $sws_cat=$this->product_model->ProductCategory($product->category_id);  ?>
              <li class="item col-lg-3 col-md-3 col-sm-6 col-xs-6 wow fadeInUp">
                <div class="product-item">
                  <div class="item-inner">
                    <div class="product-thumbnail">                    
                       <div class="pr-img-area">
						<!--<a  href="<?php echo base_url('product/'.slugurl($product->name)).'/'.encodeurlval($product->id); ?>">	-->
<?php $sws_cat=$this->product_model->ProductCategory($product->category_id);  ?>
                           <a  href="<?php echo base_url(slugurl($sws_cat->ptitle).'/'.slugurl($sws_cat->title).'/'.slugurl($product->name).'/'.encodeurlval($product->id)); ?>">
	
						 <figure>
							<?php $product_image = $this->product_model->selectProductDoubleImage($product->id); ?>
							<?php if(count($product_image)>0){ ?>
					<img class="first-img" src="<?php echo base_url('uploads/product/thumbs'); ?>/<?php echo $product_image[0]->image; ?>" alt="<?php echo $product->name; ?>">      <?php  }?> 
							 <?php if(count($product_image)>1){ ?>
							<img class="hover-img" src="<?php echo base_url('uploads/product/thumbs'); ?>/<?php echo $product_image[1]->image; ?>" alt="<?php echo $product->name; ?>">
							<?php }else{ ?>
							<img class="first-img" src="<?php echo base_url('assets/front');?>/images/264x264.jpg" alt="<?php echo $product->name; ?>"> 
							<img class="hover-img" src="<?php echo base_url('assets/front');?>/images/264x264.jpg" alt="<?php echo $product->name; ?>">
							<?php } ?>
						 </figure>
						 </a>

						 <form method="post" action="<?php echo base_url('index.php/cart/addtocart'); ?>">
						   <input type="hidden" name="pid" value="<?php echo $product->id; ?>" >
						   <input type="hidden" name="qty" value="1" >
<!--<button type="button" name="addtocartbutton1" onclick="return addtocart(<?php echo $product->id; ?>);" class="add-to-cart-mt"> <i class="fa fa-shopping-cart"></i><span> Add to Cart</span> </button>-->
                                 <?php $loginuserid=$this->user_model->getLoginUserVar('USER_ID');?>
								<a class="add-to-cart-mt" style="font-size:11px" href="<?php echo base_url('index.php/user/addtowishlist/'.$loginuserid.'/'.$product->id);?>"><i class="fa fa-heart"></i><span>Add to Wishlist</span></a>
						   <!--<button type="submit" name="addtocartbutton" class="add-to-cart-mt1"> <i class="fa fa-shopping-cart"></i><span> Boy Now</span> </button>-->
						   <a href="<?php echo base_url(slugurl($sws_cat->ptitle).'/'.slugurl($sws_cat->title).'/'.slugurl($product->name).'/'.encodeurlval($product->id)); ?>"><button type="button" name="addtocartbutton" class="add-to-cart-mt1"> <i class="fa fa-bolt"></i><span> Buy Now</span> </button></a>
                            
						</form>
					 </div>			  			  
                      <div class="pr-info-area">
                        <div class="pr-button">
                          <!--<div class="mt-button add_to_wishlist"> <a href="#"> <i class="fa fa-heart"></i> </a> </div>-->
                        </div>
                      </div>
                    </div>
                    <div class="item-info">
                      <div class="info-inner">
                        <div class="item-title"> 
							<a title="<?php echo $product->name; ?>" href="<?php echo base_url(slugurl($sws_cat->ptitle).'/'.slugurl($sws_cat->title).'/'.slugurl($product->name)).'/'.encodeurlval($product->id); ?>">
								<?php echo $product->name; ?>
							</a>
						</div>
                        <div class="item-content">
                          <div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                          <div class="price-box">
							  <?php if($product->spacel_price!=""){ ?>
                              <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> <?php echo CURRENCY.$product->spacel_price; ?> </span> </p>
                              <p class="old-price"> <span class="price-label">Regular Price:</span> <span class="price"> <?php echo CURRENCY.$product->price; ?> </span> </p>
							  <?php }else{ ?>
							  <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> <?php echo CURRENCY.$product->price; ?> </span> </p>
							  <?php } ?>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </li>
              <?php } } ?>
            </ul>
          </div>
		     <div class="product-list-area" id="listdata" style="display:none;">
            <ul class="products-list" id="products-list" >
              <?php if(count($productdata)>0){ ?>
			  <?php foreach($productdata as $product){ ?>
			  <?php $proimages = $this->product_model->selectProductSingleImage($product->id); ?>
              <li class="item wow fadeInUp">
                <div class=" product-img col-sm-4 col-lg-4 col-md-4">                  
				   <?php if(count($proimages)>0){ ?>
							<a  href="<?php echo base_url('product/'.slugurl($product->name)).'/'.encodeurlval($product->id); ?>"><figure><img src="<?php echo base_url('uploads/product/thumbs/'.$proimages[0]->image); ?>"></figure></a>
					  <?php }else{ ?>
							<a  href="<?php echo base_url('product/'.slugurl($product->name)).'/'.encodeurlval($product->id); ?>"><figure><img  src="http://placehold.it/100x100?text=Image not found" ></figure></a>
					  <?php } ?>
                </div>
                <div class="col-sm-8 col-lg-8 col-md-8">
					<div class="product-shop">
					  <h2 class="product-name">
							<a title="<?php echo $product->name; ?>" href="<?php echo base_url('product/'.slugurl($product->name)).'/'.encodeurlval($product->id); ?>">
								<?php echo $product->name; ?>
							</a>
						</h2>
						<div class="ratings">
						  <div class="rating"> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
						  <p class="rating-links"> <a href="#/">0 Review(s)</a> <span class="separator">|</span> <a href="#review-form">Add Your Review</a> </p>
						</div>
					  <div class="desc std"><?php echo $product->sortdesc; ?>
					  <a class="link-learn" title="Read More" href="<?php echo base_url('product/'.slugurl($product->name)).'/'.encodeurlval($product->id); ?>">Read More</a> 
					  </div>
					  <div class="price-box">
						<?php if($product->spacel_price!=""){ ?>
                             <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> <?php echo CURRENCY.$product->spacel_price; ?> </span> </p>
                              <p class="old-price"> <span class="price-label">Regular Price:</span> <span class="price"> <?php echo CURRENCY.$product->price; ?> </span> </p>
							  <?php }else{ ?>
							  <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> <?php echo CURRENCY.$product->price; ?> </span> </p>
						<?php } ?>
					  </div>
					  
						
						 <form method="post" action="<?php echo base_url('index.php/cart/addtocart'); ?>">
						   <input type="hidden" name="pid" value="<?php echo $product->id; ?>" >
						   <input type="hidden" name="qty" value="1" >
						   <div class="actions">
						  <button class="button cart-button" title="Add to Cart" type="submit" name="addtocartbutton" ><i class="fa fa-shopping-cart"></i><span>Add to Cart</span></button>
						  
						   </div>
						</form>
					</div>
				</div>
              </li>
			  <?php } } ?>
            </ul>
		  </div>
		  </span>		  
        </div>
      </div>
    </div>
  </div>
  <!-- Main Container End -->   			
<?php $this->load->view('front/layout/footer');?>

<script>
function addtocart(pid)
{
	jQuery.ajax({
        	url: "<?php echo base_url('index.php/cart/addtocart_ajax'); ?>",
			type: "POST",
			data:   { product_id : pid},
			//beforeSend: function(){$(this).html('OKKKKKK')},
			success: function(data)
		    {
				jQuery('#cartresponsedata').html(data);
				jQuery(".mini-cart").attr("tabindex",-1).focus(500);
			},
		  	error: function() 
	    	{
				
	    	} 	        
	   });
}
</script>