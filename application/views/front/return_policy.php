﻿<?php $this->load->view('front/layout/header');?>
  <!-- end nav --> 
  
  <!-- Breadcrumbs -->
  
  <div class="breadcrumbs">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <ul>
            <li class="home"> <a title="Go to Home Page" href="index.html">Home</a><span>&raquo;</span></li>
           
            <li class="category13"><strong>Return Policy</strong></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <!-- Breadcrumbs End -->
  <?php
  $privacy=15;
  $data=$this->cms_model->selectprivacypolicy($privacy);
  ?>
<div class="container faq-page">
	<div class="row">
		<div class="col-md-12" style="margin-top:18px;">
			<div class="col-md-9">
				<?php echo $data[0]->slide_text;?>
			</div>			
			<div class="col-md-3">
				<img src="<?php echo base_url('uploads/cms/'.$data[0]->image);?>">
			</div>
		</div>
	</div>
</div>
  
 <!-- our clients Slider -->
  <?php $this->load->view('front/layout/footer');
  ?>