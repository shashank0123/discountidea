<?php $this->load->view('front/layout/header');?>
<?php $breadcrumbs = $this->category_model->breadcrumbs($pagedata[0]->category_id,0); ?>
<!-- flexslider CSS -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/front/'); ?>/css/flexslider.css" >
<style>
a.mytooltip {outline:none; }
a.mytooltip strong {line-height:30px;}
a.mytooltip:hover {text-decoration:none;} 
a.mytooltip span {
    z-index:1000000000000;display:none; padding:14px 20px;
    margin-top:-10px; margin-left:5px;
    width:320px; line-height:16px;
}
a.mytooltip:hover span{
    display:inline; position:absolute; color:#111;
    border:1px solid #DCA; background:#fffAF0;}
.callout {z-index:20;position:absolute;top:30px;border:0;left:-12px;}
    
/*CSS3 extras*/
a.mytooltip span
{
    border-radius:4px;
    box-shadow: 5px 5px 8px #CCC;
}


.cuuuu .nav-tabs { border-bottom: 2px solid #DDD; }
  .cuuuu  .nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover { border-width: 0; }
 .cuuuu   .nav-tabs > li > a { border: none; color: #666; }
  .cuuuu      .nav-tabs > li.active > a, .nav-tabs > li > a:hover { border: none; color: #4285F4 !important; background: transparent; }
  .cuuuu      .nav-tabs > li > a::after { content: ""; background: #4285F4; height: 2px; position: absolute; width: 100%; left: 0px; bottom: -1px; transition: all 250ms ease 0s; transform: scale(0); }
  .cuuuu  .nav-tabs > li.active > a::after, .nav-tabs > li:hover > a::after { transform: scale(1); }
.cuuuu .tab-nav > li > a::after { background: #21527d none repeat scroll 0% 0%; color: #fff; }
.cuuuu .tab-pane { padding: 15px 0; }
.cuuuu .tab-content{padding:20px}

.cuuuu .card {background: #FFF none repeat scroll 0% 0%; box-shadow: 0px 1px 3px rgba(0, 0, 0, 0.3); margin-bottom: 30px; }



/* Style the tab */
div.tab {
    overflow: hidden;
    border: 1px solid #ccc;
    background-color: #f1f1f1;
}

/* Style the buttons inside the tab */
div.tab button {
    background-color: inherit;
    float: left;
    border: none;
    outline: none;
    cursor: pointer;
    padding: 14px 16px;
    transition: 0.3s;
    font-size: 17px;
}

/* Change background color of buttons on hover */
div.tab button:hover {
    background-color: #ddd;
}

/* Create an active/current tablink class */
div.tab button.active {
    background-color: #ccc;
}

/* Style the tab content */
.tabcontent {
    display: none;
    padding: 6px 12px;
    border: 1px solid #ccc;
    border-top: none;
}
</style>
  <!-- Breadcrumbs -->
  <div class="breadcrumbs">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <ul>
            <li class="home"> <a title="Go to Home Page" href="<?php echo base_url(); ?>">Home</a><span>&raquo;</span></li>
            <?php if(count($breadcrumbs)>0)
			{
				foreach($breadcrumbs as $breadcrumb)
				{
					echo $breadcrumb['breadcrumbs'];
				}
			} ?>
            <li><strong><?php echo $pagedata[0]->name; ?></strong></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <!-- Breadcrumbs End --> 
  <!-- Main Container -->
  <div class="main-container col1-layout">
  <div class="container">
    <div class="row">
      <div class="col-main">
        <div class="product-view-area" id="prostock">
          <div class="product-big-image col-xs-12 col-sm-5 col-lg-5 col-md-5" > 
            <?php $proimages = $this->product_model->selectProductSingleImage($pagedata[0]->id); ?>	
            <?php if(count($proimages)>0){ ?>			
				<div class="large-image"> <a href="<?php echo base_url('uploads/product/'.$proimages[0]->image); ?>" class="cloud-zoom" id="zoom1" rel="useWrapper: false, adjustY:0, adjustX:20"> <img class="zoom-img" src="<?php echo base_url('uploads/product/'.$proimages[0]->image); ?>" alt="products"> </a> </div>
            <?php } ?>			
			<?php $pro_image = $this->product_model->selectProductImages($pagedata[0]->id); ?>
			<?php if(count($pro_image)>0){ ?>
			<div class="flexslider flexslider-thumb">
              <ul class="previews-list slides">
			    <?php foreach($pro_image as $p_image){ ?>
                <li><a href='<?php echo base_url('uploads/product/'.$p_image->image); ?>"' class='cloud-zoom-gallery' rel="useZoom: 'zoom1', smallImage: '<?php echo base_url('uploads/product/'.$p_image->image); ?>' "><img src="<?php echo base_url('uploads/product/thumbs/'.$p_image->image); ?>" alt = "Thumbnail 2"/></a></li>
                <?php } ?>
              </ul>
            </div>
            <?php } ?> 
            <!-- end: more-images --> 
            
          </div>
          <div class="col-xs-12 col-sm-7 col-lg-7 col-md-7">
            <div class="product-details-area">
              <div class="product-name">
                <h1><?php echo $pagedata[0]->name; ?></h1>
              </div>
              <div class="price-box">
			    <span style="color:#ff3366;"><b>Save : <?php echo $pagedata[0]->descount; ?>%</b></span><br>
			    <?php if($pagedata[0]->spacel_price!=""){ ?>
                <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> <?php echo CURRENCY.$pagedata[0]->spacel_price; ?> </span> </p>
                <p class="old-price"> <span class="price-label">Regular Price:</span> <span class="price"> <?php echo CURRENCY.$pagedata[0]->price; ?> </span> </p>
				<?php }else{ ?>
				<p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> <?php echo CURRENCY.$pagedata[0]->price; ?> </span> </p>
				<?php } ?>
				
				
				<?php if(!empty($pagedata[0]->vender_type) && $pagedata[0]->vender_type!=0){ ?>
				<?php $storedata = $this->store_model->selectstoreby_uid($pagedata[0]->vender_type); ?>
				<?php if(count($storedata)>0 && $storedata[0]->status==1){ ?>
					<a href="<?php echo base_url('vendor/store/'.slugurl($storedata[0]->title).'/'.$storedata[0]->id); ?>"><button class="button pro-add-to-cart pull-right" style="width:50%;"><span>You Must Visit this Brand Store</span></button></a>
				<?php }} ?>
				
              </div>
              <div class="ratings">
                <div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                <p class="rating-links"> <a href="#">1 Review(s)</a> <span class="separator">|</span> <a href="#">Add Your Review</a> </p>
                

                <?php
                	$filter_options = $this->filters_model->getProductSizeQuantity($pagedata[0]->id);
                	$available = 'Out of Stock';
                	if(count($filter_options)>0){
                		foreach($filter_options as $k=>$size){
                			if($size->stock_quantity>0) {  
                				$available = 'In Stock';
                			}
                		}
                	}
                ?>	

                <p class="availability in-stock pull-right">Availability: <span><?php echo $available;//($pagedata[0]->product_stock > 0)?'In Stock':'Out Of Stock'; ?></span></p>
              </div>
              <div class="short-description">
                <h2>Quick Overview</h2>
                <p><?php echo $pagedata[0]->sortdesc; ?></p>               
              </div>


              <?php if(count($productGroup)>0){ ?>
				<div class="flexslider flexslider-thumb"><h2>Color: </h2>
	              <ul class="previews-list slides">
				    <?php
				    foreach($productGroup as $p_image){
				    	$sws_cat=$this->product_model->ProductCategory($p_image->category_id);
				    ?>
	                <li><a  href="<?php echo base_url(slugurl($sws_cat->ptitle).'/'.slugurl($sws_cat->title).'/'.slugurl($p_image->name).'/'.encodeurlval($p_image->id)); ?>"><img src="<?php echo base_url('uploads/product/thumbs/'.$p_image->image); ?>" alt = "Thumbnail 2"/></a></li>
	                <?php } ?>
	              </ul>
	            </div>
	            <?php } ?>
              
			  <form method="post" action="<?php echo base_url('index.php/cart/addtocart'); ?>">
              <div class="product-color-size-area">				
				<?php $filter_str = $pagedata[0]->filter_ids; ?>
				<?php if(!empty($filter_str)){ ?>
				<?php $filter_cat = $this->filters_model->selectFiltersWhereId_in(explode(",",$filter_str)); ?>
				<?php if(count($filter_cat)>0){ ?>
				<?php $s=0; foreach($filter_cat as $filter_c){ $s++; ?>	
                <div class="size-area">
                  <h2 class="saider-bar-title"><?php echo $filter_c->title; ?></h2>
                  <div class="size" id="sizediv">
                    <ul id="myoptiondata<?php echo $s; ?>">  
						<?php $filter_options = $this->filters_model->getAvailableOptions($filter_c->id,$pagedata[0]->id); ?>
						<?php if(count($filter_options)>0){ ?>	
						<?php foreach($filter_options as $options){ ?>	
						<li><a><label><input required id="mydata<?php echo $s; ?>" type="radio" name="customoption_<?php echo $filter_c->id; //$options->p_id; ?>" value="<?php echo $options->id; ?>" style="display:none1;"> <?php echo $options->title; ?></a></label></li>
						<?php } } ?>
                    </ul>
                  </div>
                </div>
				<?php } ?>
				<?php } ?>
				<?php } ?>
              </div>
			  
			  <?php $qty=0; ?>
              <?php $filter_options = $this->filters_model->getProductSizeQuantity($pagedata[0]->id); ?>
			  <?php if($available=='In Stock') { //if(count($filter_options)>0){ ?>
              <div class="product-color-size-area">
	              <h2 class="saider-bar-title">Available Size</h2>
	              <div class="size" id="sizeavl">
	                <ul id="avlsize">  
						<?php foreach($filter_options as $k=>$size){ if($size->stock_quantity>0) {  $qty+=$size->stock_quantity; ?>	
						<li><a><label><input type="hidden" id="avl_<?php echo $size->id; ?>" value="<?php echo $size->stock_quantity; ?>"><input required id="mydata<?php echo $k; ?>" type="radio" name="customoption_7" value="<?php echo $size->id; ?>" onclick="showAvailableQuantity(<?php echo $size->id; ?>)"> <?php echo $size->title."<br><span style='font-size:9px !important;font-weight=300 !important;'>In Stock: ".$size->stock_quantity."</span>"; ?></a></label></li>
						<?php } }  ?>
	                </ul>
	              </div>
              </div>
              <?php } ?>
              <script type="text/javascript">
              	function showAvailableQuantity(pos){
              		$('#shwqan').html('');
              		$('#shwqan').html($('#avl_'+pos).val());
              	}
              </script>

			  <?php
					$loginuserid = $this->user_model->getLoginUserVar('USER_ID');
					if(!empty($loginuserid))
					{
						$userdata = $this->user_model->selectuserby_id($loginuserid);
						if(count($userdata)>0)
						{
							$user_pincode = $userdata[0]->pincode;
						}
                        else
						{
							$user_pincode = '';
						}	
					}
					else
					{
						$user_pincode = '';
					}
				?>
			  <div class="product-variation"> 
				<b>Check Delivery Pincode</b><br>
				<span id="pincodeerror"><div class="alert alert-danger">Please check product availability into your pincode area.</div></span>
				<table>
					<tr>
						<td><input type="text" id='pincode' name="pincode" value="<?php echo $user_pincode; ?>" class="form-control" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');">
							<input type="hidden" id='pro_pin' name="pro_pin" value='<?php echo $pagedata[0]->id; ?>' >
						</td>
						<td><button type="button" class="form-control" onclick="return checkpincode();" id="pinchq">Check</button></td>
					</tr>
				</table>
			  </div>

			  <div class="product-variation"> 
				Available Quantity <b><span id="shwqan"><?php //echo $pagedata[0]->product_stock; ?></span></b><br><br>
				<b>Limited Stock Purchase Now</b>				
			  </div>
			  <?php echo $this->session->flashdata("stockmessage"); ?>
              <div class="product-variation">  
              
				  <input type="hidden" name="pid" value="<?php echo $pagedata[0]->id; ?>" >
                 
                 
                    <?php if($available=='In Stock') { //if($pagedata[0]->product_stock > 0 || $qty>0) { ?>
                     <!-- Available product -->
                     <div class="cart-plus-minus">
                    <label for="qty">Quantity:</label>
                    <div class="numbers-row">
						<div onClick="var result = document.getElementById('qty'); var qty = result.value; if( !isNaN( qty ) &amp;&amp; qty &gt; 0 ) result.value--;return false;" class="dec qtybutton"><i class="fa fa-minus">&nbsp;</i></div>
						<input type="text" class="qty" title="Qty" value="1" maxlength="12" id="qty" name="qty">
						<div onClick="var result = document.getElementById('qty'); var qty = result.value; if( !isNaN( qty )) result.value++;return false;" class="inc qtybutton"><i class="fa fa-plus">&nbsp;</i></div>
                    </div>
                  </div>

                  <button onclick="return checkoprion();"  class="button pro-add-to-cart" id="addtocartbutton" title="Add to Cart" type="submit" name="addtocartbutton"><span><i class="fa fa-shopping-cart"></i> Add to Cart</span></button>

                  <!--<button onclick="return checkoprion();"  class="button pro-add-to-cart" id="addtocartbutton" title="Add to Cart" type="submit" name="addtocartbutton" <?php echo ($pagedata[0]->product_stock > 0)?'':'disabled'; ?>><span><i class="fa fa-shopping-cart"></i> Add to Cart</span></button>-->

                  <!-- Available product -->
                  <?php }  else { ?>
                   <!-- not Available product -->
                     <p class="availability in-stock pull-right">Availability: <span> <?php echo $available;?> </span></p>
                      <!-- not Available product -->
                <?php } ?>
              </div>
			  </form>

              <div class="product-cart-option">
                <ul>
                <?php
					$loginuserid=$this->user_model->getLoginUserVar('USER_ID');
					if(!empty($loginuserid)){
				?>
					<li><a href="<?php echo base_url('index.php/user/addtowishlist/'.$loginuserid.'/'.$pagedata[0]->id);?>"><i class="fa fa-heart"></i><span>Add to Wishlist</span></a></li>
				<?php } else {	?>  
					<li><a href="#"><i class="fa fa-heart"></i><span>Add to Wishlist</span></a></li>
				<?php } ?>
                  <li><a href="#"><i class="fa fa-retweet"></i><span>Add to Compare</span></a></li>
                  <li><a href="#"><i class="fa fa-envelope"></i><span>Email to a Friend</span></a></li>
                </ul><br>
				<?php $terms_data = $this->product_model->select_product_terms($pagedata[0]->id); ?>
					
              </div>
<style>
#myli li{list-style-type: square;}
</style>			  
			  
			  
			  
				
					<!--li>
						<a href="#" class="mytooltip"><?php echo $terms->title;?>
							<span> <?php echo $terms->content;?> </span>  
						</a>
						   
							
					</li-->
					
					
	
<div class="tab">

<?php if(count($terms_data)>0){ ?>
	<?php foreach($terms_data as $terms){ ?>
	
		<button class="tablinks" onclick="openCity(event, '<?php echo $terms->title;?>')"><?php echo $terms->title;?></button>
  	<?php } ?>	
				<?php } ?>	
  
	</div>				
					<?php if(count($terms_data)>0){ ?>
	<?php foreach($terms_data as $terms){ ?>
	
	<div id="<?php echo $terms->title;?>" class="tabcontent">
  
		<p><?php echo $terms->content;?></p>
  
	</div>
	
		<?php } ?>	
				<?php } ?>				
					
            </div>
          </div>
        </div>
      </div>
      <div class="product-overview-tab wow fadeInUp">
        <div class="container">
          <div class="row">
            <div class="col-xs-12">
              <ul id="product-detail-tab" class="nav nav-tabs product-tabs">
                <li class="active"> <a href="#description" data-toggle="tab"> Description </a> </li>                
				<li> <a href="#reviews" data-toggle="tab">Reviews</a> </li>
              </ul>
              <div id="productTabContent" class="tab-content">
                <div class="tab-pane fade in active" id="description">
                  <div class="std">
                    <p><?php echo $pagedata[0]->description; ?></p>                    
                  </div>
                </div>
                
                  <div id="reviews" class="tab-pane fade">
							<div class="col-sm-5 col-lg-5 col-md-5">
								<div class="reviews-content-left">
									<h2>Customer Reviews</h2>
									<div class="review-ratting">
									
									<?php
				$reviewselect=$this->user_model->selectreviewbyid($pagedata[0]->id);
				//print_r($reviewselect);
				foreach($reviewselect as $review)
				{
					$review_user = $this->user_model->selectuserby_id($review->user_id);
				?>
				
									<p class="author">
										<small style="color:#ff3366;"><?php echo $review->start_rating;?> rating By <b><?php echo $review_user[0]->fname;?></b> (Posted on <?php echo $review->comment_date;?>)</small><br>
										<?php echo $review->comment;?>
									</p>
									
									<?php
				}
				?>
									</div>                                    
								</div>
							</div>
							<div class="col-sm-7 col-lg-7 col-md-7">
								<div class="reviews-content-right">
									<h2>Write Your Own Review</h2>
									<form method="POST" action="<?php echo base_url('index.php/home/selectprocutcoment/'.$pagedata[0]->id);?>">
										<h4>How do you rate this product?<em>*</em></h4>
                                        <div class="table-responsive">
										<table>
											<tbody>
											<tr>
												<th align="center">1 star</th>
												<th align="center">2 stars</th>
												<th align="center">3 stars</th>
												<th align="center">4 stars</th>
												<th align="center">5 stars</th>
											</tr>
											<tr>
												<td align="center"><input type="radio" name="start" value="1"></td>
												<td align="center"><input type="radio" name="start" value="2"></td>
												<td align="center"><input type="radio" name="start" value="3"></td>
												<td align="center"><input type="radio" name="start" value="4"></td>
												<td align="center"><input type="radio" name="start" value="5"></td>
											</tr>
											
										</tbody></table></div>
										<div class="form-area">
											
											<div class="form-element">
												<label>Review <em>*</em></label>
												<textarea class="form-control input-sm" name="comment" required></textarea>
											</div>
											<div class="buttons-set">
												<button class="button submit" title="Submit Review" name="submit" type="submit"><span><i class="fa fa-thumbs-up"></i> &nbsp;Review</span></button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>            
                
              </div>
            </div>
          </div>
        </div>
      </div>    
    </div>
  </div>
</div>
<!-- Main Container End --> 
<?php //$releted_products =  $pagedata[0]->reletedproduct;  ?>
<?php //if(!empty($releted_products)){ ?>
<?php //$releted_products_ids = explode(",",$releted_products); ?>
<?php error_reporting(0);
/*
$catdata = $this->category_model->getchilcategoryinonearray($pagedata[0]->category_id);
$child_cat_array = (count($catdata)>0)?$catdata:array(0);
print_r($catdata);

$child_cat_array = array(5,64);
*/
?>
<?php 
//$rproduct = $this->product_model->select_active_products_bycategoryid_array($child_cat_array);
$rproduct = $this->product_model->select_active_products_bycategoryid_array($pagedata[0]->category_id);
//selectAllReletedProduct($releted_products_ids); ?>
<?php if(count($rproduct)>0){ ?>
<!-- Related Product Slider -->
  <section class="related-product-area">
  <div class="container">
  <div class="row">
  <div class="col-xs-12">
          <div class="page-header-wrapper">
      <div class="container">
        <div class="page-header text-center wow fadeInUp">
          <h2>Related <span class="text-main">Products</span></h2>
          <div class="divider divider-icon divider-md">&#x268A;&#x268A; &#x2756; &#x268A;&#x268A;</div>
          
        </div>
      </div>
    </div>
 
                <div class="slider-items-products">
                  <div id="related-product-slider" class="product-flexslider hidden-buttons">
                    <div class="slider-items slider-width-col4 fadeInUp">
                      <?php
						$deal="dealday";
					$data=$this->product_model->getproductdiscountwise(50); //$this->product_model->homeproductall($deal);
					foreach($data  as $dealday)
					{

						 $product_image = $this->product_model->selectProductDoubleImage($dealday->id);
						?>
<?php $sws_cat=$this->product_model->ProductCategory($dealday->category_id);  ?>	
                      <div class="product-item">
                        <div class="item-inner fadeInUp">
                          <div class="product-thumbnail"> 
                            <div class="pr-img-area">
							<a title="<?php echo $dealday->name; ?>" href="<?php echo base_url(slugurl($sws_cat->ptitle).'/'.slugurl($sws_cat->title).'/'.slugurl($dealday->name)).'/'.encodeurlval($dealday->id); ?>">
								<figure> <?php if(count($product_image)>0){ ?>
									<img class="first-img" src="<?php echo base_url('uploads/product/thumbs'); ?>/<?php echo $product_image[0]->image; ?>" alt="<?php echo $dealday->name; ?>"  > 
									<?php if(isset($product_image[1]->image)){ ?><img class="hover-img " src="<?php echo base_url('uploads/product/thumbs'); ?>/<?php echo $product_image[1]->image; ?>" alt="<?php echo $dealday->name; ?> " ><?php } ?>
									<?php }else{ ?>
									<img class="first-img" src="http://www.discountidea.com/assets/front/images/264x264.jpg" alt="<?php echo $dealday->name; ?>"  > 
									<img class="hover-img" src="http://www.discountidea.com/assets/front/images/264x264.jpg" alt="<?php echo $dealday->name; ?>" >
									<?php } ?>
								</figure>



							</a>	<!-- <form method="post" action="<?php echo base_url('index.php/cart/addtocart'); ?>">
                <input type="hidden" name="pid" value="<?php echo $dealday->id; ?>" >
                <input type="hidden" name="qty" value="1" >              
                <button type="button" name="addtocartbutton1" onclick="return addtocart(<?php echo $dealday->id; ?>);" class="add-to-cart-mt"> <i class="fa fa-shopping-cart"></i><span> Add to Cart</span> </button>
                <?php $loginuserid=$this->user_model->getLoginUserVar('USER_ID')?>
                <a class="add-to-cart-mt" style="font-size:11px" href="<?php echo base_url('index.php/user/addtowishlist/'.$loginuserid.'/'.$dealday->id);?>"><i class="fa fa-heart"></i><span>Add to Wishlist</span></a>
          
                
              </form> -->


                            <form method="post" action="<?php echo base_url('index.php/cart/addtocart'); ?>">
								<input type="hidden" name="pid" value="<?php echo $dealday->id; ?>" >
								<input type="hidden" name="qty" value="1" >						   
								<!--<button type="button" name="addtocartbutton1" onclick="return addtocart(<?php echo $dealday->id; ?>);" class="add-to-cart-mt"> <i class="fa fa-shopping-cart"></i><span> Add to Cart</span> </button>-->
								<?php $loginuserid=$this->user_model->getLoginUserVar('USER_ID')?>
								<a class="add-to-cart-mt" style="font-size:11px" href="<?php echo base_url('index.php/user/addtowishlist/'.$loginuserid.'/'.$dealday->id);?>"><i class="fa fa-heart"></i><span>Add to Wishlist</span></a>
					
								
							</form>
							<a href="<?php echo base_url(slugurl($sws_cat->ptitle).'/'.slugurl($sws_cat->title).'/'.slugurl($dealday->name)).'/'.encodeurlval($dealday->id); ?>"><button type="button" name="Buy Now" class="add-to-cart-mt1"> <i class="fa fa-bolt"></i><span> Buy Now</span> </button></a>
                            </div>
                            
                          </div>
                          <div class="item-info">
                      <div class="info-inner">
                        <div class="item-title"> 
							<a title="<?php echo $dealday->name; ?>" href="<?php echo base_url(slugurl($sws_cat->ptitle).'/'.slugurl($sws_cat->title).'/'.slugurl($dealday->name)).'/'.encodeurlval($dealday->id); ?>">
								<?php echo $dealday->name; ?>
							</a>
						</div>
                        <div class="item-content">
                          <div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                          <div class="price-box">
							  <?php if($dealday->spacel_price!=""){ ?>
                              <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> <?php echo CURRENCY.$dealday->spacel_price; ?> </span> </p>
                              <p class="old-price"> <span class="price-label">Regular Price:</span> <span class="price"> <?php echo CURRENCY.$dealday->price; ?> </span> </p>
							  <?php }else{ ?>
							  <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> <?php echo CURRENCY.$dealday->price; ?> </span> </p>
							  <?php } ?>
                          </div>
                        </div>
                      </div>
                    </div>
                        </div>
                      </div>
					  
					  <?php
					}
					?>
                    </div>
                  </div>
                </div>
        
                </div>
                </div>
              </div>
              </section>
<!-- Related Product Slider End -->
<?php } //} ?>		
<?php $this->load->view('front/layout/footer');?>

<script>
function openCity(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}
</script>
<script>
//document.getElementById("addtocartbutton").disabled = true;
function setvalue(ID,FIELDVAL)
{
	document.getElementById(ID).value =''+FIELDVAL;
}

function checkoprion()
{
	var optiondata1 = document.getElementById('mydata1')
	if(optiondata1.checked==false)
	{
		document.getElementById("myoptiondata1").style.border = "1px solid red";
		document.getElementById("myoptiondata1").style.overflow = "hidden";
		document.getElementById("myoptiondata1").style.padding = "2px";
		return false;
	}
	
	var optiondata2 = document.getElementById('mydata2')
	if(optiondata2.checked==false)
	{
		document.getElementById("myoptiondata2").style.border = "1px solid red";
		document.getElementById("myoptiondata2").style.overflow = "hidden";
		document.getElementById("myoptiondata2").style.padding = "2px";
		return false;
	}	
}

function checkpincode()
{
	var pincode = document.getElementById('pincode').value;
	var pro_pin = document.getElementById('pro_pin').value;
	

	if(pincode!="")
	{
		if(pro_pin!="")
		{
			jQuery.ajax({
				url: "<?php echo base_url('index.php/pincode/checkpincode'); ?>",
				type: "POST",
				data:   { pincode : pincode,pro_pin : pro_pin},
				beforeSend: function() {
			        $('#pinchq').html('Please wait....');
			    },
				success: function(data)
				{
					$('#pinchq').html('Check Other Pincode');
					document.getElementById('pincodeerror').innerHTML=''+ data;
					// alert(data);
					if(data=='<div class="alert alert-danger">Delivery not available in this area.</div>')
					{
						document.getElementById("addtocartbutton").disabled = true;
					}
					else
					{
						//document.getElementById("addtocartbutton").disabled = false;
					}
					//document.getElementById('pincodeerror').innerHTML=''+ data;
					$('#pincodeerror').val(data);
				},
				error: function() 
				{
					
				} 	        
		   });
		}
		else
		{			
			document.getElementById("addtocartbutton").disabled = true;
			document.getElementById('pincodeerror').innerHTML='<div class="alert alert-danger">Currently out of stock in this area.</div>';
			return false;
		}
	}
	else
	{
		document.getElementById("addtocartbutton").disabled = true;
		document.getElementById('pincodeerror').innerHTML='<div class="alert alert-danger">Please enter valid pincode.</div>';
		return false;
	}	
}
</script>
<!--cloud-zoom js --> 
<script type="text/javascript" src="<?php echo base_url('assets/front'); ?>/js/cloud-zoom.js"></script>
<!-- flexslider js --> 
<script type="text/javascript" src="<?php echo base_url('assets/front'); ?>/js/jquery.flexslider.js"></script> 


