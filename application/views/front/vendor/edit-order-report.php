<?php $this->load->view('front/layout/header');?>  <!-- end nav --> 
   <style>
div.gallery {
    margin: 5px;
    border: 1px solid #ccc;
    float: left;
    width: 180px;
}

div.gallery:hover {
    border: 1px solid #777;
}

div.gallery img {
    width: 100%;
    height: auto;
}

div.desc {
    padding: 5px;
	font-size: 22px;
    text-align: center;
}
</style> 
  <!-- Breadcrumbs -->
  
  <div class="breadcrumbs">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <ul>
            <li class="home"> <a title="Go to Home Page" href="<?php echo base_url(); ?>">Home</a><span>&raquo;</span></li>
            <li class="category13"><strong>My Products</strong></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <!-- Breadcrumbs End --> 
  
  <!-- Main Container -->
  <section class="main-container col1-layout">
    <div class="main container">
      <div class="account-login mb-30">
        	<div class="row">
            <div class="col-md-3">
              <div id="left-pnl" class="aside-site-left my_account_section   ">
                      
<div class="action_links">
     <?php $this->load->view('front/layout/my-account-left-sidebar'); ?>   
</div>


                <div class="sidebox-bottom"> <span> &nbsp; </span> </div>
                              </div>
            </div>
            <div class="col-md-9">
			
              <div class="aside-site-content my_account_section ">
                                            <div class="site-content ">
        
                 <div class="central-content"> 
        
        



<div class="cm-notification-container"></div>
            
                   
                          
    
    <div class="mainbox-container margin margin">
   <div class="mainbox-body"><div class="main_section" style="display: block;">
    <div class="my_order" style="display: block;">
	<div class="sub_action_links">
	    <h3>Edit Order Report -<?php echo $order[0]->id; ?></h3>
		
	    <?php echo $this->session->flashdata('message'); ?>
	    	
		    <div class="my_order_list table-responsive" style="padding:3px;">
			<form method="post" enctype='multipart/form-data'>
			<table class="table table-bordered table-hover" id="imageproduct">
				<tr>
					<td>AWB ID</td>
					<td><input type="text" name="awb_id"  class="form-control" value="<?php echo $order[0]->awb_id; ?>" autocomplete="off" required></td>
				</tr>
				
				<tr>
					<td>Courier Company Name</td>
					<td><input type="text" name="courier_company"  class="form-control" value="<?php echo $order[0]->courier_company; ?>" autocomplete="off" required></td>
				</tr>

				<tr>
					<td>Status</td>
					<td>
					 <select name="status" class="form-control">
									<option value="">Select Status</option>
									<option <?php echo ($order[0]->status=='ready to pack')?'selected':''; ?> value="ready to pack">Ready to pack</option>
									<option <?php echo ($order[0]->status=='pending to dispatch')?'selected':''; ?> value="pending to dispatch">Pending to dispatch</option>
									<option <?php echo ($order[0]->status=='shipment ready to dispatch')?'selected':''; ?> value="shipment ready to dispatch">Shipment ready to dispatch</option>
									<option <?php echo ($order[0]->status=='shipment from vendor point')?'selected':''; ?> value="shipment from vendor point">Shipment from vendor point</option>
									<option <?php echo ($order[0]->status=='delivery under proccess')?'selected':''; ?> value="delivery under proccess">Delivery under proccess</option>  
									<!--<option <?php echo ($order[0]->status=='delivered successfully')?'selected':''; ?> value="delivered successfully">Delivered successfully</option>-->
									<option <?php echo ($order[0]->status=='cancelled')?'selected':''; ?> value="cancelled">Cancelled</option>
									<option <?php echo ($order[0]->status=='disputed')?'selected':''; ?> value="disputed">Disputed</option>
									<option <?php echo ($order[0]->status=='pending')?'selected':''; ?> value="pending">Pending</option>
									<option <?php echo ($order[0]->status=='under process')?'selected':''; ?> value="under process">Under Process</option>
								</select>
					</td>
				</tr>
				
				<tr>
					<td colspan="2"><button type="submit" class="btn btn-info" name="updatedata">Submit</button></td>
				</tr>
									
			</table> 	
			</form>	
	    </div>
				
		</div>
		
    </div>
</div>

        
</div>
    </div>
           
                 </div> 
                   </div>
        
                    </div>
            </div>
          </div>
      </div>
     
    </div>
  </section>
 
  <?php $this->load->view('front/layout/footer');?>