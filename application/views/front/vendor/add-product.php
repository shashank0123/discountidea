<?php $this->load->view('front/layout/header');?>  <!-- end nav --> 
   <style>
.naseemdiv
{
	height: 210px;
    overflow: scroll;
    border: 2px solid rgba(34, 34, 34, 0.41);
    padding: 10px;
}
.naseemdiv label{padding: 3px;}
.naseemdiv ul {padding-left:5px; list-style:none;}
</style> 
  <!-- Breadcrumbs -->
  
  <div class="breadcrumbs">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <ul>
            <li class="home"> <a title="Go to Home Page" href="<?php echo base_url(); ?>">Home</a><span>&raquo;</span></li>
            <li class="category13"><strong>My Products</strong></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <!-- Breadcrumbs End --> 
  
<!-- Main Container -->
<section class="main-container col1-layout">
    <div class="main container">
      	<div class="account-login mb-30">
        	<div class="row">
            	<div class="col-md-3">
              		<div id="left-pnl" class="aside-site-left my_account_section   ">
						<div class="action_links">
				     		<?php $this->load->view('front/layout/my-account-left-sidebar'); ?>   
						</div>
		          	  	<div class="sidebox-bottom"> <span> &nbsp; </span> </div>
		            </div>
	            </div>
            	
            	<div class="col-md-8">
              		<div class="aside-site-content my_account_section ">
                		<div class="site-content ">
                 			<div class="central-content"> 
                 				<div class="cm-notification-container"></div> 
								<div class="mainbox-container margin margin">
								<div class="mainbox-body">
									<div class="main_section" style="display: block;">
										<div class="my_order" style="display: block;">
											<div class="sub_action_links">
							  				  <h3>Add Product</h3>
							    			</div>
										    <div class="my_order_list table-responsive" style="padding:3px;">
												<form method="post">
													<table class="table table-bordered table-hover">
														<input type="hidden" name="cat_pos" id="cat_pos" value="0">
														<!--<tr>
															<td>Category</td>
															<td><select name="category_id" class="form-control">
													  		<?php $data = $this->category_model->getSelectOption(0); ?>
					                                		</select></td>
														</tr>-->
													
														<tr>
															<td> Category</td>
															<td>
																<?php echo form_dropdown('category_id[0]',$listCat,set_value('category_id[0]'),'id="category_id0" required="required" class="form-control" onchange="get_subcat(this.value,0)"')?>
															</td>
														</tr>
									
														<tr id="vendorRow">
															<td>Product Name</td>
															<td><input type="text" name="name"  class="form-control" autocomplete="off" required></td>
														</tr>
														<tr>
															<td>HSN Code</td>
															<td><input type="text" name="hsn_code" id="hsn_code" class="form-control" autocomplete="off" required><a data-toggle="modal" href="#myModal">Find Relevant HSN Codes</a></td>
														</tr>
														<tr>
															<td>GST Tax Code</td>
															<td>
																<select name="gst_percentage" id="gst_percentage" class="form-control" <?php if($EDITPRODUCT[0]->status==1){ echo "readonly"; }?>>
																	<option value="">Select</option>
																	<?php if(count($taxCodes)>0) { foreach($taxCodes as $tax) { ?>
																	<option value="<?php echo $tax->tax_percentage;?>"><?php echo $tax->tax_code;?></option>
																	<?php } } ?>
																</select>
															</td>
														</tr>
														<tr>
															<td>Brand (Trade Mark)</td>
															<td><input type="text" name="brand"  class="form-control" autocomplete="off" required></td>
														</tr>
														<tr>
															<td>Height (in inch)	</td>
															<td><?php if (form_error('height')) { ?>
																	<div class="form-group has-error"><label class="control-label" for="inputError">
																	<?php print form_error('height')?></label>
																<?php } ?>
																<input type="text" name="height"  class="form-control" autocomplete="off" required oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');"></td>
														</tr>
														<tr>
															<td>Length (in inch)	</td>
															<td> <?php if (form_error('length')) { ?>
						              							  <div class="form-group has-error"><label class="control-label" for="inputError">
						              							  	<?php print form_error('length')?></label>
						                    					<?php } ?><input type="text" name="length"  class="form-control" autocomplete="off" required oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');"></td>
														</tr>
														<tr>  
															<td>Width (in inch)</td>
															<td>	<?php if (form_error('width')) { ?>
					              							 	 <div class="form-group has-error"><label class="control-label" for="inputError">
					              							  	<?php print form_error('width')?></label>
					                    						<?php } ?><input type="text" name="width"  class="form-control" autocomplete="off" required oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');"></td>
														</tr>
														<tr>
															<td>Weight (in grams ) </td>
															<td><?php if (form_error('weight')) { ?>
					          								  	<div class="form-group has-error"><label class="control-label" for="inputError">
					          							  		<?php print form_error('weight')?></label>
					                    					<?php } ?><input type="text" name="weight"  class="form-control" autocomplete="off" required oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');">	</td>
														</tr>
														<tr>
															<td> Sort Description</td>
															<td><textarea name="sortdesc" id="sortdesctext" value="" class="form-control" rows="3"></textarea></td>
														</tr>
														<tr>
															<td> Description</td>
															<td><textarea name="description" id="fulldesctext"  class="form-control" rows="4"></textarea></td>
														</tr>
														<tr>
															<td>Price</td>
															<td><input type="text" name="price" id="price"  class="form-control" autocomplete="off" onKeyUp="return calculatespecialpeice();" required></td>
														</tr>
														
														<tr>
															<td>Special Price</td>
															<td><input type="text" name="spacel_price" id="spacel_price" class="form-control" autocomplete="off" readonly></td>
														</tr>
														<tr>
															<td>Discount</td>
															<td><input type="text" id="discount" name="descount" class="form-control" autocomplete="off"  onkeyup="return calculatespecialpeice();"></td>
														</tr>
														<!--<tr>
															<td>Product Stock</td>
															<td>
					                                            <input type="text" name="product_stock"  class="form-control" autocomplete="off">
															</td>
														</tr>-->

														<tr>
															<td>Product Size Quantity</td>
															<td>
																<div class="naseemdiv" style="height: 300px;">
																	<div class="col-md-12" style="border:1px solid #ddd; overflow: scroll;">
																		<table>  
																			<tr>
																				<th width="40%"><b>Size</b></th>
																				<th width="60%"><b>Available Quantity</b></th>
																			</tr>
																			<?php 
																			$proSizeQuan = $this->filters_model->getProductSizeQuantity(0);
																			foreach($proSizeQuan as $size){
																			?>
																			<tr>
																				<td>
																					<input type='checkbox' name="sizes[<?php echo $size->id; ?>]" value="<?php echo $size->id; ?>" <?php echo ($size->stock_quantity>0) ? "checked" : ""; ?>> <?php echo $size->title; ?>													
																				</td>
																				<td>
																					<input type="text" name="quan[<?php echo $size->id; ?>]" value="<?php echo $size->stock_quantity; ?>"oninput="this.value = this.value.replace(/[^0-9]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');">
																				</td>
																			</tr>
																			<tr><td colspan="2">&nbsp;</td></tr>
																			<?php } ?>
																		</table>
																	</div>
																</div>							
															</td>
														</tr>

														<tr>
															<td>Product Filters</td>
															<td>
																<div class="naseemdiv" style="height: 300px;">
																	
																	<?php $filter_data = $this->filters_model->selectAllFilters(); ?>
																	<?php foreach($filter_data as $filter){ ?>
																	<?php $option_data = $this->filters_model->selectAllFiltersOptions($filter->id); ?>
																	<div class="col-md-4" style="border:1px solid #ddd; overflow: scroll; height: 200px;">
																		<h4><?php echo $filter->title; ?></h4>
																		<small><label><input type='checkbox' name="filter[]" value="<?php echo $filter->id; ?>"> Dispay In product details page<label></small>
																		<ul>  
																			<?php if(count($option_data)>0){ ?>
																			<?php foreach($option_data as $option){ ?>
																			<li><label><input  type='checkbox' name="options[]" value="<?php echo $option->id; ?>"> <?php echo $option->title; ?></label> </li>
																			<?php } ?>
																			<?php } ?>
																		</ul>
																		
																	</div>
																	<?php } ?>
																</div>
															</td>
														</tr>


														<tr>
															<td>Pincode</td>
															<td>
																<div class="naseemdiv">
																<?php 
																$pincodequery = $this->pincode_model->selectAllPincode();								
																 ?>
																			           <section>
											                <input class="magicsearch" id="basic" name="value" placeholder="search names...">
											                <input type="file" class="form-control" id="xlsfile"  onchange="uploadfile()">
											                <a href="<?=base_url()?>assets/samplepincodefile/sample_pincode_file.csv">Download Sample File </a>
											                <div id="daysp"></div>
											            </section>
														<?php
														$newpin= array();
															foreach ($pincodequery as $row){
																if(in_array($row->id,$newpin)){
																?>

									                            <label><input type="checkbox"  name="pincode_ids[<?php echo $row->id;?>][id]"  value="<?php echo $row->id;?>"  /> <?php echo $row->pincode; ?></label>
									                            	
									                            <input type="text" name="pincode_ids[<?php echo $row->id;?>][days]" id="mypin" value="<?php if(!empty($newarray[$row->id])){echo $newarray[$row->id];} ?>" placeholder="">
								
															<?php } 
																	}
																 ?>

															</div>
														</td>
													<!--</tr>
																
																		      						<tr style="">
													  <td> Pincode:  &nbsp;</td>
													  <td> <?php echo $row->pincode; ?>&nbsp;  <input type="text" name="pds[]" value=""> &nbsp;</td>
													  </tr> -->

																
																<tr style="display:none;">
																	<td>Status</td>										
																	<td>
																		<select  name="status" class="form-control">
																			<!--<option value="1">Active</option>-->
																			<option value="0">Inactive</option>
																		</select>
																	</td>
																</tr>
																<tr>
																	<td colspan="2"><button type="submit" class="btn btn-info" name="add_product" onclick="return validate_checkbox();">Submit</button></td>
																</tr>
															</table> 	
														</form>	
										    		</div>
												</div>
									    	</div>
										</div>  
									</div>
    							</div>
                 			</div> 
                   		</div>        
                    </div>
            	</div>
          	</div>
      	</div>
    </div>
</section>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header" style="padding:10px 50px;">
          <button type="button" class="close" data-dismiss="modal">&times; Close</button>
          <h4>HSN Details</h4>
        </div>
        <div class="modal-body" style="padding:20px 50px;">
          	<div class="table-responsive">
				<table class="table" id="hsn-code">
                	<thead>
		                <tr class="row title-row">
		                    <th>HSN CODE</th>
		                    <th>DESCRIPTION</th>
		                    <th>ACTIONS</th>
		                </tr>
	                </thead>

	                <tbody>
		                <?php if(count($hsnCodes)>0) { foreach($hsnCodes as $hsn) { ?>
		                <tr class="row border-top">
		                    <td class="col-xs-2"><?php echo $hsn->hsn_code;?></td>
		                    <td class="col-xs-8"><?php echo $hsn->description;?></td>
		                    <td>
		                    	<button type="button" onclick="$('#hsn_code').val('<?php echo $hsn->hsn_code;?>');" class="btn btn-primary hsn-select" data-dismiss="modal">Select</button>
		                    </td>
		                </tr>
		                <?php } } else { ?>
		                <tr class="row border-top">
		                    <td colspan="3">No HSN Code Found!!</td>
		                </tr>
		                <?php } ?>
		            </tbody>
            	</table>
			</div>
        </div>
      </div>
    </div>
</div>

<script>
function uploadfile()
{
   var file_data = $("#xlsfile").prop('files')[0];  
   console.log(file_data); 
    var form_data = new FormData();                  
    form_data.append('file', file_data);
    console.log(form_data); 

   	jQuery.ajax({
				url: "<?php echo base_url('index.php/user/uploadexcel'); ?>",
				type: "POST",
				dataType: 'text',  // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
				data:  form_data,
				success: function(res)
				{
	                //alert(res)  ; 
	                $("#daysp").html(res);

				},
				error: function() 
				{
					alert('error');
				} 	        
		   });
   	
   }


                CKEDITOR.replace( 'sortdesctext' );
                CKEDITOR.replace( 'fulldesctext' );
            </script>
  <!-- Main Container End --> 
  <!-- our clients Slider -->
  <?php $this->load->view('front/layout/footer');?>
    <?php  $user_id=$this->user_model->getLoginUserVar('USER_ID'); ?>
  <script>
   function addpincode()
  {	  
	var pincode = document.getElementById('mypin').value;
	if(pincode!="")
	{	
		jQuery.ajax({
				url: "<?php echo base_url('index.php/vendor/addpincode'); ?>",
				type: "POST",
				data:   { pincode : pincode,uid : <?php echo $user_id; ?>},
				success: function(data)
				{
	                                if(data==0)
                                        {
                                             $('#pinerrror').html(' <div class="alert alert-danger">this pincode allready exists.</div>');
                                         }
                                         else
                                         {
                                              $('#pinresponse').append(data);
                                         } 			
                                       
				},
				error: function() 
				{
					alert('error');
				} 	        
		   });
	}
  }
function validate_checkbox()
{
    var checkbox=document.getElementsByid("pincode_ids[]");
	var ln = 0;
	for(var i=0; i< checkbox.length; i++) 
	{
		if(checkbox[i].checked)
			ln++
	}
	//alert(ln);
	if(ln==0)
	{
		alert('please select pincode');
		//return false;
	}	
		
}

function get_subcat(mCat,nextcat=0)
{
	var incrVal = $('#cat_pos').val();
	var lastr = incrVal;
	incrVal++;
	$('#cat_pos').val(incrVal);

	for(r=incrVal;r>nextcat;r--){
		$('#catrow_'+r).remove(); console.log($('#catrow_'+r).remove());
	}

	if(mCat>0) {
		$.ajax({
			type: "POST",
			url: "http://www.discountidea.com/index.php/categorycontroller/getSubCategory/"+mCat+'/'+incrVal,
			success: function(datas) {
	        	$('#vendorRow').before(datas);
			},
		});
	}
}

function calculatespecialpeice()
{
	var spacel_price = document.getElementById('spacel_price');
	var price = document.getElementById('price').value;
	var discount = document.getElementById('discount').value;
	if(discount<96)
	{	
		if(price>0  && discount>0)
		{
			var discount_amount = price * (discount/100);
			spacel_price.value = price - discount_amount 
		}
		else {
			spacel_price.value  ='';
		}
	}
	else
	{
	  return false	
	}
}
</script>



<link href="<?php echo base_url('assets/admin');?>/css/jquery.magicsearch.css">

<script  src="http://code.jquery.com/jquery-2.2.3.min.js"></script>
<script src="<?php echo base_url('assets/admin'); ?>/js/jquery.magicsearch.js"></script>
    <script>
        $(function() {
        	$.ajax({ method: "POST", url: "http://www.discountidea.com/index.php/adminproducts/pincode_ajax"
			}).done(function( msg ) {
			    //alert( "Data Saved: " + msg );
			    var dataSource = msg;
			    //console.log(dataSource);
			    $('#basic').magicsearch({
                dataSource: dataSource,
                fields: ['pincode'],
                id: 'id',
                format: '%pincode%',
                multiple: true,
                multiField: 'pincode',
                multiStyle: {
                    space: 5,
                    width: 80
                },
                success: function($input, data) {
                	console.log(data);
                	$("#daysp").append('<div class="form-group" id="autofill_'+data['id']+'"><label><input type="checkbox"  name="pincode_ids['+data['id']+'][id]"  value="'+data['id']+'"/>'+data['pincode']+'</label><input type="text" name="pincode_ids['+data['id']+'][days]" id="mypin" value="" placeholder=""></div>'); //add input box
      				return true;
  		},
  		afterDelete: function($input, data) {
  			  $("#autofill_"+data['id']+"").remove();
		      return true;		
		}

            });
			  });
           /* var dataSource = [
                {id: 1, pincode: 'Tim', days: 'Cook'},
                {id: 2, pincode: 'Eric', days: 'Baker'},
                {id: 3, pincode: 'Victor', days: 'Brown'},
                {id: 4, pincode: 'Lisa', days: 'White'},
                {id: 5, pincode: 'Oliver', days: 'Bull'},
                {id: 6, pincode: 'Zade', days: 'Stock'},
                {id: 7, pincode: 'David', days: 'Reed'},
                {id: 8, pincode: 'George', days: 'Hand'},
                {id: 9, pincode: 'Tony', days: 'Well'},
                {id: 10, pincode: 'Bruce', days: 'Wayne'},
            ];
            */
            
             

        });
    </script>