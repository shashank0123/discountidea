<?php $this->load->view('front/layout/header');?>  <!-- end nav --> 
 <style>
.naseemdiv
{
	height: 210px;
    overflow: scroll;
    border: 2px solid rgba(34, 34, 34, 0.41);
    padding: 10px;
}
.naseemdiv label{padding: 3px;}
.naseemdiv ul {padding-left:5px; list-style:none;}
</style> 
  <!-- Breadcrumbs -->
  <div class="breadcrumbs">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <ul>
            <li class="home"> <a title="Go to Home Page" href="<?php echo base_url(); ?>">Home</a><span>&raquo;</span></li>
            <li class="category13"><strong>Shipment Report</strong></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <!-- Breadcrumbs End --> 
   <!-- Main Container -->
  <section class="main-container col1-layout">
    <div class="main container">
      <div class="account-login mb-30">
        <div class="row">
            <div class="col-md-3">
              	<div id="left-pnl" class="aside-site-left my_account_section   ">
					<div class="action_links">
		    		 <?php $this->load->view('front/layout/my-account-left-sidebar'); ?>   
					</div>
  					<div class="sidebox-bottom"> <span> &nbsp; </span> </div>
    			</div>
    		</div>
            <div class="col-md-9">
			<div class="aside-site-content my_account_section ">

				<a href="<?php echo base_url('index.php/vendor/order_report'); ?>"><button class="btn-warning orange" style="padding:5px;">Order Report</button></a>

				<a href="<?php echo base_url('index.php/vendor/cancellation_report'); ?>"><button class="btn-warning orange" style="padding:5px;">Cancellation Report</button></a>			

				<a href="<?php echo base_url('index.php/vendor/my_sell_payout'); ?>"><button class="btn-warning orange" style="padding:5px;">My Sell Payout</button></a>

				<a href="<?php echo base_url('index.php/vendor/my_deducation'); ?>"><button class="btn-warning orange" style="padding:5px;">My Deducation</button></a>

				<a href="<?php echo base_url('index.php/vendor/final_payout'); ?>"><button class="btn-warning orange" style="padding:5px;">Final Payout</button></a>
				
				<a href="<?php echo base_url('index.php/vendor/shipping_report'); ?>"><button class="btn-warning orange" style="padding:5px;">Shipment Report</button></a>
			</div>

              <div class="aside-site-content my_account_section ">
                <div class="site-content ">
                 <div class="central-content"> 
			      <div class="cm-notification-container"></div>
  			     <div class="mainbox-container margin margin">
  				  <div class="mainbox-body"><div class="main_section" style="display: block;">
    				<div class="my_order" style="display: block;">
    				<div class="sub_action_links">
	  					  <h3>My Sell Payout</h3>
						<?php echo $this->session->flashdata('message'); ?>	    	
		   				 <div class="my_order_list table-responsive" >
						<table class="table table-bordered table-hover" tyle="width:100%;">
						<thead>
			                <tr>
								<th>S.No.</th>
								<th>Order ID</th>
								<th>Delivery Company</th>
								<th>Transaction ID</th>
								<th>Product id</th>
								<th>Product Name</th>
								<th>Order Amount</th>
								<th>Shipping  Amount</th>
								<th>Status</th>
								<th>Date</th>
								
			                </tr>
            			</thead>
			            <tbody>
						    <?php if(count($orderdata)>0){ ?>
							<?php $i=0; foreach($orderdata as $data){ $i++; ?>	
								<tr>
									<td><?php echo $i; ?></td>
									<td><?php echo $data->order_id; ?></td>
									<td><?php echo ucfirst($data->name); ?></td>
									<td><?php echo $data->mihpayid; ?></td>
									  <?php
									 $result=$this->orderdetail_model->selectorderitme($data->order_id);   
									 print "<td>"	;	
									 foreach ($result as $key ) {
									 	
									 	echo ucfirst($key->pro_id.'<br>');
									 }
									  print "</td>"	;
									 print "<td>"	;	
									 foreach ($result as $key ) {
									 	
									 	echo ucfirst($key->pro_name.'<br>');
									 }
									  print "</td>";	
									  print "<td>"	;	
									 foreach ($result as $key ) {
									 	
									 	echo ucfirst($key->total_amount.'<br>');
									 }
									  print "</td>";				
									 ?>
									 
									<td><?php echo $data->amount; ?></td>

									<td><?php echo $data->status; ?></td>
									<td ><?php echo $data->added_date; ?></td>
								</tr>

								<?php }} ?>
			            	</tbody>
						</table> 		
	    			</div>
				</div>
   			 </div>
			</div>
	 	 </div>
   	 </div>
  	 </div> 
 	</div>
 </div>
</div>
</div>
</div>
</div>

  </section>

  <?php $this->load->view('front/layout/footer');?>

  <?php  $user_id=$this->user_model->getLoginUserVar('USER_ID'); ?>

  