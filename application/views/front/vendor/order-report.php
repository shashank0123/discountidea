<?php error_reporting(0);$this->load->view('front/layout/header');?>  <!-- end nav --> 

 <style>

.naseemdiv

{

	height: 210px;

    overflow: scroll;

    border: 2px solid rgba(34, 34, 34, 0.41);

    padding: 10px;

}

.naseemdiv label{padding: 3px;}

.naseemdiv ul {padding-left:5px; list-style:none;}

</style> 

  <!-- Breadcrumbs -->

  

  <div class="breadcrumbs">

    <div class="container">

      <div class="row">

        <div class="col-xs-12">

          <ul>

            <li class="home"> <a title="Go to Home Page" href="<?php echo base_url(); ?>">Home</a><span>&raquo;</span></li>

            <li class="category13"><strong>My Order Report</strong></li>

          </ul>

        </div>

      </div>

    </div>

  </div>

  <!-- Breadcrumbs End --> 

  

  <!-- Main Container -->

  <section class="main-container col1-layout">

    <div class="main container">

      <div class="account-login mb-30">

        	<div class="row">

            <div class="col-md-3">

              <div id="left-pnl" class="aside-site-left my_account_section   ">

                      

<div class="action_links">

     <?php $this->load->view('front/layout/my-account-left-sidebar'); ?>   

</div>





                <div class="sidebox-bottom"> <span> &nbsp; </span> </div>

                              </div>

            </div>

            <div class="col-md-9">

			<div class="aside-site-content my_account_section ">

				<a href="<?php echo base_url('index.php/vendor/order_report'); ?>"><button class="btn-warning orange" style="padding:5px;">Order Report</button></a>

				<a href="<?php echo base_url('index.php/vendor/cancellation_report'); ?>"><button class="btn-warning orange" style="padding:5px;">Cancellation Report</button></a>

				<a href="<?php echo base_url('index.php/vendor/my_sell_payout'); ?>"><button class="btn-warning orange" style="padding:5px;">My Sell Payout</button></a>

				<a href="<?php echo base_url('index.php/vendor/my_deducation'); ?>"><button class="btn-warning orange" style="padding:5px;">My Deducation</button></a>

				<a href="<?php echo base_url('index.php/vendor/final_payout'); ?>"><button class="btn-warning orange" style="padding:5px;">Final Payout</button></a>

				<a href="<?php echo base_url('index.php/vendor/shipping_report'); ?>"><button class="btn-warning orange" style="padding:5px;">Shipment Report</button></a>
				

			</div>

              <div class="aside-site-content my_account_section ">

                                            <div class="site-content ">

        

                 <div class="central-content"> 

        

        







<div class="cm-notification-container"></div>

            

                   

                          

    

    <div class="mainbox-container margin margin">

   <div class="mainbox-body"><div class="main_section" style="display: block;">

    <div class="my_order" style="display: block;">

	

		    <div class="sub_action_links">

	    <h3>My Order Report

		<div class="pull-right">

		<?php 

		$totalsale=0; foreach($orderdata as $items)

		{

			$order = $this->shoppingdetail_model->selectorder($items->order_id); 

			if($order[0]->status=='delivered successfully')

			{

				

				$totalsale += $items->total_amount;

			}

		}

		echo 'Total Amount of Delivered Products '.$totalsale;

		?>

		</div>

		</h3>

		<?php echo $this->session->flashdata('message'); ?>	    	

		    <div class="my_order_list table-responsive" style="padding:3px;">

            <?php if(count($orderdata)>0){ ?>

			<table class="table table-bordered table-hover">

				<thead>

					<tr>

											<th>Order ID</th>

											<th>Product Name</th>

											<th>Product ID</th>

											<th>QTY</th>

											<th>Item Amount</th>

											<th>Order Time</th>

											<th>AWB ID</th>		

											<th>Courier Company</th>

											<th>Delivery Address</th>

											<th>Expected Dispatch Date</th>

											<th>Expected Delivery Date</th>	

                                            <th>Customer Name</th>	

											<th>Customer Email</th>

											<th>Customer Mobile</th>											

											<th>Customer Address</th>

											<th>Customer Pincode</th>											

											<th>Transaction ID</th>

											<th>Payment Status</th>
											<th>Payment Method</th>

											<th>Status</th>

											<th>Status Date</th>

											<th>Action Admin ID</th>

											<th>Vendor Admin Comments</th>

											<th></th>

										</tr>

				</thead>

				<tbody>

					<?php $i=0; foreach($orderdata as $items){ $i++; ?>

					<?php  $shipping = $this->shoppingdetail_model->shippingdetail($items->id);
//print_r($shipping[0]['address']);					?>

					<?php $customer=$this->orderdetail_model->selectusertable($items->user_id); ?>

					<?php $order = $this->shoppingdetail_model->selectorder($items->order_id); ?>

					<tr>

						<td><?php echo $items->order_id; ?></td>

						<td><?php echo $items->pro_name.'<br>'.$items->options; ?></td>

						<td><?php echo $items->id; ?></td>

						<td><?php echo $items->qty; ?></td>

						<td><?php echo $items->total_amount; ?></td>

						<td><?php echo $items->order_date; ?></td>

						<td><?php echo $order[0]->awb_id; ?></td>

						<td><?php echo $order[0]->courier_company; ?></td>

						<td><?php echo $shipping[0]['address']; ?><br>							

											<?php echo $shipping[0]->city;?><br>

											<?php echo $shipping[0]->state;?><br>

											<?php echo $shipping[0]->country?></td>

						<td><?php echo date('Y-m-d', strtotime('+3 day',strtotime($items->order_date))) ?></td>	

						<td><?php echo date('Y-m-d', strtotime('+2 day',strtotime($items->order_date))) ?></td>

						<td><?php echo $customer[0]->fname; ?></td>

						<td><?php echo $customer[0]->email; ?></td>

						<td><?php echo $customer[0]->mobile; ?></td>

						<td><?php echo $customer[0]->address; ?></td>

						<td><?php echo $customer[0]->pincode; ?></td>

						<td></td>

						<td><?php echo  $order[0]->payment_status; ?></td>
						<td>
							<?php 
							if($order[0]->payment_method=='cashondelivery')
								echo  'Cash On Delivery'; 
							if($order[0]->payment_method=='payubiz')
								echo  'PayU Biz';
							?>							
						</td>

						<td><?php echo  $order[0]->status; ?></td>

						<td><?php echo $order[0]->statusdata; ?></td>

						<td></td>

						<td></td>

						<td>

						<a href="<?php echo base_url('vendor/edit_order_report/'.$order[0]->id); ?>">Edit</a>
						<a Title="Edit Order Shipment" href="<?php echo base_url('vendor/order_shipment/'.$items->order_id); ?>"><i class="fa fa-truck"></i></a>

						</td>

					</tr>

					<?php } ?>

				</tbody>

			</table> 		

			<?php } ?> 		 		    								    				

	    </div>

		</div>

		

    </div>

</div>



        

</div>

    </div>

           

                 </div> 

                   </div>

        

                    </div>

            </div>

          </div>

      </div>

     

    </div>

  </section>

  <!-- Main Container End --> 

  <!-- our clients Slider -->

  <?php $this->load->view('front/layout/footer');?>

  <?php  $user_id=$this->user_model->getLoginUserVar('USER_ID'); ?>

  <script>

   function addoption(PRODUCTID,FILTERCAT)

  {	  

		var filterval = document.getElementById('option_'+FILTERCAT).value; // $('#option_'+FILTERCAT)val();

    //alert(filterval);

	if(filterval!="")

	{	

		jQuery.ajax({

				url: "<?php echo base_url('index.php/vendor/addfilteroption'); ?>",

				type: "POST",

				data:   { product_id : PRODUCTID,filter_cat : FILTERCAT,filter_val : filterval,uid : <?php echo $user_id; ?>},

				success: function(data)

				{

					location.reload();

				},

				error: function() 

				{

					

				} 	        

		   });

	}

  }

  </script>