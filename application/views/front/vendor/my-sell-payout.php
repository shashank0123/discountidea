<?php $this->load->view('front/layout/header');?>  <!-- end nav --> 

 <style>

.naseemdiv

{

	height: 210px;

    overflow: scroll;

    border: 2px solid rgba(34, 34, 34, 0.41);

    padding: 10px;

}

.naseemdiv label{padding: 3px;}

.naseemdiv ul {padding-left:5px; list-style:none;}

</style> 

  <!-- Breadcrumbs -->

  

  <div class="breadcrumbs">

    <div class="container">

      <div class="row">

        <div class="col-xs-12">

          <ul>

            <li class="home"> <a title="Go to Home Page" href="<?php echo base_url(); ?>">Home</a><span>&raquo;</span></li>

            <li class="category13"><strong>My Sell Payout</strong></li>

          </ul>

        </div>

      </div>

    </div>

  </div>

  <!-- Breadcrumbs End --> 

  

  <!-- Main Container -->

  <section class="main-container col1-layout">

    <div class="main container">

      <div class="account-login mb-30">

        	<div class="row">

            <div class="col-md-3">

              <div id="left-pnl" class="aside-site-left my_account_section   ">

                      

<div class="action_links">

     <?php $this->load->view('front/layout/my-account-left-sidebar'); ?>   

</div>





                <div class="sidebox-bottom"> <span> &nbsp; </span> </div>

                              </div>

            </div>

            <div class="col-md-9">

			<div class="aside-site-content my_account_section ">

				<a href="<?php echo base_url('index.php/vendor/order_report'); ?>"><button class="btn-warning orange" style="padding:5px;">Order Report</button></a>

				<a href="<?php echo base_url('index.php/vendor/cancellation_report'); ?>"><button class="btn-warning orange" style="padding:5px;">Cancellation Report</button></a>

			

				<a href="<?php echo base_url('index.php/vendor/my_sell_payout'); ?>"><button class="btn-warning orange" style="padding:5px;">My Sell Payout</button></a>

				<a href="<?php echo base_url('index.php/vendor/my_deducation'); ?>"><button class="btn-warning orange" style="padding:5px;">My Deducation</button></a>

				<a href="<?php echo base_url('index.php/vendor/final_payout'); ?>"><button class="btn-warning orange" style="padding:5px;">Final Payout</button></a>
				
				<a href="<?php echo base_url('index.php/vendor/shipping_report'); ?>"><button class="btn-warning orange" style="padding:5px;">Shipment Report</button></a>
				
				

			</div>

              <div class="aside-site-content my_account_section ">

                                            <div class="site-content ">

        

                 <div class="central-content"> 

        

        







<div class="cm-notification-container"></div>

            

                   

                          

    

    <div class="mainbox-container margin margin">

   <div class="mainbox-body"><div class="main_section" style="display: block;">

    <div class="my_order" style="display: block;">

	

		    <div class="sub_action_links">

	    <h3>My Sell Payout</h3>

		<?php echo $this->session->flashdata('message'); ?>	    	

		    <div class="my_order_list table-responsive" style="padding:3px;">

			<table class="table table-bordered table-hover">

				<thead>

                                    <tr>

										<th>S.No.</th>

										<th>Order ID</th>

										<th>Product ID</th>

										<th>Transaction ID</th>

										<th>Sell Amount</th>

                                        <th>Commission</th>

                                        <th>Admin Charge</th>

										<th>Promotional Charge</th>

										<th>Taxable Amount</th>

										<th>Service Tax / GTS</th>

										<th>Final Deduction</th>

										<th>Remittance</th>

										<th>TCS Deducted</th>

										<th>Final Remittance</th>

										<th>Invoice Date</th>

										<th>Status</th>

										<th>Ready to Pay</th>

                                    </tr>

                                </thead>

                                <tbody>

								    <?php if(count($income)>0){ ?>

									<?php $i=0; foreach($income as $data){ $i++; ?>	

										<tr>

											<td><?php echo $i; ?></td>

											<td><?php echo $data->order_id; ?></td>

											<td><?php echo $data->product_id; ?></td>

											<td><?php echo $data->transaction_id; ?></td>

											<td><?php echo $data->sell_amount; ?></td>

											<td><?php echo $data->commission; ?></td>

											<td><?php echo $data->admin_charge; ?></td>

											<td><?php echo $data->promotion_charge; ?></td>

											<td><?php echo $data->taxable_amount; ?></td>

											<td><?php echo $data->servicetax_gst; ?></td>

											<td><?php echo $data->final_deduction; ?></td>

											<td><?php echo $data->remittance; ?></td>

											<td><?php echo $data->tcs_deducted; ?></td>

											<td><?php echo $data->final_remittance; ?></td>

											<td><?php echo $data->inv_date; ?></td>

											<td><?php echo $data->status; ?></td>											

											<td><?php echo $data->ready_to_pay; ?></td>											

										</tr>

									<?php }} ?>

                                </tbody>

			</table> 		

	    </div>

		</div>

		

    </div>

</div>



        

</div>

    </div>

           

                 </div> 

                   </div>

        

                    </div>

            </div>

          </div>

      </div>

     

    </div>

  </section>

  <!-- Main Container End --> 

  <!-- our clients Slider -->

  <?php $this->load->view('front/layout/footer');?>

  <?php  $user_id=$this->user_model->getLoginUserVar('USER_ID'); ?>

  <script>

   function addoption(PRODUCTID,FILTERCAT)

  {	  

		var filterval = document.getElementById('option_'+FILTERCAT).value; // $('#option_'+FILTERCAT)val();

    //alert(filterval);

	if(filterval!="")

	{	

		jQuery.ajax({

				url: "<?php echo base_url('index.php/vendor/addfilteroption'); ?>",

				type: "POST",

				data:   { product_id : PRODUCTID,filter_cat : FILTERCAT,filter_val : filterval,uid : <?php echo $user_id; ?>},

				success: function(data)

				{

					location.reload();

				},

				error: function() 

				{

					

				} 	        

		   });

	}

  }

  </script>