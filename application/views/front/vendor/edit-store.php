<?php $this->load->view('front/layout/header');?>  <!-- end nav --> 

   <style>

div.gallery {

    margin: 5px;

    border: 1px solid #ccc;

    float: left;

    width: 180px;

}



div.gallery:hover {

    border: 1px solid #777;

}



div.gallery img {

    width: 100%;

    height: auto;

}



div.desc {

    padding: 5px;

	font-size: 22px;

    text-align: center;

}

</style> 

  <!-- Breadcrumbs -->

  

  <div class="breadcrumbs">

    <div class="container">

      <div class="row">

        <div class="col-xs-12">

          <ul>

            <li class="home"> <a title="Go to Home Page" href="<?php echo base_url(); ?>">Home</a><span>&raquo;</span></li>

            <li class="category13"><strong>My Products</strong></li>

          </ul>

        </div>

      </div>

    </div>

  </div>

  <!-- Breadcrumbs End --> 

  

  <!-- Main Container -->

  <section class="main-container col1-layout">

    <div class="main container">

      <div class="account-login mb-30">

        	<div class="row">

            <div class="col-md-3">

              <div id="left-pnl" class="aside-site-left my_account_section   ">

                      

<div class="action_links">

     <?php $this->load->view('front/layout/my-account-left-sidebar'); ?>   

</div>





                <div class="sidebox-bottom"> <span> &nbsp; </span> </div>

                              </div>

            </div>

            <div class="col-md-9">

			<div class="aside-site-content my_account_section ">

				<a href="<?php echo base_url('index.php/vendor/order_report'); ?>"><button class="btn-warning orange" style="padding:5px;">Order Report</button></a>

				<a href="<?php echo base_url('index.php/vendor/cancellation_report'); ?>"><button class="btn-warning orange" style="padding:5px;">Cancellation Report</button></a>

			

				<a href="<?php echo base_url('index.php/vendor/my_sell_payout'); ?>"><button class="btn-warning orange" style="padding:5px;">My Sell Payout</button></a>

				<a href="<?php echo base_url('index.php/vendor/my_deducation'); ?>"><button class="btn-warning orange" style="padding:5px;">My Deducation</button></a>

				<a href="<?php echo base_url('index.php/vendor/final_payout'); ?>"><button class="btn-warning orange" style="padding:5px;">Final Payout</button></a>

				<a href="<?php echo base_url('index.php/vendor/shipping_report'); ?>"><button class="btn-warning orange" style="padding:5px;">Shipment Report</button></a>

			</div>

              <div class="aside-site-content my_account_section ">

                                            <div class="site-content ">

        

                 <div class="central-content"> 

        

        







<div class="cm-notification-container"></div>

            

                   

                          

    

    <div class="mainbox-container margin margin">

   <div class="mainbox-body"><div class="main_section" style="display: block;">

    <div class="my_order" style="display: block;">

	<div class="sub_action_links">

	    <h3>Manage your store

		<a class="pull-right" target="_blank" href="<?php echo base_url('vendor/store/'.slugurl($store[0]->title).'/'.$store[0]->id); ?>"> view store</a>

		</h3>

		

	    <?php echo $this->session->flashdata('message'); ?>

	    	

		    <div class="my_order_list table-responsive" style="padding:3px;">

			<form method="post" enctype='multipart/form-data'>

			<input type="hidden" name="store_id" value="<?php echo $store[0]->id; ?>">

			<table class="table table-bordered table-hover" id="imageproduct">

				<tr>

										<td>Store Title</td>

										<td><input type="text" name="title"  class="form-control" value="<?php echo $store[0]->title; ?>" autocomplete="off" required></td>

									</tr>

								    <tr>

										<td>Logo</td>

										<td>

											<img src="<?php echo base_url('uploads/store/'.$store[0]->logo); ?>"  width="70">

											<input type="file" name="logo"  class="form-control">

											<input type="hidden" name="old_logo" value="<?php echo $store[0]->logo; ?>">

										</td>

									</tr>

									<tr>

										<td>About your store</td>

										<td>
											<textarea name="content" value="" id="content-editor" class="form-control" rows="3" ><?php echo $store[0]->content; ?></textarea>
										</td>

									</tr>

									

									<tr>

										<td colspan="2"><button type="submit" class="btn btn-info" name="createstore">Submit</button></td>

									</tr>

									

			</table> 	

			</form>	

	    </div>

             <script>
                CKEDITOR.replace( 'content-editor' );
            </script>

		<div class="sub_action_links" >

	    <h3>Manage Slider Images</h3>

			<?php echo $this->session->flashdata('message1'); ?>	    	

		    <div class="my_order_list table-responsive" style="padding:3px;">

			<?php if(count($LISTCATEGORYIMAGE)>0){ ?>

									<?php foreach($LISTCATEGORYIMAGE as $data){ ?>

									<div class="gallery">

										<img src="<?php echo base_url('uploads/store/'.$data->image); ?>" alt="Fjords" width="100">									  

										<div class="desc"><a href="<?php echo base_url('index.php/user/deletestoreimages/'.$data->id);?>" title="delete this record" onclick="return confirm('Are you sure you want to delete this item?');"><i class="fa fa-trash" aria-hidden="true"></i></a></div>

									</div>

									<?php } }  ?>

                                    <form enctype="multipart/form-data" method="POST">									

										<table class="table table-bordered table-striped mb-none">

											<tr>

												<td>

													<a class="btn btn-xs btn-warning" onclick="return addimage();" style="float: right;"><i class="fa fa-plus" aria-hidden="true"></i> Add</a>

													<div id="appendimage"></div>

													<button class="btn btn-success" id="uploadbutton" name="uploadfiles">Upload</button>

												</td>

											</tr>										

										</table>

									</form>

	    </div> </div>

		

		</div>

		

    </div>

</div>



        

</div>

    </div>

           

                 </div> 

                   </div>

        

                    </div>

            </div>

          </div>

      </div>

     

    </div>

  </section>

  <!-- Main Container End --> 

  <!-- our clients Slider -->

  <script>

	var i = 0;

function addimage()

{

	i++;

	var fileHtml = '<span id="rmdiv'+ i +'"><div class="col-md-12" style="padding: 9px;"><div class="col-md-4"><input type="file" class="form-control" name="image[]" id="image" required></div><div class="col-md-7"> <input type="text" class="form-control" name="image_url[]" id="image_url" placeholder="enter url" required></div><div class="col-md-1"><button onclick="return removefile('+ i +')" type="button" class=" btn-xs btn-danger imgremove"> <b>X</b></button></div></div></span>';

	$('#appendimage').append(fileHtml);

}

function removefile(ID)

{

	$('#rmdiv'+ ID).remove();

}

</script>

  <?php $this->load->view('front/layout/footer');?>