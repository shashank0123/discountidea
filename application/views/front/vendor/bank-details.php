<?php $this->load->view('front/layout/header');?>  <!-- end nav --> 
  
  <!-- Breadcrumbs -->
  <style>
		
		#lightbox {
			position:fixed; /* keeps the lightbox window in the current viewport */
			top:0; 
			left:0; 
			width:100%; 
			height:100%; 
			background:url(<?php echo base_url('assets/front/overlay.png'); ?>) repeat; 
			text-align:center;
			z-index: 10000000;
		}
		#lightbox p {
			text-align:right; 
			color:#fff; 
			margin-right:20px; 
			font-size:12px; 
		}
		#lightbox img {
			box-shadow:0 0 25px #111;
			-webkit-box-shadow:0 0 25px #111;
			-moz-box-shadow:0 0 25px #111;
			max-width:940px;
		}
		</style>
  <div class="breadcrumbs">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <ul>
            <li class="home"> <a title="Go to Home Page" href="<?php echo base_url(); ?>">Home</a><span>&raquo;</span></li>
            <li class="category13"><strong>My Products</strong></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <!-- Breadcrumbs End --> 
  
  <!-- Main Container -->
  <section class="main-container col1-layout">
    <div class="main container">
      <div class="account-login mb-30">
        	<div class="row">
            <div class="col-md-3">
              <div id="left-pnl" class="aside-site-left my_account_section   ">
                      
<div class="action_links">
     <?php $this->load->view('front/layout/my-account-left-sidebar'); ?>   
</div>


                <div class="sidebox-bottom"> <span> &nbsp; </span> </div>
                              </div>
            </div>
            <div class="col-md-9">
			<?php  if($user[0]->vender_type==1){ ?> 
			<div class="aside-site-content my_account_section ">
				<a href="<?php echo base_url('index.php/vendor/order_report'); ?>"><button class="btn-warning orange" style="padding:5px;">Order Report</button></a>
				<a href="<?php echo base_url('index.php/vendor/cancellation_report'); ?>"><button class="btn-warning orange" style="padding:5px;">Cancellation Report</button></a>
			
				<a href="<?php echo base_url('index.php/vendor/my_sell_payout'); ?>"><button class="btn-warning orange" style="padding:5px;">My Sell Payout</button></a>
				<a href="<?php echo base_url('index.php/vendor/my_deducation'); ?>"><button class="btn-warning orange" style="padding:5px;">My Deducation</button></a>
				<a href="<?php echo base_url('index.php/vendor/final_payout'); ?>"><button class="btn-warning orange" style="padding:5px;">Final Payout</button></a>
			</div>
			<?php } ?>
              <div class="aside-site-content my_account_section ">
                                            <div class="site-content ">
        
                 <div class="central-content"> 
        
        



<div class="cm-notification-container"></div>
            
                   
                          
    
    <div class="mainbox-container margin margin">
   <div class="mainbox-body"><div class="main_section" style="display: block;">
    <div class="my_order" style="display: block;">
	<div class="sub_action_links">
	    <h3>Bank Details</h3>
	    <?php echo $this->session->flashdata('message'); ?>
	    	
		    <div class="my_order_list table-responsive" style="padding:3px;">
			<form method="post" enctype='multipart/form-data'>
			<table class="table table-bordered table-hover">
				<tr>
					<td>Account No.</td>
					<td><input type="text" name="account_no" id="acno" class="form-control" autocomplete="off" value="<?php echo $user[0]->account_no; ?>" required></td>
				</tr>
				<tr>
					<td>Re-type Account No.</td>
					<td><input type="text" name="retype_account_no" id="acno_retype" class="form-control" value="" autocomplete="off" required></td>
				</tr>
				<tr>
					<td>Account Holder Name</td>
					<td><input type="text" name="account_holder"  class="form-control" value="<?php echo $user[0]->account_holder; ?>" autocomplete="off" required></td>
				</tr>
				<tr>
					<td>IFSC Code</td>
					<td><input type="text" name="ifsc_code"  class="form-control" value="<?php echo $user[0]->ifsc_code; ?>" autocomplete="off" required></td>
				</tr>				
				<tr>
					<td>Bank Name</td>
					<td><input type="text" name="bank_name"  class="form-control" value="<?php echo $user[0]->bank_name; ?>" autocomplete="off" required></td>
				</tr>
				<tr>
					<td>Bank Branch</td>
					<td><input type="text" name="bank_branch"  class="form-control" value="<?php echo $user[0]->bank_branch; ?>" autocomplete="off" required></td>
				</tr>
				<tr>
					<td>Bank City</td>
					<td><input type="text" name="bank_city"  class="form-control" value="<?php echo $user[0]->bank_city; ?>" autocomplete="off" required></td>
				</tr>
				<tr>
					<td>Bank State</td>
					<td><input type="text" name="bank_state"  class="form-control" value="<?php echo $user[0]->bank_state; ?>" autocomplete="off" required></td>
				</tr>
				<tr>
					<td>Upload Cancel Cheque or passbook</td>
					<td>
						<input type="file" name="passbook"  class="form-control" required >
						<input type="hidden" name="old_passbook" value="<?php echo $user[0]->passbook; ?>">
						<?php if($user[0]->passbook!=""){ ?>
						     <a href="<?php echo base_url('uploads/vendor/'.$user[0]->passbook) ?>" class="lightbox_trigger">
							<img src="<?php echo base_url('uploads/vendor/'.$user[0]->passbook) ?>" width="150" >
							</a>
						<?php } ?>
					</td>
				</tr>
				<tr>
				<?php if($user[0]->bank_status==0){ ?>
				<td colspan="2"><button type="submit" class="btn btn-info" name="bankdetails" onclick="return checkaccountno();">Submit</button></td><?php } ?> 
				</tr>					
			</table> 	
			</form>	
	    </div></div>
		
    </div>
</div>

        
</div>
    </div>
           
                 </div> 
                   </div>
        
                    </div>
            </div>
          </div>
      </div>
     
    </div>
  </section>
  <script>
	function checkaccountno()
	{
		var acno = document.getElementById('acno').value;
		var acno_retype = document.getElementById('acno_retype').value;
		if(acno!=acno_retype)
		{
			alert('re-type account no not matched account no.');
			return false;
		}	
	}
  </script>
  
  <!-- Main Container End --> 
  <!-- our clients Slider -->
  <?php $this->load->view('front/layout/footer');?>
 <script src="http://code.jquery.com/jquery-1.6.2.min.js"></script>
  <script>
jQuery(document).ready(function($) {
	
	$('.lightbox_trigger').click(function(e) {
		
		//prevent default action (hyperlink)
		e.preventDefault();
		
		//Get clicked link href
		var image_href = $(this).attr("href");
				
		if ($('#lightbox').length > 0) { // #lightbox exists
			
			//place href as img src value
			$('#content1').html('<img src="' + image_href + '" />');
		   	
			//show lightbox window - you could use .show('fast') for a transition
			$('#lightbox').show();
		}
		
		else { //#lightbox does not exist - create and insert (runs 1st time only)
			
			//create HTML markup for lightbox window
			var lightbox = 
			'<div id="lightbox">' +
				'<p>Click to close</p>' +
				'<div id="content1">' + //insert clicked link's href into img src
					'<img src="' + image_href +'" />' +
				'</div>' +	
			'</div>';
				
			//insert lightbox HTML into page
			$('body').append(lightbox);
		}
		
	});
	
	//Click anywhere on the page to get rid of lightbox window
	$('#lightbox').live('click', function() { //must use live, as the lightbox element is inserted into the DOM
		$('#lightbox').hide();
	});

});
</script>