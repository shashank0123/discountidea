<?php $this->load->view('front/layout/header');?>  <!-- end nav --> 
  <?php $order = $this->user_model->getOrderByOrderId($items[0]->order_id); ?>
  <?php $vandor = $this->user_model->select_vendor_by_productid($items[0]->pro_id); ?> 
  <?php $product_extra_text = $this->product_model->select_product_terms($items[0]->pro_id); ?>
  <!-- Breadcrumbs -->
  <div class="breadcrumbs">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <ul>
            <li class="home"> <a title="Go to Home Page" href="index.html">Home</a><span>&raquo;</span></li>
            <li class="category13"><strong>My Orders</strong></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <!-- Breadcrumbs End --> 
  
  <!-- Main Container -->
  <section class="main-container col1-layout">
    <div class="main container">
      <div class="account-login mb-30">
        	<div class="row">
            <div class="col-md-3">
              <div id="left-pnl" class="aside-site-left my_account_section   ">
                      
<div class="action_links">
     <?php $this->load->view('front/layout/my-account-left-sidebar'); ?>   
</div>

                <div class="sidebox-bottom"> <span> &nbsp; </span> </div>
                              </div>
            </div>
            <div class="col-md-8">
              <div class="aside-site-content my_account_section ">
                    <div class="site-content ">
                 <div class="central-content">
<div class="cm-notification-container"></div>
    
    <div class="mainbox-container margin margin">
   <div class="mainbox-body"><div class="main_section" >
    <div class="my_order" style="display: block;">
		<div class="sub_action_links">
			<h3>Order Information  <a href="#" class="pull-right" onclick="return printData();">Print</a></h3>			
	    </div>
	    <div class="my_order_list table-responsive" style="padding:3px;" id="printData143567">
		    	
            <table class="table table-bordered table-hover" id="printTable_1" style="width:100%; border: 1px solid #ddd;">
            <?php $store = $this->store_model->selectstoreby_uid($vandor[0]->id); ?>
				<thead>
					<tr>
						<td class="text-left" colspan="2" style="text-align:center;">					
						<img src="<?php echo base_url('uploads/store/'.$store[0]->logo); ?>" >
						<br><?php echo $vandor[0]->company_name; ?>
						</td>
					</tr>
				</thead>
				<tbody>
					<tr >
						<td class="text-left" style="border: 1px solid #ddd;"> 
							<b>Order ID:</b> #<?php echo $order[0]->id; ?><br>
							<b>Order Date:</b> <?php echo date('d/m/Y',$order[0]->ord_time); ?><br>
							<b>Order Status:</b> <?php echo $order[0]->status; ?>
						</td>
						<td class="text-left" style="border: 1px solid #ddd;"> 
							<b>Payment Method:</b> <?php echo $order[0]->payment_method; ?><br>
							<?php if(strtolower($order[0]->payment_method)!='cashondelivery') {?>
							<b>Getway Payment ID:</b> <?php echo $order[0]->transaction_id; ?>
							<?php } ?>
						</td>
					</tr>
					<tr >
						<td class="text-left" style="border: 1px solid #ddd;" colspan="2"> 
							<h3>Seller Information</h3>	
						</td>						
					</tr>
					<tr >
						<td class="text-left" style="border: 1px solid #ddd;"> 
							<b>Business Name:</b> <?php echo $vandor[0]->company_name; ?><br>
							<b>Business Address:</b> <?php echo $vandor[0]->company_address; ?><br>
							<b>TAN / VAT No. :</b> <?php echo $vandor[0]->tin_vat; ?><br>
							<b>TAN No. :</b> <?php echo $vandor[0]->tan; ?><br>
							<b>GSTIN / Provisional ID :</b> <?php echo $vandor[0]->gstin; ?><br>
						</td>
						<td class="text-left" style="border: 1px solid #ddd;">
							<b>Vendor ID :</b> <?php echo $vandor[0]->id; ?><br>
							<b>Vendor Name :</b> <?php echo $vandor[0]->fname; ?><br>
							<?php /* ?><b>Vendor Email :</b> <?php echo $vandor[0]->email; ?><br>
							<b>Vendor Mobile No. :</b> <?php echo $vandor[0]->mobile; ?><br><?php */ ?>							
						</td>
					</tr>
				</tbody>
			</table>
			<?php $billing = $this->shoppingdetail_model->billingdetail($order[0]->id); ?>
			<?php $shipping = $this->shoppingdetail_model->shippingdetail($order[0]->id); ?>
			<table class="table table-bordered table-hover" id="printTable_2" style="width:100%; border: 1px solid #ddd;">
				<thead>
					<tr>
						<td class="text-left"  style="border: 1px solid #ddd;"><b>Billing Address</b></td>
						<td class="text-left"  style="border: 1px solid #ddd;"><b>Shipping Address</b></td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="text-left" style="border: 1px solid #ddd;">
							<?php echo $billing[0]->firstname.' '.$billing[0]->lastname; ?><br>
							<?php echo $billing[0]->mobile;?><br>
							<?php echo $billing[0]->address; ?><br>	
                            <?php echo $billing[0]->pincode?><br> 							
							<?php echo $billing[0]->city;?><br>
							<?php echo $billing[0]->state;?><br>
							<?php echo $billing[0]->country?>							
						</td>
						<td class="text-left"  style="border: 1px solid #ddd;">
							<?php echo $shipping[0]['firstname'].' '.$shipping[0]['lastname']; ?><br>
							<?php //echo $shipping['mobile'];?>
							<?php echo $shipping[0]['address']; ?><br>	
                            <?php echo $shipping[0]['pincode']?><br> 							
							<?php echo $shipping[0]['city'];?><br>
							<?php echo $shipping[0]['state'];?><br>
							<?php echo $shipping[0]['country'];?>
						</td>
					</tr>
				</tbody>
			</table>
			<table class="table table-bordered table-hover" id="printTable_3" style="width:100%; border: 1px solid #ddd;">
				<thead>
					<tr>
						<td class="text-left"  style="border: 1px solid #ddd;">Product ID</td>
						<td class="text-left"  style="border: 1px solid #ddd;">Product Name</td>
						<td class="text-right"  style="border: 1px solid #ddd;">Quantity</td>
						<td class="text-right"  style="border: 1px solid #ddd;">Gross Amount</td>
						<td class="text-right"  style="border: 1px solid #ddd;">Discount</td>
						<td class="text-right"  style="border: 1px solid #ddd;">Taxable Value</td>
						<td class="text-right"  style="border: 1px solid #ddd;">IGST OR (CGST+SGST)</td>
						<td class="text-right"  style="border: 1px solid #ddd;">Total</td>
					</tr>
				</thead>
				<tbody>
				   <?php //$items = $this->shoppingdetail_model->prodcutdetail($order[0]->id); ?>
				   <?php
				   if(count($items)>0){
				   	$totalTaxable = 0;
				   	$totalTax = 0;
				   	$totalAmount = 0;
				   	foreach($items as $item){
				   ?>				   
					<tr>
					    <td class="text-left"  style="border: 1px solid #ddd;"><?php echo $item->pro_id; ?></td>
						<td class="text-left"  style="border: 1px solid #ddd;"><?php echo $item->pro_name; ?>
						<?php if(!empty($item->options) && $item->options!='0')
						{
							echo '<br>'.$item->options;
						}
						$unitPrice = $item->unit_price;
						$discountValue = $item->discountvalue;
						$taxableValue = get_net_amount($item->total_amount,$item->gst_percentage);
						$tax = ($unitPrice-$taxableValue);
						$total = $item->finaltopay;
				   		$totalTaxable += $taxableValue;
						$totalTax += $tax;
				   		$totalAmount += $total;
						?>
						</td>
						<td class="text-right" style="border: 1px solid #ddd;"><?php echo $item->qty; ?></td>
						<td class="text-right"  style="border: 1px solid #ddd;"><?php echo CURRENCY.number_format($unitPrice,2); ?></td>
						<td class="text-right"  style="border: 1px solid #ddd;"><?php echo CURRENCY.number_format($discountValue,2); ?></td>
						<td class="text-right"  style="border: 1px solid #ddd;"><?php echo CURRENCY.number_format($taxableValue,2); ?></td>
						<td class="text-right"  style="border: 1px solid #ddd;"><?php echo CURRENCY.number_format($tax,2); ?></td>
						<td class="text-right"   style="border: 1px solid #ddd;"><?php echo CURRENCY.number_format($total,2); ?></td>
					</tr>
				   <?php } ?> 
				   <?php } ?>
				</tbody>
				<tfoot>
					<tr>
						<td colspan="6" style="border: 1px solid #ddd;"></td>
						<td class="text-right"  style="border: 1px solid #ddd;"><b>Net Amount</b></td>
						<td class="text-right"  style="border: 1px solid #ddd;"><?php echo CURRENCY.number_format($totalTaxable,2); ?></td>
					</tr>
					
					<tr>
						<td colspan="6" style="border: 1px solid #ddd;"></td>
						<td class="text-right" style="border: 1px solid #ddd;"><b>Shipping</b></td>
						<td class="text-right"  style="border: 1px solid #ddd;">Free</td>
					</tr>

					<tr>
						<td colspan="6" style="border: 1px solid #ddd;"></td>
						<td class="text-right" style="border: 1px solid #ddd;"><b>Tax</b></td>
						<td class="text-right"  style="border: 1px solid #ddd;"><?php echo CURRENCY.number_format($totalTax,2); ?></td>
					</tr>

					<tr>
						<td colspan="6" style="border: 1px solid #ddd;"></td>
						<td class="text-right"  style="border: 1px solid #ddd;"><b>Total</b></td>
						<td class="text-right" style="border: 1px solid #ddd;"><?php echo CURRENCY.number_format($totalAmount,2); ?></td>
					</tr>
					<tr>
						<td colspan="8" style="border: 1px solid #ddd; text-align:center;">
							<img src="<?php echo base_url('assets/front/images/invoice-logo.png'); ?>">
							<br>www.DiscountIdea.com
						</td>
					</tr>
					<?php if(count($product_extra_text)>0){ foreach($product_extra_text as $text){  ?>
					<tr>
						<td  colspan="8" style="border: 1px solid #ddd;">
							<p>
								<b><?php echo $text->title; ?></b><br>
								<?php echo $text->content; ?>
							</p>
						</td>
					</tr>
					<?php } } ?>
					
				</tfoot> 
			</table>
            <table style="width:100%;">
				
			</table>
	    </div>
		
    </div>
</div>

        
</div>
    </div>
           
                 </div> 
                   </div>
        
                    </div>
            </div>
          </div>
      </div>
     
    </div>
  </section>
  <?php 
    function get_net_amount($amount,$tax)
	{
		return $amount/(1+($tax/100));
	}
  ?>
  <script>
  function printData()
{
   var divToPrint=document.getElementById("printTable_1").outerHTML +''+ document.getElementById("printTable_2").outerHTML +''+ document.getElementById("printTable_3").outerHTML;
   newWin= window.open("");
   newWin.document.write(divToPrint);
   newWin.print();
   newWin.close();
}
  </script>
  <!-- Main Container End --> 
  <!-- our clients Slider -->
  <?php $this->load->view('front/layout/footer');?>
  