<?php $this->load->view('front/layout/header');?>  <!-- end nav --> 
 <style>
.naseemdiv
{
	height: 210px;
    overflow: scroll;
    border: 2px solid rgba(34, 34, 34, 0.41);
    padding: 10px;
}
.naseemdiv label{padding: 3px;}
.naseemdiv ul {padding-left:5px; list-style:none;}
</style> 
  <!-- Breadcrumbs -->
 <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> 
  <div class="breadcrumbs">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <ul>
            <li class="home"> <a title="Go to Home Page" href="<?php echo base_url(); ?>">Home</a><span>&raquo;</span></li>
            <li class="category13"><strong>My Orders Invoice</strong></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <!-- Breadcrumbs End --> 
  
  <!-- Main Container -->
  <section class="main-container col1-layout">
    <div class="main container">
      <div class="account-login mb-30">
        	<div class="row">
            <div class="col-md-3">
              <div id="left-pnl" class="aside-site-left my_account_section   ">
                      
<div class="action_links">
     <?php $this->load->view('front/layout/my-account-left-sidebar'); ?>   
</div>


                <div class="sidebox-bottom"> <span> &nbsp; </span> </div>
                              </div>
            </div>
            <div class="col-md-9">
			
              <div class="aside-site-content my_account_section ">
                                            <div class="site-content ">
        
                 <div class="central-content"> 
        
        



<div class="cm-notification-container"></div>
    
    <div class="mainbox-container margin margin">
	<div class="mainbox-body"><div class="main_section" style="display: block;">
    <div class="my_order" style="display: block;">
	    
		<div class="sub_action_links">
	    <h3>Invoice Listing</h3>
		<?php echo $this->session->flashdata('message'); ?>	           		
		    <div class="my_order_list table-responsive" style="padding:3px;">
			
			<table class="table table-bordered table-hover">
				<tr>
					<td width="15%">
						<input type="text" value="" class="form-control" name="order_id" placeholder="order id">
					</td>
					<td width="15%">
						<input type="text" value="" class="form-control" name="pro_id" placeholder="Product id">
					</td>
					<td width="20%">
						<input type="text" value="" class="form-control" name="sdate" id="datepicker1" placeholder="Start Date (yyyy-mm-dd)">
					</td>
					<td width="20%">
						<input type="text" value="" class="form-control" name="edate" id="datepicker2" placeholder="Start Date (yyyy-mm-dd)">
					</td>
					<td width="10%">
						<button type="submit" class="form-control">Search</button>
					</td>
				</tr>
			</table><br>
			
            <?php if(count($orderdata)>0){ ?>
			<table class="table table-bordered table-hover">
				<thead>
					<tr>
						<th>Order ID</th>
						<th>Product ID</th>
						<th>Product Name</th>											
						<th>Order Date</th>											
						<th>Status</th>											
						<th></th>
					</tr>
				</thead>
				<tbody>
					<?php $i=0; foreach($orderdata as $items){ $i++; ?>					
					<?php $order = $this->shoppingdetail_model->selectorder($items->order_id); ?>
					<tr>
						<td><?php echo $items->order_id; ?></td>
						<td><?php echo $items->pro_id; ?></td>
						<td><?php echo $items->pro_name.'<br>'.$items->options; ?></td>						
						<td><?php echo $items->order_date; ?></td>						
						<td><?php echo  $order[0]->status; ?></td>						
						<td>
							<a href="<?php echo base_url('vendor/invoice_details/'.$items->id); ?>">
								<i class="fa fa-eye"></i>
							</a>
						</td>
					</tr>
					<?php } ?>
				</tbody>
			</table> 		
			<?php } ?> 		 		    								    				
	    </div>
		</div>
		
    </div>
</div>

        
</div>
    </div>
           
                 </div> 
                   </div>
        
                    </div>
            </div>
          </div>
      </div>
     
    </div>
  </section>
  <!-- Main Container End --> 
  <!-- our clients Slider -->
  <?php $this->load->view('front/layout/footer');?>
  <?php  $user_id=$this->user_model->getLoginUserVar('USER_ID'); ?>
  
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
   <script>
  jQuery( function() {
    jQuery( "#datepicker1, #datepicker2" ).datepicker({ dateFormat: 'yy-mm-dd' });
    
  } );
  </script>