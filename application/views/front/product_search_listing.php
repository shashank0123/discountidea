<?php $this->load->view('front/layout/header');?>
  
  <!-- Breadcrumbs -->
  <div class="breadcrumbs">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <ul>
            <li class="home"> <a title="Go to Home Page" href="<?php echo base_url(); ?>">Home</a><span>&raquo;</span></li>
            <li class=""><strong>Search</strong></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <!-- Breadcrumbs End --> 
  <!-- Main Container -->
  <div class="main-container col2-left-layout">
    <div class="container">
      <div class="row">        
        <div class="col-main col-sm-12">
          <div class="page-title">
            <h2><?php echo (isset($pagedata[0]->title))?$pagedata[0]->title:''; ?></h2>
          </div>
          <?php echo  $this->session->flashdata('message');	  ?>
		  <span id="filterresponse" >
          <div class="product-grid-area" id="griddata">
            <ul class="products-grid">
			  <?php if(count($productdata)>0){ ?>
			  <?php foreach($productdata as $product){ ?>
              <li class="item col-lg-3 col-md-3 col-sm-6 col-xs-6 wow fadeInUp">
                <div class="product-item">
                  <div class="item-inner">
                    <div class="product-thumbnail">                      
                      <div class="pr-img-area">
						  
							<?php $product_image = $this->product_model->selectProductDoubleImage($product->id); ?>
							<?php if(count($product_image)>0){ 
$sws_cat=$this->product_model->ProductCategory($product->category_id); 
								?>
							<a href="<?php echo base_url(slugurl($sws_cat->ptitle).'/'.slugurl($sws_cat->title).'/'.slugurl($product->name).'/'.encodeurlval($product->id)); ?>">
							 	<figure>
					<img class="first-img" src="<?php echo base_url('uploads/product/thumbs'); ?>/<?php echo $product_image[0]->image; ?>" alt="<?php echo $product->name; ?>">      <?php  }?> 
							 <?php if(count($product_image)>1){ ?>
							<img class="hover-img" src="<?php echo base_url('uploads/product/thumbs'); ?>/<?php echo $product_image[1]->image; ?>" alt="<?php echo $product->name; ?>">
							<?php }else{ ?>
							<img class="first-img" src="<?php echo base_url('assets/front');?>/images/264x264.jpg" alt="<?php echo $product->name; ?>"> 
							<img class="hover-img" src="<?php echo base_url('assets/front');?>/images/264x264.jpg" alt="<?php echo $product->name; ?>">
							<?php } ?>
						 </figure>
						</a>
						 <form method="post" action="<?php echo base_url('index.php/cart/addtocart'); ?>">
						   <input type="hidden" name="pid" value="<?php echo $product->id; ?>" >
						   <input type="hidden" name="qty" value="1" >
<!-- <button type="button" name="addtocartbutton1" onclick="return addtocart(<?php echo $product->id; ?>);" class="add-to-cart-mt"> <i class="fa fa-shopping-cart"></i><span> Add to Cart</span> </button> -->
<?php
						$loginuserid=$this->user_model->getLoginUserVar('USER_ID');
						if(!empty($loginuserid))
						{
						?>
                          <div class="mt-button add_to_wishlist"> <a class="add-to-cart-mt" href="<?php echo base_url('index.php/user/addtowishlist/'.$loginuserid.'/'.$product->id);?>"> <i class="fa fa-heart"></i> Add to Wishlist </a> </div>
						  <?php
						}else {
							?>
							<div class="mt-button add_to_wishlist"> <a class="add-to-cart-mt" href="<?php echo base_url('user/loginuser');?>"> <i class="fa fa-heart"></i> Add to Wishlist</a> </div>
						<?php 
						}
						?>
							<a target="_blank" href="<?php echo base_url(slugurl($sws_cat->ptitle).'/'.slugurl($sws_cat->title).'/'.slugurl($product->name)).'/'.encodeurlval($product->id); ?>"><button type="button" name="Buy Now" class="add-to-cart-mt1"> <i class="fa fa-bolt"></i><span> Buy Now</span> </button></a>  

						   <!-- <button type="submit" name="addtocartbutton" class="add-to-cart-mt1"> <i class="fa fa-shopping-cart"></i><span> Buy Now</span> </button> -->
						</form>
					 </div>					  
                      <div class="pr-info-area">
                        <!-- <div class="pr-button">
						<?php
						$loginuserid=$this->user_model->getLoginUserVar('USER_ID');
						if(!empty($loginuserid))
						{
						?>
                          <div class="mt-button add_to_wishlist"> <a href="<?php echo base_url('index.php/user/addtowishlist/'.$loginuserid.'/'.$product->id);?>"> <i class="fa fa-heart"></i> </a> </div>
						  <?php
						}else {
							?>
							<div class="mt-button add_to_wishlist"> <a href="<?php echo base_url('user/loginuser');?>"> <i class="fa fa-heart"></i> </a> </div>
						<?php 
						}
						?>
                        </div> -->


                        <!-- add to wishlist on image -->
                      </div>
                    </div>
                    <div class="item-info">
                      <div class="info-inner">
                        <div class="item-title"> 
							<a title="<?php echo $product->name; ?>" href="<?php echo base_url('product/'.slugurl($product->name)).'/'.encodeurlval($product->id); ?>">
								<?php echo $product->name; ?>
							</a>
						</div>
                        <div class="item-content">
                          <div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                          <div class="price-box">
							  <?php if($product->spacel_price!=""){ ?>
                              <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> <?php echo CURRENCY.$product->spacel_price; ?> </span> </p>
                              <p class="old-price"> <span class="price-label">Regular Price:</span> <span class="price"> <?php echo CURRENCY.$product->price; ?> </span> </p>
							  <?php }else{ ?>
							  <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> <?php echo CURRENCY.$product->price; ?> </span> </p>
							  <?php } ?>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </li>
              <?php } }else{ ?>
					<center><br><br>
						<!--<p>You searched for <strong><?php echo $_GET['q']; ?></strong><p>-->
						<h2>Sorry, no results found!</h2>
						<p class="lead text-gray">Please check the spelling or try searching for something else</p>
					</center>	
			  <?php } ?>
            </ul>
          </div>
		     
		  </span>
		 
        </div>
      </div>
    </div>
  </div>
  <!-- Main Container End -->
<script>
function addtocart(pid)
{
	jQuery.ajax({
        	url: "<?php echo base_url('index.php/cart/addtocart_ajax'); ?>",
			type: "POST",
			data:   { product_id : pid},
			//beforeSend: function(){$(this).html('OKKKKKK')},
			success: function(data)
		    {
				jQuery('#cartresponsedata').html(data);
				jQuery(".mini-cart").attr("tabindex",-1).focus(500);
			},
		  	error: function() 
	    	{
				
	    	} 	        
	   });
}
</script>			
<?php $this->load->view('front/layout/footer');?>
