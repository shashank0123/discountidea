<?php $this->load->view('front/layout/header');?>
<?php $breadcrumbs = $this->category_model->breadcrumbs($pagedata[0]->id,$pagedata[0]->id); ?>
  <!-- Breadcrumbs -->
  <div class="breadcrumbs">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
		
          <ul>
            <li class="home"> <a title="Go to Home Page" href="<?php echo base_url(); ?>">Home</a><span>&raquo;</span></li>
            <?php if(count($breadcrumbs)>0)
			{
				foreach($breadcrumbs as $breadcrumb)
				{
					echo $breadcrumb['breadcrumbs'];
				}
			} ?>			
		 </ul>
        </div>
      </div>
    </div>
  </div>
  <!-- Breadcrumbs End --> 
  <!-- Main Container -->
  <div class="main-container col2-left-layout">
    <div class="container">
      <div class="row">
        <aside class="col-md-3">      
          <?php $this->load->view('front/layout/left-side-bar');?>
        </aside>
        <div class="col-md-9 ">
          <div class="page-title">
            <h1><?php echo (isset($pagedata[0]->title))?$pagedata[0]->title:''; ?></h1>
          </div>
          <div class="category-description std">
            <div class="slider-items-products">
              <div id="category-desc-slider" class="product-flexslider hidden-buttons">
                <div class="slider-items slider-width-col1 owl-carousel owl-theme"> 
           
                  <!-- Item -->
                  <?php if(count($categoryimages)>0){ ?>
				  <?php foreach($categoryimages as $cat_image){ ?>
				  <div class="item"><a href="<?php echo $cat_image->url ?>" target="_blank"><img alt="" src="<?php echo base_url('uploads/category/'.$cat_image->image); ?>"></a></div>
                  <?php }}else{ ?>
                  <div class="item"> <a href="#x"><img alt="" src="#x"></a> </div>
                  <?php } ?>
                  <!-- End Item --> 
                  
                </div>
              </div>
            </div>
          </div>
          <div class="toolbar">
            <div class="view-mode">
              <ul id="productdisplay">
                <li class="active" onclick="return mytab('griddata','listdata');"> <a href="javascript:void(0);" > <i class="fa fa-th-large"></i> </a> </li>
                <li onclick="return mytab('listdata','griddata');"> <a href="javascript:void(0);"> <i class="fa fa-th-list"></i> </a> </li>
              </ul>
			  
            </div>
			<div  class="view-mode" style="margin-top: 10px; float: right; margin-right: 10px;">
			  <ul>
                
					<form id="filter_low"  method="post"><li id="active_class">Price -- Low to High</li>
					<input type="hidden" id="filter" name="filter_low" value="asc">
					<input type="hidden" id="cat_id" name="category" value="<?php echo $pagedata[0]->id; ?>">

					</form>
				
                <form id="filter_high"  method="post"><li id="active_class1"> Price -- High to Low </li>
					<input type="hidden" id="filter" name="filter_high" value="asc">
					<input type="hidden" id="cat_id" name="category" value="<?php echo $pagedata[0]->id; ?>">

					</form>
              </ul>
			  </div>
            <div class="sorter">
			  
            </div>
          </div>
          <style type="text/css">
            .bottom-banner-img {
    width: 100%;
    float: left;
    height: 240px;
    margin: 15px 0px;
    position: relative;
    display: inline-block;
}

.bottom-banner-img img {
    max-width: 100%;
}

.bottom-banner-img .banner-overly {
    width: 100%;
    height: 240px;
    background-color: rgba(55, 55, 55, 0.5);
}

.banner-overly {
    top: 0px;
    left: 0px;
    z-index: 1;
    position: absolute;
    background-color: rgba(0, 0, 0, 0.1);
    -moz-transition: 0.4s;
    -o-transition: 0.4s;
    -webkit-transition: 0.4s;
    transition: 0.4s;
}

.bottom-img-info {
    width: 100%;
    z-index: 1;
    position: absolute;
    height: 100%;
    color: #fff;
    text-align: center;
    top: 0px;
}
a:hover {
    text-decoration: none;
    color: #0083c1;
}
.bottom-banner-img:hover .banner-overly {
    background-color: rgba(55, 55, 55, 0.9);
}

.bottom-banner-img .shop-now-btn {
    opacity: 0;
    -moz-transition: 0.4s;
    -o-transition: 0.4s;
    -webkit-transition: 0.4s;
    transition: 0.4s;
    background-color: #e62263;
    border-color: #e62263;
}
.bottom-banner-img h3 {
    margin-top: 100px;
    margin-bottom: 5px;
    -moz-transition: 0.4s;
    -o-transition: 0.4s;
    -webkit-transition: 0.4s;
    transition: 0.4s;
    text-align: center;
    font-weight: 900;
    letter-spacing: 2px;
    text-transform: uppercase;
    color: #fff;
    font-size: 30px;
} 

.bottom-banner-img:hover .shop-now-btn {
  opacity: 1;
  background-color: #0083c1;
  border-color: #0083c1;
  padding: 8px 12px;
  text-transform: uppercase;
  font-weight: 600;
  letter-spacing: 1px;
}
.bottom-banner-img:hover h3 {
  margin-top: 60px;
  color: #fff;
}
          </style>
          <!--category div -->
         <div class="bottom-banner-section">   
    <div class="container">
      
      <div class="row">
        <div class="col-md-9">
    
    <?php
    error_reporting(0);
    $categorydata=$this->category_model->selectCategoryForHome();
    foreach($allcategory as $data)
    {
      $id=$data->id;
      $categoryimage=$this->category_model->selectAllCategoryImages($id);
      //print_r($categoryimage);
?>
<?php $sws_cat=$this->product_model->ProductCategory($data->id);  ?>
      <div class="col-sm-6"> <a href="<?php echo base_url(slugurl($sws_cat->ptitle).'/'.slugurl($data->title)).'/'. encodeurlval($data->id);?>" class="bottom-banner-img">
    <img src="<?php echo base_url('uploads/category');?>/<?php echo $categoryimage[0]->image;?>" alt="bottom banner"> <span class="banner-overly"></span>
          <div class="bottom-img-info">
            <h3><?php echo $data->title;?></h3>
           
            <span class="shop-now-btn">View more</span> </div>
          </a> 
      </div>
      <?php
    }
    ?>
      
  
                  
  
        
        
      </div>
      </div>
    </div></div>  <!--category div end -->
		  <span id="filterresponse" >
          <div class="product-grid-area" id="griddata">
            <ul class="products-grid">
			<?php //print_r($productdata); ?>
			  <?php if(count($productdata)>0){ ?>
			  <?php foreach($productdata as $product){ ?>
              <li class="item col-lg-4 col-md-4 col-sm-6 col-xs-12 wow fadeInUp">
                <div class="product-item">
                  <div class="item-inner">
                    <div class="product-thumbnail">
                      <!--<div class="icon-sale-label sale-left">Sale</div>
                      <div class="icon-new-label new-right">New</div>-->
                      <div class="pr-img-area">
						<!--<a  href="<?php echo base_url('product/'.slugurl($product->name)).'/'.encodeurlval($product->id); ?>">	-->
<?php $sws_cat=$this->product_model->ProductCategory($product->category_id);  ?>
                           <a  href="<?php echo base_url(slugurl($sws_cat->ptitle).'/'.slugurl($sws_cat->title).'/'.slugurl($product->name).'/'.encodeurlval($product->id)); ?>"target="_blank">

						 <figure>
							<?php $product_image = $this->product_model->selectProductDoubleImage($product->id); ?>
							<?php if(count($product_image)>0){ ?>
					<img class="first-img" src="<?php echo base_url('uploads/product/thumbs'); ?>/<?php echo $product_image[0]->image; ?>" alt="<?php echo $product->name; ?>"target="_blank">      <?php  }?> 
							 <?php if(count($product_image)>1){ ?>
							<img class="hover-img" src="<?php echo base_url('uploads/product/thumbs'); ?>/<?php echo $product_image[1]->image; ?>" alt="<?php echo $product->name; ?>"target="_blank">
							<?php }else{ ?>
							<img class="first-img" src="<?php echo base_url('assets/front');?>/images/264x264.jpg" alt="<?php echo $product->name; ?>"target="_blank"> 
							<img class="hover-img" src="<?php echo base_url('assets/front');?>/images/264x264.jpg" alt="<?php echo $product->name; ?>"target="_blank">
							<?php } ?>
						 </figure>
						 </a>
						 <form method="post" action="<?php echo base_url('index.php/cart/addtocart'); ?>">
						   <input type="hidden" name="pid" value="<?php echo $product->id; ?>" >
						   <input type="hidden" name="qty" value="1" >
<!--<button type="button" name="addtocartbutton1" onclick="return addtocart(<?php echo $product->id; ?>);" class="add-to-cart-mt"> <i class="fa fa-shopping-cart"></i><span> Add to Cart</span> </button>-->
                                 <?php $loginuserid=$this->user_model->getLoginUserVar('USER_ID');?>
								<a class="add-to-cart-mt" style="font-size:11px" href="<?php echo base_url('index.php/user/addtowishlist/'.$loginuserid.'/'.$product->id);?>"><i class="fa fa-heart"></i><span>Add to Wishlist</span></a>
						   <!--<button type="submit" name="addtocartbutton" class="add-to-cart-mt1"> <i class="fa fa-shopping-cart"></i><span> Buy Now</span> </button>-->
						   <a href="<?php echo base_url(slugurl($sws_cat->ptitle).'/'.slugurl($sws_cat->title).'/'.slugurl($product->name).'/'.encodeurlval($product->id)); ?>"target="_blank"><button type="button" name="addtocartbutton" class="add-to-cart-mt1"> <i class="fa fa-bolt"></i><span> Buy Now</span> </button></a>
                            
						</form>
					 </div>					  
                      
                    </div>
                    <div class="item-info">
                      <div class="info-inner">
                        <div class="item-title"> 
							<a title="<?php echo $product->name; ?>" href="<?php echo base_url(slugurl($sws_cat->ptitle).'/'.slugurl($sws_cat->title).'/'.slugurl($product->name).'/'.encodeurlval($product->id)); ?>"target="_blank">
								<?php echo $product->name; ?>
							</a>
						</div>
                        <div class="item-content">
                          <div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                          <div class="price-box">
							  <?php if($product->spacel_price!=""){ ?>
                              <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> <?php echo CURRENCY.$product->spacel_price; ?> </span> </p>
                              <p class="old-price"> <span class="price-label">Regular Price:</span> <span class="price"> <?php echo CURRENCY.$product->price; ?> </span> </p>
							  <?php }else{ ?>
							  <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> <?php echo CURRENCY.$product->price; ?> </span> </p>
							  <?php } ?>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </li>
              <?php } } ?>
            </ul>
          </div>
		     
		  
		  <div class="pagination-area wow fadeInUp">
            <ul>
				<?php echo $pagination; ?>
            </ul>
          </div>
		  
		  </span>	  
        </div>
      </div>
    </div>
  </div>
  <!-- Main Container End -->   			
<?php $this->load->view('front/layout/footer');?>
<script>
	function mytab(SHOWDIV,HIDEDIV)
	{
		//$("#productdisplay .active").removeClass("active");
		//alert($(this).html());
		
		$('#'+HIDEDIV).hide();
		$('#'+SHOWDIV).show();		
	}


function addtocart(pid)
{
	jQuery.ajax({
        	url: "<?php echo base_url('index.php/cart/addtocart_ajax'); ?>",
			type: "POST",
			data:   { product_id : pid},
			//beforeSend: function(){$(this).html('OKKKKKK')},
			success: function(data)
		    {
				jQuery('#cartresponsedata').html(data);
				jQuery(".mini-cart").attr("tabindex",-1).focus(500);
			},
		  	error: function() 
	    	{
				
	    	} 	        
	   });
}
</script>

<!DOCTYPE html>
<html lang="en">
<html itemscope>
<head>

<style>     

div.cities {
    background-color: #F8F5F5;
    color: black;
    margin: 20px 0 20px 0;
    padding: 20px;   
}  
</style>
</head>

<body>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.10';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div class="cities">


<script src="https://apis.google.com/js/platform.js"></script>

<script>
  function onYtEvent(payload) {
    if (payload.eventType == 'subscribe') {
      // Add code to handle subscribe event.
    } else if (payload.eventType == 'unsubscribe') {
      // Add code to handle unsubscribe event.
    }
    if (window.console) { // for debugging only
      window.console.log('YT event: ', payload);
    }
  }
</script>

<div class="g-plus" data-action="share" data-annotation="vertical-bubble" data-height="100" data-href="http://www.discountidea.com/mens-footwear/footwear/64"></div><script type="text/javascript">(function(){ var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true; po.src = 'https://apis.google.com/js/platform.js'; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s); })();</script><br>

<div class="fb-share-button" data-href="http://www.discountidea.com/mens-footwear/footwear/64" data-layout="button_count" data-size="large" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fwww.discountidea.com%2F&amp;src=sdkpreparse">Share</a></div>

<a href="https://twitter.com/share" class="twitter-share-button" data-via="@DiscountIdea" data-count="horizontal">Tweet</a><script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>

<div class="fb-follow" data-href="https://www.facebook.com/Discount-Idea-712660268934928/" data-width="100" data-height="100" data-layout="standard" data-size="large" data-show-faces="true"></div>

<div class="fb-send" data-href="http://www.discountidea.com/mens-footwear/footwear/64"></div>

<div class="fb-like" data-href="https://www.facebook.com/Discount-Idea-712660268934928/" data-width="100" data-layout="standard" data-action="like" data-size="large" data-show-faces="true" data-share="true"></div>

<div class="fb-like" data-href="https://www.facebook.com/Discount-Idea-712660268934928/" data-layout="standard" data-action="like" data-size="large" data-show-faces="true" data-share="true"></div>
