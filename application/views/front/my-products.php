<?php $this->load->view('front/layout/header');?>  <!-- end nav --> 
  
  <!-- Breadcrumbs -->
  
  <div class="breadcrumbs">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <ul>
            <li class="home"> <a title="Go to Home Page" href="<?php echo base_url(); ?>">Home</a><span>&raquo;</span></li>
            <li class="category13"><strong>My Products</strong></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <!-- Breadcrumbs End --> 
  
  <!-- Main Container -->
  <section class="main-container col1-layout">
    <div class="main container">
      <div class="account-login mb-30">
        	<div class="row">
            <div class="col-md-3">
              <div id="left-pnl" class="aside-site-left my_account_section   ">
                      
<div class="action_links">
     <?php $this->load->view('front/layout/my-account-left-sidebar'); ?>   
</div>


                <div class="sidebox-bottom"> <span> &nbsp; </span> </div>
                              </div>
            </div>
            <div class="col-md-9">
			<div class="aside-site-content my_account_section ">
				<a href="<?php echo base_url('index.php/vendor/order_report'); ?>"><button class="btn-warning orange" style="padding:5px;">Order Report</button></a>
				<a href="<?php echo base_url('index.php/vendor/cancellation_report'); ?>"><button class="btn-warning orange" style="padding:5px;">Cancellation Report</button></a>
			
				<a href="<?php echo base_url('index.php/vendor/my_sell_payout'); ?>"><button class="btn-warning orange" style="padding:5px;">My Sell Payout</button></a>
				<a href="<?php echo base_url('index.php/vendor/my_deducation'); ?>"><button class="btn-warning orange" style="padding:5px;">My Deducation</button></a>
				<a href="<?php echo base_url('index.php/vendor/final_payout'); ?>"><button class="btn-warning orange" style="padding:5px;">Final Payout</button></a>
			</div>
              <div class="aside-site-content my_account_section ">
                                            <div class="site-content ">
        
                 <div class="central-content"> 
        
        



<div class="cm-notification-container"></div>
            
                   
                          
    
    <div class="mainbox-container margin margin">
   <div class="mainbox-body">
   <div class="main_section" style="display: block;">
    <div class="my_order" style="display: block;">
	
	<div class="sub_action_links">
	    <h3>My Products
			<a href="<?php echo base_url('index.php/user/add_product'); ?>"><button class=" pull-right btn-warning orange">Add New Product</button></a>
		</h3>
		<?php echo $this->session->flashdata('message'); ?>	    	
		    <div class="my_order_list table-responsive" style="padding:3px;">
            <?php if(count($myproduct)>0){ ?>
			<table class="table table-bordered table-hover">
				<thead>
					<tr>
						<th>SNo.</th>
						<th>ID</th>
						<th>Name</th>
						<th>Brand</th>
						<th>Price</th>
						<th>Special<br>Price</th>
						<th>Available<br>Stock</th>
						<th>Sold<br>Out</th>
						<th>Upload<br>Date</th>
						<th>Status</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<?php $i=0; foreach($myproduct as $product){ $i++; ?>
					<?php $sold_out_products = $this->user_model->getSoldOutProducts($product->id); ?>
					<?php //$user = $this->user_model->selectuserby_id($order->user_id); ?>
					<tr>
						<td><?php echo $i; ?></td>
						<td><?php echo $product->id; ?></td>
						<td><?php echo $product->name; ?></td>
						<td><?php echo $product->product_brand; ?></td>
						<td>₹<?php echo $product->price; ?></td>
						<td>₹<?php echo $product->spacel_price; ?></td>
						<td><?php echo $product->product_stock; ?></td>
						<td><?php echo $sold_out_products; ?></td>
						<td><?php echo $product->create_date; ?></td>
						<td><?php echo ($product->status==1)?'Active':'Inactive'; ?></td>
						
						<?php $cat=$this->product_model->ProductCategory($product->category_id); ?>
						<td>
						<a href="<?php echo base_url('index.php/user/manageimage/'.$product->id.'');?>" title="Upload Images"><i class="fa fa-picture-o" aria-hidden="true"></i> Upload Images</a><br>
						<a href="<?php echo base_url('user/edit_product/'.$product->id); ?>" title="Edit Product"><i class="fa fa-edit"></i> Edit</a><br>
						<?php //echo base_url('product/'.slugurl($product->name)).'/'.encodeurlval($product->id); ?>
						<a href="<?php echo base_url(slugurl($cat->ptitle).'/'.slugurl($cat->title).'/'.slugurl($product->name).'/'.encodeurlval($product->id)); ?>" target="_blank" title="View Product"><i class="fa fa-eye"></i> View</a><br>
						<a href="<?php echo site_url('user/copyproduct/'.$product->id.''); ?>" title="Copy Product"><i class="fa fa-copy" aria-hidden="true"></i> Copy</a><br>
						<a href="<?php echo base_url('user/add_pincode/'.$product->id); ?>" title="Add  Pincode" ><i class="fa fa-file-zip-o"></i> Pincode</a> 
						</td>
					</tr>
					<?php } ?>
				</tbody>
			</table> 		
			<?php } ?> 		 		    								    				
	    </div>
		</div>
		<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Request to Cancel this Order</h4>
        </div>
        <div class="modal-body">
          <div class="my_order">
		<div style="display: block;" class="overlay">
		    <div style="display: block;" class="o_container">
			
			<div class="cont">
			    <div class="order_cancel_req">
				<div class="my_order_list">
				    					<div class="item">
					    <h4><span class="order_id">Order ID:<span>111294586</span></span>Order Placed on Fri, 03-Mar-2017 </h4>
					    <div class="row">
						<div class="p_info">
						    <div class="p-image-sec">
							<div class="p_image">
							    <img style="width: 90px; height: 90px;" src="http://cdn.shopclues.com/images/thumbnails/59354/160/160/Axe14885242861488525726.jpg">
							</div>	
						    </div>
						    <div class="p_group">
							<div class="p_details">							
							    <h3>Pack of Two Axe Deo 150 gm</h3>
							    <p>Total Amount: Rs 223.00</p>							
							    <p class="status">Status: <span class="bold500">Cash on Delivery Confirmed</span></p>
							</div>
						    </div>
						</div>
					    </div>
					</div>
				    				    <div class="information">
					<div class="info_box">
					    <div class="default_form">
						<h3>Select Reason for cancellation</h3>
						<form method="POST" action="index.php" name="frmProfile">
						    <input type="hidden" name="token" value="f6d2754eee48a6211a0f912b0ab73279">
						    <input type="hidden" id="cancel_order_id" name="orderid" value="111294586">
						    <input type="hidden" id="current_status" name="cur_status" value="Q">
						    <input type="hidden" name="dispatch" value="orders.cancel_order">
						    <fieldset>
							<div class="s_row">
							    <select id="reasons" name="reasons">								
								<option value="">Select</option>
																    <option value="6">Address/ Phone no./ Quantity is Incorrect</option>
																    <option value="4">Found Lower Price Elsewhere</option>
																    <option value="2">Delivery Time is Long</option>
																    <option value="7">Unsure about Product Quality</option>
																    <option value="8">Shipping Cost/ COD Fee is Too High</option>
																    <option value="5">Wrong Product Ordered</option>
																    <option value="1">Not interested in item(s) any more</option>
																    <option value="3">Other</option>
															    </select>      
							</div>
							<div class="s_row">
							    <input type="text" value="" name="comment" class="label_jump">
							    <span></span>
							    <label>Write your comments</label>							    
							</div>
						    </fieldset>
						</form>
					    </div>
					</div>
				    </div>
				</div>
			    </div>	
			</div>
			<div class="btn_container"> 
			    <div class="btn_size">
				<input type="button" class="btn orange" value="Cancel Order" onclick="cancel_order_function();" style="cursor: pointer;">
			    </div>
			</div>
		    </div>		
		</div>
	    </div>        </div>
        
      </div>
      
    </div>
  </div>
    </div>
</div>

        
</div>
    </div>
           
                 </div> 
                   </div>
        
                    </div>
            </div>
          </div>
      </div>
     
    </div>
  </section>
  <!-- Main Container End --> 
  <!-- our clients Slider -->
  <?php $this->load->view('front/layout/footer');?>