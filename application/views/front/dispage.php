<?php $this->load->view('front/layout/header');?>


  <div class="breadcrumbs">

    <div class="container">

      <div class="row">

        <div class="col-xs-12">

          <ul>

            <li class="home"> <a title="Go to Home Page" href="index.html">Home</a><span>&raquo;</span></li>

            <li class="category13"><strong><?php echo $page->title?></strong></li>

          </ul>

        </div>

      </div>

    </div>

  </div>

  <!-- Breadcrumbs End --> 



  <!-- Main Container -->

  

  <div class="main container">

    <div class="row">

      <div class="">

        <div class="col-xs-12">

          <div class="page-title">	  

            <h2><?=$page->title?></h2>

          </div>
          

          <?php echo $page->desc;?>

        </div>
<style>
  .youtubevideo{
    margin-top: 30px;
        margin-bottom: 30px;
    position: relative;
  }
.hoverplaybtn{
     position: absolute;
    width: 82%;
    top: 0;
    left: 15px;
    background: #00000038;
    height: 81%;
    text-align: center;
    vertical-align: middle;
    display:  none; 
}
.youtubevideo div:hover .hoverplaybtn{
   display:  block; 
  }
.hoverplaybtn img {
  width: 25%;
    vertical-align: middle;
    margin-top: 55px;
}
.headp{
      font-size: 16px;
    font-weight: 600;
    margin-top: 10px;
    line-height: 20px;
}


</style>
       <div class="row">
        <div class="col-md-12 youtubevideo">
          
         <?php
         foreach($youtubevideos->items as $video){

          if(isset($video->id->videoId)){

          ?>
         <div class="col-md-4" id="<?php echo $video->id->videoId; ?>" onclick="playvideo('<?php echo $video->id->videoId; ?>')">
           <img src="<?php echo $video->snippet->thumbnails->medium->url; ?>" alt="<?php echo $video->snippet->title; ?>" >
           <p class="headp" title="<?php echo $video->snippet->title; ?>"><?php echo substr($video->snippet->title,0,45); ?>...</p>
           <div class="hoverplaybtn">
           <img  src="http://www.pngmart.com/files/3/YouTube-Play-Button-PNG-Free-Download.png" alt="Play">
         </div>
         </div>

       <?php  } }
         ?>
       </div>
       </div>

      </div>

    </div>

  </div>

  <div id="youtubepopdp" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog" style="width: 860px;">

    <!-- Modal content-->
    <div class="modal-content">  
     <div class="modal-header">
        <button type="button" class="close" onclick="stoprvideo()" data-dismiss="modal">&times;</button>
      </div>   
      <div class="modal-body">
        <div id="videocontentplay"></div>
      </div>      
    </div>

  </div>
</div>

  <script>
   function playvideo(vid){
    var ifram= '<iframe width="100%" height="560" src="https://www.youtube.com/embed/'+vid+'?autoplay=1" frameborder="0" allowfullscreen ></iframe>';
$("#videocontentplay").html(ifram);
$("#youtubepopdp").modal("show");
   }

   function stoprvideo(){
    $("#videocontentplay").html('');
   }
  </script>
          


    

  <!-- Section: services -->

  <?php $this->load->view('front/layout/footer');?>