﻿<?php $this->load->view('front/layout/header');?>  <!-- end nav --> 
  
  <!-- Breadcrumbs -->
  
  <div class="breadcrumbs">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <ul>
            <li class="home"> <a title="Go to Home Page" href="index.html">Home</a><span>&raquo;</span></li>
            <li class="category13"><strong>My Orders</strong></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <!-- Breadcrumbs End --> 
  
  <!-- Main Container -->
  <section class="main-container col1-layout">
    <div class="main container">
      <div class="account-wishlist mb-30">
        	<div class="row">
            <div class="col-md-3">
              <div id="left-pnl" class="aside-site-left my_account_section   ">
                      
                
    
<div class="action_links">
     <?php $this->load->view('front/layout/my-account-left-sidebar'); ?>   
</div>



                <div class="sidebox-bottom"> <span> &nbsp; </span> </div>
                              </div>
            </div>
            <div class="col-md-8">
              <div class="aside-site-content my_account_section">
                                            <div class="site-content    ">
        
                 <div class="central-content"> 
        
        



<div class="cm-notification-container"></div>
            
                        <!-- New design part mainbox_general start  -->
                          
    
    <div class="mainbox-container margin margin">
                                                                <div class="mainbox-body"><style type="text/css">
	.fixed  	.absolute</style>

    <div class="main_section">
        <div class="my_wishlist">
            <h3>My Wishlist</h3>
                        <div class="product_list_row">
                         
					<?php
					foreach($WDATA as $data)
					{
						$qry=$data->pro_id;
						$res=$this->user_model->productwislist($qry);
						$image=$this->user_model->productwilimage($qry);
					?>
                  <div class="item">
                    <div class="p_info">
                            <div class="p-image-sec">
                                    <div class="p_image">
										<?php if(count($image)>0){ ?>
                                        <a href="<?php echo base_url('product/'.slugurl($res[0]->name)).'/'.encodeurlval($data->pro_id); ?>"><img src="<?php echo base_url('uploads/product/'.$image[0]->image);?>"></a>
										<?php } ?>
                                    </div>	
                            </div>
                        <div class="p_group">
                            <div class="p_details">
                                <h3> <?php echo $res[0]->name;?></h3>
                                                               <p>
                                                                    <span class="ori_price" id="old0"><?php echo CURRENCY.$res[0]->price;?></span>                                                                     
                                </p>
                                                               <p>
																 <?php
																		$price=$res[0]->price;
																		$desount=$res[0]->descount;
													$descountprice=($price*$desount)/100;
													$total=$price - $descountprice;
																?>
                                                                            <span class="price"><?php echo $total;?></span>
                                                                    <?php /*<span class="disc_off"><?php echo $res[0]->descount;?>%Off</span>*/ ?>
                                                                    </p>
                            </div>
                            <div class="sort_links">
                                <p><a class="btn orange-white" href="<?php echo base_url('index.php/user/wishlistdelete');?>/<?php echo $res[0]->id;?>">Remove</a></p>                                                                  
                            </div>
                        </div>
                    </div>
                </div>
				
				<?php
					}
					?>
                                                       
                            </div>
                    </div>
    </div>

	




</div>
    </div>            
            
                
                        <div class="clear-both"></div>
                 </div> 
                   </div>
        
                    </div>
            </div>
          </div>
      </div>
     
    </div>
  </section>
  <!-- Main Container End --> 
  <!-- our clients Slider -->
  <?php $this->load->view('front/layout/footer');?>