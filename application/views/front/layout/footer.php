<!-- Footer -->
<?php
$block1 = $this->block_model->selectblockbyid(1);
$block2 = $this->block_model->selectblockbyid(2);
$block3 = $this->block_model->selectblockbyid(3);
$block4 = $this->block_model->selectblockbyid(4);
$block5 = $this->block_model->selectblockbyid(5);
$block6 = $this->block_model->selectblockbyid(6);
$block7 = $this->block_model->selectblockbyid(7);
 ?> 
  <footer>
    <div class="container">
      <div class="row">
        <div class="col-sm-12 col-xs-12 col-lg-4">
          <div class="footer-logo"><a href="http://www.discountidea.com"><img src="<?php echo base_url('assets/front'); ?>/images/Short Logo Footer.jpg" alt="Discount Idea"></a></div>
          <p style="text-align:center"><?php echo geturl('{BASE_URL}',$block1[0]->description); ?></p>
          
        </div>
        <div class="col-sm-6 col-md-3 col-xs-12 col-lg-2 collapsed-block">
          <div class="footer-links">
            <?php echo geturl('{BASE_URL}',$block2[0]->description); ?>
          </div>
        </div>
        <div class="col-sm-6 col-md-3 col-xs-12 col-lg-2 collapsed-block">
          <div class="footer-links">
            <?php echo geturl('{BASE_URL}',$block3[0]->description); ?>
          </div>
        </div>
        <div class="col-sm-6 col-md-3 col-xs-12 col-lg-2 collapsed-block">
          <div class="footer-links">
            <?php echo geturl('{BASE_URL}',$block4[0]->description); ?>
          </div>
        </div>
        <div class="col-sm-6 col-md-3 col-xs-12 col-lg-2 collapsed-block">
          <div class="footer-links">
           <?php echo geturl('{BASE_URL}',$block5[0]->description); ?>


          </div>
        </div>
      </div>
    </div>
    <div class="footer-newsletter" id="emailmy">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
		  <?php echo $this->session->flashdata('newslattermessage'); ?>
            <h3>Subscribe for Our Newsletter!</h3>
            <p>Enjoy our newsletter to stay updated with the latest news and special sales.</p>
            <p>Let's your email address here!</p>
            <form id="newsletter-validate-detail" method="post" action="<?php echo base_url('index.php/home/submitnewslatter'); ?>">
              <div class="newsletter-inner">
                <input class="newsletter-email" name='email' placeholder='Enter Your Email'/>
                <button class="button subscribe" type="submit" name="subscribenow" title="Subscribe">Subscribe</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <div class="footer-coppyright">
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-xs-12 coppyright"> <?php echo geturl('{BASE_URL}',$block6[0]->description); ?></div>
        </div>
      </div>
    </div>
  </footer>
  <a href="#" class="totop"> </a> </div>

<!-- End Footer --> 
<!-- JS --> 

<!-- jquery js --> 

<!-- bootstrap js --> 
<!--<script type="text/javascript" src="<?php echo base_url('assets/front'); ?>/js/bootstrap.min.js"></script> -->

<!-- owl.carousel.min js --> 
<script type="text/javascript" src="<?php echo base_url('assets/front'); ?>/js/owl.carousel.min.js"></script> 

<!-- bxslider js --> 
<script type="text/javascript" src="<?php echo base_url('assets/front'); ?>/js/jquery.bxslider.js"></script> 

<!--jquery-slider js --> 
<script type="text/javascript" src="<?php echo base_url('assets/front'); ?>/js/slider.js"></script> 

<!-- megamenu js --> 
<script type="text/javascript" src="<?php echo base_url('assets/front'); ?>/js/megamenu.js"></script> 
<script type="text/javascript">
        /* <![CDATA[ */   
        var mega_menu = '0';
        
        /* ]]> */
        </script> 

<!-- jquery.mobile-menu js --> 
<script type="text/javascript" src="<?php echo base_url('assets/front'); ?>/js/mobile-menu.js"></script> 

<!-- jquery.waypoints js --> <script type="text/javascript" src="<?php echo base_url('assets/front'); ?>/js/waypoints.js"></script>  

<!--jquery-ui.min js --> 
<script src="<?php echo base_url('assets/front'); ?>/js/jquery-ui.js"></script> 

<!-- main js --> 
<script type="text/javascript" src="<?php echo base_url('assets/front'); ?>/js/main.js"></script>

 <script type="text/javascript">
                  function searchAdvice()
                  {
                  var q=$("input[name=q]").val();

                  console.log("test "+q);
                  }
                </script>
</body>

</html>