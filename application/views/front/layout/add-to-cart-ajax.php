<div data-toggle="dropdown" data-hover="dropdown" class="basket dropdown-toggle"> <a href="#"><i class="fa fa-shopping-cart"></i><span class="cart-title">Shopping Cart (<?php echo count($this->cart->contents()); ?>)</span></a></div>
                <?php if(count($this->cart->contents())>0){ ?>
				<div>
                  <div class="top-cart-content">
                    <div class="block-subtitle">Recently added item(s)</div>
                    <ul id="cart-sidebar" class="mini-products-list">
					  <?php foreach($this->cart->contents() as $items){ ?>
					  <?php $proimages = $this->product_model->selectProductSingleImage($items['id']); ?>
					  <?php $product = $this->product_model->selectProductById($items['id']);  ?>
                      <li class="item odd"> 					    
						<?php if(count($proimages)>0){ ?>
							<a  href="<?php echo base_url('product/'.slugurl($product[0]->name)).'/'.encodeurlval($product[0]->id); ?>" class="product-image"><img src="<?php echo base_url('uploads/product/'.$proimages[0]->image); ?>" ></a>
					    <?php }else{ ?>
							<a  href="<?php echo base_url('product/'.slugurl($product[0]->name)).'/'.encodeurlval($product[0]->id); ?>" class="product-image"><img  src="http://placehold.it/100x100?text=Image not found"></a>
					    <?php } ?>
                        <div class="product-details"> <a href="<?php echo base_url('cart/remove/'.$items['rowid']); ?>" title="Remove This Item" class="remove-cart"><i class="icon-close"></i></a>
                          <p class="product-name"><a href="<?php echo base_url('product/'.slugurl($product[0]->name)).'/'.encodeurlval($product[0]->id); ?>"><?php echo $product[0]->name; ?></a> </p>
                          <strong><?php echo $items['qty']; ?></strong> x <span class="price"><?php echo CURRENCY.$items['price']; ?></span> </div>
                      </li>
                      <?php } ?>                      
                    </ul>
                    <div class="top-subtotal">Subtotal: <span class="price"><?php echo CURRENCY.$this->cart->total(); ?></span></div>
                    <div class="actions">
                      <a href="<?php echo base_url('index.php/cart/checkout');?>"><button class="btn-checkout" type="button"><i class="fa fa-check"></i><span>Checkout</span></button></a>
                      <a href="<?php echo base_url('index.php/cart/'); ?>"><button class="view-cart" type="button"><i class="fa fa-shopping-cart"></i> <span>View Cart</span></button></a>
                    </div>
                  </div>
                </div>
				<?php } ?>