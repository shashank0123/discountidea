<!DOCTYPE html>
<html lang="en">
<html itemscope>
<head>
     <?php
     $CI =& get_instance();

     $CI->load->model('Publicmodel');
     $url =base_url(uri_string());
     $data = $CI->Publicmodel->selectallmeta($url);
     if(count($data)>0){
     echo $data->metatag;
   }
     ?>               
</head>


<!DOCTYPE html>
<html lang="en">

<!-- Mobile specific metas  -->
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Favicon  -->
<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">

<!-- Google Fonts -->
<!--<link href='https://fonts.googleapis.com/css?family=Karla:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,200italic,300,300italic,400,400italic,600,600italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Dancing+Script:400,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Source+Code+Pro:400,500,600,700,300' rel='stylesheet' type='text/css'>-->
<link href="https://fonts.googleapis.com/css?family=Noto+Sans" rel="stylesheet" sans-serif;>

<!-- CSS Style -->
<!-- jquery js --> 
<script type="text/javascript" src="<?php echo base_url('assets/front');?>/js/jquery.min.js"></script> 

<!-- bootstrap js --> 
<script type="text/javascript" src="<?php echo base_url('assets/front');?>/js/bootstrap.min.js"></script> 
<!-- Bootstrap CSS -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/front');?>/css/bootstrap.min.css">

<!-- font-awesome & simple line icons CSS -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/front');?>/css/font-awesome.css" media="all">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/front');?>/css/simple-line-icons.css" media="all">

<!-- owl.carousel CSS -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/front');?>/css/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/front');?>/css/owl.theme.css">

<!-- animate CSS  -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/front');?>/css/animate.css" media="all">

<!-- flexslider CSS -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/front');?>/css/flexslider.css" >

<!-- Nivo Slider CSS -->
<link href="<?php echo base_url('assets/front');?>/css/nivo-slider.css" rel="stylesheet">

<!-- style CSS -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/front');?>/css/style.css" media="all">

<!-- shortcodes -->
<link rel="stylesheet" media="screen" href="<?php echo base_url('assets/front');?>/css/shortcodes/shortcodes.css" type="text/css" />

<!-- accordion -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/front');?>/js/accordion/accordion.css" />

<!-- tooltips -->
<link rel="stylesheet" href="<?php echo base_url('assets/front');?>/js/tooltips/tooltip.css" />

<!-- popup newsletter css -->
<link rel="stylesheet" href="<?php echo base_url('assets/front');?>/css/popup-newsletter.css">

<!-- Home3 CSS -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/front');?>/css/home3.css" media="all">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/front/css/my_account.css" media="all">

<style>
.customlink a{    font-size: 14px;
    color: #FDFEFE;
    margin-right: 6px;
	margin-left:2px !important;
	font-size:13px;
	}
	
	  
</style>

<style>
  
  #loading {
   width: 100%;
   height: 100%;
   top: 0;
   left: 0;
   position: fixed;
   display: block;
   background-color: #FDFEFE;
   z-index: 99999999;
   text-align: center;
}

#loading-image {
    position: absolute;
    padding: 0% 25%;
    left: 0;
    top: 40%;
    z-index: 100;
}
.ma-nivoslider
{
  margin-left: -5% !important;
}
</style>

<!-- <script>
window.onload = function(e) {
   jQuery(".loaderpage").fadeOut("slow");
   document.getElementById('mypopupaa').click();
};
</script> -->

<script language="javascript" type="text/javascript">
     $(window).load(function() {
     $('#loading').hide();
  });
</script>


<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script>
$(function() {
    $( "#skills" ).autocomplete({
        source: "<?php echo base_url('index.php/home/getSearch'); ?>"
    });

    $.getSearch = function()
    {
          var qtext = $("skill").val();

          $.ajax({
          type: "POST",
          url: "<?php echo base_url('index.php/home/searchSuggestion'); ?>",
          data: {'qtext': qtext},
          cache: false,
          success: function(info) {//alert(info);return false;  
            return info;
          }  
        });  
     }
});

$(function() {
  var availableTags = [
      "ActionScript",
    "AngularJS",
      "AppleScript",
      "Asp",
      "BASIC",
      "C",
      "C++",
      "Clojure",
      "COBOL",
      "ColdFusion",
      "Erlang",
      "Fortran",
      "Groovy",
      "Haskell",
      "Java",
      "JavaScript",
      "Lisp",
    "MSSQL",
    "MySQL",
    "NodeJs",
    "Oracle",
      "Perl",
      "PHP",
      "Python",
      "Ruby",
      "Scala",
      "Scheme",
    "SQL"
    ];
    $( "#skills123" ).autocomplete({
      source: availableTags
    });
 });
</script>

</head>

<body class="cms-index-index cms-home-page home-3">


<!-- <div class="loaderpage"><img src="<?php echo base_url('assets/front');?>/images/loading-logo.png"></div> -->

<div id="loading">
  <img id="loading-image" src="<?php echo base_url('assets/front');?>/images/loading-logo.png" alt="Discount Idea Loading..." />
</div>


<!-- mobile menu -->
<?php $this->load->view('front/layout/mobile-menu'); ?>
<!-- end mobile menu -->
<div id="page"> 
  <!-- newsletter popup -->
  
  <div class="newsletter-popup" style="display:none;">
    <div class="newsletter-bg newsletter-ready"></div>
    <div class="newsletter-wrap newsletter-close-btn-in">
      <div class="newsletter-container">
        <div class="newsletter-content">
          <div class="news-inner">
            <div class="news-popup">
              <div class="popup-title">
                <h2>Newsletter <span class="text-main">Popup</span></h2>
                <div class="divider divider-icon divider-md">&#x268A;&#x268A; &#x2756; &#x268A;&#x268A;</div>
                <p class="notice">Sign up to our email newsletter to be the first to hear about great offers &amp; more</p>
              </div>
              <form class="form-subscribe">
                <input placeholder="Sign up your email..." type="text">
                <button class="button"><i class="fa fa-envelope"></i>SUBSCRIBE</button>
              </form>
              <div class="checkbox">
                <label>
                  <input value="" type="checkbox">
                  DON’T SHOW THIS POPUP AGAIN</label>
              </div>
            </div>
            <button title="Close (Esc)" type="button" class="newsletter-close">×</button>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  <!-- end newsletter popup --> 
  <!-- Header -->
  <header>
    <div class="header-container" >
      <!-- <div class="container-fluid"> -->
        <div>
        <div class="row">
          <div class="col-sm-4 col-md-3 col-xs-12"> 
            <!-- Header Logo -->
            <div class="logo" style="margin:0"><a title="Logo" href="<?php echo base_url(); ?>"><img alt="Logo" src="<?php echo base_url('assets/front');?>/images/Logo for Facebook 2.jpg"></a> </div>
            <!-- End Header Logo --> 
			<div class="links my-blog my-show" >
				<ul class="list-inline">
		<li><a title="Blog" href="http://www.discountidea.com/readers/public-event"><span class="">Our Public Events</span></a></li>
			  <?php
			  $user_id=$this->user_model->getLoginUserVar('USER_ID');
			  if(empty($user_id)) {
			  ?> 
              <li><a title="My Account" href="<?php echo base_url('vendor/loginuser'); ?>"><span class="">Sell your products</span></a></li>
			  
			  
			  
              <?php }?>
			   <?php  if($this->session->userdata('user_type')=="0" && !empty($user_id)) { ?>
			  <li>			  <a title="earn your discount" href="<?php echo base_url('earnyourdiscount'); ?>" ><span class="">Earn Your Discount</span></a></li>
			  
			  <?php } ?>
			  
          </div>
		  </div>
          <div class="col-xs-8 col-sm-4 col-md-6 hidden-xs">
            <div class="support-client">
              <div class="headerlinkmenu" style="text-align:left;">
            <div class="links   ">
			   <div class="blog hidden-xs"><a title="Blog" href="http://www.discountidea.com/readers/public-event"><span class="hidden-xs">Our Public Events</span></a></div>
			  <?php
			  $user_id=$this->user_model->getLoginUserVar('USER_ID');
			  if(empty($user_id)) {
			  ?> 
              <div class="myaccount "><a title="My Account" href="<?php echo base_url('vendor/loginuser'); ?>"><span class="hidden-xs">Sell your products</span></a></div>
			  
              <?php }?>
			   <?php  if($this->session->userdata('user_type')=="0" && !empty($user_id)) { ?>
			  <div class="myaccount">			  <a title="earn your discount" href="<?php echo base_url('earnyourdiscount'); ?>" ><span class="hidden-xs">Earn Your Discount</span></a></div>
			  <?php } ?>
			 
              <style>
             
              #product-list{float:left;list-style:none;margin-top:-3px;padding:0;position: absolute;}
              #product-list li{padding: 10px; background: #f0f0f0; border-bottom: #bbb9b9 1px solid;}
              #product-list li:hover{background:#ece3d2;cursor: pointer;}
              #product-box{padding: 10px;border: #a8d4b1 1px solid;border-radius:4px;}
              </style>
          
              <form method="get" action="<?php echo base_url('home/searchproduct');?>">
                 
                <div class="search-box">
                  <!--<input name="q" id="search-box" placeholder="Search" class="form-control" type="text" onkeyup="searchAdvice()"  required>-->
                  <input name="q" id="skills" placeholder="Search" class="form-control" type="text">
                  
                  <button type="submit" class="btn btn-warning search-bth" ><i class="fa fa-search"></i></button>
                  <div id="suggesstion-box" ></div>
                </div>
              
              </form>
               

            </div>
            
          </div>
            </div>
          </div>
          <!-- top cart -->
		  
		   <div class="col-lg-3 col-xs-12 my-hide" >
		    <div class="top-cart-contain">
              <div class="mini-cart" id="cartresponsedata">
                <div data-toggle="dropdown" data-hover="dropdown" class="basket dropdown-toggle"> <a href="#"><i class="fa fa-shopping-cart"></i><span class="cart-title">Shopping Cart (<?php echo count($this->cart->contents()); ?>)</span></a></div>
                <?php if(count($this->cart->contents())>0){ ?>
				<div>
                  <div class="top-cart-content">
                    <div class="block-subtitle">Recently added item(s)</div>
                    <ul id="cart-sidebar" class="mini-products-list">
					  <?php foreach($this->cart->contents() as $items){ ?>
					  <?php $proimages = $this->product_model->selectProductSingleImage($items['id']); ?>
					  <?php $product = $this->product_model->selectProductById($items['id']);  ?>
                      <li class="item odd"> 					    
						<?php if(count($proimages)>0){ ?>
							<a  href="<?php echo base_url('product/'.slugurl($product[0]->name)).'/'.encodeurlval($product[0]->id); ?>" class="product-image"><img src="<?php echo base_url('uploads/product/'.$proimages[0]->image); ?>" ></a>
					    <?php }else{ ?>
							<a  href="<?php echo base_url('product/'.slugurl($product[0]->name)).'/'.encodeurlval($product[0]->id); ?>" class="product-image"><img  src="http://placehold.it/100x100?text=Image not found"></a>
					    <?php } ?>
                        <div class="product-details"> <a href="<?php echo base_url('cart/remove/'.$items['rowid']); ?>" title="Remove This Item" class="remove-cart"><i class="icon-close"></i></a>
                          <p class="product-name"><a href="<?php echo base_url('product/'.slugurl($product[0]->name)).'/'.encodeurlval($product[0]->id); ?>"><?php echo $product[0]->name; ?></a> </p>
                          <strong><?php echo $items['qty']; ?></strong> x <span class="price"><?php echo CURRENCY.$items['price']; ?></span> </div>
                      </li>
                      <?php } ?>                      
                    </ul>
                    <div class="top-subtotal">Subtotal: <span class="price"><?php echo CURRENCY.$this->cart->total(); ?></span></div>
                    <div class="actions">
                      <a href="<?php echo base_url('index.php/cart/checkout');?>"><button class="btn-checkout" type="button"><i class="fa fa-check"></i><span>Checkout</span></button></a>
                      <a href="<?php echo base_url('index.php/cart/'); ?>"><button class="view-cart" type="button"><i class="fa fa-shopping-cart"></i> <span>View Cart</span></button></a>
                    </div>
                  </div>
                </div>
				<?php } ?>
              </div>
            </div>
			</div>
          
          <div class="col-lg-3 col-xs-12 "  data-spy="affix" data-offset-top="197">
		  <div class="top-cart">
            <div class="mm-toggle-wrap left">
              <div class="mm-toggle"> <i class="fa fa-align-justify"></i><span class="mm-label">Menu</span> </div>
            </div>
			<div class="support-client left  my-show">
              <div class="headerlinkmenu" style="text-align:left;">
            <div class="links my-blog">
			   <div class="blog hidden-xs "><a title="Blog" href="#"><span class="">track your order</span></a></div>
			  <?php
			  $user_id=$this->user_model->getLoginUserVar('USER_ID');
			  if(empty($user_id)) {
			  ?> 
              <div class="myaccount hidden-xs "><a title="My Account" href="<?php echo base_url('vendor/loginuser'); ?>"><span class="">Sell your products</span></a></div>
			  
              <?php }?>
			   <?php  if($this->session->userdata('user_type')=="0" && !empty($user_id)) { ?>
			  <div class="myaccount hidden-xs">			  <a title="earn your discount" href="<?php echo base_url('earnyourdiscount'); ?>" ><span class="hidden-xs">Earn Your Discount</span></a></div>
			  <?php } ?>
			 
           
              <form method="get" action="<?php echo base_url('home/searchproduct');?>">
              <div class="search-box my-search">
					<input name="q" placeholder="Search" class="form-control" type="text" required>
                    <button type="submit" class="btn btn-warning search-bth" ><i class="fa fa-search"></i></button>
                  
              </div>
			  </form>
            </div>
            
          </div>
            </div>
            <div class="top-cart-contain my-show">
              <div class="mini-cart" id="cartresponsedata">
                <div data-toggle="dropdown" data-hover="dropdown" class="basket dropdown-toggle"> <a href="#"><i class="fa fa-shopping-cart"></i><span class="cart-title">Shopping Cart (<?php echo count($this->cart->contents()); ?>)</span></a></div>
                <?php if(count($this->cart->contents())>0){ ?>
				<div>
                  <div class="top-cart-content" >
                    <div class="block-subtitle">Recently added item(s)</div>
                    <ul id="cart-sidebar" class="mini-products-list">
					  <?php foreach($this->cart->contents() as $items){ ?>
					  <?php $proimages = $this->product_model->selectProductSingleImage($items['id']); ?>
					  <?php $product = $this->product_model->selectProductById($items['id']);  ?>
                      <li class="item odd"> 					    
						<?php if(count($proimages)>0){ ?>
							<a  href="<?php echo base_url('product/'.slugurl($product[0]->name)).'/'.encodeurlval($product[0]->id); ?>" class="product-image"><img src="<?php echo base_url('uploads/product/'.$proimages[0]->image); ?>" ></a>
					    <?php }else{ ?>
							<a  href="<?php echo base_url('product/'.slugurl($product[0]->name)).'/'.encodeurlval($product[0]->id); ?>" class="product-image"><img  src="http://placehold.it/100x100?text=Image not found"></a>
					    <?php } ?>
                        <div class="product-details"> <a href="<?php echo base_url('cart/remove/'.$items['rowid']); ?>" title="Remove This Item" class="remove-cart"><i class="icon-close"></i></a>
                          <p class="product-name"><a href="<?php echo base_url('product/'.slugurl($product[0]->name)).'/'.encodeurlval($product[0]->id); ?>"><?php echo $product[0]->name; ?></a> </p>
                          <strong><?php echo $items['qty']; ?></strong> x <span class="price"><?php echo CURRENCY.$items['price']; ?></span> </div>
                      </li>
                      <?php } ?>                      
                    </ul>
                    <div class="top-subtotal">Subtotal: <span class="price"><?php echo CURRENCY.$this->cart->total(); ?></span></div>
                    <div class="actions">
                      <a href="<?php echo base_url('index.php/cart/checkout');?>"><button class="btn-checkout" type="button"><i class="fa fa-check"></i><span>Checkout</span></button></a>
                      <a href="<?php echo base_url('index.php/cart/'); ?>" target="_blank"><button class="view-cart" type="button"><i class="fa fa-shopping-cart"></i> <span>View Cart</span></button></a>
                    </div>
                  </div>
                </div>
				<?php } ?>
              </div>
            </div>
          </div>
		  </div>
        </div>
      </div>
    </div>
  </header>
  <!-- end header --> 
  
  <!-- Navbar -->
  <nav style="margin-top:1px;display:block;">
  <!--   <div class="container-fluid"> -->
    <div>
      <div class="row">
        <div class="col-md-3 col-sm-3">
          <div class="mega-container visible-lg visible-md visible-sm">
            <div class="navleft-container">
              <div class="mega-menu-title">
                <h3>Category</h3>
              </div>
              <div class="mega-menu-category">
                <ul class="nav">
                  <!-- <li> <a href="#">Home</a></li> -->
                  <?php
                   $d=0;
                    foreach($this->category_model->selectAllParentCategory() as $category){ $d++; ?>
				  <?php if($d<10){ ?>
                  <li> 				  
					<a href="<?php echo base_url(slugurl($category->title)).'/'.encodeurlval($category->id); ?>"><?php echo $category->title; ?></a>
					<?php  echo $this->category_model->getNavigation($category->id);  ?>                    
                  </li>
				  <?php }} ?>
				  <?php $e=0; foreach($this->category_model->selectAllParentCategory() as $category){ $e++; ?>
				  <?php if($e>9){ ?>
<?php $sws_cat=$this->product_model->ProductCategory($category->id); ?>
					<li class="more-menu"><a href="<?php echo base_url(slugurl($sws_cat->ptitle).'/'.slugurl($category->title).'/'.encodeurlval($category->id)); ?>"><?php echo $category->title; ?></a>
					<?php  echo $this->category_model->getNavigation($category->id);  ?> </li>
				  <?php }} ?>	
					 <li class="view-more"><a href="#">More category</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-9 ">
          <div class="language-currency-wrapper col-sm-3 col-md-5 col-lg-6">
           
            <!--Default Welcome Message-->
            <div class="welcome-msg"> 
			    <div class="customlink">
				        <a href="http://www.publicfunda.com/index.php/user/signup" target="_blank">Public Funda</a>
				        <a href="http://www.publicfunda.com/index.php/user/todaytask" target="_blank">Request Discount</a>
					<!--<a href="http://www.discountidea.com/blog" target="_blank">BLOG</a>-->
                                        <a href="http://www.publicfunda.com/index.php/user/wallet" target="_blank">Redeem Points</a>

				</div>
			</div>
			
            <!-- End Default Welcome Message -->  
          </div>
          <!-- top links -->
          <div class="headerlinkmenu col-lg-6 col-md-7 col-sm-6">
            <div class="links my-blog">
			  <div class="myaccount"><a title="My Account" href="<?php echo base_url('user/profile');?>"><i class="fa fa-user"></i><span class="">My Account</span></a></div>
                <div class="wishlist"><a title="My Wishlist" href="<?php echo base_url('user/wishlist');?>"><i class="fa fa-heart"></i><span class="">Wishlist</span></a></div>
			  
			  <div class="blog hidden-xs"><a title="Blog" target="_blank" href="<?php echo base_url('blog');?>"><i class="fa fa-rss"></i><span class="hidden-xs">Blog</span></a></div>


			  <?php
			  $user_id=$this->user_model->getLoginUserVar('USER_ID');
			  if(!empty($user_id))
			  {
			  ?>                
				<div class="login"><a href="<?php echo base_url('user/logoutuser');?>"><i class="fa fa-unlock-alt"></i><span class="">Logout</span></a></div>
				<?php
			  }else
			  {
			  ?>
                
                <div class="login"><a href="<?php echo base_url('user/loginuser');?>"><i class="fa fa-unlock-alt"></i><span class="">Log In</span></a></div>
				<?php
			  }
			  ?>
              </div>
			
          </div>
        </div>
      </div>
    
  </nav>
  <!-- end nav -->