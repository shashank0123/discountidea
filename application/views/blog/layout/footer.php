<footer>Copyright Social Discount Idea Private Limited <span>2017</span></footer>

<script src="<?php echo base_url('assets/front/'); ?>js/jquery.js"></script>

<script src="<?php echo base_url('assets/front/'); ?>js/bootstrap.min.js"></script>

<script src="<?php echo base_url('assets/front/'); ?>js/sparkline/retina.js"></script>

<script src="<?php echo base_url('assets/front/'); ?>js/sparkline/custom-sparkline.js"></script>

<script src="<?php echo base_url('assets/front/'); ?>js/scrollup/jquery.scrollUp.js"></script>

<script src="<?php echo base_url('assets/front/'); ?>js/d3/d3.v3.min.js"></script>

<script src="<?php echo base_url('assets/front/'); ?>js/d3/d3.powergauge.js"></script>

<script src="<?php echo base_url('assets/front/'); ?>js/d3/gauge.js"></script>

<script src="<?php echo base_url('assets/front/'); ?>js/d3/gauge-custom.js"></script>

<script src="<?php echo base_url('assets/front/'); ?>js/c3/c3.min.js"></script>

<script src="<?php echo base_url('assets/front/'); ?>js/c3/c3.custom.js"></script>

<script src="<?php echo base_url('assets/front/'); ?>js/nvd3/nv.d3.js"></script>

<script src="<?php echo base_url('assets/front/'); ?>js/nvd3/nv.d3.custom.boxPlotChart.js"></script>

<script src="<?php echo base_url('assets/front/'); ?>js/nvd3/nv.d3.custom.stackedAreaChart.js"></script>

<script src="<?php echo base_url('assets/front/'); ?>js/horizontal-bar/horizBarChart.min.js"></script>

<script src="<?php echo base_url('assets/front/'); ?>js/horizontal-bar/horizBarCustom.js"></script>

<script src="<?php echo base_url('assets/front/'); ?>js/gaugemeter/gaugeMeter-2.0.0.min.js"></script>

<script src="<?php echo base_url('assets/front/'); ?>js/gaugemeter/gaugemeter.custom.js"></script>

<script src="<?php echo base_url('assets/front/'); ?>js/heatmap/cal-heatmap.min.js"></script>

<script src="<?php echo base_url('assets/front/'); ?>js/heatmap/cal-heatmap.custom.js"></script>

<script src="<?php echo base_url('assets/front/'); ?>js/odometer/odometer.min.js"></script>

<script src="<?php echo base_url('assets/front/'); ?>js/odometer/custom-odometer.js"></script>

<script src="<?php echo base_url('assets/front/'); ?>js/circliful/circliful.min.js"></script>

<script src="<?php echo base_url('assets/front/'); ?>js/circliful/circliful.custom.js"></script>

<script src="<?php echo base_url('assets/front/'); ?>js/peity/peity.min.js"></script>

<script src="<?php echo base_url('assets/front/'); ?>js/peity/custom-peity.js"></script>

<script src="<?php echo base_url('assets/front/'); ?>js/custom.js"></script>

</body>

</html>

