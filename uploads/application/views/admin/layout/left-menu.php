<div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li class="active">
                        <a href="<?php echo base_url('index.php/admin/dashboard'); ?>"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                    </li>	

					
			<li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#popup">
							<i class="fa fa-fw fa-arrows-v"></i>Home Page<i class="fa fa-fw fa-caret-down"></i>
						</a>
                        <ul id="popup" class="collapse">
                            <li>
                                <a href="<?php echo base_url('index.php/homepagepopup/listing'); ?>">
									<i class="fa fa-arrow-circle-right"></i> Manage Popup
								</a>
                            </li>
							<li>
								<a href="<?php echo base_url('index.php/banner/listing/'); ?>">
									<i class="fa fa-arrow-circle-right"></i> Manage Banner
								</a>
							</li>
							<li>
								<a href="<?php echo base_url('index.php/categorycontroller/home_category/'); ?>">
									<i class="fa fa-arrow-circle-right"></i> Home Category
								</a>
							</li>
                        </ul>
                    </li>

<li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#blog">
							<i class="fa fa-fw fa-arrows-v"></i>Manage Blog<i class="fa fa-fw fa-caret-down"></i>
						</a>
                        <ul id="blog" class="collapse">
                            <li>
                                <a href="<?php echo base_url('index.php/blogcategory/cat_listing'); ?>">
									<i class="fa fa-arrow-circle-right"></i> Manage Category
								</a>
                            </li>
							<li>
								<a href="<?php echo base_url('index.php/blog/listing/'); ?>">
									<i class="fa fa-arrow-circle-right"></i> Manage Blog
								</a>
							</li>
							<li>
								<a href="<?php echo base_url('index.php/admin/edit_blogbanner/'); ?>">
									<i class="fa fa-arrow-circle-right"></i> Blog Banner
								</a>
							</li>
                        </ul>
                    </li>					
					
							<li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#demo">
							<i class="fa fa-fw fa-arrows-v"></i>Manage Users<i class="fa fa-fw fa-caret-down"></i>
						</a>
                        <ul id="demo" class="collapse">
							<li>
                                <a href="<?php echo base_url('index.php/user/listing'); ?>">
								<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
								Normal Users</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('index.php/admin/userList'); ?>">
								<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
								Vender List</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('index.php/admin/adduser'); ?>">
								<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
								Add Vender</a>
                            </li>
							<li>
								<a href="<?php echo base_url('index.php/vendor/allstore'); ?>">
								<i class="fa fa-arrow-circle-right" aria-hidden="true"></i> Manage Vendor Store
								</a>                        
							</li>
                        </ul>
                    </li>   
										
					<li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#cat">
							<i class="fa fa-fw fa-arrows-v"></i>Manage Category<i class="fa fa-fw fa-caret-down"></i>
						</a>
                        <ul id="cat" class="collapse">
                            <li>
                                <a href="<?php echo base_url('index.php/categorycontroller/listing'); ?>">
								<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
								Category Listing</a>
                            </li>
                        </ul>
                    </li>
					
					<li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#product">
							<i class="fa fa-fw fa-arrows-v"></i>Manage Products<i class="fa fa-fw fa-caret-down"></i>
						</a>
                        <ul id="product" class="collapse">
                            <li>
                                <a href="<?php echo base_url('index.php/adminproducts/listing'); ?>">
								<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
								Product  Listing</a>
                            </li>
							 <li>
                                <a href="<?php echo base_url('index.php/csv/'); ?>">
								<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
								Upload Csv</a>
                            </li>
							<li>
                                <a href="<?php echo base_url('index.php/pincode/listing'); ?>">
								<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
								Pincode  Listing</a>
                            </li>
                        </ul>
                    </li>
					
					
	

					
						<li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#filters">
							<i class="fa fa-fw fa-arrows-v"></i>Manage Filters<i class="fa fa-fw fa-caret-down"></i>
						</a>
                        <ul id="filters" class="collapse">
                            <li>
                                <a href="<?php echo base_url('index.php/filters/listing'); ?>">
								<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
								Filters Master</a>
                            </li>
                        </ul>
                    </li>	
	

					
					<li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#coupon">
                            <i class="fa fa-fw fa-arrows-v"></i>Manage Coupon Code<i class="fa fa-fw fa-caret-down"></i>
                        </a>
                        <ul id="coupon" class="collapse">
                            <li>
                                <a href="<?php echo base_url('index.php/offer/listing'); ?>">
                                    <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
                                    Manage Coupon Listing
                                </a>
                            </li>
                            
                        </ul>
                    </li>
					
					<li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#faq">
							<i class="fa fa-fw fa-arrows-v"></i>Manage Faq<i class="fa fa-fw fa-caret-down"></i>
						</a>
                        <ul id="faq" class="collapse">
                            <li>
                                <a href="<?php echo base_url('index.php/faq/listing'); ?>">
									<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
									FAQ Listing
								</a>
                            </li>							
                        </ul>
                    </li>
					
					<li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#cms">
							<i class="fa fa-fw fa-arrows-v"></i>Manage Cms<i class="fa fa-fw fa-caret-down"></i>
						</a>
                        <ul id="cms" class="collapse">
                            <li>
                                <a href="<?php echo base_url('index.php/cms/listing'); ?>">
									<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
									Page Listing
								</a>
                            </li>
							
                        </ul>
                    </li>
					
					<li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#newsletter">
							<i class="fa fa-fw fa-arrows-v"></i>News Letter<i class="fa fa-fw fa-caret-down"></i>
						</a>
                        <ul id="newsletter" class="collapse">
                            <li>
                                <a href="<?php echo base_url('index.php/newsletter/listing'); ?>">
									<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
									News Letter
								</a>
                            </li>
							
                        </ul>
                    </li>
					
					<li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#clint">
							<i class="fa fa-fw fa-arrows-v"></i>Our Clients<i class="fa fa-fw fa-caret-down"></i>
						</a>
                        <ul id="clint" class="collapse">	
                            <li>
                                <a href="<?php echo base_url('index.php/our_client/listing'); ?>">
									<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
									Our Clients
								</a>
                            </li>
                        </ul>
                    </li>
					
					<li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#order">
							<i class="fa fa-fw fa-arrows-v"></i>Manage Orders<i class="fa fa-fw fa-caret-down"></i>
						</a>
                        <ul id="order" class="collapse">
                            <li>
                                <a href="<?php echo base_url('index.php/order/listing/'); ?>">
								<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
								Orders Listing</a>
                            </li>
							<li>
                                <a href="<?php echo base_url('index.php/order/report/'); ?>">
								<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
								Orders Report</a>
                            </li>
                        </ul>
                    </li>
					<li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#metatg">
							<i class="fa fa-fw fa-arrows-v"></i>Manage Meta Tag<i class="fa fa-fw fa-caret-down"></i>
						</a>
                        <ul id="metatg" class="collapse">
                            <li>
                                <a href="<?php echo base_url('index.php/admin_meta_tag/'); ?>">
								<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
								Meta Tag</a>
                            </li>
                        </ul>
                    </li>
					<li>
                        <a href="<?php echo base_url('index.php/block/listing/'); ?>">
							<i class="fa fa-fw fa-arrows-v"></i>Manage Static Block</i>
						</a>
                    </li>
					
            </div>