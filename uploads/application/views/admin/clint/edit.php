<?php 
$admin_id = $this->session->userdata('admin_id'); 
if(empty($admin_id))
{
	redirect('index.php/admin');
}	
?>
<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/layout/head'); ?>
<style>
.naseemdiv
{
	height: 210px;
    overflow: scroll;
    border: 2px solid rgba(34, 34, 34, 0.41);
    padding: 10px;
}
.naseemdiv label{padding: 3px;}
</style>
<body>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <?php $this->load->view('admin/layout/header'); ?>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <?php $this->load->view('admin/layout/left-menu'); ?>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Edit Product</h1>
						<div class="col-md-6 col-lg-12 col-xl-6">
						 <div class="panel panel-sign">
					
					<div class="panel-body">
 
					<form enctype="multipart/form-data"  action="" method="POST">
						<?php //print_r($EDITPRODUCT);?>
						<table class="table table-bordered table-striped table-condensed" style="">										
						<tbody>
						<tr>
							<td>Category</td>
							<td>
								<select  name="category_id" class="form-control">
								<?php 
								$query = $this->category_model->selectAllCategory();
								//$query = $this->product_model->list_product();
								foreach ($query as $row){
								?>
									<option <?php echo ($row->id==$EDITPRODUCT[0]->category_id)?"selected" :"" ?>  	value="<?php echo $row->id; ?>" > 
										<?php echo $row->title; ?>
									</option>
								<?php
								} 
								?>
								</select>
							</td>
						</tr>
						<tr>
							<td>Vender Name</td>
							<td>
								<select  name="vendername" class="form-control">
								<option>select Vender</option>
								<?php 
								$query = $this->admin_model->selectvendername(001);	
								//$query = $this->product_model->list_product();
								foreach ($query as $row)
								{
								?>
									<option <?php echo ($row->id==$EDITPRODUCT[0]->vender_type)?"selected" :"" ?> value="<?php echo $row->id; ?>" > 
										<?php echo $row->username; ?>
									</option>
								<?php
								} 
								?>
								</select>
							</td>
						</tr>
						
						<tr>
							<td>Product Name</td>
							<td>
								<input type="text" name="name" value="<?php echo  $EDITPRODUCT[0]->name; ?>" class="form-control" autocomplete="off">
							</td>
						</tr>

						<tr>
							<td>Sort Description</td>
						<td>
							<textarea name="sortdesc" value="" class="form-control" rows="3" autocomplete="off"><?php echo $EDITPRODUCT[0]->sortdesc; ?></textarea>
						</td>
						</tr>
						<tr>
							<td>Description</td>
						<td>
							<textarea name="description" value="<?php echo $EDITPRODUCT[0]->description; ?>" class="form-control" rows="4"><?php echo $EDITPRODUCT[0]->description; ?></textarea>
						</td>
						</tr>
						<tr>
							<td> Price</td>
						<td>
							<input type="text" name="price" value="<?php echo $EDITPRODUCT[0]->price; ?>" class="form-control" autocomplete="off">
						</td>
						</tr>
						<tr>
							<td>Spacel Price</td>
						<td>
							<input type="text" name="spacel_price" value="<?php echo $EDITPRODUCT[0]->spacel_price; ?>" class="form-control" autocomplete="off">
						</td>
						</tr>
						<tr>
							<td>Descount</td>
						<td>
							<input type="text" name="descount" value="<?php echo $EDITPRODUCT[0]->descount; ?>" class="form-control" autocomplete="off">
						</td>
						</tr>
						<tr>
							<td>Releted Product</td>
							<td>
								<div class="naseemdiv">
								<?php 
								$query = $this->product_model->selectAllProduct();								
								$arr= $EDITPRODUCT[0]->reletedproduct;
								$val_array=explode(',', $arr); 
								foreach ($query as $row){ ?>
		<label><input type="checkbox"  name="reletedproduct[]"  value="<?php echo $row->id;?>" <?php echo(in_array($row->id,$val_array))?'checked':''; ?> /> <?php echo $row->name; ?></label><br>
								<?php } ?>
								</div>
							</td>
						</tr>
						
						<tr>
							<td>Product Size</td>
							<td>
								<div class="naseemdiv">
								<?php 
								//$args=func_get_args();
								$pro_id=$EDITPRODUCT[0]->id;
								$prosize=$this->size_model->selectsizebysizeproid($pro_id);
								$query = $this->size_model->selectsize();
								foreach ($query as $row)
								{ 
								//	echo $id=$row->id;					
								?>
<label><input type="checkbox"  name="sizeproduct[]"  value="<?php echo $row->id;?>" <?php echo(in_array($row->id,$prosize))?'checked':''; ?> /> <?php echo $row->size; ?></label><br>
								<?php } ?>
								</div>
							</td>
						</tr>
						
						
							<tr>
							<td>Product Color</td>
							<td>
								<div class="naseemdiv">
								<?php 
								//$args=func_get_args();
								$pro_id1=$EDITPRODUCT[0]->id;
								$prosize1=$this->color_model->selectcolorbyproid($pro_id1);
								$query1 = $this->color_model->selectcolor();
								foreach ($query1 as $row1)
								{ 
																	
								?>
<label><input type="checkbox"  name="coloproduct[]"  value="<?php echo $row1->id;?>" <?php echo(in_array($row1->id,$prosize1))?'checked':''; ?> /> <?php echo $row1->color; ?></label><br>
								<?php } ?>
								</div>
							</td>
						</tr>
						
						
						
						
						<tr>
							<td>Stock</td>
							<td>
								<input type="text" name="product_stock" value="<?php echo $EDITPRODUCT[0]->product_stock; ?>" class="form-control" autocomplete="off">
							</td>
						</tr>					
						
						<tr>
							<td>Status</td>
							<td>
								<select  name="status" class="form-control">
								<option <?php echo ($EDITPRODUCT[0]->status==1)?"selected" :"" ?> value="1">Active</option>
								<option <?php echo ($EDITPRODUCT[0]->status==0)?"selected" :"" ?> value="0">Inactive</option>
								</select>
							</td>
						</tr>


						<tr>
						<td colspan="2"><button type="submit" class="btn btn-info" name="edit">Submit</button></td>
						</tr>
						</tbody>
						</table>
					</form>
					
					   </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
<?php $this->load->view('admin/layout/footer-js'); ?>
</body>

</html>
