<?php 
$admin_id = $this->session->userdata('admin_id'); 
if(empty($admin_id))
{
	redirect('index.php/admin');
}	
?>
<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/layout/head'); ?>
<body>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <?php $this->load->view('admin/layout/header'); ?>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <?php $this->load->view('admin/layout/left-menu'); ?>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Admin Settings</h1>
                        <form role="form" method="post" enctype="multipart/form-data">
                         <!-- <div class="form-group">
                                 <label>Logo</label>
			                     <img src="<?php //echo base_url('uploads/image/'.$admindata[0]->logo); ?>" width="100" >
                                 <input type="file" class="form-control" placeholder="Title" name="image">
				                 <input type="hidden" value="<?php //echo $admindata[0]->logo; ?>" name="oldimage">
                              </div> -->

                               
                <!-- <input type="hidden" value="<?php //echo $admindata[0]->password; ?>" name="opwd" >  -->
			  
		
			  
			  
			  <div class="control-group ">
                <label class="control-label">New Password<font color="#FF0000">* </font> </label>
                <div class="controls  ">
                  <input type="password" class="form-control" name="pwd" >
                </div>
              </div> 
			  
		   <div class="control-group ">
                <label class="control-label">Confirm Password<font color="#FF0000">* </font> </label>
                <div class="controls  ">
                  <input type="password" class="form-control" name="cpwd">
                </div>
              </div> 

            </br>
						
							<button type="submit" name="updatedata" class="btn btn-default">Submit Button</button>
                        </form>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
<?php $this->load->view('admin/layout/footer-js'); ?>


</body>

</html>
