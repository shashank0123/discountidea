<?php 
$admin_id = $this->session->userdata('admin_id'); 
if(empty($admin_id))
{
	redirect('index.php/admin');
}	
?>
<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/layout/head'); ?>
<style>
.naseemdiv
{
	height: 210px;
    overflow: scroll;
    border: 2px solid rgba(34, 34, 34, 0.41);
    padding: 10px;
}
.naseemdiv label{padding: 3px;}
.naseemdiv ul {padding-left:5px; list-style:none;}
</style>
<body>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <?php $this->load->view('admin/layout/header'); ?>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <?php $this->load->view('admin/layout/left-menu'); ?>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Edit Product</h1>
						<div class="col-md-6 col-lg-12 col-xl-6">
						 <div class="panel panel-sign">
					
					<div class="panel-body">
 
					<form enctype="multipart/form-data"  action="" method="POST">
						<?php //print_r($EDITPRODUCT);?>
						<table class="table table-bordered table-striped table-condensed" style="">										
						<tbody>
						<tr>
							<td>Category</td>
							<td>
								<select  name="category_id" class="form-control">
								<?php  $data = $this->category_model->getSelectOption(0); ?>
								</select>
							</td>
						</tr>
						<tr>
							<td>Vendor Name</td>
							<td>
								<select  name="vendername" class="form-control">
								<option value="0">select Vender</option>
								<?php 
								$query = $this->admin_model->selectvendername();	
								foreach ($query as $row)
								{
								?>
									<option <?php echo ($row->id==$EDITPRODUCT[0]->vender_type)?"selected" :"" ?> value="<?php echo $row->id; ?>" > 
										<?php echo $row->fname.' '.$row->lname; ?>
									</option>
								<?php
								} 
								?>
								</select>
							</td>
						</tr>
						
						<tr>
							<td>Product Name</td>
							<td>
								<input type="text" name="name" value="<?php echo  $EDITPRODUCT[0]->name; ?>" class="form-control" autocomplete="off">
							</td>
						</tr>

						<tr>
							<td>Sort Description</td>
						<td>
							<textarea name="sortdesc" value="" class="form-control" rows="3" autocomplete="off"><?php echo $EDITPRODUCT[0]->sortdesc; ?></textarea>
						</td>
						</tr>
						<tr>
							<td>Description</td>
						<td>
							<textarea name="description" class="form-control" rows="4"><?php echo $EDITPRODUCT[0]->description; ?></textarea>
						</td>
						</tr>
						<tr>
							<td> Price</td>
						<td>
							<input type="text" name="price" value="<?php echo $EDITPRODUCT[0]->price; ?>" class="form-control" autocomplete="off">
						</td>
						</tr>
						<tr>
							<td>Spacel Price</td>
						<td>
							<input type="text" name="spacel_price" value="<?php echo $EDITPRODUCT[0]->spacel_price; ?>" class="form-control" autocomplete="off">
						</td>
						</tr>
						<tr>
							<td>Descount</td>
						<td>
							<input type="text" name="descount" value="<?php echo $EDITPRODUCT[0]->descount; ?>" class="form-control" autocomplete="off">
						</td>
						</tr>
						<tr>
							<td>Pincode</td>
							<td>
								<div class="naseemdiv">
								<?php 
								$pincodequery = $this->pincode_model->selectAllPincode();								
								$pincode= $EDITPRODUCT[0]->pincode_ids;
								$pincode_array=explode(',', $pincode); 
								foreach ($pincodequery as $row){ ?>
		<label><input type="checkbox"  name="pincode_ids[]"  value="<?php echo $row->id;?>" <?php echo(in_array($row->id,$pincode_array))?'checked':''; ?> /> <?php echo $row->pincode; ?></label>
								<?php } ?>
								</div>
							</td>
						</tr>
						
						<tr>
							<td>Releted Product</td>
							<td>
								<div class="naseemdiv">
								<?php 
								$query = $this->product_model->selectAllProduct();								
								$arr= $EDITPRODUCT[0]->reletedproduct;
								$val_array=explode(',', $arr); 
								foreach ($query as $row){ ?>
		<label><input type="checkbox"  name="reletedproduct[]"  value="<?php echo $row->id;?>" <?php echo(in_array($row->id,$val_array))?'checked':''; ?> /> <?php echo $row->name; ?></label><br>
								<?php } ?>
								</div>
							</td>
						</tr>
						
						<tr>
							<td>Product Filters</td>
							<td>
								<div class="naseemdiv" style="height: 300px;">
									<?php $filters_array =(!empty($EDITPRODUCT[0]->filter_ids))? explode(",",$EDITPRODUCT[0]->filter_ids): array(); ?>
									<?php $filter_data = $this->filters_model->selectAllFilters(); ?>
									<?php $pro_option_array = $this->filters_model->selectOptionByProductID($EDITPRODUCT[0]->id); ?>
									<?php foreach($filter_data as $filter){ ?>
									<?php $option_data = $this->filters_model->selectAllFiltersOptions($filter->id); ?>
									<div class="col-md-4" style="border:1px solid #ddd; overflow: scroll; height: 180px;">
										<h4><?php echo $filter->title; ?></h4>
										<small><label><input <?php echo (in_array($filter->id, $filters_array))?"checked" :"" ?> type='checkbox' name="filter[]" value="<?php echo $filter->id; ?>"> Dispay In product details page<label></small>
										<ul>  
											<?php if(count($option_data)>0){ ?>
											<?php foreach($option_data as $option){ ?>
											<li><label><input <?php echo (in_array($option->id, $pro_option_array))?"checked" :"" ?> type='checkbox' name="options[]" value="<?php echo $option->id; ?>"> <?php echo $option->title; ?></label> </li>
											<?php } ?>
											<?php } ?>
										</ul>
									</div>
									<?php } ?>
								</div>
							</td>
						</tr>
						
						<tr>
							<td>Product Stock</td>
							<td>
								<input type="text" name="product_stock" value="<?php echo $EDITPRODUCT[0]->product_stock; ?>" class="form-control" autocomplete="off">
							</td>
						</tr>		

						
						
						<tr>
							<td>Meta Title</td>
							<td>
								<input type="text" name="producttitle" value="<?php echo $EDITPRODUCT[0]->meta_title; ?>" class="form-control" autocomplete="off">
							</td>
						</tr>	

							<tr>
							<td>Meta Keyword</td>
							<td>
								<input type="text" name="productkeyword" value="<?php echo $EDITPRODUCT[0]->meta_keyword; ?>" class="form-control" autocomplete="off">
							</td>
						</tr>					
						<tr>
							<td>Meta Description</td>
							<td>
								<input type="text" name="productdescription" value="<?php echo $EDITPRODUCT[0]->meta_description; ?>" class="form-control" autocomplete="off">
							</td>
						</tr>					
						
						
						
						<tr>
							<td>Status</td>
							<td>
								<select  name="status" class="form-control">
								<option <?php echo ($EDITPRODUCT[0]->status==1)?"selected" :"" ?> value="1">Active</option>
								<option <?php echo ($EDITPRODUCT[0]->status==0)?"selected" :"" ?> value="0">Inactive</option>
								</select>
							</td>
						</tr>


						<tr>
						<td colspan="2"><button type="submit" class="btn btn-info" name="edit">Submit</button></td>
						</tr>
						</tbody>
						</table>
					</form>
					
					   </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
<?php $this->load->view('admin/layout/footer-js'); ?>
</body>

</html>
