 <?php 
$admin_id = $this->session->userdata('admin_id'); 
if(empty($admin_id))
{
	redirect('index.php/admin');
}	
?>
<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/layout/head'); ?>
<body>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <?php $this->load->view('admin/layout/header'); ?>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <?php $this->load->view('admin/layout/left-menu'); ?>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">

<a class="btn btn-success" href="<?php echo base_url('color/add');?>" style="float: right;">Add Color</a>
					<!-- start: page -->
					<div class="row">
					<div class="col-md-12"> <br><?php echo $this->session->flashdata('message'); ?>
						<div class="col-md-6 col-lg-12 col-xl-6">
						    <div class="panel panel-sign">
					
					<div class="panel-body">

			<form enctype="multipart/form-data" action="<?php echo base_url();?>index.php/category/list_sub_admin/" method="POST">

									<table class="table table-bordered table-striped mb-none">
									<?php
								//print_r($COLORLIST);die("fff");
									?>
									
							<thead>
								<tr class="gradeX">
								    <th>Sr No</th>
									<th>Color Name</th>
									<th>action</th>
								</tr>
							</thead>
							<tbody>
							
							<?php if(count($COLORLIST)>0){ ?>
							 <?php $i=0; foreach($COLORLIST as $COLORLIST){ $i++;?>
								<tr class="gradeX">
									<td>
									<?php echo $i;?>	
									</td>
									<td>
									<?php echo $COLORLIST->color;?>
									</td>
			<td><a href="<?php echo site_url('index.php/color/edit/'.$COLORLIST->id.''); ?>" title="edit record"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a><?php echo "&nbsp;&nbsp;&nbsp;&nbsp;" ?>
			<a href="<?php echo site_url('index.php/color/delete/'.$COLORLIST->id.''); ?>" title="delete record"><i class="fa fa-trash" aria-hidden="true"></i></a>
								</td>
								</tr>
								<?php
							}
						}
							?>																				
							</tbody>
						</table>

						</form>
					</div>
				</div>
						</div>
					<!-- end: page -->
			</div>

 </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
	 </div>
	  </div>
	   </div>
    <!-- /#wrapper -->
<?php $this->load->view('admin/layout/footer-js'); ?>
</body>

</html>