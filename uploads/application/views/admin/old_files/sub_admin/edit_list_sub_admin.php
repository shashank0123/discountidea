 <?php $this->load->view('admin/layout/header'); ?>
			<!-- end: header -->

			<div class="inner-wrapper">
				<!-- start: sidebar -->
				<aside id="sidebar-left" class="sidebar-left">
				
					 <?php $this->load->view('admin/layout/nave'); ?>
				
				</aside>
				<!-- end: sidebar -->

				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Edit Sub Admin</h2>
					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="index-2.html">
										<i class="fa fa-home"></i>
									</a>
								</li>
								<li><span>Edit Sub Admin</span></li>
							</ol>
					
							<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
						</div>

					</header>

					<!-- start: page -->
					<div class="row">
					<div class="col-md-12"> <br><?php echo $this->session->flashdata('message'); ?>
						<div class="col-md-6 col-lg-12 col-xl-6">
						    <div class="panel panel-sign">
					
					<div class="panel-body">
 
				<form action="" method="POST">
                                   <?php //print_r($LISTADMIN);?>
							<table class="table table-bordered table-striped table-condensed" style="">										
								<tbody>
									<tr>
										<td>Name</td>
										<td>
		<input type="text" name="name" value="<?php echo  $LISTADMIN[0]->name; ?>" class="form-control">
										</td>
									</tr>
									<tr>
										<td>Username</td>
										<td>
<input type="text" name="username" value="<?php echo $LISTADMIN[0]->username; ?>" class="form-control">
										</td>
									</tr>

									<tr>
										<td>Email</td>
										<td>
	<input type="text" name="email" value="<?php echo $LISTADMIN[0]->email; ?>" class="form-control">
										</td>
									</tr>
									<tr>
										<td>Phone No</td>
										<td>
<input type="text" name="phoneno" value="<?php echo  $LISTADMIN[0]->phoneno; ?>" class="form-control">
										</td>
									</tr>
									<tr>
										<td>type</td>
										<td>
<input type="text" name="type" value="<?php echo $LISTADMIN[0]->type; ?>" class="form-control">
										</td>
									</tr>
									<tr>
										<td>status</td>
										<td>
										<select  name="status" class="form-control">
										<option <?php echo ($LISTADMIN[0]->status==1)?"selected" :"" ?> value="1">Active</option>
										<option <?php echo ($LISTADMIN[0]->status==0)?"selected" :"" ?> value="0">Inactive</option>
										</select>
										</td>
									</tr>
									
																	
									<tr>
			<td colspan="2"><button type="submit" class="btn btn-info" name="edit_list_sub_admin">Submit</button></td>
									</tr>
								</tbody>
							</table>

						</form>
					
					</div>
				</div>
						</div>
					<!-- end: page -->
				</section>
			</div>

			
		</section>

		<!-- Vendor -->
		<script src="<?php echo base_url(); ?>assets/vendor/jquery/jquery.js"></script>		
		<script src="<?php echo base_url(); ?>assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>	
		<script src="<?php echo base_url(); ?>assets/vendor/jquery-cookie/jquery.cookie.js">
		</script>		
	<script src="<?php echo base_url(); ?>assets/vendor/style-switcher/style.switcher.js"></script>	
		<script src="<?php echo base_url(); ?>assets/vendor/bootstrap/js/bootstrap.js"></script>
		<script src="<?php echo base_url(); ?>assets/vendor/nanoscroller/nanoscroller.js"></script>	
		<script src="<?php echo base_url(); ?>assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>		
		<script src="<?php echo base_url(); ?>assets/vendor/magnific-popup/magnific-popup.js"></script>	
		<script src="<?php echo base_url(); ?>assets/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		
		<!-- Specific Page Vendor -->		
		
				
	
		
		<!-- Theme Base, Components and Settings -->
		<script src="<?php echo base_url(); ?>assets/javascripts/theme.js"></script>
		
		<!-- Theme Custom -->
		<script src="<?php echo base_url(); ?>assets/javascripts/theme.custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="<?php echo base_url(); ?>assets/javascripts/theme.init.js"></script>
		<!-- Analytics to Track Preview Website -->		<script>		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)		  })(window,document,'script','../../../www.google-analytics.com/analytics.js','ga');		  ga('create', 'UA-42715764-8', 'auto');		  ga('send', 'pageview');		</script>

		<!-- Examples -->
		<script src="<?php echo base_url(); ?>assets/javascripts/dashboard/examples.dashboard.js"></script>
		
	</body>

<!-- Mirrored from preview.oklerthemes.com/porto-admin/1.2.0/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 07 Oct 2014 06:55:34 GMT -->
</html>