<?php 
$admin_id = $this->session->userdata('admin_id'); 
if(empty($admin_id))
{
	redirect('index.php/admin');
}	
?>
<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/layout/head'); ?>
<body>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <?php $this->load->view('admin/layout/header'); ?>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <?php $this->load->view('admin/layout/left-menu'); ?>
            <!-- /.navbar-collapse -->
        </nav>
        <div id="page-wrapper">
            <div class="container-fluid">
                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h2 class="page-header">Edit Farmer</h2>
                         <form role="form" method="post" enctype="multipart/form-data">
							<div class="form-group">
                               
                            </div>
						 
                            <div class="form-group">
                                <label>Title</label>
                                <input type="text" class="form-control" placeholder="Page Title" value="<?php echo $EDITFARMER[0]->title; ?>" name="title">
                            </div>
							<div class="form-group">
                                <img src="<?php echo base_url('uploads/farmer/'.$EDITFARMER[0]->image); ?>" width="80">
								<input type="hidden" name="oldimage" value="<?php echo $EDITFARMER[0]->image; ?>">
                            </div>	
							<div class="form-group">
                                <label>Image</label>
                                <input type="file" class="form-control"  name="image">
                            </div>
							<div class="form-group">
                                <label>description</label>
                                <textarea class="form-control" name="description"><?php echo $EDITFARMER[0]->description; ?></textarea>
                            </div>
							<div class="form-group">
                                <label>Facebook Url</label>
                                <input type="text" class="form-control" placeholder="Facebook Url" name="facebook" value="<?php echo $EDITFARMER[0]->facebook; ?>">
                            </div>
							<div class="form-group">
                                <label>Twitter Url</label>
                                <input type="text" class="form-control" placeholder="Twitter Url" name="twitter" value="<?php echo $EDITFARMER[0]->twitter; ?>">
                            </div>
							<div class="form-group">
                                <label>Google Plus Url</label>
                                <input type="text" class="form-control" placeholder="Google Plus Url" name="googlepluse" value="<?php echo $EDITFARMER[0]->googlepluse; ?>">
                            </div>
                            <div class="form-group">
                                <label>Status</label>
                                <select name="status" class="form-control">
                                    <option <?php echo($EDITFARMER[0]->status==1)?'selected':''; ?> value="1">Active</option>
                                    <option <?php echo($EDITFARMER[0]->status==0)?'selected':''; ?> value="0">Inactive</option>
                                </select>
                            </div>                            

                            <button type="submit" name="updatedata" class="btn btn-default">Submit Button</button>
                            <button type="reset" class="btn btn-default">Reset Button</button>

                        </form>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
<?php $this->load->view('admin/layout/footer-js'); ?>
</body>

</html>
