<?php 
$admin_id = $this->session->userdata('admin_id'); 
if(empty($admin_id))
{
	redirect('index.php/admin');
}	
?>
<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/layout/head'); ?>
<body>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <?php $this->load->view('admin/layout/header'); ?>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <?php $this->load->view('admin/layout/left-menu'); ?>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Add New User</h1>
                         <form enctype="multipart/form-data" role="form" method="post">
                            <div class="form-group">
                                <label>Full Name</label>
                                <input type="text" class="form-control" placeholder="Full Name" name="name">
                            </div>
							
							<div class="control-group ">
                                <label class="control-label">Photo<font color="#FF0000">* </font></label>
                              <div class="controls">
                                <input type="file" class="form-control" id="image" name="image" required>
                              </div>
                            </div>
							
							<div class="form-group">
                                <label>Email Address</label>
                                <input type="text" class="form-control" placeholder="Email Address" name="email">
                            </div>	
							
							<div class="form-group">
                                <label>Gender</label>
                                <input type="text" class="form-control" placeholder="Gender" name="gender">
                            </div>
                            
							<div class="form-group">
                                <label>Contact Number</label>
                                <input type="text" class="form-control" placeholder="Contact Number" name="contact_no">
                            </div>
							
							<div class="form-group">
                                <label>Address</label>
                                <input type="text" class="form-control" placeholder="Address" name="address">
                            </div>
							
							<div class="form-group">
                                <label>City</label>
                                <input type="text" class="form-control" placeholder="City" name="city">
                            </div>
							
							<div class="form-group">
                                <label>State</label>
                                <input type="text" class="form-control" placeholder="State" name="state">
                            </div>
							
                            <div class="form-group">
                                <label>Status</label>
                                <select class="form-control">
                                    <option value="1">Active</option>
                                    <option value="0">Inactive</option>
                                </select>
                            </div>                            

                            <button type="submit" name="saveuser" class="btn btn-default">Submit Button</button>
                            <button type="reset" class="btn btn-default">Reset Button</button>

                        </form>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
<?php $this->load->view('admin/layout/footer-js'); ?>
</body>

</html>
