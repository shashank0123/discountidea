<?php 
$admin_id = $this->session->userdata('admin_id'); 
if(empty($admin_id))
{
	redirect('index.php/admin');
}	
?>
<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/layout/head'); ?>
<body>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <?php $this->load->view('admin/layout/header'); ?>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <?php $this->load->view('admin/layout/left-menu'); ?>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h2 class="page-header">Static Block						
						<a href="<?php echo base_url('index.php/block/add'); ?>" class="btn btn-primary pull-right">Add New</a></h2>
						<?php echo $this->session->flashdata('message'); ?>
                         <div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
										<th width="5%">S.No.</th>
                                        <th>Block Title</th>
                                        <th width="20%">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
								<?php $i=0; foreach($BLOCKDATA as $data){ $i++; ?>
                                    <tr>
										<td><?php echo $i; ?></td>
                                        <td><?php echo $data->title; ?></td>
                                        <td>
											<a href="<?php echo base_url('index.php/block/edit/'.$data->id); ?>" title="edit record"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
											<?php /* ?><a href="<?php echo base_url('index.php/block/delete/'.$data->id); ?>" onclick="return deleteConfirm();" title="delete record"><i class="fa fa-trash" aria-hidden="true"></i></a><?php */ ?>																				
										</td>
                                    </tr>
                                <?php } ?>                                       
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
<?php $this->load->view('admin/layout/footer-js'); ?>
<script>
function checkpancard()
{
	if(document.getElementById("nopancard").checked==true)
	{
		alert('Kindly Note you need to submit PAN Card in 30 days else your home based work will be stopped & previous work will be washed out.');
		document.getElementById("pancard").style.backgroundColor = "#ccc";
		document.getElementById("pancard").readOnly = true;
	}
	else
	{
		document.getElementById("pancard").style.backgroundColor = "#fff";
		document.getElementById("pancard").readOnly = false;
	}	
}

</script>
</body>

</html>
