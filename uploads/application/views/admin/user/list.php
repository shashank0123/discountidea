<?php 
$admin_id = $this->session->userdata('admin_id'); 
if(empty($admin_id))
{
	redirect('index.php/admin');
}	
?>
<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/layout/head'); ?>
<body>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <?php $this->load->view('admin/layout/header'); ?>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <?php $this->load->view('admin/layout/left-menu'); ?>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">User List</h1>
                         <?php echo $this->session->flashdata('message'); ?>
						 <div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
										<th>S.No.</th>
                                        <th>Name</th>
										<th>Email</th>
										<th>Password</th>
										<th>Phone Number</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
								<?php $i=0; foreach($USERLIST as $data){ $i++; ?>
                                    <tr>
										<td><?php echo $i; ?></td>
                                        <td><?php echo $data->fname; ?></td>
										<td><?php echo $data->email; ?></td>
										<td><?php echo $data->password; ?></td>
										<td><?php echo $data->mobile; ?></td>
                                        <td><img src="<?php echo($data->status=='0')?base_url('assets/admin/image/inactive.png'):base_url('assets/admin/image/active.png'); ?>"> </td>
                                        <td>
										<a target="_blank" href="<?php echo base_url('index.php/admin/user_ligin/'.$data->id); ?>"><button class="mb-xs mt-xs mr-xs btn btn-info" title="Login user"><i class="fa fa-sign-in"></i></button></a>
										<a href="<?php echo base_url('admin/viewuser/'.$data->id); ?>" title="view record"><i class="fa fa-eye" aria-hidden="true"></i></a>
										<a href="<?php echo base_url('admin/edituser/'.$data->id); ?>" title="edit record"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
									<!--	<a href="<?php //echo base_url('admin/delete/'.$data->id); ?>"><i class="fa fa-trash-o" aria-hidden="true"></i></a> -->
										<?php 
											
											/*
											<a href="#" title="delete record"><i class="fa fa-trash" aria-hidden="true"></i></a>	
											*/
										?>									
										</td>
                                    </tr>
                                <?php } ?>                                       
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
<?php $this->load->view('admin/layout/footer-js'); ?>
</body>

</html>
