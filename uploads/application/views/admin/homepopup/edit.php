<?php 
$admin_id = $this->session->userdata('admin_id'); 
if(empty($admin_id))
{
	redirect('index.php/admin');
}	
?>
<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/layout/head'); ?>
<body>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <?php $this->load->view('admin/layout/header'); ?>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <?php $this->load->view('admin/layout/left-menu'); ?>
            <!-- /.navbar-collapse -->
        </nav>
        <div id="page-wrapper">
            <div class="container-fluid">
                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h2 class="page-header">Edit Page</h2>
                         <form role="form" method="post" enctype="multipart/form-data">
							<div class="form-group">
                               
                            </div>
                            <div class="form-group">
                                <label>Page Title</label>
                                <input type="text" class="form-control" placeholder="Page Title" value="<?php echo $edithomepopup[0]->title; ?>" name="title">
                            </div>
						<?php /*	<div class="form-group">
                                <label>Youtube</label>
                                <input type="text" class="form-control" placeholder="youtube" value="<?php echo $EDITCMS[0]->youtube; ?>" name="youtube">
                            </div> */ ?>
							<div class="form-group">
                                <label>Content</label>
                               <!-- <input type="text" class="form-control" placeholder="Slide Content" name="slide_text" value="<?php //echo $EDITCMS[0]->slide_text; ?>"> -->
                                 <textarea class="form-control" placeholder="Slide Content" name="slide_text"><?php echo $edithomepopup[0]->content; ?></textarea>
                            </div>
						 	<div class="form-group">
							<?php
							if(!empty($edithomepopup[0]->image))
							{								
							?>
                                <img src="<?php echo base_url('uploads/cms/'.$edithomepopup[0]->image); ?>" width="80">
								<input type="hidden" name="oldimage" value="<?php echo $edithomepopup[0]->image; ?>">
								<?php
							}
							?>
							
                            </div>	
							
                						
 						<div class="form-group">
                                <label>Image</label>
                                <input type="file" class="form-control"  name="image">
                            </div>	
                            
						<tr>
										<td>Status</td>
										<td>
										<select  name="status" class="form-control">
										<option <?php echo ($edithomepopup[0]->status==1)?"selected" :"" ?> value="1">Active</option>
										<option <?php echo ($edithomepopup[0]->status==0)?"selected" :"" ?> value="0">Inactive</option>
										</select>
										</td>
									</tr>
							
                                                     

                            <button type="submit" name="updatedata" class="btn btn-default">Submit Button</button>
                            <button type="reset" class="btn btn-default">Reset Button</button>

                        </form>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
<?php $this->load->view('admin/layout/footer-js'); ?>
</body>

</html>
