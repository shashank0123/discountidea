<?php 
$admin_id = $this->session->userdata('admin_id'); 
if(empty($admin_id))
{
	redirect('index.php/admin');
}	
?>
<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/layout/head'); ?>
<body>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <?php $this->load->view('admin/layout/header'); ?>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <?php $this->load->view('admin/layout/left-menu'); ?>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12 ">
						<div class="panel panel-green">
						<div class="panel-heading">
                            <h3 class="panel-title">User Details</h3>
						</div>
						<div class="panel-body">	
                            <div class="col-lg-12">
								
								<div class="col-lg-3">
									<div class="form-group">
										<?php
											if(!empty($USERDATA[0]->image))
											{
										?>
										<img src="<?php echo base_url(); ?>uploads/image/<?php echo $USERDATA[0]->image; ?>" width="200" height="150"/>
										<?php
										}
										?>
									</div>
								</div>	 
								<div class="col-lg-9">
								
									<ul class="list-group">
									 <!--	<li class="list-group-item"><strong>Loging IP : </strong> <?php //echo $_SERVER['REMOTE_ADDR']; ?> </li> -->
									</ul>								
								</div>
																				
								<div class="clearfix"></div>						
								<div class="form-group">
								  <ul class="list-group">
									<li class="list-group-item"><strong>Name : </strong>	<?php echo $USERDATA[0]->fname; ?> </li>
									<li class="list-group-item"><strong>Email : </strong>	<?php echo $USERDATA[0]->email; ?> </li>
									<li class="list-group-item"><strong>Contact Number : </strong>	<?php echo $USERDATA[0]->mobile; ?> </li> 
									<li class="list-group-item"><strong>Gender : </strong>	<?php echo $USERDATA[0]->gender; ?> </li>
									<li class="list-group-item"><strong>Address : </strong>	<?php echo $USERDATA[0]->address; ?> </li>
									<li class="list-group-item"><strong>City : </strong>	<?php echo $USERDATA[0]->city; ?> </li>
									<li class="list-group-item"><strong>State : </strong>	<?php echo $USERDATA[0]->state; ?> </li>
									<li class="list-group-item"><strong>PinCode : </strong>	<?php echo $USERDATA[0]->pincode; ?> </li>
										
										
										
									</ul>
									
									
								</div>	
																								
									
							</div>					
							
							</div>
						</div>
                        </form>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
<?php $this->load->view('admin/layout/footer-js'); ?>
</body>

</html>
