﻿<?php $this->load->view('front/layout/header');?>
  <!-- end header --> 
  
  <!-- Navbar -->
  
  <!-- end nav --> 
  
  <!-- Breadcrumbs -->
  
  <div class="breadcrumbs">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <ul>
            <li class="home"> <a title="Go to Home Page" href="index.html">Home</a><span>&raquo;</span></li>
            <li class="category13"><strong>Login or Create an Account</strong></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <!-- Breadcrumbs End --> 
  
  <!-- Main Container -->
  <section class="main-container col1-layout">
    <div class="main container">
      <div class="account-login">
        <div class="page-title">
          <h2>Login or Create an Account</h2>
        </div>
        <div class="page-content">
          <div class="row">
		  <?php echo $this->session->flashdata('message1');?>
									
		  <form method="post" action="<?php echo base_url('user/create_account');?>">
            <div class="col-sm-6">
              <div class="box-authentication">
                <h3>Create an account</h3>
                <p>Please enter your detail to create an account.</p>
                
				<label for="emmail_register">Name</label>
                <input id="emmail_register" type="text" name="fname" class="form-control" required>	
				
				<label for="emmail_register">Contact No.</label>
                <input id="emmail_register" type="text" name="mobile" class="form-control" required>
				
				<label for="emmail_register">Email address</label>
                <input id="emmail_register" type="email" name="email" class="form-control" required>
				
				<label for="emmail_register">Password</label>
                <input id="emmail_register" type="password" name="password" class="form-control" required>
				
                <button class="button" name="createbutn"><i class="fa fa-user"></i>&nbsp; <span>Create an account</span></button>
              </div>
            </div>
			</form>
			<?php $back_url = (isset($_GET['backurl']) && $_GET['backurl']!="")?$_GET['backurl']:''; ?>
			<form method="post" action="<?php echo base_url('user/loginuser'.'?backurl='.$back_url);?>">
            <div class="col-sm-6">
              <div class="box-authentication">
                <h3>Already registered?</h3>
                <label for="emmail_login">Email address</label>
                <input id="emmail_login" name="email1" type="email" class="form-control" required>
                <label for="password_login">Password</label>
                <input id="password_login" type="password" name="password1" class="form-control" required>
                <p class="forgot-pass"><a href="<?php echo base_url('user/forgotpassword');?>">Forgot your password?</a></p>
                <button class="button" name="buttonlogin"><i class="fa fa-lock"></i>&nbsp; <span>Sign in</span></button>
              </div>
            </div>
			</form>
          </div>
        </div>
      </div>
      <br>
      <br>
      <br>
      <br>
      <br>
    </div>
  </section>
  <!-- Main Container End --> 
  <!-- our clients Slider -->
 
  <!-- Footer -->
  <?php $this->load->view('front/layout/footer');?>