<?php $this->load->view('front/layout/header');?>  <!-- end nav --> 
  
  <!-- Breadcrumbs -->
  
  <div class="breadcrumbs">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <ul>
            <li class="home"> <a title="Go to Home Page" href="<?php echo base_url(); ?>">Home</a><span>&raquo;</span></li>
            <li class="category13"><strong>My Products</strong></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <!-- Breadcrumbs End --> 
  
  <!-- Main Container -->
  <section class="main-container col1-layout">
    <div class="main container">
      <div class="account-login mb-30">
        	<div class="row">
            <div class="col-md-3">
              <div id="left-pnl" class="aside-site-left my_account_section   ">
                      
<div class="action_links">
     <?php $this->load->view('front/layout/my-account-left-sidebar'); ?>   
</div>


                <div class="sidebox-bottom"> <span> &nbsp; </span> </div>
                              </div>
            </div>
            <div class="col-md-8">
              <div class="aside-site-content my_account_section ">
                                            <div class="site-content ">
        
                 <div class="central-content"> 
        
        



<div class="cm-notification-container"></div>
            
                   
                          
    
    <div class="mainbox-container margin margin">
   <div class="mainbox-body"><div class="main_section" style="display: block;">
    <div class="my_order" style="display: block;">
	<div class="sub_action_links">
	    <h3>Add Product</h3>
	
	    	</div>
		    <div class="my_order_list table-responsive" style="padding:3px;">
			<form method="post">
			<table class="table table-bordered table-hover">
			
			
			<tr>
										<td>Category</td>
										<td><select name="category_id" class="form-control">
								  <?php $data = $this->category_model->getSelectOption(0); ?>
                                </select></td>
									</tr>
								
			
				<tr>
										<td>Product Name</td>
										<td><input type="text" name="name"  class="form-control" autocomplete="off" required></td>
									</tr>
								
									<tr>
										<td> Sort Description</td>
										<td><textarea name="sortdesc" value="" class="form-control" rows="3"></textarea></td>
									</tr>
									<tr>
										<td> Description</td>
										<td><textarea name="description"  class="form-control" rows="4"></textarea></td>
									</tr>
									<tr>
										<td>Price</td>
										<td><input type="text" name="price"  class="form-control" autocomplete="off" required></td>
									</tr>
									<tr>
										<td>Special Price</td>
										<td><input type="text" name="spacel_price"  class="form-control" autocomplete="off"></td>
									</tr>
									<tr>
										<td>Discount</td>
										<td><input type="text" name="descount" class="form-control" autocomplete="off"></td>
									</tr>
									<tr>
										<td>Product Stock</td>
										<td>
                                            <input type="text" name="product_stock"  class="form-control" autocomplete="off">
										</td>
									</tr>
									<tr>
										<td>Status</td>										
										<td>
											<select  name="status" class="form-control">
												<option value="1">Active</option>
												<option value="0">Inactive</option>
											</select>
										</td>
									</tr>
									<tr>
										<td colspan="2"><button type="submit" class="btn btn-info" name="add_product">Submit</button></td>
									</tr>
									
			</table> 	
			</form>	
	    </div></div>
		<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Request to Cancel this Order</h4>
        </div>
        <div class="modal-body">
          <div class="my_order">
		<div style="display: block;" class="overlay">
		    <div style="display: block;" class="o_container">
			
			<div class="cont">
			    <div class="order_cancel_req">
				<div class="my_order_list">
				    					<div class="item">
					    <h4><span class="order_id">Order ID:<span>111294586</span></span>Order Placed on Fri, 03-Mar-2017 </h4>
					    <div class="row">
						<div class="p_info">
						    <div class="p-image-sec">
							<div class="p_image">
							    <img style="width: 90px; height: 90px;" src="http://cdn.shopclues.com/images/thumbnails/59354/160/160/Axe14885242861488525726.jpg">
							</div>	
						    </div>
						    <div class="p_group">
							<div class="p_details">							
							    <h3>Pack of Two Axe Deo 150 gm</h3>
							    <p>Total Amount: Rs 223.00</p>							
							    <p class="status">Status: <span class="bold500">Cash on Delivery Confirmed</span></p>
							</div>
						    </div>
						</div>
					    </div>
					</div>
				    				    <div class="information">
					<div class="info_box">
					    <div class="default_form">
						<h3>Select Reason for cancellation</h3>
						<form method="POST" action="index.php" name="frmProfile">
						    <input type="hidden" name="token" value="f6d2754eee48a6211a0f912b0ab73279">
						    <input type="hidden" id="cancel_order_id" name="orderid" value="111294586">
						    <input type="hidden" id="current_status" name="cur_status" value="Q">
						    <input type="hidden" name="dispatch" value="orders.cancel_order">
						    <fieldset>
							<div class="s_row">
							    <select id="reasons" name="reasons">								
								<option value="">Select</option>
																    <option value="6">Address/ Phone no./ Quantity is Incorrect</option>
																    <option value="4">Found Lower Price Elsewhere</option>
																    <option value="2">Delivery Time is Long</option>
																    <option value="7">Unsure about Product Quality</option>
																    <option value="8">Shipping Cost/ COD Fee is Too High</option>
																    <option value="5">Wrong Product Ordered</option>
																    <option value="1">Not interested in item(s) any more</option>
																    <option value="3">Other</option>
															    </select>      
							</div>
							<div class="s_row">
							    <input type="text" value="" name="comment" class="label_jump">
							    <span></span>
							    <label>Write your comments</label>							    
							</div>
						    </fieldset>
						</form>
					    </div>
					</div>
				    </div>
				</div>
			    </div>	
			</div>
			<div class="btn_container"> 
			    <div class="btn_size">
				<input type="button" class="btn orange" value="Cancel Order" onclick="cancel_order_function();" style="cursor: pointer;">
			    </div>
			</div>
		    </div>		
		</div>
	    </div>        </div>
        
      </div>
      
    </div>
  </div>
    </div>
</div>

        
</div>
    </div>
           
                 </div> 
                   </div>
        
                    </div>
            </div>
          </div>
      </div>
     
    </div>
  </section>
  <!-- Main Container End --> 
  <!-- our clients Slider -->
  <?php $this->load->view('front/layout/footer');?>