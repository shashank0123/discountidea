<?php $this->load->view('front/layout/header');?>
  <!-- Breadcrumbs -->
  <div class="breadcrumbs">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
		
          <ul>
            <li class="home"> <a title="Go to Home Page" href="<?php echo base_url(); ?>">Home</a><span>&raquo;</span></li>
			<li class="home"> <a title="Store" href="<?php echo base_url(); ?>">Store</a><span></span></li>
		 </ul>
        </div>
      </div>
    </div>
  </div>
  <!-- Breadcrumbs End --> 
  <!-- Main Container -->
  <div class="main-container col2-left-layout">
    <div class="container">
      <div class="row">
        <div class="col-main col-sm-12">
          <div class="page-title">
            <h2>
			<img src="<?php echo base_url('uploads/store/'.$store[0]->logo); ?>">
			<center><?php echo (isset($store[0]->title))?$store[0]->title:''; ?></center></h2>
          </div>
          <div class="category-description std">
            <div class="slider-items-products">
              <div id="category-desc-slider" class="product-flexslider hidden-buttons">
                <div class="slider-items slider-width-col1 owl-carousel owl-theme"> 
           
                  <!-- Item -->
                  <?php if(count($categoryimages)>0){ ?>
				  <?php foreach($categoryimages as $cat_image){ ?>
				  <div class="item"><a href="<?php echo (!empty($cat_image->image_url))?$cat_image->image_url:'#'; ?>" target="_blank"><img alt="" src="<?php echo base_url('uploads/store/'.$cat_image->image); ?>"></a></div>
                  <?php }}else{ ?>
                  <div class="item"> <a href="#x"><img alt="" src="http://placehold.it/850x320?text=Image not found"></a> </div>
                  <?php } ?>
                  <!-- End Item --> 
                  
                </div>
              </div>
            </div>
			<div style="padding:10px;"><?php echo (isset($store[0]->content))?$store[0]->content:''; ?><br></div>
          </div>
           
		  <span id="filterresponse" >
          <div class="product-grid-area" id="griddata">
            <ul class="products-grid">
			<?php //print_r($productdata); ?>
			  <?php if(count($productdata)>0){ ?>
			  <?php foreach($productdata as $product){ ?>
              <li class="item col-lg-3 col-md-3 col-sm-6 col-xs-6 wow fadeInUp">
                <div class="product-item">
                  <div class="item-inner">
                    <div class="product-thumbnail">
                      <!--<div class="icon-sale-label sale-left">Sale</div>
                      <div class="icon-new-label new-right">New</div>-->
                      <div class="pr-img-area">
						<a title="<?php echo $product->name; ?>" href="<?php echo base_url('product/'.slugurl($product->name)).'/'.encodeurlval($product->id); ?>">
						  <figure>
							<?php $product_image = $this->product_model->selectProductDoubleImage($product->id); ?>
							<?php if(count($product_image)>0){ ?>
							<img class="first-img" src="<?php echo base_url('uploads/product/thumbs'); ?>/<?php echo $product_image[0]->image; ?>"> 
							<?php }else{ ?>
							<img class="first-img" src="http://placehold.it/264x264?text=Image not found"> 
							<?php } ?>
						 </figure>
						</a> 
						 <form method="post" action="<?php echo base_url('index.php/cart/addtocart'); ?>">
						   <input type="hidden" name="pid" value="<?php echo $product->id; ?>" >
						   <input type="hidden" name="qty" value="1" >
<button type="button" name="addtocartbutton1" onclick="return addtocart(<?php echo $product->id; ?>);" class="add-to-cart-mt"> <i class="fa fa-shopping-cart"></i><span> Add to Cart</span> </button>
						   <button type="submit" name="addtocartbutton" class="add-to-cart-mt1"> <i class="fa fa-shopping-cart"></i><span> Boy Now</span> </button>
						</form>
					 </div>					  
                      
                    </div>
                    <div class="item-info">
                      <div class="info-inner">
                        <div class="item-title"> 
							<a title="<?php echo $product->name; ?>" href="<?php echo base_url('product/'.slugurl($product->name)).'/'.encodeurlval($product->id); ?>">
								<?php echo $product->name; ?>
							</a>
						</div>
                        <div class="item-content">
                          <div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                          <div class="price-box">
							  <?php if($product->spacel_price!=""){ ?>
                              <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> $<?php echo $product->spacel_price; ?> </span> </p>
                              <p class="old-price"> <span class="price-label">Regular Price:</span> <span class="price"> $<?php echo $product->price; ?> </span> </p>
							  <?php }else{ ?>
							  <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> $<?php echo $product->price; ?> </span> </p>
							  <?php } ?>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </li>
              <?php } } ?>
            </ul>
          </div>
		  
		  </span>	  
        </div>
      </div>
    </div>
  </div>
  <!-- Main Container End -->   			
<?php $this->load->view('front/layout/footer');?>
<script>

function addtocart(pid)
{
	jQuery.ajax({
        	url: "<?php echo base_url('index.php/cart/addtocart_ajax'); ?>",
			type: "POST",
			data:   { product_id : pid},
			//beforeSend: function(){$(this).html('OKKKKKK')},
			success: function(data)
		    {
				jQuery('#cartresponsedata').html(data);
				jQuery(".mini-cart").attr("tabindex",-1).focus(500);
			},
		  	error: function() 
	    	{
				
	    	} 	        
	   });
}
</script>