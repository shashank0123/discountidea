<?php $this->load->view('front/layout/header');?>  <!-- end nav --> 
  
  <!-- Breadcrumbs -->
  
  <div class="breadcrumbs">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <ul>
            <li class="home"> <a title="Go to Home Page" href="<?php echo base_url(); ?>">Home</a><span>&raquo;</span></li>
            <li class="category13"><strong>My Products</strong></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <!-- Breadcrumbs End --> 
  
  <!-- Main Container -->
  <section class="main-container col1-layout">
    <div class="main container">
      <div class="account-login mb-30">
        	<div class="row">
            <div class="col-md-3">
              <div id="left-pnl" class="aside-site-left my_account_section   ">
                      
<div class="action_links">
     <?php $this->load->view('front/layout/my-account-left-sidebar'); ?>   
</div>


                <div class="sidebox-bottom"> <span> &nbsp; </span> </div>
                              </div>
            </div>
            <div class="col-md-8">
              <div class="aside-site-content my_account_section ">
                                            <div class="site-content ">
        
                 <div class="central-content"> 
        
        



<div class="cm-notification-container"></div>
            
                   
                          
    
    <div class="mainbox-container margin margin">
   <div class="mainbox-body"><div class="main_section" style="display: block;">
    <div class="my_order" style="display: block;">
	<div class="sub_action_links">
	    <h3>Bank Details</h3>
	    <?php echo $this->session->flashdata('message'); ?>
	    	
		    <div class="my_order_list table-responsive" style="padding:3px;">
			<form method="post" enctype='multipart/form-data'>
			<table class="table table-bordered table-hover">
				<tr>
					<td>Account No.</td>
					<td><input type="text" name="account_no" id="acno" class="form-control" autocomplete="off" value="<?php echo $user[0]->account_no; ?>" required></td>
				</tr>
				<tr>
					<td>Re-type Account No.</td>
					<td><input type="text" name="retype_account_no" id="acno_retype" class="form-control" value="" autocomplete="off" required></td>
				</tr>
				<tr>
					<td>Account Holder Name</td>
					<td><input type="text" name="account_holder"  class="form-control" value="<?php echo $user[0]->account_holder; ?>" autocomplete="off" required></td>
				</tr>
				<tr>
					<td>IFSC Code</td>
					<td><input type="text" name="ifsc_code"  class="form-control" value="<?php echo $user[0]->ifsc_code; ?>" autocomplete="off" required></td>
				</tr>				
				<tr>
					<td>Bank Name</td>
					<td><input type="text" name="bank_name"  class="form-control" value="<?php echo $user[0]->bank_name; ?>" autocomplete="off" required></td>
				</tr>
				<tr>
					<td>Bank Branch</td>
					<td><input type="text" name="bank_branch"  class="form-control" value="<?php echo $user[0]->bank_branch; ?>" autocomplete="off" required></td>
				</tr>
				<tr>
					<td>Bank City</td>
					<td><input type="text" name="bank_city"  class="form-control" value="<?php echo $user[0]->bank_city; ?>" autocomplete="off" required></td>
				</tr>
				<tr>
					<td>Bank State</td>
					<td><input type="text" name="bank_state"  class="form-control" value="<?php echo $user[0]->bank_state; ?>" autocomplete="off" required></td>
				</tr>
				<tr>
					<td>Upload Cancel Cheque or passbook</td>
					<td>
						<input type="file" name="passbook"  class="form-control" autocomplete="off" >
						<input type="hidden" name="old_passbook" value="<?php echo $user[0]->passbook; ?>">
						<?php if($user[0]->passbook!=""){ ?>
							<img src="<?php echo base_url('uploads/vendor/'.$user[0]->passbook) ?>" width="150">
						<?php } ?>
					</td>
				</tr>
				<tr>
					<td colspan="2"><button type="submit" class="btn btn-info" name="bankdetails" onclick="return checkaccountno();">Submit</button></td>
				</tr>					
			</table> 	
			</form>	
	    </div></div>
		
    </div>
</div>

        
</div>
    </div>
           
                 </div> 
                   </div>
        
                    </div>
            </div>
          </div>
      </div>
     
    </div>
  </section>
  <script>
	function checkaccountno()
	{
		var acno = document.getElementById('acno').value;
		var acno_retype = document.getElementById('acno_retype').value;
		if(acno!=acno_retype)
		{
			alert('re-type account no not matched account no.');
			return false;
		}	
	}
  </script>
  <!-- Main Container End --> 
  <!-- our clients Slider -->
  <?php $this->load->view('front/layout/footer');?>