  <!-- end header --> 
<?php $this->load->view('front/layout/header-home.php');?>  
  <!-- Navbar -->
  
  <!-- end nav -->
  <div class="slider">
    <div class="container">
      <div class="row">
        <div class="col-md-9 col-md-offset-3">
          <div class="flexslider ma-nivoslider">
                  <div id="ma-inivoslider-banner7" class="slides">
  
  <?php
  $bannerimage=$this->banner_model->selectAllBanner();
  foreach($bannerimage as $banner)
  {
  ?>
  <a href="<?php echo $banner->url;?>" ><img src="<?php echo base_url('uploads/banner');?>/<?php echo $banner->image;?>" class="dn" onclick="location.href='<?php echo $banner->url;?>'" /></a>
  <?php
  }
  ?>
  
  
  
 </div>
          
          </div>
          <!-- /.flexslider --> 
        </div>
      </div>
    </div>
  </div>
  <!-- .fullwidth -->
 
 

  
  <!-- main container -->
  <div class="main-container col1-layout">
    <div class="container">
      <div class="row"> 
      <div class="col-md-2 col-sm-4 hot-deal">

          <ul class="products-grid">
            <li class="item">
            <div class="product-item">
                        <div class="item-inner fadeInUp">
                          <div class="product-thumbnail right-hadding-sec">
                            <?php //$dis_data = $this->product_model->getproductdiscountwise(70); ?>
							
                            <div class="haddin-sale">
                              <h2 class="text-center">Deals of the Day</h2>
                              <div class="timer-sale"><i class="fa fa-clock-o"></i> <span id="timer"></span></div>                              
                              <div class="view-all">							
								<a href="<?php echo base_url('home/discount/deals-of-the-day');?>" class="view-btn"> <span> View All</span> </a>
                              </div>
                            </div>
                            
                             
                          </div>
                          
                        </div>
                      </div>
                      
              
            </li>
          </ul>
        </div>
        <!-- Home Tabs  -->
        <div class="home-tab col-sm-8 col-md-10 col-xs-12">
          
          <div id="productTabContent" class="tab-content">
            <div class="tab-pane active in" id="new-arrivals">
              <div class="new-arrivals-pro">
                <div class="slider-items-products">
                  <div id="new-arrivals-slider" class="product-flexslider hidden-buttons">
                    <div class="slider-items slider-width-col4">
                      <?php
						$deal="dealday";
					$data=$this->product_model->getproductdiscountwise(50); //$this->product_model->homeproductall($deal);
					foreach($data  as $dealday)
					{

						 $product_image = $this->product_model->selectProductDoubleImage($dealday->id);
						?>	
                      <div class="product-item">
                        <div class="item-inner fadeInUp">
                          <div class="product-thumbnail">
                            <div class="pr-img-area">
							<a title="<?php echo $dealday->name; ?>" href="<?php echo base_url('product/'.slugurl($dealday->name)).'/'.encodeurlval($dealday->id); ?>">
								<figure> <?php if(count($product_image)>0){ ?>
									<img class="first-img" src="<?php echo base_url('uploads/product/thumbs'); ?>/<?php echo $product_image[0]->image; ?>" alt="<?php echo $dealday->name; ?>"> 
									<img class="hover-img" src="<?php echo base_url('uploads/product/thumbs'); ?>/<?php echo $product_image[1]->image; ?>" alt="<?php echo $dealday->name; ?>">
									<?php }else{ ?>
									<img class="first-img" src="http://placehold.it/264x264?text=Image not found" alt="<?php echo $dealday->name; ?>"> 
									<img class="hover-img" src="http://placehold.it/264x264?text=Image not found" alt="<?php echo $dealday->name; ?>">
									<?php } ?>
								</figure>
							</a>	
                            <form method="post" action="<?php echo base_url('index.php/cart/addtocart'); ?>">
								<input type="hidden" name="pid" value="<?php echo $dealday->id; ?>" >
								<input type="hidden" name="qty" value="1" >						   
								<button type="button" name="addtocartbutton1" onclick="return addtocart(<?php echo $dealday->id; ?>);" class="add-to-cart-mt"> <i class="fa fa-shopping-cart"></i><span> Add to Cart</span> </button>
								<button type="submit" name="addtocartbutton" class="add-to-cart-mt1"> <i class="fa fa-bolt"></i><span> Buy Now</span> </button>
							</form>
                            </div>
                            
                          </div>
                          <div class="item-info">
                      <div class="info-inner">
                        <div class="item-title"> 
							<a title="<?php echo $dealday->name; ?>" href="<?php echo base_url('product/'.slugurl($dealday->name)).'/'.encodeurlval($dealday->id); ?>">
								<?php echo $dealday->name; ?>
							</a>
						</div>
                        <div class="item-content">
                          <div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                          <div class="price-box">
							  <?php if($dealday->spacel_price!=""){ ?>
                              <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> $<?php echo $dealday->spacel_price; ?> </span> </p>
                              <p class="old-price"> <span class="price-label">Regular Price:</span> <span class="price"> $<?php echo $dealday->price; ?> </span> </p>
							  <?php }else{ ?>
							  <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> $<?php echo $dealday->price; ?> </span> </p>
							  <?php } ?>
                          </div>
                        </div>
                      </div>
                    </div>
                        </div>
                      </div>
					  
					  <?php
					}
					?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
            
          </div>
        </div>
      </div>
    </div>
  </div>
  
  
  
  
  <div class="main-container col1-layout">
    <div class="container">
      <div class="row"> 
      <div class="col-md-2 col-sm-4 hot-deal">
     
          <ul class="products-grid">
            <li class="item">
            <div class="product-item">
                        <div class="item-inner fadeInUp">
                          <div class="product-thumbnail right-hadding-sec">
                         
                            <div class="haddin-sale">
                              <h2 class="text-center">The Grand Gadget Sale</h2>
                               <div class="view-all">
							    <?php
								$gadget="grand_gadget_sale";
								$datagadget=$this->product_model->homeproductall($gadget);
							    ?>
                              	<a href="<?php echo base_url('index.php/home/homepageallproduct/'.$gadget).'/'.encodeurlval($datagadget[0]->category_id);?>" class="view-btn"> <span> View All</span> </a>
                              </div>
                            </div>
                            
                             
                          </div>
                          
                        </div>
                      </div>
                      
              
            </li>
          </ul>
        </div>
        <!-- Home Tabs  -->
        <div class="home-tab col-sm-8 col-md-10 col-xs-12">
          
          <div id="productTabContent" class="tab-content">
            <div class="tab-pane active in" id="new-arrivals">
              <div class="new-arrivals-pro">
                <div class="slider-items-products">
                  <div id="new-arrivals-slider" class="product-flexslider hidden-buttons">
                    <div class="slider-items slider-width-col4"> 
                    <?php
						$grandsale="grand_gadget_sale";
						$datagrandsale=$this->product_model->homeproductall($grandsale);
						foreach($datagrandsale  as $grandsale)
						{
						$product_image = $this->product_model->selectProductDoubleImage($grandsale->id);
					?>	
                      <div class="product-item">
                        <div class="item-inner fadeInUp">
                          <div class="product-thumbnail">
                            <div class="pr-img-area">
                            <a title="<?php echo $grandsale->name; ?>" href="<?php echo base_url('product/'.slugurl($grandsale->name)).'/'.encodeurlval($grandsale->id); ?>">
								<figure> 
									<?php if(count($product_image)>0){ ?>
									<img class="first-img" src="<?php echo base_url('uploads/product/thumbs'); ?>/<?php echo $product_image[0]->image; ?>" alt="<?php echo $grandsale->name; ?>"> 
									<img class="hover-img" src="<?php echo base_url('uploads/product/thumbs'); ?>/<?php echo $product_image[1]->image; ?>" alt="<?php echo $grandsale->name; ?>">
									<?php }else{ ?>
									<img class="first-img" src="http://placehold.it/264x264?text=Image not found" alt="<?php echo $grandsale->name; ?>"> 
									<img class="hover-img" src="http://placehold.it/264x264?text=Image not found" alt="<?php echo $grandsale->name; ?>">
									<?php } ?>
								</figure>
							</a>
                            <form method="post" action="<?php echo base_url('index.php/cart/addtocart'); ?>">
								<input type="hidden" name="pid" value="<?php echo $grandsale->id; ?>" >
								<input type="hidden" name="qty" value="1" >
								<button type="button" name="addtocartbutton1" onclick="return addtocart(<?php echo $grandsale->id; ?>);" class="add-to-cart-mt"> <i class="fa fa-shopping-cart"></i><span> Add to Cart</span> </button>
								<button type="submit" name="addtocartbutton" class="add-to-cart-mt1"> <i class="fa fa-bolt"></i><span> Buy Now</span> </button>
							</form>
                            </div>                           
                          </div>
                          <div class="item-info">
                      <div class="info-inner">
                        <div class="item-title"> 
							<a title="<?php echo $grandsale->name; ?>" href="<?php echo base_url('product/'.slugurl($grandsale->name)).'/'.encodeurlval($grandsale->id); ?>">
								<?php echo $grandsale->name; ?>
							</a>
						</div>
                        <div class="item-content">
                          <div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                          <div class="price-box">
							  <?php if($grandsale->spacel_price!=""){ ?>
                              <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> $<?php echo $grandsale->spacel_price; ?> </span> </p>
                              <p class="old-price"> <span class="price-label">Regular Price:</span> <span class="price"> $<?php echo $grandsale->price; ?> </span> </p>
							  <?php }else{ ?>
							  <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> $<?php echo $grandsale->price; ?> </span> </p>
							  <?php } ?>
                          </div>
                        </div>
                      </div>
                    </div>
                        </div>
                      </div>
					  
					  <?php
					}
					?>
					  
					  
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
            
          </div>
        </div>
      </div>
    </div>
  </div>
  
    <div class="main-container col1-layout">
    <div class="container">
      <div class="row"> 
      <div class="col-md-2 col-sm-4 hot-deal">
     
          <ul class="products-grid">
            <li class="item">
            <div class="product-item">
                        <div class="item-inner fadeInUp">
                          <div class="product-thumbnail right-hadding-sec">
                         
                            <div class="haddin-sale">
                              <h2 class="text-center">Top Selling Smartphones</h2>
                              
                              
                               <div class="view-all">
							  <?php
								$smartph="selling_smart_phone";
								$datasmartph=$this->product_model->homeproductall($smartph);
							  ?>
                              	<a href="<?php echo base_url('index.php/home/homepageallproduct/'.$smartph).'/'.encodeurlval($datasmartph[0]->category_id);?>" class="view-btn"> <span> View All</span> </a>
                              </div>
                            </div>
                            
                             
                          </div>
                          
                        </div>
                      </div>
                      
              
            </li>
          </ul>
        </div>
        <!-- Home Tabs  -->
        <div class="home-tab col-sm-8 col-md-10 col-xs-12">
          
          <div id="productTabContent" class="tab-content">
            <div class="tab-pane active in" id="new-arrivals">
              <div class="new-arrivals-pro">
                <div class="slider-items-products">
                  <div id="new-arrivals-slider" class="product-flexslider hidden-buttons">
                    <div class="slider-items slider-width-col4">                          
                    <?php
					$selling="selling_smart_phone";
					$datatoselling=$this->product_model->homeproductall($selling);
					foreach($datatoselling  as $sellingsmart)
					{
						$product_image = $this->product_model->selectProductDoubleImage($sellingsmart->id);
					?>	
                      <div class="product-item">
                        <div class="item-inner fadeInUp">
                          <div class="product-thumbnail">
                            <div class="pr-img-area">
								<a title="<?php echo $sellingsmart->name; ?>" href="<?php echo base_url('product/'.slugurl($sellingsmart->name)).'/'.encodeurlval($sellingsmart->id); ?>">
									<figure> 
										<?php if(count($product_image)>0){ ?>
										<img class="first-img" src="<?php echo base_url('uploads/product/thumbs'); ?>/<?php echo $product_image[0]->image; ?>" alt="<?php echo $sellingsmart->name; ?>"> 
										<img class="hover-img" src="<?php echo base_url('uploads/product/thumbs'); ?>/<?php echo $product_image[1]->image; ?>" alt="<?php echo $sellingsmart->name; ?>">
										<?php }else{ ?>
										<img class="first-img" src="http://placehold.it/264x264?text=Image not found" alt="<?php echo $sellingsmart->name; ?>"> 
										<img class="hover-img" src="http://placehold.it/264x264?text=Image not found" alt="<?php echo $sellingsmart->name; ?>">
										<?php } ?>
									</figure>
								</a>
								<form method="post" action="<?php echo base_url('index.php/cart/addtocart'); ?>">
									<input type="hidden" name="pid" value="<?php echo $sellingsmart->id; ?>" >
									<input type="hidden" name="qty" value="1" >
									<button type="button" name="addtocartbutton1" onclick="return addtocart(<?php echo $sellingsmart->id; ?>);" class="add-to-cart-mt"> <i class="fa fa-shopping-cart"></i><span> Add to Cart</span> </button>
									<button type="submit" name="addtocartbutton" class="add-to-cart-mt1"> <i class="fa fa-bolt"></i><span> Buy Now</span> </button>
								</form>
                            </div>                            
                          </div>
						  <div class="item-info">
							  <div class="info-inner">
								<div class="item-title"> 
									<a title="<?php echo $sellingsmart->name; ?>" href="<?php echo base_url('product/'.slugurl($sellingsmart->name)).'/'.encodeurlval($sellingsmart->id); ?>">
										<?php echo $sellingsmart->name; ?>
									</a>
								</div>
								<div class="item-content">
								  <div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
								  <div class="price-box">
									  <?php if($sellingsmart->spacel_price!=""){ ?>
									  <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> $<?php echo $sellingsmart->spacel_price; ?> </span> </p>
									  <p class="old-price"> <span class="price-label">Regular Price:</span> <span class="price"> $<?php echo $sellingsmart->price; ?> </span> </p>
									  <?php }else{ ?>
									  <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> $<?php echo $sellingsmart->price; ?> </span> </p>
									  <?php } ?>
								  </div>
								</div>
							  </div>
							</div>
                        </div>
                      </div>
					  
					  <?php
					}
					?>
					  
					  
					  
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
           
          </div>
        </div>
      </div>
    </div>
  </div>
  
  
    <div class="main-container col1-layout">
    <div class="container">
      <div class="row"> 
      <div class="col-md-2 col-sm-4 hot-deal">
     
          <ul class="products-grid">
            <li class="item">
            <div class="product-item">
                        <div class="item-inner fadeInUp">
                          <div class="product-thumbnail right-hadding-sec">
                         
                            <div class="haddin-sale">
                              <h2 class="text-center">Discounts for You</h2>
                              
                              
                              <div class="view-all">							  
                              	<a href="<?php echo base_url('home/discount/discounts-for-you');?>" class="view-btn"> <span> View All</span> </a>
                              </div>
                            </div>
                            
                             
                          </div>
                          
                        </div>
                      </div>
                      
              
            </li>
          </ul>
        </div>
        <!-- Home Tabs  -->
        <div class="home-tab col-sm-8 col-md-10 col-xs-12">
          
          <div id="productTabContent" class="tab-content">
            <div class="tab-pane active in" id="new-arrivals">
              <div class="new-arrivals-pro">
                <div class="slider-items-products">
                  <div id="new-arrivals-slider" class="product-flexslider hidden-buttons">
                    <div class="slider-items slider-width-col4">
                    
                              
                      
                      <?php
						$discount="discount_you";
					$datadiscount= $this->product_model->getproductdiscountwise(30);//$this->product_model->homeproductall($discount);
					foreach($datadiscount  as $discountre)
					{

						 $product_image = $this->product_model->selectProductDoubleImage($discountre->id);
						?>	
                      <div class="product-item">
                        <div class="item-inner fadeInUp">
                          <div class="product-thumbnail">
                            <div class="pr-img-area">
								<a title="<?php echo $discountre->name; ?>" href="<?php echo base_url('product/'.slugurl($discountre->name)).'/'.encodeurlval($discountre->id); ?>">
									<figure> 
										<?php if(count($product_image)>0){ ?>
										<img class="first-img" src="<?php echo base_url('uploads/product/thumbs'); ?>/<?php echo $product_image[0]->image; ?>" alt="<?php echo $discountre->name; ?>"> 
										<img class="hover-img" src="<?php echo base_url('uploads/product/thumbs'); ?>/<?php echo $product_image[1]->image; ?>" alt="<?php echo $discountre->name; ?>">
										<?php }else{ ?>
										<img class="first-img" src="http://placehold.it/264x264?text=Image not found" alt="<?php echo $discountre->name; ?>"> 
										<img class="hover-img" src="http://placehold.it/264x264?text=Image not found" alt="<?php echo $discountre->name; ?>">
										<?php } ?>
									</figure>
								</a>
								<form method="post" action="<?php echo base_url('index.php/cart/addtocart'); ?>">
									<input type="hidden" name="pid" value="<?php echo $discountre->id; ?>" >
									<input type="hidden" name="qty" value="1" >
									<button type="button" name="addtocartbutton1" onclick="return addtocart(<?php echo $discountre->id; ?>);" class="add-to-cart-mt"> <i class="fa fa-shopping-cart"></i><span> Add to Cart</span> </button>
									<button type="submit" name="addtocartbutton" class="add-to-cart-mt1"> <i class="fa fa-bolt"></i><span> Buy Now</span> </button>
								</form>
                            </div>                            
                          </div>
                        <div class="item-info">
							<div class="info-inner">
								<div class="item-title"> 
									<a title="<?php echo $discountre->name; ?>" href="<?php echo base_url('product/'.slugurl($discountre->name)).'/'.encodeurlval($discountre->id); ?>">
										<?php echo $discountre->name; ?>
									</a>
								</div>
								<div class="item-content">
								  <div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
								  <div class="price-box">
									  <?php if($discountre->spacel_price!=""){ ?>
									  <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> $<?php echo $discountre->spacel_price; ?> </span> </p>
									  <p class="old-price"> <span class="price-label">Regular Price:</span> <span class="price"> $<?php echo $discountre->price; ?> </span> </p>
									  <?php }else{ ?>
									  <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> $<?php echo $discountre->price; ?> </span> </p>
									  <?php } ?>
								  </div>
								</div>
							</div>
						</div>
                        </div>
                      </div>					  
					  <?php } ?>					  
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
            
          </div>
        </div>
      </div>
    </div>
  </div>
  
  <div class="main-container col1-layout">
    <div class="container">
      <div class="row"> 
      <div class="col-md-2 col-sm-4 hot-deal">
     
          <ul class="products-grid">
            <li class="item">
            <div class="product-item">
                        <div class="item-inner fadeInUp">
                          <div class="product-thumbnail right-hadding-sec">
                         
                            <div class="haddin-sale">
                              <h2 class="text-center">Offers for You</h2>
                              
                              
                              <div class="view-all">							 
                              	<a href="<?php echo base_url('home/discount/offers-for-you');?>" class="view-btn"> <span> View All</span> </a>
                              </div>
                            </div>
                            
                             
                          </div>
                          
                        </div>
                      </div>
                      
              
            </li>
          </ul>
        </div>
        <!-- Home Tabs  -->
        <div class="home-tab col-sm-8 col-md-10 col-xs-12">
          
          <div id="productTabContent" class="tab-content">
            <div class="tab-pane active in" id="new-arrivals">
              <div class="new-arrivals-pro">
                <div class="slider-items-products">
                  <div id="new-arrivals-slider" class="product-flexslider hidden-buttons">
                    <div class="slider-items slider-width-col4">
                    <?php
					$offer="offer_for_you";
					$datafooter= $this->product_model->getproductdiscountwise(1);//$this->product_model->homeproductall($offer);
					foreach($datafooter  as $dataofferre)
					{
						$product_image = $this->product_model->selectProductDoubleImage($dataofferre->id);
					?>	
                      <div class="product-item">
                        <div class="item-inner fadeInUp">
                          <div class="product-thumbnail">
                            <div class="pr-img-area">
								<a title="<?php echo $dataofferre->name; ?>" href="<?php echo base_url('product/'.slugurl($dataofferre->name)).'/'.encodeurlval($dataofferre->id); ?>">
									<figure> 
										<?php if(count($product_image)>0){ ?>
										<img class="first-img" src="<?php echo base_url('uploads/product/thumbs'); ?>/<?php echo $product_image[0]->image; ?>" alt="<?php echo $dataofferre->name; ?>"> 
										<img class="hover-img" src="<?php echo base_url('uploads/product/thumbs'); ?>/<?php echo $product_image[1]->image; ?>" alt="<?php echo $dataofferre->name; ?>">
										<?php }else{ ?>
										<img class="first-img" src="http://placehold.it/264x264?text=Image not found" alt="<?php echo $dataofferre->name; ?>"> 
										<img class="hover-img" src="http://placehold.it/264x264?text=Image not found" alt="<?php echo $dataofferre->name; ?>">
										<?php } ?>
									</figure>
								</a>
								<form method="post" action="<?php echo base_url('index.php/cart/addtocart'); ?>">
									<input type="hidden" name="pid" value="<?php echo $dataofferre->id; ?>" >
									<input type="hidden" name="qty" value="1" >
									<button type="button" name="addtocartbutton1" onclick="return addtocart(<?php echo $dataofferre->id; ?>);" class="add-to-cart-mt"> <i class="fa fa-shopping-cart"></i><span> Add to Cart</span> </button>
									<button type="submit" name="addtocartbutton" class="add-to-cart-mt1"> <i class="fa fa-bolt"></i><span> Buy Now</span> </button>
								</form>
                            </div>                            
                          </div>
                          <div class="item-info">
                      <div class="info-inner">
                        <div class="item-title"> 
							<a title="<?php echo $dataofferre->name; ?>" href="<?php echo base_url('product/'.slugurl($dataofferre->name)).'/'.encodeurlval($dataofferre->id); ?>">
								<?php echo $dataofferre->name; ?>
							</a>
						</div>
                        <div class="item-content">
                          <div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                          <div class="price-box">
							  <?php if($dataofferre->spacel_price!=""){ ?>
                              <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> $<?php echo $dataofferre->spacel_price; ?> </span> </p>
                              <p class="old-price"> <span class="price-label">Regular Price:</span> <span class="price"> $<?php echo $dataofferre->price; ?> </span> </p>
							  <?php }else{ ?>
							  <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> $<?php echo $dataofferre->price; ?> </span> </p>
							  <?php } ?>
                          </div>
                        </div>
                      </div>
                    </div>
                        </div>
                      </div>					  
					  <?php }  ?>
					  
					  
					  
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
            
          </div>
        </div>
      </div>
    </div>
  </div>
  
  <div class="main-container col1-layout">
    <div class="container">
      <div class="row"> 
      <div class="col-md-2 col-sm-4 hot-deal">
     
          <ul class="products-grid">
            <li class="item">
            <div class="product-item">
                        <div class="item-inner fadeInUp">
                          <div class="product-thumbnail right-hadding-sec">
                         
                            <div class="haddin-sale">
                              <h2 class="text-center">Laptops and Desktops</h2>
                               <div class="view-all">
							  <?php
								$lapto="laptop_desktop";
								$datalapto=$this->product_model->homeproductall($lapto);
							  ?>
                              	<a href="<?php echo base_url('index.php/home/homepageallproduct/'.$lapto).'/'.encodeurlval($datalapto[0]->category_id);?>" class="view-btn"> <span> View All</span> </a>
                              </div>
                            </div>
                            
                             
                          </div>
                          
                        </div>
                      </div>
                      
              
            </li>
          </ul>
        </div>
        <!-- Home Tabs  -->
        <div class="home-tab col-sm-8 col-md-10 col-xs-12">
          
          <div id="productTabContent" class="tab-content">
            <div class="tab-pane active in" id="new-arrivals">
              <div class="new-arrivals-pro">
                <div class="slider-items-products">
                  <div id="new-arrivals-slider" class="product-flexslider hidden-buttons">
                    <div class="slider-items slider-width-col4">
                    <?php
					$laptop="laptop_desktop";
					$datalaptop=$this->product_model->homeproductall($laptop);
					foreach($datalaptop  as $laptodata)
					{
						$product_image = $this->product_model->selectProductDoubleImage($laptodata->id);
					?>	
                      <div class="product-item">
                        <div class="item-inner fadeInUp">
                          <div class="product-thumbnail">
                            <div class="pr-img-area">
								<a title="<?php echo $laptodata->name; ?>" href="<?php echo base_url('product/'.slugurl($laptodata->name)).'/'.encodeurlval($laptodata->id); ?>">
									<figure> 
										<?php if(count($product_image)>0){ ?>
										<img class="first-img" src="<?php echo base_url('uploads/product/thumbs'); ?>/<?php echo $product_image[0]->image; ?>" alt="<?php echo $laptodata->name; ?>"> 
										<img class="hover-img" src="<?php echo base_url('uploads/product/thumbs'); ?>/<?php echo $product_image[1]->image; ?>" alt="<?php echo $laptodata->name; ?>">
										<?php }else{ ?>
										<img class="first-img" src="http://placehold.it/264x264?text=Image not found" alt="<?php echo $laptodata->name; ?>"> 
										<img class="hover-img" src="http://placehold.it/264x264?text=Image not found" alt="<?php echo $laptodata->name; ?>">
										<?php } ?>
									</figure>
								</a>
								<form method="post" action="<?php echo base_url('index.php/cart/addtocart'); ?>">
									<input type="hidden" name="pid" value="<?php echo $laptodata->id; ?>" >
									<input type="hidden" name="qty" value="1" >
									<button type="button" name="addtocartbutton1" onclick="return addtocart(<?php echo $laptodata->id; ?>);" class="add-to-cart-mt"> <i class="fa fa-shopping-cart"></i><span> Add to Cart</span> </button>
									<button type="submit" name="addtocartbutton" class="add-to-cart-mt1"> <i class="fa fa-bolt"></i><span> Buy Now</span> </button>
								</form>
                            </div>                           
                          </div>
                          <div class="item-info">
                      <div class="info-inner">
                        <div class="item-title"> 
							<a title="<?php echo $laptodata->name; ?>" href="<?php echo base_url('product/'.slugurl($laptodata->name)).'/'.encodeurlval($laptodata->id); ?>">
								<?php echo $laptodata->name; ?>
							</a>
						</div>
                        <div class="item-content">
                          <div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                          <div class="price-box">
							  <?php if($laptodata->spacel_price!=""){ ?>
                              <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> $<?php echo $laptodata->spacel_price; ?> </span> </p>
                              <p class="old-price"> <span class="price-label">Regular Price:</span> <span class="price"> $<?php echo $laptodata->price; ?> </span> </p>
							  <?php }else{ ?>
							  <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> $<?php echo $laptodata->price; ?> </span> </p>
							  <?php } ?>
                          </div>
                        </div>
                      </div>
                    </div>
                        </div>
                      </div>
					  <?php } ?>
					  
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
            
          </div>
        </div>
      </div>
    </div>
  </div>
  
  
  
  <div class="main-container col1-layout">
    <div class="container">
      <div class="row"> 
      <div class="col-md-2 col-sm-4 hot-deal">
     
          <ul class="products-grid">
            <li class="item">
            <div class="product-item">
                        <div class="item-inner fadeInUp">
                          <div class="product-thumbnail right-hadding-sec">
                         
                            <div class="haddin-sale">
								<h2 class="text-center">Gas Stoves</h2>                              
								<div class="view-all">
								  <?php
									$stgs="gas_stove";
									$datastgs=$this->product_model->homeproductall($stgs);
								  ?>
									<a href="<?php echo base_url('index.php/home/homepageallproduct/'.$stgs).'/'.encodeurlval($datastgs[0]->category_id);?>" class="view-btn"> <span> View All</span> </a>
								</div>
                            </div>
                          </div>
                        </div>
                      </div>
            </li>
          </ul>
        </div>
        <!-- Home Tabs  -->
        <div class="home-tab col-sm-8 col-md-10 col-xs-12">
          
          <div id="productTabContent" class="tab-content">
            <div class="tab-pane active in" id="new-arrivals">
              <div class="new-arrivals-pro">
                <div class="slider-items-products">
                  <div id="new-arrivals-slider" class="product-flexslider hidden-buttons">
                    <div class="slider-items slider-width-col4">
                    <?php
					$stove="gas_stove";
					$datastove=$this->product_model->homeproductall($stove);
					foreach($datastove  as $stovedatare)
					{
						$product_image = $this->product_model->selectProductDoubleImage($stovedatare->id);
					?>	
                    <div class="product-item">
                        <div class="item-inner fadeInUp">
							<div class="product-thumbnail">
								<div class="pr-img-area">
									<a title="<?php echo $stovedatare->name; ?>" href="<?php echo base_url('product/'.slugurl($stovedatare->name)).'/'.encodeurlval($stovedatare->id); ?>">
										<figure> 
											<?php if(count($product_image)>0){ ?>
											<img class="first-img" src="<?php echo base_url('uploads/product/thumbs'); ?>/<?php echo $product_image[0]->image; ?>" alt="<?php echo $stovedatare->name; ?>"> 
											<img class="hover-img" src="<?php echo base_url('uploads/product/thumbs'); ?>/<?php echo $product_image[1]->image; ?>" alt="<?php echo $stovedatare->name; ?>">
											<?php }else{ ?>
											<img class="first-img" src="http://placehold.it/264x264?text=Image not found" alt="<?php echo $stovedatare->name; ?>"> 
											<img class="hover-img" src="http://placehold.it/264x264?text=Image not found" alt="<?php echo $stovedatare->name; ?>">
											<?php } ?>
										</figure>
									</a>	
									<form method="post" action="<?php echo base_url('index.php/cart/addtocart'); ?>">
										<input type="hidden" name="pid" value="<?php echo $stovedatare->id; ?>" >
										<input type="hidden" name="qty" value="1" >
										<button type="button" name="addtocartbutton1" onclick="return addtocart(<?php echo $stovedatare->id; ?>);" class="add-to-cart-mt"> <i class="fa fa-shopping-cart"></i><span> Add to Cart</span> </button>
										<button type="submit" name="addtocartbutton" class="add-to-cart-mt1"> <i class="fa fa-bolt"></i><span> Buy Now</span> </button>
									</form>								
								</div>                            
							</div>
							<div class="item-info">
								<div class="info-inner">
									<div class="item-title"> 
										<a title="<?php echo $stovedatare->name; ?>" href="<?php echo base_url('product/'.slugurl($stovedatare->name)).'/'.encodeurlval($stovedatare->id); ?>">
											<?php echo $stovedatare->name; ?>
										</a>
									</div>
									<div class="item-content">
									  <div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
									  <div class="price-box">
										  <?php if($stovedatare->spacel_price!=""){ ?>
										  <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> $<?php echo $stovedatare->spacel_price; ?> </span> </p>
										  <p class="old-price"> <span class="price-label">Regular Price:</span> <span class="price"> $<?php echo $stovedatare->price; ?> </span> </p>
										  <?php }else{ ?>
										  <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> $<?php echo $sellingsmart->price; ?> </span> </p>
										  <?php } ?>
									  </div>
									</div>
								</div>
							</div>
                        </div>
                    </div>
					<?php } ?>					  
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
          
          </div>
        </div>
      </div>
    </div>
  </div>
  
  <div class="main-container col1-layout">
    <div class="container">
      <div class="row"> 
      <div class="col-md-2 col-sm-4 hot-deal">
     
          <ul class="products-grid">
            <li class="item">
            <div class="product-item">
                        <div class="item-inner fadeInUp">
                          <div class="product-thumbnail right-hadding-sec">
                         
                            <div class="haddin-sale">
                              <h2 class="text-center">Women's Kurtis</h2>
                              
                              
                              <div class="view-all">
							  <?php
								$womkurti="women_kurti";
								$datawomkurti=$this->product_model->homeproductall($womkurti);
							  ?>
                              	<a href="<?php echo base_url('index.php/home/homepageallproduct/'.$womkurti).'/'.encodeurlval($datawomkurti[0]->category_id);?>" class="view-btn"> <span> View All</span> </a>
                              </div>
                            </div>
                            
                             
                          </div>
                          
                        </div>
                      </div>
                      
              
            </li>
          </ul>
        </div>
        <!-- Home Tabs  -->
        <div class="home-tab col-sm-8 col-md-10 col-xs-12">
          
          <div id="productTabContent" class="tab-content">
            <div class="tab-pane active in" id="new-arrivals">
              <div class="new-arrivals-pro">
                <div class="slider-items-products">
                  <div id="new-arrivals-slider" class="product-flexslider hidden-buttons">
                    <div class="slider-items slider-width-col4">
                      
                      
                      
                      
                     
                      
                    <?php
					$kurti="women_kurti";
					$datakurti=$this->product_model->homeproductall($kurti);
					foreach($datakurti  as $kurtidatare)
					{
						$product_image = $this->product_model->selectProductDoubleImage($kurtidatare->id);
					?>	
                      <div class="product-item">
                        <div class="item-inner fadeInUp">
                          <div class="product-thumbnail">
                            <div class="pr-img-area">
							<a title="<?php echo $kurtidatare->name; ?>" href="<?php echo base_url('product/'.slugurl($kurtidatare->name)).'/'.encodeurlval($kurtidatare->id); ?>">
								<figure>
									<?php if(count($product_image)>0){ ?>
									<img class="first-img" src="<?php echo base_url('uploads/product/thumbs'); ?>/<?php echo $product_image[0]->image; ?>" alt="<?php echo $kurtidatare->name; ?>"> 
									<img class="hover-img" src="<?php echo base_url('uploads/product/thumbs'); ?>/<?php echo $product_image[1]->image; ?>" alt="<?php echo $kurtidatare->name; ?>">
									<?php }else{ ?>
									<img class="first-img" src="http://placehold.it/264x264?text=Image not found" alt="<?php echo $kurtidatare->name; ?>"> 
									<img class="hover-img" src="http://placehold.it/264x264?text=Image not found" alt="<?php echo $kurtidatare->name; ?>">
									<?php } ?>
								</figure>
								<form method="post" action="<?php echo base_url('index.php/cart/addtocart'); ?>">
									<input type="hidden" name="pid" value="<?php echo $kurtidatare->id; ?>" >
									<input type="hidden" name="qty" value="1" >
									<button type="button" name="addtocartbutton1" onclick="return addtocart(<?php echo $kurtidatare->id; ?>);" class="add-to-cart-mt"> <i class="fa fa-shopping-cart"></i><span> Add to Cart</span> </button>
									<button type="submit" name="addtocartbutton" class="add-to-cart-mt1"> <i class="fa fa-bolt"></i><span> Buy Now</span> </button>
								</form>
							</a>
                            </div>
                            
                          </div>
                          <div class="item-info">
                      <div class="info-inner">
                        <div class="item-title"> 
							<a title="<?php echo $kurtidatare->name; ?>" href="<?php echo base_url('product/'.slugurl($kurtidatare->name)).'/'.encodeurlval($kurtidatare->id); ?>">
								<?php echo $kurtidatare->name; ?>
							</a>
						</div>
                        <div class="item-content">
                          <div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                          <div class="price-box">
							  <?php if($kurtidatare->spacel_price!=""){ ?>
                              <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> $<?php echo $kurtidatare->spacel_price; ?> </span> </p>
                              <p class="old-price"> <span class="price-label">Regular Price:</span> <span class="price"> $<?php echo $kurtidatare->price; ?> </span> </p>
							  <?php }else{ ?>
							  <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> $<?php echo $kurtidatare->price; ?> </span> </p>
							  <?php } ?>
                          </div>
                        </div>
                      </div>
                    </div>
                        </div>
                    </div>
					<?php } ?>					  
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  <div class="main-container col1-layout">
    <div class="container">
      <div class="row"> 
      <div class="col-md-2 col-sm-4 hot-deal">
     
          <ul class="products-grid">
            <li class="item">
            <div class="product-item">
                        <div class="item-inner fadeInUp">
                          <div class="product-thumbnail right-hadding-sec">
                         
                            <div class="haddin-sale">
                              <h2 class="text-center">Great Offers On Furniture</h2>
                              
                              
                              <div class="view-all">
							  <?php
								$fur="offer_furniture";
								$datafur=$this->product_model->homeproductall($fur);
							  ?>
                              	<a href="<?php echo base_url('index.php/home/homepageallproduct/'.$fur).'/'.encodeurlval($datafur[0]->category_id);?>" class="view-btn"> <span> View All</span> </a>
                              </div>
                            </div>
                            
                             
                          </div>                          
                        </div>
                      </div>                     
              
            </li>
          </ul>
        </div>
        <!-- Home Tabs  -->
        <div class="home-tab col-sm-8 col-md-10 col-xs-12">
          
          <div id="productTabContent" class="tab-content">
            <div class="tab-pane active in" id="new-arrivals">
              <div class="new-arrivals-pro">
                <div class="slider-items-products">
                  <div id="new-arrivals-slider" class="product-flexslider hidden-buttons">
                    <div class="slider-items slider-width-col4">
                    <?php
						$furniture="offer_furniture";
						$datafuniture=$this->product_model->homeproductall($furniture);
						foreach($datafuniture  as $furnituredata)
						{
						$product_image = $this->product_model->selectProductDoubleImage($furnituredata->id);
					?>	
                      <div class="product-item">
                        <div class="item-inner fadeInUp">
                          <div class="product-thumbnail">
                            <div class="pr-img-area">
								<a title="<?php echo $furnituredata->name; ?>" href="<?php echo base_url('product/'.slugurl($furnituredata->name)).'/'.encodeurlval($furnituredata->id); ?>">
									<figure> 
										<?php if(count($product_image)>0){ ?>
										<img class="first-img" src="<?php echo base_url('uploads/product/thumbs'); ?>/<?php echo $product_image[0]->image; ?>" alt="<?php echo $furnituredata->name; ?>"> 
										<img class="hover-img" src="<?php echo base_url('uploads/product/thumbs'); ?>/<?php echo $product_image[1]->image; ?>" alt="<?php echo $furnituredata->name; ?>">
										<?php }else{ ?>
										<img class="first-img" src="http://placehold.it/264x264?text=Image not found" alt="<?php echo $furnituredata->name; ?>"> 
										<img class="hover-img" src="http://placehold.it/264x264?text=Image not found" alt="<?php echo $furnituredata->name; ?>">
										<?php } ?>
									</figure>
								</a>
								<form method="post" action="<?php echo base_url('index.php/cart/addtocart'); ?>">
									<input type="hidden" name="pid" value="<?php echo $furnituredata->id; ?>" >
									<input type="hidden" name="qty" value="1" >
									<button type="button" name="addtocartbutton1" onclick="return addtocart(<?php echo $furnituredata->id; ?>);" class="add-to-cart-mt"> <i class="fa fa-shopping-cart"></i><span> Add to Cart</span> </button>
									<button type="submit" name="addtocartbutton" class="add-to-cart-mt1"> <i class="fa fa-bolt"></i><span> Buy Now</span> </button>
								</form>
                            </div>                            
                          </div>
                          <div class="item-info">
                      <div class="info-inner">
                        <div class="item-title"> 
							<a title="<?php echo $furnituredata->name; ?>" href="<?php echo base_url('product/'.slugurl($furnituredata->name)).'/'.encodeurlval($furnituredata->id); ?>">
								<?php echo $furnituredata->name; ?>
							</a>
						</div>
                        <div class="item-content">
                          <div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                          <div class="price-box">
							  <?php if($furnituredata->spacel_price!=""){ ?>
                              <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> $<?php echo $furnituredata->spacel_price; ?> </span> </p>
                              <p class="old-price"> <span class="price-label">Regular Price:</span> <span class="price"> $<?php echo $furnituredata->price; ?> </span> </p>
							  <?php }else{ ?>
							  <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> $<?php echo $furnituredata->price; ?> </span> </p>
							  <?php } ?>
                          </div>
                        </div>
                      </div>
                    </div>
                        </div>
                      </div>
					  
					  <?php
					}
					?>
					  
					  
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
            
          </div>
        </div>
      </div>
    </div>
  </div>
  
  
  
  <div class="main-container col1-layout">
    <div class="container">
      <div class="row"> 
      <div class="col-md-2 col-sm-4 hot-deal">
     
          <ul class="products-grid">
            <li class="item">
            <div class="product-item">
                        <div class="item-inner fadeInUp">
                          <div class="product-thumbnail right-hadding-sec">
                         
                            <div class="haddin-sale">
                              <h2 class="text-center">Sports & Fitness Essentials</h2>
                              
                              
                               <div class="view-all">
							  <?php
						$sptfitn="sport_fitness";
					$datasptfitn=$this->product_model->homeproductall($sptfitn);

							  ?>
                              	<a href="<?php echo base_url('index.php/home/homepageallproduct/'.$sptfitn).'/'.encodeurlval($datasptfitn[0]->category_id);?>" class="view-btn"> <span> View All</span> </a>
                              </div>
                            </div>
                            
                             
                          </div>
                          
                        </div>
                      </div>
                      
              
            </li>
          </ul>
        </div>
        <!-- Home Tabs  -->
        <div class="home-tab col-sm-8 col-md-10 col-xs-12">
          
          <div id="productTabContent" class="tab-content">
            <div class="tab-pane active in" id="new-arrivals">
              <div class="new-arrivals-pro">
                <div class="slider-items-products">
                  <div id="new-arrivals-slider" class="product-flexslider hidden-buttons">
                    <div class="slider-items slider-width-col4">
                    <?php
					$sport="sport_fitness";
					$datasports=$this->product_model->homeproductall($sport);
					foreach($datasports  as $sportsres)
					{
						$product_image = $this->product_model->selectProductDoubleImage($sportsres->id);
					?>	
                      <div class="product-item">
                        <div class="item-inner fadeInUp">
                          <div class="product-thumbnail">
                            <div class="pr-img-area">
								<a title="<?php echo $sportsres->name; ?>" href="<?php echo base_url('product/'.slugurl($sportsres->name)).'/'.encodeurlval($sportsres->id); ?>">
									<figure>
										<?php if(count($product_image)>0){ ?>
										<img class="first-img" src="<?php echo base_url('uploads/product/thumbs'); ?>/<?php echo $product_image[0]->image; ?>" alt="<?php echo $sportsres->name; ?>"> 
										<img class="hover-img" src="<?php echo base_url('uploads/product/thumbs'); ?>/<?php echo $product_image[1]->image; ?>" alt="<?php echo $sportsres->name; ?>">
										<?php }else{ ?>
										<img class="first-img" src="http://placehold.it/264x264?text=Image not found" alt="<?php echo $sportsres->name; ?>"> 
										<img class="hover-img" src="http://placehold.it/264x264?text=Image not found" alt="<?php echo $sportsres->name; ?>">
										<?php } ?>
									</figure>
								</a>
								<form method="post" action="<?php echo base_url('index.php/cart/addtocart'); ?>">
									<input type="hidden" name="pid" value="<?php echo $sportsres->id; ?>" >
									<input type="hidden" name="qty" value="1" >
									<button type="button" name="addtocartbutton1" onclick="return addtocart(<?php echo $sportsres->id; ?>);" class="add-to-cart-mt"> <i class="fa fa-shopping-cart"></i><span> Add to Cart</span> </button>
									<button type="submit" name="addtocartbutton" class="add-to-cart-mt1"> <i class="fa fa-bolt"></i><span> Buy Now</span> </button>
								</form>
                            </div>                            
                          </div>
                          <div class="item-info">
                      <div class="info-inner">
                        <div class="item-title"> 
							<a title="<?php echo $sportsres->name; ?>" href="<?php echo base_url('product/'.slugurl($sportsres->name)).'/'.encodeurlval($sportsres->id); ?>">
								<?php echo $sportsres->name; ?>
							</a>
						</div>
                        <div class="item-content">
                          <div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                          <div class="price-box">
							  <?php if($sportsres->spacel_price!=""){ ?>
                              <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> $<?php echo $sportsres->spacel_price; ?> </span> </p>
                              <p class="old-price"> <span class="price-label">Regular Price:</span> <span class="price"> $<?php echo $sportsres->price; ?> </span> </p>
							  <?php }else{ ?>
							  <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> $<?php echo $sportsres->price; ?> </span> </p>
							  <?php } ?>
                          </div>
                        </div>
                      </div>
                    </div>
                        </div>
                      </div>					  
					  <?php }?>					  
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  
  
  <div class="main-container col1-layout">
    <div class="container">
      <div class="row"> 
      <div class="col-md-2 col-sm-4 hot-deal">
     
          <ul class="products-grid">
            <li class="item">
            <div class="product-item">
                        <div class="item-inner fadeInUp">
                          <div class="product-thumbnail right-hadding-sec">
                         
                            <div class="haddin-sale">
                              <h2 class="text-center">Small Home Appliances</h2>
                              
                              
                              <div class="view-all">
							  <?php
								$appli="home_appline";
								$dataappli=$this->product_model->homeproductall($appli);
							  ?>
                              	<a href="<?php echo base_url('index.php/home/homepageallproduct/'.$appli).'/'.encodeurlval($dataappli[0]->category_id);?>" class="view-btn"> <span> View All</span> </a>
                              </div>
                            </div>
                            
                             
                          </div>
                          
                        </div>
                      </div>
                      
              
            </li>
          </ul>
        </div>
        <!-- Home Tabs  -->
        <div class="home-tab col-sm-8 col-md-10 col-xs-12">
          
          <div id="productTabContent" class="tab-content">
            <div class="tab-pane active in" id="new-arrivals">
              <div class="new-arrivals-pro">
                <div class="slider-items-products">
                  <div id="new-arrivals-slider" class="product-flexslider hidden-buttons">
                    <div class="slider-items slider-width-col4">
                     
                      
                      
                      
                     
						<?php
						$homeappline="home_appline";
						$datahomeapp=$this->product_model->homeproductall($homeappline);
						foreach($datahomeapp  as $homeapplinedata)
						{
							$product_image = $this->product_model->selectProductDoubleImage($homeapplinedata->id);
						?>	
                      <div class="product-item">
                        <div class="item-inner fadeInUp">
                          <div class="product-thumbnail">
                            <div class="pr-img-area">
							<a title="<?php echo $homeapplinedata->name; ?>" href="<?php echo base_url('product/'.slugurl($homeapplinedata->name)).'/'.encodeurlval($homeapplinedata->id); ?>">
								<figure>
									<?php if(count($product_image)>0){ ?>
									<img class="first-img" src="<?php echo base_url('uploads/product/thumbs'); ?>/<?php echo $product_image[0]->image; ?>" alt="<?php echo $homeapplinedata->name; ?>"> 
									<img class="hover-img" src="<?php echo base_url('uploads/product/thumbs'); ?>/<?php echo $product_image[1]->image; ?>" alt="<?php echo $homeapplinedata->name; ?>">
									<?php }else{ ?>
									<img class="first-img" src="http://placehold.it/264x264?text=Image not found" alt="<?php echo $homeapplinedata->name; ?>"> 
									<img class="hover-img" src="http://placehold.it/264x264?text=Image not found" alt="<?php echo $homeapplinedata->name; ?>">
									<?php } ?>
								</figure>
							</a>
                            <form method="post" action="<?php echo base_url('index.php/cart/addtocart'); ?>">
								<input type="hidden" name="pid" value="<?php echo $homeapplinedata->id; ?>" >
								<input type="hidden" name="qty" value="1" >
								<button type="button" name="addtocartbutton1" onclick="return addtocart(<?php echo $homeapplinedata->id; ?>);" class="add-to-cart-mt"> <i class="fa fa-shopping-cart"></i><span> Add to Cart</span> </button>
								<button type="submit" name="addtocartbutton" class="add-to-cart-mt1"> <i class="fa fa-bolt"></i><span> Buy Now</span> </button>
							</form>
                            </div>
                           
                          </div>
                          <div class="item-info">
                      <div class="info-inner">
                        <div class="item-title"> 
							<a title="<?php echo $homeapplinedata->name; ?>" href="<?php echo base_url('product/'.slugurl($homeapplinedata->name)).'/'.encodeurlval($homeapplinedata->id); ?>">
								<?php echo $homeapplinedata->name; ?>
							</a>
						</div>
                        <div class="item-content">
                          <div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                          <div class="price-box">
							  <?php if($homeapplinedata->spacel_price!=""){ ?>
                              <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> $<?php echo $homeapplinedata->spacel_price; ?> </span> </p>
                              <p class="old-price"> <span class="price-label">Regular Price:</span> <span class="price"> $<?php echo $homeapplinedata->price; ?> </span> </p>
							  <?php }else{ ?>
							  <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> $<?php echo $homeapplinedata->price; ?> </span> </p>
							  <?php } ?>
                          </div>
                        </div>
                      </div>
                    </div>
                        </div>
                      </div>					  
						<?php } ?>					  
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
           
          </div>
        </div>
      </div>
    </div>
  </div>
  
  
  <div class="main-container col1-layout">
    <div class="container">
      <div class="row"> 
      <div class="col-md-2 col-sm-4 hot-deal">
     
          <ul class="products-grid">
            <li class="item">
            <div class="product-item">
                        <div class="item-inner fadeInUp">
                          <div class="product-thumbnail right-hadding-sec">
                         
                            <div class="haddin-sale">
                              <h2 class="text-center">Deals in IT Peripherals</h2>
                              
                              
                              <div class="view-all">
							  <?php
						$dealit="deal_it";
					$datadealit=$this->product_model->homeproductall($dealit);

							  ?>
                              	<a href="<?php echo base_url('index.php/home/homepageallproduct/'.$dealit).'/'.encodeurlval($datadealit[0]->category_id);?>" class="view-btn"> <span> View All</span> </a>
                              </div>
                            </div>
                            
                             
                          </div>
                          
                        </div>
                      </div>
                      
              
            </li>
          </ul>
        </div>
        <!-- Home Tabs  -->
        <div class="home-tab col-sm-8 col-md-10 col-xs-12">
          
          <div id="productTabContent" class="tab-content">
            <div class="tab-pane active in" id="new-arrivals">
              <div class="new-arrivals-pro">
                <div class="slider-items-products">
                  <div id="new-arrivals-slider" class="product-flexslider hidden-buttons">
                    <div class="slider-items slider-width-col4">
                     
                      <?php
						$it="deal_it";
					$datait=$this->product_model->homeproductall($it);
					foreach($datait  as $itdatare)
					{

						 $product_image = $this->product_model->selectProductDoubleImage($itdatare->id);
						?>	
                      <div class="product-item">
                        <div class="item-inner fadeInUp">
                          <div class="product-thumbnail">
                            <div class="pr-img-area">
							<a title="<?php echo $itdatare->name; ?>" href="<?php echo base_url('product/'.slugurl($itdatare->name)).'/'.encodeurlval($itdatare->id); ?>">
								<figure>
									<?php if(count($product_image)>0){ ?>
									<img class="first-img" src="<?php echo base_url('uploads/product/thumbs'); ?>/<?php echo $product_image[0]->image; ?>" alt="<?php echo $itdatare->name; ?>"> 
									<img class="hover-img" src="<?php echo base_url('uploads/product/thumbs'); ?>/<?php echo $product_image[1]->image; ?>" alt="<?php echo $itdatare->name; ?>">
									<?php }else{ ?>
									<img class="first-img" src="http://placehold.it/264x264?text=Image not found" alt="<?php echo $itdatare->name; ?>"> 
									<img class="hover-img" src="http://placehold.it/264x264?text=Image not found" alt="<?php echo $itdatare->name; ?>">
									<?php } ?>
								</figure>
							</a>	
                            <form method="post" action="<?php echo base_url('index.php/cart/addtocart'); ?>">
								<input type="hidden" name="pid" value="<?php echo $itdatare->id; ?>" >
								<input type="hidden" name="qty" value="1" >
								<button type="button" name="addtocartbutton1" onclick="return addtocart(<?php echo $itdatare->id; ?>);" class="add-to-cart-mt"> <i class="fa fa-shopping-cart"></i><span> Add to Cart</span> </button>
								<button type="submit" name="addtocartbutton" class="add-to-cart-mt1"> <i class="fa fa-bolt"></i><span> Buy Now</span> </button>
							</form>
                            </div>
                            
                          </div>
                          <div class="item-info">
                      <div class="info-inner">
                        <div class="item-title"> 
							<a title="<?php echo $itdatare->name; ?>" href="<?php echo base_url('product/'.slugurl($itdatare->name)).'/'.encodeurlval($itdatare->id); ?>">
								<?php echo $itdatare->name; ?>
							</a>
						</div>
                        <div class="item-content">
                          <div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                          <div class="price-box">
							  <?php if($itdatare->spacel_price!=""){ ?>
                              <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> $<?php echo $itdatare->spacel_price; ?> </span> </p>
                              <p class="old-price"> <span class="price-label">Regular Price:</span> <span class="price"> $<?php echo $itdatare->price; ?> </span> </p>
							  <?php }else{ ?>
							  <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> $<?php echo $itdatare->price; ?> </span> </p>
							  <?php } ?>
                          </div>
                        </div>
                      </div>
                    </div>
                        </div>
                      </div>
					<?php } ?>
                    </div>
                  </div>
                </div>
              </div>
            </div>            
          </div>
        </div>
      </div>
    </div>
  </div>
  
  <div class="main-container col1-layout">
    <div class="container">
      <div class="row"> 
      <div class="col-md-2 col-sm-4 hot-deal">
     
          <ul class="products-grid">
            <li class="item">
            <div class="product-item">
                        <div class="item-inner fadeInUp">
                          <div class="product-thumbnail right-hadding-sec">
                         
                            <div class="haddin-sale">
                              <h2 class="text-center">Fashion & Travel Accessoriess</h2>
                              <div class="view-all">
							  <?php
								$fstr="fasion_travel";
								$datafstr=$this->product_model->homeproductall($fstr);
							  ?>
                              	<a href="<?php echo base_url('index.php/home/homepageallproduct/'.$fstr).'/'.encodeurlval($datafstr[0]->category_id);?>" class="view-btn"> <span> View All</span> </a>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
            </li>
          </ul>
        </div>
        <!-- Home Tabs  -->
        <div class="home-tab col-sm-8 col-md-10 col-xs-12">
          
          <div id="productTabContent" class="tab-content">
            <div class="tab-pane active in" id="new-arrivals">
              <div class="new-arrivals-pro">
                <div class="slider-items-products">
                  <div id="new-arrivals-slider" class="product-flexslider hidden-buttons">
                    <div class="slider-items slider-width-col4">                     
                    <?php
					$fashion="fasion_travel";
					$datafashion=$this->product_model->homeproductall($fashion);
					foreach($datafashion  as $fashiondata)
					{
						$product_image = $this->product_model->selectProductDoubleImage($fashiondata->id);
					?>	
                      <div class="product-item">
                        <div class="item-inner fadeInUp">
                          <div class="product-thumbnail">
                            <div class="pr-img-area">
							<a title="<?php echo $fashiondata->name; ?>" href="<?php echo base_url('product/'.slugurl($fashiondata->name)).'/'.encodeurlval($fashiondata->id); ?>">
								<figure>
									<?php if(count($product_image)>0){ ?>
									<img class="first-img" src="<?php echo base_url('uploads/product/thumbs'); ?>/<?php echo $product_image[0]->image; ?>" alt="<?php echo $fashiondata->name; ?>"> 
									<img class="hover-img" src="<?php echo base_url('uploads/product/thumbs'); ?>/<?php echo $product_image[1]->image; ?>" alt="<?php echo $fashiondata->name; ?>">
									<?php }else{ ?>
									<img class="first-img" src="http://placehold.it/264x264?text=Image not found" alt="<?php echo $fashiondata->name; ?>"> 
									<img class="hover-img" src="http://placehold.it/264x264?text=Image not found" alt="<?php echo $fashiondata->name; ?>">
									<?php } ?>
								</figure>
							</a>
                            <form method="post" action="<?php echo base_url('index.php/cart/addtocart'); ?>">
								<input type="hidden" name="pid" value="<?php echo $fashiondata->id; ?>" >
								<input type="hidden" name="qty" value="1" >
								<button type="button" name="addtocartbutton1" onclick="return addtocart(<?php echo $fashiondata->id; ?>);" class="add-to-cart-mt"> <i class="fa fa-shopping-cart"></i><span> Add to Cart</span> </button>
								<button type="submit" name="addtocartbutton" class="add-to-cart-mt1"> <i class="fa fa-bolt"></i><span> Buy Now</span> </button>
							</form>
                            </div>                            
                          </div>
                          <div class="item-info">
                      <div class="info-inner">
                        <div class="item-title"> 
							<a title="<?php echo $fashiondata->name; ?>" href="<?php echo base_url('product/'.slugurl($fashiondata->name)).'/'.encodeurlval($fashiondata->id); ?>">
								<?php echo $fashiondata->name; ?>
							</a>
						</div>
                        <div class="item-content">
                          <div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                          <div class="price-box">
							  <?php if($fashiondata->spacel_price!=""){ ?>
                              <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> $<?php echo $fashiondata->spacel_price; ?> </span> </p>
                              <p class="old-price"> <span class="price-label">Regular Price:</span> <span class="price"> $<?php echo $fashiondata->price; ?> </span> </p>
							  <?php }else{ ?>
							  <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> $<?php echo $fashiondata->price; ?> </span> </p>
							  <?php } ?>
                          </div>
                        </div>
                      </div>
                    </div>
                        </div>
                      </div>
					  
					  <?php
					}
					?>
					  
					  
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
            
          </div>
        </div>
      </div>
    </div>
  </div>
  

  
  <div class="main-container col1-layout">
    <div class="container">
      <div class="row"> 
      <div class="col-md-2 col-sm-4 hot-deal">
     
          <ul class="products-grid">
            <li class="item">
            <div class="product-item">
                        <div class="item-inner fadeInUp">
                          <div class="product-thumbnail right-hadding-sec">
                         
                            <div class="haddin-sale">
                              <h2 class="text-center">Best Selling TVs</h2>
                              
                              
                              <div class="view-all">
							  <?php
						$betv="best_tv";
					$databetv=$this->product_model->homeproductall($betv);

							  ?>
                              	<a href="<?php echo base_url('index.php/home/homepageallproduct/'.$betv).'/'.encodeurlval($databetv[0]->category_id);?>" class="view-btn"> <span> View All</span> </a>
                              </div>
                            </div>
                            
                             
                          </div>
                          
                        </div>
                      </div>
                      
              
            </li>
          </ul>
        </div>
        <!-- Home Tabs  -->
        <div class="home-tab col-sm-8 col-md-10 col-xs-12">
          
          <div id="productTabContent" class="tab-content">
            <div class="tab-pane active in" id="new-arrivals">
              <div class="new-arrivals-pro">
                <div class="slider-items-products">
                  <div id="new-arrivals-slider" class="product-flexslider hidden-buttons">
                    <div class="slider-items slider-width-col4">
                    <?php
						$tv="best_tv";
					$databest_tv=$this->product_model->homeproductall($tv);
					foreach($databest_tv  as $datatv)
					{
						$product_image = $this->product_model->selectProductDoubleImage($datatv->id);
					?>	
                      <div class="product-item">
                        <div class="item-inner fadeInUp">
                          <div class="product-thumbnail">
                            <div class="pr-img-area">
								<a title="<?php echo $datatv->name; ?>" href="<?php echo base_url('product/'.slugurl($datatv->name)).'/'.encodeurlval($datatv->id); ?>">
									<figure> 
										<?php if(count($product_image)>0){ ?>
										<img class="first-img" src="<?php echo base_url('uploads/product/thumbs'); ?>/<?php echo $product_image[0]->image; ?>" alt="<?php echo $datatv->name; ?>"> 
										<img class="hover-img" src="<?php echo base_url('uploads/product/thumbs'); ?>/<?php echo $product_image[1]->image; ?>" alt="<?php echo $datatv->name; ?>">
										<?php }else{ ?>
										<img class="first-img" src="http://placehold.it/264x264?text=Image not found" alt="<?php echo $datatv->name; ?>"> 
										<img class="hover-img" src="http://placehold.it/264x264?text=Image not found" alt="<?php echo $datatv->name; ?>">
										<?php } ?>
									</figure>
								</a>
								<form method="post" action="<?php echo base_url('index.php/cart/addtocart'); ?>">
									<input type="hidden" name="pid" value="<?php echo $datatv->id; ?>" >
									<input type="hidden" name="qty" value="1" >
									<button type="button" name="addtocartbutton1" onclick="return addtocart(<?php echo $datatv->id; ?>);" class="add-to-cart-mt"> <i class="fa fa-shopping-cart"></i><span> Add to Cart</span> </button>
									<button type="submit" name="addtocartbutton" class="add-to-cart-mt1"> <i class="fa fa-bolt"></i><span> Buy Now</span> </button>
								</form>
                            </div>                            
                          </div>
                          <div class="item-info">
						  <div class="info-inner">
							<div class="item-title"> 
								<a title="<?php echo $datatv->name; ?>" href="<?php echo base_url('product/'.slugurl($datatv->name)).'/'.encodeurlval($datatv->id); ?>">
									<?php echo $datatv->name; ?>
								</a>
							</div>
							<div class="item-content">
							  <div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
							  <div class="price-box">
								  <?php if($datatv->spacel_price!=""){ ?>
								  <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> $<?php echo $datatv->spacel_price; ?> </span> </p>
								  <p class="old-price"> <span class="price-label">Regular Price:</span> <span class="price"> $<?php echo $datatv->price; ?> </span> </p>
								  <?php }else{ ?>
								  <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> $<?php echo $datatv->price; ?> </span> </p>
								  <?php } ?>
							  </div>
							</div>
						  </div>
						</div>
                        </div>
                      </div>
					  
					  <?php
					}
					?>
					  
					  
					  
					  
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
           
          </div>
        </div>
      </div>
    </div>
  </div>
  
 
  
  <div class="main-container col1-layout">
    <div class="container">
      <div class="row"> 
      <div class="col-md-2 col-sm-4 hot-deal">
     
          <ul class="products-grid">
            <li class="item">
            <div class="product-item">
                        <div class="item-inner fadeInUp">
                          <div class="product-thumbnail right-hadding-sec">
                         
                            <div class="haddin-sale">
                              <h2 class="text-center">New arrivals</h2>
                              
                              
                              <div class="view-all">
							  <?php
						$near="new_arrival";
					$datanear=$this->product_model->homeproductall($near);

							  ?>
                              	<a href="<?php echo base_url('index.php/home/homepageallproduct/'.$near).'/'.encodeurlval($datanear[0]->category_id);?>" class="view-btn"> <span> View All</span> </a>
                              </div>
                            </div>
                            
                             
                          </div>
                          
                        </div>
                      </div>
                      
              
            </li>
          </ul>
        </div>
        <!-- Home Tabs  -->
        <div class="home-tab col-sm-8 col-md-10 col-xs-12">
          
          <div id="productTabContent" class="tab-content">
            <div class="tab-pane active in" id="new-arrivals">
              <div class="new-arrivals-pro">
                <div class="slider-items-products">
                  <div id="new-arrivals-slider" class="product-flexslider hidden-buttons">
                    <div class="slider-items slider-width-col4">
                    
                      <?php
					$newarrival="new_arrival";
					$databest_tv=$this->product_model->homeproductall($newarrival);
					foreach($databest_tv  as $dataarival)
					{
						$product_image = $this->product_model->selectProductDoubleImage($dataarival->id);
					?>	
                      <div class="product-item">
                        <div class="item-inner fadeInUp">
                          <div class="product-thumbnail">
                            <div class="pr-img-area">
							<a title="<?php echo $dataarival->name; ?>" href="<?php echo base_url('product/'.slugurl($dataarival->name)).'/'.encodeurlval($dataarival->id); ?>">
								<figure>
									<?php if(count($product_image)>0){ ?>
									<img class="first-img" src="<?php echo base_url('uploads/product/thumbs'); ?>/<?php echo $product_image[0]->image; ?>" alt="<?php echo $dataarival->name; ?>"> 
									<img class="hover-img" src="<?php echo base_url('uploads/product/thumbs'); ?>/<?php echo $product_image[1]->image; ?>" alt="<?php echo $dataarival->name; ?>">
									<?php }else{ ?>
									<img class="first-img" src="http://placehold.it/264x264?text=Image not found" alt="<?php echo $dataarival->name; ?>"> 
									<img class="hover-img" src="http://placehold.it/264x264?text=Image not found" alt="<?php echo $dataarival->name; ?>">
									<?php } ?>
								</figure>
							</a>
                            <form method="post" action="<?php echo base_url('index.php/cart/addtocart'); ?>">
								<input type="hidden" name="pid" value="<?php echo $dataarival->id; ?>" >
								<input type="hidden" name="qty" value="1" >
								<button type="button" name="addtocartbutton1" onclick="return addtocart(<?php echo $dataarival->id; ?>);" class="add-to-cart-mt"> <i class="fa fa-shopping-cart"></i><span> Add to Cart</span> </button>
								<button type="submit" name="addtocartbutton" class="add-to-cart-mt1"> <i class="fa fa-bolt"></i><span> Buy Now</span> </button>
							</form>
                            </div>                            
                          </div>
                          <div class="item-info">
                      <div class="info-inner">
                        <div class="item-title"> 
							<a title="<?php echo $dataarival->name; ?>" href="<?php echo base_url('product/'.slugurl($dataarival->name)).'/'.encodeurlval($dataarival->id); ?>">
								<?php echo $dataarival->name; ?>
							</a>
						</div>
                        <div class="item-content">
                          <div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                          <div class="price-box">
							  <?php if($dataarival->spacel_price!=""){ ?>
                              <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> $<?php echo $dataarival->spacel_price; ?> </span> </p>
                              <p class="old-price"> <span class="price-label">Regular Price:</span> <span class="price"> $<?php echo $dataarival->price; ?> </span> </p>
							  <?php }else{ ?>
							  <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> $<?php echo $dataarival->price; ?> </span> </p>
							  <?php } ?>
                          </div>
                        </div>
                      </div>
                    </div>
                        </div>
                      </div>
					  
					  <?php
					}
					?>
					  
					  
					  
					  
					  
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
           
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php $recently_product_ids = $this->product_model->getRecentlyViewedProduct(); ?>
  <?php if(count($recently_product_ids)>0){  ?>
  <div class="main-container col1-layout">
    <div class="container">
      <div class="row"> 
      <div class="col-md-2 col-sm-4 hot-deal">
     
          <ul class="products-grid">
            <li class="item">
            <div class="product-item">
                        <div class="item-inner fadeInUp">
                          <div class="product-thumbnail right-hadding-sec">
                         
                            <div class="haddin-sale">
                              <h2 class="text-center">Recently Viewed</h2>
                              
                              
                              <div class="view-all">
                              	<a href="btn" class="view-btn"> <span> View All</span> </a>
                              </div>
                            </div>
                            
                             
                          </div>
                          
                        </div>
                      </div>
                      
              
            </li>
          </ul>
        </div>
        <!-- Home Tabs  -->
		
		<?php $recent_product = $this->product_model->selectAllReletedProduct($recently_product_ids); ?>
		<?php if(count($recent_product)>0){ ?>
        <div class="home-tab col-sm-8 col-md-10 col-xs-12">
          
          <div id="productTabContent" class="tab-content">
            <div class="tab-pane active in" id="new-arrivals">
              <div class="new-arrivals-pro">
                <div class="slider-items-products">
                  <div id="new-arrivals-slider" class="product-flexslider hidden-buttons">
                    <div class="slider-items slider-width-col4">
                     <?php foreach($recent_product  as $dataarival){
						 $product_image = $this->product_model->selectProductDoubleImage($dataarival->id);
						?>	
                      <div class="product-item">
                        <div class="item-inner fadeInUp">
                          <div class="product-thumbnail">
                            <div class="pr-img-area">
								<a title="<?php echo $dataarival->name; ?>" href="<?php echo base_url('product/'.slugurl($dataarival->name)).'/'.encodeurlval($dataarival->id); ?>">
									<figure> 
										<?php if(count($product_image)>0){ ?>
										<img class="first-img" src="<?php echo base_url('uploads/product/thumbs'); ?>/<?php echo $product_image[0]->image; ?>" alt="<?php echo $dataarival->name; ?>"> 
										<img class="hover-img" src="<?php echo base_url('uploads/product/thumbs'); ?>/<?php echo $product_image[1]->image; ?>" alt="<?php echo $dataarival->name; ?>">
										<?php }else{ ?>
										<img class="first-img" src="http://placehold.it/264x264?text=Image not found" alt="<?php echo $dataarival->name; ?>"> 
										<img class="hover-img" src="http://placehold.it/264x264?text=Image not found" alt="<?php echo $dataarival->name; ?>">
										<?php } ?>
									</figure>
								</a>
								<form method="post" action="<?php echo base_url('index.php/cart/addtocart'); ?>">
									<input type="hidden" name="pid" value="<?php echo $dataarival->id; ?>" >
									<input type="hidden" name="qty" value="1" >
									<button type="button" name="addtocartbutton1" onclick="return addtocart(<?php echo $dealday->id; ?>);" class="add-to-cart-mt"> <i class="fa fa-shopping-cart"></i><span> Add to Cart</span> </button>
									<button type="submit" name="addtocartbutton" class="add-to-cart-mt1"> <i class="fa fa-bolt"></i><span> Buy Now</span> </button>
								</form>
                            </div>                            
                          </div>
                          <div class="item-info">
                      <div class="info-inner">
                        <div class="item-title"> 
							<a title="<?php echo $dataarival->name; ?>" href="<?php echo base_url('product/'.slugurl($dataarival->name)).'/'.encodeurlval($dataarival->id); ?>">
								<?php echo $dataarival->name; ?>
							</a>
						</div>
                        <div class="item-content">
                          <div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                          <div class="price-box">
							  <?php if($dataarival->spacel_price!=""){ ?>
                              <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> $<?php echo $dataarival->spacel_price; ?> </span> </p>
                              <p class="old-price"> <span class="price-label">Regular Price:</span> <span class="price"> $<?php echo $dataarival->price; ?> </span> </p>
							  <?php }else{ ?>
							  <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> $<?php echo $dataarival->price; ?> </span> </p>
							  <?php } ?>
                          </div>
                        </div>
                      </div>
                    </div>
                        </div>
                      </div>					  
					  <?php } ?>					
                    </div>
                  </div>
                </div>
              </div>
            </div>            
            
          </div>
        </div>
		<?php } ?>
      </div>
    </div>
  </div>
  <?php } ?>
  
  <!-- end main container --> 
 <div class="bottom-banner-section">   
    <div class="container">
      
      <div class="row">
	  
	  <?php
	  error_reporting(0);
	  $categorydata=$this->category_model->selectCategoryForHome();
	  foreach($categorydata as $data)
	  {
		  $id=$data->id;
		  $categoryimage=$this->category_model->selectAllCategoryImages($id);
		  //print_r($categoryimage);
?>
      <div class="col-md-4 col-sm-6"> <a href="<?php echo base_url('products/'.slugurl($data->title)).'/'. encodeurlval($data->id);?>" class="bottom-banner-img">
	  <img src="<?php echo base_url('uploads/category');?>/<?php echo $categoryimage[0]->image;?>" alt="bottom banner"> <span class="banner-overly"></span>
          <div class="bottom-img-info">
            <h3><?php echo $data->title;?></h3>
           
            <span class="shop-now-btn">View more</span> </div>
          </a> 
		  </div>
		  <?php
	  }
	  ?>
		  
  
                  
  
        
        
      </div>
    </div></div>  
    
    <!--special-products-->
 <div class="special-products"> 
  <div class="container">  
  <div class="page-header">
                  <h2>special products</h2>
                  
                </div>
              <div class="special-products-pro">
                <div class="slider-items-products">
                  <div id="special-products-slider3" class="product-flexslider hidden-buttons">
                    <div class="slider-items slider-width-col4">
                    
                     <?php
					$randata=$this->product_model->selecproducthomerand();
					foreach($randata  as $homerandedataprod)
					{
						$product_image = $this->product_model->selectProductDoubleImage($homerandedataprod->id);
					?>	
                      <div class="product-item">
                        <div class="item-inner fadeInUp">
                          <div class="product-thumbnail">
                            <div class="pr-img-area">
							<a title="<?php echo $homerandedataprod->name; ?>" href="<?php echo base_url('product/'.slugurl($homerandedataprod->name)).'/'.encodeurlval($homerandedataprod->id); ?>">
								<figure> 
									<?php if(count($product_image)>0){ ?>
									<img class="first-img" src="<?php echo base_url('uploads/product/thumbs'); ?>/<?php echo $product_image[0]->image; ?>" alt="<?php echo $homerandedataprod->name; ?>"> 
									<img class="hover-img" src="<?php echo base_url('uploads/product/thumbs'); ?>/<?php echo $product_image[1]->image; ?>" alt="<?php echo $homerandedataprod->name; ?>">
									<?php }else{ ?>
									<img class="first-img" src="http://placehold.it/264x264?text=Image not found" alt="<?php echo $homerandedataprod->name; ?>"> 
									<img class="hover-img" src="http://placehold.it/264x264?text=Image not found" alt="<?php echo $homerandedataprod->name; ?>">
									<?php } ?>
								</figure>
							</a>
                            <form method="post" action="<?php echo base_url('index.php/cart/addtocart'); ?>">
								<input type="hidden" name="pid" value="<?php echo $homerandedataprod->id; ?>" >
								<input type="hidden" name="qty" value="1" >
								<button type="button" name="addtocartbutton1" onclick="return addtocart(<?php echo $homerandedataprod->id; ?>);" class="add-to-cart-mt"> <i class="fa fa-shopping-cart"></i><span> Add to Cart</span> </button>
								<button type="submit" name="addtocartbutton" class="add-to-cart-mt1"> <i class="fa fa-bolt"></i><span> Buy Now</span> </button>
							</form>
                            </div>
                            
                          </div>
                          <div class="item-info">
                      <div class="info-inner">
                        <div class="item-title"> 
							<a title="<?php echo $homerandedataprod->name; ?>" href="<?php echo base_url('product/'.slugurl($homerandedataprod->name)).'/'.encodeurlval($homerandedataprod->id); ?>">
								<?php echo $homerandedataprod->name; ?>
							</a>
						</div>
                        <div class="item-content">
                          <div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                          <div class="price-box">
							  <?php if($homerandedataprod->spacel_price!=""){ ?>
                              <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> $<?php echo $homerandedataprod->spacel_price; ?> </span> </p>
                              <p class="old-price"> <span class="price-label">Regular Price:</span> <span class="price"> $<?php echo $homerandedataprod->price; ?> </span> </p>
							  <?php }else{ ?>
							  <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> $<?php echo $homerandedataprod->price; ?> </span> </p>
							  <?php } ?>
                          </div>
                        </div>
                      </div>
                    </div>
                        </div>
                      </div>
					  
					  <?php
					}
					?>
					  
					  
                    </div>
                  </div>
                </div>
              </div>
   
            </div></div>
 


  <!-- our clients Slider -->
 <section class="our-clients">
    <div class="container">
      <div class="slider-items-products">
        <div id="our-clients-slider3" class="product-flexslider hidden-buttons"> 
              <div class="slider-items slider-width-col6"> 
            <?php
			$data=$this->our_clint_model->selectallclint();
			foreach($data as $val)
			{
			?>
            <!-- Item -->
            <div class="item wow zoomIn"> <a href="#"><img src="<?php echo base_url('uploads/our_clint');?>/<?php echo $val->image;?>" alt="Image" class="grayscale"></a> </div>
            <!-- End Item --> 
            <?php
			}
			?>
             
            
          </div>
        </div>
      </div>
    </div>
  </section>	
<script>
// Set the date we're counting down to
var countDownDate = new Date("April 16, 2017").getTime();

// Update the count down every 1 second
var x = setInterval(function() {

    // Get todays date and time
    var now = new Date().getTime();
    
    // Find the distance between now an the count down date
    var distance = countDownDate - now;
    
    // Time calculations for days, hours, minutes and seconds
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    
    // Output the result in an element with id="demo"
    document.getElementById("timer").innerHTML =  + hours + ":"
    + minutes + ":" + seconds + "";
    
    // If the count down is over, write some text 
    if (distance < 0) {
        clearInterval(x);
        document.getElementById("timer").innerHTML = "EXPIRED";
    }
}, 1000);
	

function addtocart(pid)
{
	jQuery.ajax({
        	url: "<?php echo base_url('index.php/cart/addtocart_ajax'); ?>",
			type: "POST",
			data:   { product_id : pid},
			//beforeSend: function(){$(this).html('OKKKKKK')},
			success: function(data)
		    {
				jQuery('#cartresponsedata').html(data);
				jQuery(".mini-cart").attr("tabindex",-1).focus(500);
			},
		  	error: function() 
	    	{
				
	    	} 	        
	   });
}
</script>
  <!-- Footer -->
  <?php $this->load->view('front/layout/home-footer');?>