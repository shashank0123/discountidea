<?php $this->load->view('front/layout/header');?>  <!-- end nav --> 
  
  <!-- Breadcrumbs -->
  
  <div class="breadcrumbs">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <ul>
            <li class="home"> <a title="Go to Home Page" href="index.html">Home</a><span>&raquo;</span></li>
            <li class="category13"><strong>My Orders</strong></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <!-- Breadcrumbs End --> 
  
  <!-- Main Container -->
  <section class="main-container col1-layout">
    <div class="main container">
      <div class="account-login mb-30">
        	<div class="row">
            <div class="col-md-3">
              <div id="left-pnl" class="aside-site-left my_account_section   ">
                      
<div class="action_links">
     <?php $this->load->view('front/layout/my-account-left-sidebar'); ?>   
</div>

                <div class="sidebox-bottom"> <span> &nbsp; </span> </div>
                              </div>
            </div>
            <div class="col-md-8">
              <div class="aside-site-content my_account_section ">
                                            <div class="site-content ">
        
                 <div class="central-content"> 
        
        



<div class="cm-notification-container"></div>
            
                   
                          
    
    <div class="mainbox-container margin margin">
   <div class="mainbox-body"><div class="main_section" >
    <div class="my_order" style="display: block;">
		<div class="sub_action_links">
			<h3>Order Information  <a href="<?php echo base_url('user/myorders/'); ?>" class="pull-right">Back</a></h3>			
	    </div>
	    <div class="my_order_list table-responsive" style="padding:3px;">
            <table class="table table-bordered table-hover">
				<thead>
					<tr>
						<td class="text-left" colspan="2">Order Details</td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="text-left"> 
							<b>Order ID:</b> #<?php echo $order[0]->id; ?><br>
							<b>Date Added:</b> <?php echo date('d/m/Y',$order[0]->ord_time); ?>
						</td>
						<td class="text-left"> 
							<b>Payment Method:</b> <?php echo $order[0]->payment_method; ?><br>
						</td>
					</tr>
				</tbody>
			</table>
			<?php $billing = $this->shoppingdetail_model->billingdetail($order[0]->id); ?>
			<?php $shipping = $this->shoppingdetail_model->shippingdetail($order[0]->id); ?>
			<table class="table table-bordered table-hover">
				<thead>
					<tr>
						<td class="text-left"><b>Billing Address</b></td>
						<td class="text-left"><b>Shipping Address</b></td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="text-left">
							<?php echo $billing[0]->firstname.' '.$billing[0]->lastname; ?><br>
							<?php echo $billing[0]->mobile;?><br>
							<?php echo $billing[0]->address; ?><br>	
                            <?php echo $billing[0]->pincode?><br> 							
							<?php echo $billing[0]->city;?><br>
							<?php echo $billing[0]->state;?><br>
							<?php echo $billing[0]->country?>							
						</td>
						<td class="text-left">
							<?php echo $shipping[0]->firstname.' '.$shipping[0]->lastname; ?><br>
							<?php //echo $shipping[0]->mobile;?>
							<?php echo $shipping[0]->address; ?><br>	
                            <?php echo $shipping[0]->pincode?><br> 							
							<?php echo $shipping[0]->city;?><br>
							<?php echo $shipping[0]->state;?><br>
							<?php echo $shipping[0]->country?>
						</td>
					</tr>
				</tbody>
			</table>
			<table class="table table-bordered table-hover">
				<thead>
					<tr>
						<td class="text-left">Product Name</td>
						<td class="text-right">Price</td>
						<td class="text-right">Quantity</td>						
						<td class="text-right">Total</td>
					</tr>
				</thead>
				<tbody>
				   <?php $items = $this->shoppingdetail_model->prodcutdetail($order[0]->id); ?>
				   <?php if(count($items)>0){ ?>
				   <?php foreach($items as $item){ ?>
					<tr>
						<td class="text-left"><?php echo $item->pro_name; ?>
						<?php if(!empty($item->options) && $item->options!='0')
						{
							echo '<br>'.$item->options;
						}
						?>
						</td>
						<td class="text-right">$<?php echo $item->unit_price; ?></td>
						<td class="text-right"><?php echo $item->qty; ?></td>						
						<td class="text-right">$<?php echo $item->total_amount; ?></td>
					</tr>
				   <?php } ?> 
				   <?php } ?>
				</tbody>
				<tfoot>
					<tr>
						<td colspan="2"></td>
						<td class="text-right"><b>Sub-Total</b></td>
						<td class="text-right">$<?php echo $order[0]->total_amount; ?></td>
					</tr>
					<?php $dicount_amount = ($order[0]->total_amount*$order[0]->coupon_discount)/100; ?>
					<tr>
					  <td colspan="2"></td>
                      <td class="text-right">Applied Coupon - <strong><?php echo $order[0]->coupon_title; ?> (<?php echo $order[0]->coupon_discount; ?>%)</strong></td>
                      <td class="text-right">$<?php echo $dicount_amount; //$order[0]->total_amount; ?> </td>
                    </tr>
					<tr>
						<td colspan="2"></td>
						<td class="text-right"><b>Shipping</b></td>
						<td class="text-right">Free</td>
					</tr>
					<tr>
						<td colspan="2"></td>
						<td class="text-right"><b>Total</b></td>
						<td class="text-right">$<?php echo $order[0]->total_amount-$dicount_amount; ?></td>
					</tr>
				</tfoot>
			</table>		 		    								    				
	    </div>
		
    </div>
</div>

        
</div>
    </div>
           
                 </div> 
                   </div>
        
                    </div>
            </div>
          </div>
      </div>
     
    </div>
  </section>
  <!-- Main Container End --> 
  <!-- our clients Slider -->
  <?php $this->load->view('front/layout/footer');?>