<?php $this->load->view('front/layout/header');?>  <!-- end nav --> 
  
  <!-- Breadcrumbs -->
  
  <div class="breadcrumbs">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <ul>
            <li class="home"> <a title="Go to Home Page" href="index.html">Home</a><span>&raquo;</span></li>
            <li class="category13"><strong>My Orders</strong></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <!-- Breadcrumbs End --> 
  
  <!-- Main Container -->
  <section class="main-container col1-layout">
    <div class="main container">
      <div class="account-login mb-30">
        	<div class="row">
            <div class="col-md-3">
              <div id="left-pnl" class="aside-site-left my_account_section   ">
                      
<div class="action_links">
     <?php $this->load->view('front/layout/my-account-left-sidebar'); ?>   
</div>


                <div class="sidebox-bottom"> <span> &nbsp; </span> </div>
                              </div>
            </div>
            <div class="col-md-8">
              <div class="aside-site-content my_account_section ">
                                            <div class="site-content ">
        
                 <div class="central-content"> 
        
        



<div class="cm-notification-container"></div>
            
                   
                          
    
    <div class="mainbox-container margin margin">
   <div class="mainbox-body"><div class="main_section" style="display: block;">
    <div class="my_order" style="display: block;">
	<div class="sub_action_links">
	    <h3>My Orders</h3>
	    	</div>
		    <div class="my_order_list table-responsive" style="padding:3px;">
            <?php if(count($myorder)>0){ ?>
			<table class="table table-bordered table-hover">
				<thead>
					<tr>
						<th>Order ID</th>
						<th>Customer</th>
						<th>No. of Products</th>
						<th>Order Status</th>
						<th>Order Date</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($myorder as $order){ ?>
					<?php $items = $this->user_model->getOrderItemsByOrderId($order->id); ?>
					<?php $user = $this->user_model->selectuserby_id($order->user_id); ?>
					<tr>
						<td>#<?php echo $order->id; ?></td>
						<td><?php echo $user[0]->fname.' '.$user[0]->lname; ?></td>
						<td><?php echo count($items); ?></td>
						<td>Pending</td>
						<td><?php echo date('d/m/Y',$order->ord_time); ?></td>
						<td><a href="<?php echo base_url('user/orderview/'.$order->id); ?>" ><i class="fa fa-eye"></i></a></td>
					</tr>
					<?php } ?>
				</tbody>
			</table> 		
			<?php } ?> 		 		    								    				
	    </div>
		
    </div>
</div>

        
</div>
    </div>
           
                 </div> 
                   </div>
        
                    </div>
            </div>
          </div>
      </div>
     
    </div>
  </section>
  <!-- Main Container End --> 
  <!-- our clients Slider -->
  <?php $this->load->view('front/layout/footer');?>