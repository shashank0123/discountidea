<?php $this->load->view('blog/layout/header'); ?>
<!-- banner -->
<div class="banner1">
	
</div>
<!-- technology -->
<div class="technology-1">
	<div class="container">
		<div class="col-md-9 technology-left">
			<div class="business">
				<div class=" blog-grid2">
					<img src="<?php echo base_url('uploads/blog/'.$blogdata[0]->image); ?>" class="img-responsive" alt="">
					<div class="blog-text">
						<h5><?php echo $blogdata[0]->title; ?></h5>
						<p><?php echo $blogdata[0]->content; ?></p>				
					</div>
				</div>
				<div class="comment">
					<h3>Leave a Comment</h3>
					<div class=" comment-bottom">
						<form>
							<textarea placeholder="Message" required=""></textarea>
							<input type="submit" value="Send">
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- technology-right -->
		<div class="col-md-3 technology-right">
			<?php $this->load->view('blog/layout/right-side-bar'); ?>
		</div>
		<div class="clearfix"></div>
		<!-- technology-right -->
	</div>
</div>
<!-- technology -->
<!-- footer -->
	<div class="footer">
		<div class="container">
			<div class="col-md-4 footer-left">
				<h6>THIS LOOKS GREAT</h6>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt</p>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt consectetur adipisicing elit,</p>
			</div>
			<div class="col-md-4 footer-middle">
			<h4>Twitter Feed</h4>
			<div class="mid-btm">
				<p>Consectetur adipisicing</p>
				<p>Sed do eiusmod tempor</p>
				<a href="https://w3layouts.com/">https://w3layouts.com/</a>
			</div>
			
				<p>Consectetur adipisicing</p>
				<p>Sed do eiusmod tempor</p>
				<a href="https://w3layouts.com/">https://w3layouts.com/</a>
		
			</div>
			<div class="col-md-4 footer-right">
				<h4>Quick Links</h4>
				<li><a href="#">Eiusmod tempor</a></li>
				<li><a href="#">Consectetur </a></li>
				<li><a href="#">Adipisicing elit</a></li>
				<li><a href="#">Eiusmod tempor</a></li>
				<li><a href="#">Consectetur </a></li>
				<li><a href="#">Adipisicing elit</a></li>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
<!-- footer -->
<!-- footer-bottom -->
	
<!-- footer-bottom -->
			<div class="copyright">
				<div class="container">
					<p>� 2016 Business_Blog. All rights reserved | Template by <a href="http://w3layouts.com/">W3layouts</a></p>
				</div>
			</div>
	

</body>
</html>