<?php $this->load->view('blog/layout/header'); ?>
<!-- banner -->
<?php $banner_data = $this->admin_model->checkAdminByID(1); ?>
<a href="<?php echo $banner_data[0]->blogimageurl; ?>" target="_blank">
<div class="banner" style="background: url(<?php echo base_url('uploads/blog/'.$banner_data[0]->blogimage); ?>) no-repeat 0px 0px;"></div>
</a>
<?php $loginUserId = $this->user_model->getLoginUserVar('USER_ID'); ?>
<?php if(!empty($loginUserId)){ ?>
<div class="head-bottom">
      <div class="container">
		<?php $blod_cat = $this->blog_model->select_all_active_blogcat(); ?>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
		    <li class="active_1"><a href="<?php echo base_url('index.php/blog/myblog'); ?>">Creat Your Blog Time Line</a></li>
			<li class="active_1"><a href="http://www.publicfunda.com/index.php/user/profile" target="_blank">My Profile</a></li>
			<li class="active_1"><a href="http://www.publicfunda.com/index.php/user/todaytask" target="_blank">Click to Earn</a></li>
			<li class="active_1"><a href="http://www.publicfunda.com/index.php/user/myworkpayouts" target="_blank">My Total Earning</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
</div>
<?php } ?>
<!-- technology -->
<div class="technology-1">
	<div class="container">
		<div class="col-md-9 technology-left">
		<div class="tech-no_">
			<?php $blod_data = $this->blog_model->select_all_blog(); ?>
			<?php foreach($blod_data as $blog){ ?>
			<!-- technology-top -->
			<!--
			<div class="soci">
				<ul>
					<li><a href="#" class="facebook-1"> </a></li>
					<li><a href="#" class="facebook-1 twitter"> </a></li>
					<li><a href="#" class="facebook-1 chrome"> </a></li>
					<li><a href="#"><i class="glyphicon glyphicon-envelope"> </i></a></li>
					<li><a href="#"><i class="glyphicon glyphicon-print"> </i></a></li>
					<li><a href="#"><i class="glyphicon glyphicon-plus"> </i></a></li>
				</ul>
			</div>-->
			 <div class="tc-ch">				
					<div class="tch-img">
						<a href="<?php echo base_url('blog/details/'.$blog->id); ?>"><img src="<?php echo base_url('uploads/blog/'.$blog->image); ?>" class="img-responsive" alt=""/></a>
					</div>
					<a class="blog blue" href="<?php echo base_url('blog/details/'.$blog->id); ?>"><?php  $cat_b = $this->blog_model->select_blog_cat_byid($blog->cat_id); echo $cat_b[0]->title; ?></a>
					<h3><a href="<?php echo base_url('blog/details/'.$blog->id); ?>"><?php echo $blog->title; ?></a></h3>
					<p><?php echo substr(strip_tags($blog->content),0,455); ?></p>
					<div class="blog-poast-info">
						<ul>
							<li><i class="glyphicon glyphicon-user"> </i><a class="admin" href="#"> Admin </a></li>
							<li><i class="glyphicon glyphicon-calendar"> </i><?php echo $blog->create_date; ?></li>
							<li><i class="glyphicon glyphicon-comment"> </i><a class="p-blog" href="#">3 Comments </a></li>
							<li><i class="glyphicon glyphicon-eye-open"> </i>5 views</li>
						</ul>
					</div>
			</div>
			<div class="clearfix"></div>
			<!-- technology-top -->
			<?php } ?>
			</div>
		</div>
		<!-- technology-right -->
		<div class="col-md-3 technology-right">  
			<?php $this->load->view('blog/layout/right-side-bar'); ?>
		</div>
		<div class="clearfix"></div>
		<!-- technology-right -->
	</div>
</div>
<!-- technology -->
<!-- footer -->
	<div class="footer">
		<div class="container">
			<div class="col-md-4 footer-left">
				<h6>THIS LOOKS GREAT</h6>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt</p>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt consectetur adipisicing elit,</p>
			</div>
			<div class="col-md-4 footer-middle">
			<h4>Twitter Feed</h4>
			<div class="mid-btm">
				<p>Consectetur adipisicing</p>
				<p>Sed do eiusmod tempor</p>
				<a href="https://w3layouts.com/">https://w3layouts.com/</a>
			</div>
			
				<p>Consectetur adipisicing</p>
				<p>Sed do eiusmod tempor</p>
				<a href="https://w3layouts.com/">https://w3layouts.com/</a>
		
			</div>
			<div class="col-md-4 footer-right">
				<h4>Quick Links</h4>
				<li><a href="#">Eiusmod tempor</a></li>
				<li><a href="#">Consectetur </a></li>
				<li><a href="#">Adipisicing elit</a></li>
				<li><a href="#">Eiusmod tempor</a></li>
				<li><a href="#">Consectetur </a></li>
				<li><a href="#">Adipisicing elit</a></li>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
<!-- footer -->
<!-- footer-bottom -->
	
<!-- footer-bottom -->
			<div class="copyright">
				<div class="container">
					<p>� 2016 Business_Blog. All rights reserved | Template by <a href="http://w3layouts.com/">W3layouts</a></p>
				</div>
			</div>
	

</body>
</html>