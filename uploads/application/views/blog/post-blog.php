<?php $this->load->view('blog/layout/header'); ?>
<!-- banner -->
<div class="banner1">
	
</div>
<!-- technology -->
<div class="technology-1">
	<div class="container">
		<div class="col-md-9 technology-left">
			<div class="business">
				
				<div class="comment">
					<h3>Post a Blog</h3>
					<div class=" comment-bottom">
						<form method="post" enctype='multipart/form-data'>
							<select name="cat_id" required style="color:#000;">
								<option value="" >-- Select Categoty --</option>
								<?php foreach($CATEGORY as  $cat){ ?>								    
									<option value="<?php echo $cat->id; ?>" ><b><?php echo $cat->title; ?></b></option>
								<?php } ?>
							</select>
							<input type="text" name="title" placeholder="title" required>
							<input type="file"  name="image" required>
							<textarea placeholder="Content" name="content" required></textarea>
							<input type="submit" value="Submit" name="savedata">
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- technology-right -->
		<div class="col-md-3 technology-right">
			<?php $this->load->view('blog/layout/right-side-bar'); ?>
		</div>
		<div class="clearfix"></div>
		<!-- technology-right -->
	</div>
</div>
<!-- technology -->
<!-- footer -->
	<div class="footer">
		<div class="container">
			<div class="col-md-4 footer-left">
				<h6>THIS LOOKS GREAT</h6>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt</p>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt consectetur adipisicing elit,</p>
			</div>
			<div class="col-md-4 footer-middle">
			<h4>Twitter Feed</h4>
			<div class="mid-btm">
				<p>Consectetur adipisicing</p>
				<p>Sed do eiusmod tempor</p>
				<a href="https://w3layouts.com/">https://w3layouts.com/</a>
			</div>
			
				<p>Consectetur adipisicing</p>
				<p>Sed do eiusmod tempor</p>
				<a href="https://w3layouts.com/">https://w3layouts.com/</a>
		
			</div>
			<div class="col-md-4 footer-right">
				<h4>Quick Links</h4>
				<li><a href="#">Eiusmod tempor</a></li>
				<li><a href="#">Consectetur </a></li>
				<li><a href="#">Adipisicing elit</a></li>
				<li><a href="#">Eiusmod tempor</a></li>
				<li><a href="#">Consectetur </a></li>
				<li><a href="#">Adipisicing elit</a></li>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
<!-- footer -->
<!-- footer-bottom -->
	
<!-- footer-bottom -->
			<div class="copyright">
				<div class="container">
					<p>� 2016 Business_Blog. All rights reserved | Template by <a href="http://w3layouts.com/">W3layouts</a></p>
				</div>
			</div>
	

</body>
</html>