<?php $this->load->view('blog/layout/header'); ?>
<!-- banner -->
<div class="banner1">
	
</div>
<!-- technology -->
<div class="technology-1">
	<div class="container">
		<div class="col-md-9 technology-left">
		<div class="tech-no_">
			<?php //$blod_data = $this->blog_model->select_all_blog(); ?>
			<?php foreach($blogdata as $blog){ ?>			
			 <div class="vide-1">
				<div class="rev">
					<div class="rev-img">
						<a href="<?php echo base_url('blog/details/'.$blog->id); ?>"><img src="<?php echo base_url('uploads/blog/'.$blog->image); ?>" class="img-responsive" alt=""/></a>
					</div>
					<div class="rev-info">
						<h3><a href="<?php echo base_url('blog/details/'.$blog->id); ?>"><?php echo $blog->title; ?></a></h3>
						<p><?php echo substr(strip_tags($blog->content),0,150); ?></p>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="clearfix"></div>
			<!-- technology-top -->
			<?php } ?>
			</div><div class="clearfix"></div>
		</div>
		<!-- technology-right -->
		<div class="col-md-3 technology-right">
			<?php $this->load->view('blog/layout/right-side-bar'); ?>
		</div>
		<div class="clearfix"></div>
		<!-- technology-right -->
	</div>
</div>
<!-- technology -->
<!-- footer -->
	<div class="footer">
		<div class="container">
			<div class="col-md-4 footer-left">
				<h6>THIS LOOKS GREAT</h6>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt</p>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt consectetur adipisicing elit,</p>
			</div>
			<div class="col-md-4 footer-middle">
			<h4>Twitter Feed</h4>
			<div class="mid-btm">
				<p>Consectetur adipisicing</p>
				<p>Sed do eiusmod tempor</p>
				<a href="https://w3layouts.com/">https://w3layouts.com/</a>
			</div>
			
				<p>Consectetur adipisicing</p>
				<p>Sed do eiusmod tempor</p>
				<a href="https://w3layouts.com/">https://w3layouts.com/</a>
		
			</div>
			<div class="col-md-4 footer-right">
				<h4>Quick Links</h4>
				<li><a href="#">Eiusmod tempor</a></li>
				<li><a href="#">Consectetur </a></li>
				<li><a href="#">Adipisicing elit</a></li>
				<li><a href="#">Eiusmod tempor</a></li>
				<li><a href="#">Consectetur </a></li>
				<li><a href="#">Adipisicing elit</a></li>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
<!-- footer -->
<!-- footer-bottom -->
	
<!-- footer-bottom -->
			<div class="copyright">
				<div class="container">
					<p>� 2016 Business_Blog. All rights reserved | Template by <a href="http://w3layouts.com/">W3layouts</a></p>
				</div>
			</div>
	

</body>
</html>