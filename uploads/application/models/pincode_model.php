<?php
class Pincode_model extends CI_Model
{
	function selectAllPincode()
	{
		$data= $this->db->get("tbl_pincode");
		return $data->result();		
	}
	
	function checkpincode($ids,$pin)
	{
		$this->db->where_in('id', $ids);
		$this->db->where('pincode', $pin);
		$data= $this->db->get("tbl_pincode");
		return $data->result();		
	}
	
	function selectAllActivePincode()
	{
		$data= $this->db->get("tbl_pincode");
		return $data->result();		
	}
	
	function selectPincodeById($id)
	{
		$this->db->where('id', $id);
		$data= $this->db->get('tbl_pincode');
		return $data->result();
	}
	
	function insert($data)
	{
		$result= $this->db->insert('tbl_pincode', $data);
		return $result;
	}
	
	function update($id,$data)
	{
		$this->db->where('id', $id);
		return $this->db->update('tbl_pincode',$data);
	}
	
	function delete($id)
	{
		$this->db->where('id', $id);
		return $this->db->delete('tbl_pincode');
	}
}
?>