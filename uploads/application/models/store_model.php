<?php
class Store_model extends CI_Model
{
	function selectAllStore()
	{
		$data= $this->db->get("tbl_store");
		return $data->result();		
	}
	
	function selectAllActiveStore()
	{
		$this->db->where('status', 1);
		$data= $this->db->get("tbl_store");
		return $data->result();		
	}
	
	
	function selectstorebyid($id)
	{
		$this->db->where('id', $id);
		$data= $this->db->get('tbl_store');
		return $data->result();
	}
	
	function selectstoreby_uid($id)
	{
		$this->db->where('uid', $id);
		$data= $this->db->get('tbl_store');
		return $data->result();
	}
	
	function insert_store($data)
	{
		$result= $this->db->insert('tbl_store', $data);
		return $result;
	}
	
	function update_store($id,$data)
	{
		$this->db->where('id', $id);
		return $this->db->update('tbl_store',$data);
	}
	
	function delete_store($id)
	{
		$this->db->where('id', $id);
		return $this->db->delete('tbl_store');
	}
	
	function insert_store_image($data)
	{
		$result= $this->db->insert('tbl_store_image', $data);
		return $result;
	}
	
	function select_store_image($id)
	{
		$this->db->where('store_id', $id);
		$data= $this->db->get('tbl_store_image');
		return $data->result();
	}
	
	function deleteStoreImageById($id)
	{
		$this->db->where('id', $id);
		return $this->db->delete('tbl_store_image');
	}
}
?>