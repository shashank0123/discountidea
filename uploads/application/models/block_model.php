<?php
class Block_model extends CI_Model
{	
	function selectallstaticblock()
	{		
		$data=$this->db->get('tbl_block');
		return $data->result();
		
	}
	function insertdata($data)
	{		
		$this->db->insert('tbl_block',$data);		
	}
	
	function selectblockbyid($id)
	{
		$this->db->where('id',$id);
		$data=$this->db->get('tbl_block');
		return $data->result();
		
	}
	function updatedata($id,$data)
	{		
		$this->db->where('id',$id);
		$res=$this->db->update('tbl_block',$data);		
	}
}



?>