<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Pincode extends CI_Controller 
{
	function listing()
	{		
		$data['PINCODEDATA']= $this->pincode_model->selectAllPincode();
		$this->load->view('admin/pincode/list',$data);
	}
	function add()
	{		
		if(isset($_POST['savedata']))
		{						
			$data['pincode'] = $this->input->post('pincode');
			$data['day'] = $this->input->post('day');
		
			$this->pincode_model->insert($data);			
			$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully saved.</div>');
			redirect('index.php/pincode/listing');			
		}
		$this->load->view('admin/pincode/add');
	}
	function edit()
	{		
		$args=func_get_args();
		if(isset($_POST['updatedata']))
		{			
			$data['pincode'] = $this->input->post('pincode');
			$data['day'] = $this->input->post('day');
			$this->pincode_model->update($args[0],$data);
			$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully updated.</div>');
			redirect('index.php/pincode/listing');
		}
		$data['EDITPINCODE']= $this->pincode_model->selectPincodeById($args[0]);
		$this->load->view('admin/pincode/edit',$data);
	}
	
	function delete()
	{
		$args=func_get_args();		
		$this->pincode_model->delete($args[0]);
		$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully deleted.</div>');
		redirect('index.php/pincode/listing');
	}
    
	function getcartitempincode()
	{
		$cart_data = $this->cart->contents();
		$available_pins = "";
		foreach($cart_data as $items)
		{			
			$pid = $items['id'];
			$product = $this->product_model->selectProductById($pid);
			if(!empty($product[0]->pincode_ids))
			{
				$available_pins .= $product[0]->pincode_ids;
			}				
		}
		if($available_pins!="")
		{	
			return explode(",",$available_pins);
		}
		else
		{
			return array();
		}	
	}
	function checkpincodeoncheckout()
	{
		$vailable_pins = $this->getcartitempincode();
		$pincode = $_POST['pincode'];	
		$data = $this->pincode_model->checkpincode($vailable_pins,$pincode);
		if(count($data)>0)
		{
			//echo '<div class="alert alert-success">Delivery in '.$data[0]->day.' days.</div>';
		}
		else
		{
			echo '<div class="alert alert-danger">Delivery not available in this pincode.</div>';
		}
	}
	function checkpincode()
	{		
		//print_r($_POST);
		$pincode  = $_POST['pincode'];
		$pro_pin = explode(",",$_POST['pro_pin']);
		$data = $this->pincode_model->checkpincode($pro_pin,$pincode);
		if(count($data)>0)
		{
			echo '<div class="alert alert-success">Delivery in '.$data[0]->day.' days.</div>';
		}
		else
		{
			echo '<div class="alert alert-danger">Delivery not available in this area.</div>';
		}
	}
}