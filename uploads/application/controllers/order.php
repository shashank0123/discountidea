<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Order extends CI_Controller 
{
	function listing()
	{		
	
		if(isset($_POST['submitstatus']))
		{
			$status = $_POST['status'];
			
			if(!empty($status) && !empty($_POST['order_ids'][0]))
			{
				$ids = $_POST['order_ids'];
				foreach($ids as $id)
				{
					$odata['status'] = $status;
					$this->orderdetail_model->update_order($id,$odata);
				}
				$this->session->set_flashdata('message','<div class="alert alert-success">Status has been successfully changed.</div>');
				redirect('order/listing');	
			}	
		}
		$data['orderdetail']= $this->orderdetail_model->selectorder();
		$this->load->view('admin/order/listing',$data);
	}
	
	
	function report()
	{		
		$data['orderdetail']= $this->orderdetail_model->selectorder();
		$this->load->view('admin/order/order-report',$data);
	}
	
	
	function details()
	{		
		$args=func_get_args();
		$data['order']= $this->shoppingdetail_model->selectorder($args[0]);
		$this->load->view('admin/order/order-details',$data);
	}
		
	function invoice()
	{		
		$args=func_get_args();
		$data['shpping']= $this->shoppingdetail_model->selectorder($args[0]);
		$this->load->view('admin/order/invoice',$data);
	}	
	
	function success()
	{
		$this->load->view('front/order-success');
	}
}