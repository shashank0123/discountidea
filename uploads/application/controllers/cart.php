<?php 
class cart extends CI_Controller
{
	function index()
	{
		if(isset($_POST['applycounponcode']))
        {
			$code = $this->input->post('coupon_code');
			if(!empty($code))
			{
				$counpondata = $this->offer_model->selectCouponCode($code);
				if(count($counpondata)==1)
				{					
					$code_data=array('coupon' => array($counpondata[0]->couponcode,$counpondata[0]->discount));
					$this->session->set_userdata($code_data);
					
					$this->session->set_flashdata("cmessage","<div class='alert alert-success'>coupon code successfully apply.</div>");
					redirect('index.php/cart#couponcode1');
				}
				else
				{
					$this->session->set_flashdata("cmessage","<div class='alert alert-danger'>Invalid coupon code.</div>");
					redirect('index.php/cart#couponcode1');
				}
			}
			else
			{
				$this->session->set_flashdata("cmessage","<div class='alert alert-danger'>Please enter coupon code.</div>");
				redirect('index.php/cart#couponcode1');
			}	
		}
		
		$data['cart_data'] = $this->cart->contents();
		$this->load->view('front/cart',$data);
	}
	
	function addtocart()
	{
		if(isset($_POST['addtocartbutton']))
		{			
			
			$product_id = $this->input->post('pid');	
			$qty = $this->input->post('qty');
			if(isset($_POST['size_id'])){ $size = $this->input->post('size_id'); }else{ $size=0; }	
            if(isset($_POST['color_id'])){ $color = $this->input->post('color_id'); }else{ $color=0; }	
			$product = $this->product_model->selectProductById($product_id);
			$product_qty = $product[0]->product_stock;
			if($product_qty>=$qty)
			{	
				if($product[0]->spacel_price!="")
				{
					$price = $product[0]->spacel_price;
				}
				else
				{
					$price = $product[0]->price;
				}
				unset($_POST['pid']);
				unset($_POST['qty']);
				unset($_POST['pincode']);
				unset($_POST['pro_pin']);
				unset($_POST['addtocartbutton']);
				$data = array('id'=> $product[0]->id, 'qty'=> $qty, 'options'=> $_POST, 'price'=> $price, 'name' => $product[0]->name,);
				$this->cart->insert($data);
				$this->session->set_flashdata("message","<div class='alert alert-success'><strong>".$product[0]->name."</strong> successfully added to your cart.</div>");
				redirect('cart/');
			}
			else
			{
				$this->session->set_flashdata("stockmessage","<div class='alert alert-danger'><strong></strong> Only ".$product_qty." Quantity in stock.</div>");
				redirect($_SERVER['HTTP_REFERER'].'#prostock');
			}	
			
		}
	}

function addtocart_ajax()
	{
		error_reporting(0);
		if(isset($_POST['product_id']))
		{	
			$product_id = $_POST['product_id'];
			$qty = 1;
			$product = $this->product_model->selectProductById($product_id);
			if($product[0]->spacel_price!="")
			{
				$price = $product[0]->spacel_price;
			}
			else
			{
				$price = $product[0]->price;
			}			
			$cart_data = array('id'=> $product[0]->id, 'qty'=> $qty, 'options'=>0, 'price'=> $price, 'name' => $product[0]->name,);
			$this->cart->insert($cart_data);
		}	
		$this->load->view('front/layout/add-to-cart-ajax');
		//print_r($this->cart->contents());
	}
		
	function remove($id)
	{
		$args = func_get_args($id);
		$data= array('rowid' =>$args[0], 'qty' =>0);
		$this->cart->update($data);
		$this->session->set_flashdata("message","<div class='alert alert-success'>item successfully removed to your cart.</div>");
		redirect('cart/');
	}
	function update()
	{
		if(isset($_POST['updatecart']))
		{
			if(count($_POST['rowid'])>0)
			{
				foreach($_POST['rowid'] as $key => $value)
				{
					if($_POST['qty'][$key]!="0")
					{	
						$data= array('rowid' =>$_POST['rowid'][$key], 'qty' =>$_POST['qty'][$key]);
						$this->cart->update($data);
						$this->session->set_flashdata("message","<div class='alert alert-success'>Cart has been successfully updated.</div>");
					}	
				}	
			}
			redirect('cart/');
		}
		else
		{
			redirect('');
		}		
	}
			
	function emptycart($data="")
	{
		$this->session->unset_userdata('SUBSIDOLD');
		$this->session->unset_userdata('coupon');
		$this->cart->destroy();
		redirect('cart/');
	}
	
	function checkout()
	{
		$user_id = $this->user_model->getLoginUserVar('USER_ID');
		$cart_data = $this->cart->contents();
		
		$coupon_data = $this->session->userdata('coupon');
		if(!empty($coupon_data) && count($coupon_data)>0)
		{
			$coupon_title = $coupon_data[0];
			$coupon_discount = $coupon_data[1];
		}
		else
		{
			$coupon_title = 0;
			$coupon_discount = 0;
		}
		
		if(isset($_POST['checkout']))
		{
			$order['user_id'] = $this->session->userdata('id');
			$order['payment_method']=$this->input->post('paymentmethod');
			$order['ord_date']=date('Y-m-d H:i:s');
			$order['ord_time']=time();
			$order['coupon_title']=$coupon_title;
			$order['coupon_discount']=$coupon_discount;
			$order['total_amount']=$this->cart->total();
			$order['ip_address']=$this->input->post('ipaddress');
			$order=$this->checkout_model->insertorder($order);
			$ordr_id=mysql_insert_id();
			if(!empty($ordr_id))
			{
				foreach($cart_data as $items)
				{
					$item['order_id']=$ordr_id;
					$item['pro_id']=$items['id'];
					$item['user_id']=$this->session->userdata('id');
					$item['pro_name']=$items['name'];
					$item['unit_price']=$items['price'];  
					$item['total_amount']=$items['subtotal'];
					$item['qty']=$items['qty'];
					$item['options']= $this->get_option_httml($items['options']);
					$item['order_date']= date('Y-m-d');
					
					$this->checkout_model->inseritem($item);					
				}
				
				$data['ord_id']=$ordr_id;
				$data['user_id']=$this->session->userdata('id');
				$data['country']=$this->input->post('countryb');
				$data['firstname']=$this->input->post('firstnameb');
				$data['lastname']=$this->input->post('lastnameb');
				$data['address']=$this->input->post('addb');
				$data['city']=$this->input->post('cityb');
				$data['state']=$this->input->post('stateb');
				$data['pincode']=$this->input->post('pincodeb');
				$data['email']=$this->input->post('emailb');
				$data['mobile']=$this->input->post('phoneb');
				$data['add_date']=date('Y-m-d H:i:s');
				$billing=$this->checkout_model->insert_data($data);
				$id=mysql_insert_id();
				if(!empty($id))
				{
					$data1['ord_id']=$ordr_id;
					$data1['user_id']=$this->session->userdata('id');
					$data1['country']=$this->input->post('countrys');
					$data1['firstname']=$this->input->post('firstnames');
					$data1['lastname']=$this->input->post('lastnames');
					$data1['email']=$this->input->post('emails');
					$data1['address']=$this->input->post('adds');
					$data1['city']=$this->input->post('citys');
					$data1['state']=$this->input->post('states');
					$data1['pincode']=$this->input->post('pincodes');
					$data1['add_date']=date('Y-m-d H:i:s');
					$this->checkout_model->insertshipping($data1);
				}

			}
			$this->emptycartoncheckoutpage();
			
		}
		$data['LOGIN_USER'] = $this->user_model->selectuserby_id($user_id);
		$data['cart_data'] = $cart_data;
		$this->load->view('front/checkout',$data);
	} 
	
	function emptycartoncheckoutpage($data="")
	{
		$this->session->unset_userdata('SUBSIDOLD');
		$this->session->unset_userdata('coupon');
		$this->cart->destroy();
		redirect('index.php/order/success');
	}
	
	function get_option_httml($option_array)
	{
		$html = "";
		if(!empty($option_array) && count($option_array)>0)
		{
			foreach($option_array as $option => $key)
			{
				$html .= $this->filters_model->getSelectedOption($key);
			}
		}
		
		return $html;		
	}	
	
	
	
} 
?>