<?php
if ( !defined('BASEPATH')) exit('No direct script access allowed');
class Vendor extends CI_Controller
{
		
	function create_account()
	{		
		if(isset($_POST['createbutn']))
		{			
			$email=$this->input->post('email');
			$user_data=$this->user_model->selectuserby_email($email);
			$checkfile= count($user_data);
			if($checkfile >0)
			{				
				$this->session->set_flashdata("message1","<br><div class='alert alert-danger'><strong>".$email."</strong> Email address already exists choose a different one.</div>");
				redirect('user/create_account');
			}
			$token=md5(time().$email);
			$password=$this->input->post('password');
            $data['fname']=$this->input->post('fname');
			$data['mobile']=$this->input->post('mobile');

			$data['email']= $email;
			$data['password']= $password;
			$data['status']= 1;
			$data['vender_type']= 1;
			$data['activate_token']=$token;
			$insertdata=$this->user_model->insert_user($data);
			if($insertdata)
			{
				$this->email->set_newline("\r\n");
				$this->email->set_mailtype("html");
				$this->email->to($email);
				$this->email->from(ADMIN_EMAIL,'Discount Idea');
			    $this->email->subject('Mail Confirmation');
				$message = "Dear ".$this->input->post('email').",<br>";
				$message .= "Thank you for registering with us.";
				$message .= "Your account has been created, you can login with the following credentials.<br>";
				$message .= "------------------------<br>";
				$message .= "USER ID: $email <br>";
				$message .= "PASSWORD: $password <br>";
				$message .= "------------------------<br><br>";
				$message .= "Thank you <br> Discount Idea support team";
				$this->email->message($message);
				$this->email->send();
				
			}
			$this->session->set_flashdata("message1","<br><div class='alert alert-success'><h5>Thanks For Registering With us! </h5></div>");
			redirect('vendor/create_account');
			
		}
		
		$this->load->view('front/login-page');
	}
	
	function loginuser()
	{
		$user_login_id = $this->session->userdata('USER_ID');
		if(!empty($user_login_id))
		{
			redirect('index.php/user/profile');
		}
		
		if(isset($_POST['buttonlogin']))
		{
			//echo "dsfdsfdsjfhdskj";die;
			$email=$this->input->post('email1');
			$password=$this->input->post('password1');
			
			if(!empty($email) && !empty($password))
			{
				$data=$this->user_model->user_login($email,$password);
				if(count($data)>0)
				{
					//echo $data[0]->status; die();
					if($data[0]->status==1)
					{
						$login_data=array('id'=>$data[0]->id,'USER_ID'=>$data[0]->id,'email'=>$data[0]->email,'user_type'=>$data[0]->vender_type,'logged_in'=>true);
						$this->session->set_userdata($login_data);
						if(isset($_GET['backurl']) && $_GET['backurl']!="")
						{
							redirect($_GET['backurl']);
						}
						else
						{
							redirect('index.php/vendor/home');
						}
					}
					else
					{
						$this->session->set_flashdata('message1','<div class="alert alert-danger">This user is inactive. Please contact us regarding this account.</div>');
						redirect('index.php/vendor/loginuser');
					}
				}
				else
				{				
					$this->session->set_flashdata('message1','<div class="alert alert-danger">Invalid username or password !</div>');
					redirect('index.php/vendor/loginuser');
				}			
			}
		}	
		$this->load->view('front/login-page');
	}
	
	
	public function forgotpassword()
	{
		if(isset($_POST['forgotbutn']))
		{
			//echo "sdfkdsfhdskj";die;
				$email=$this->input->post('email');
				//echo $email;die;
			if(!empty($email))
			{
				$userdata=$this->user_model->selectuserby_email($email);
				if(count($userdata)>0)
				{
					$password_token=sha1(time().$email);
					$data['activate_token']=$password_token;
					$upd_token = $this->user_model->update($userdata[0]->id,$data);
					if($upd_token)
					{
						$this->email->set_newline("\r\n");
						$this->email->set_mailtype("html");
						$this->email->to($userdata[0]->email);
						$this->email->from(ADMIN_EMAIL, 'Discount Idea');
						$this->email->subject('Discount Idea Forgot Password');
						$msg = "Dear ".$userdata[0]->name.",<br>Please click bellow link to reset your password.<br>"; 
						//$msg .="Please click on the following link: <br>";
						$msg .= "<a href='".base_url('vendor/reset_password/'.$password_token)."'><button style='background-color:#4ed4ff; color:#fff; padding:5px; border:1px solid black;'>Click Here For Reset Password</button></a><br><br>";
						$msg .= "Thank you,<br>";
						$msg .= "Discount Idea Support Team";
						$this->email->message($msg);
						$this->email->send();
						$this->session->set_flashdata("message","<div class='alert alert-success'>If there is an account associated with $email you will receive an email with a link to reset your password..</div>");
						redirect('vendor/forgotpassword');			
					}
				}
				else
				{
					$this->session->set_flashdata("message","<div class='alert alert-danger'>Please enter a valid email address.</div>");
					redirect('vendor/forgotpassword');
				}


			}
			else
			{
				$this->session->set_flashdata("message","<div class='alert alert-danger'>Please enter a valid email address.</div>");
				redirect('vendor/forgotpassword');
			}	
			}		
		$this->load->view('front/forgotpassword');
		
	}
	
	function reset_password()
	{
		$loginUserId = $this->user_model->getLoginUserVar('USER_ID');
		if($loginUserId!=""){ redirect('user/profile');  }
		
		$args = func_get_args();
		if(count($args)>0)
		{
			$token = $args[0];
			$userdata = $this->user_model->checkUserTokenForgotPassword($token);
			if(count($userdata)>0)
			{
				if(isset($_POST['resetPassword']))
				{
					$password=$this->input->post('password');
					$cpassword=$this->input->post('cpassword');
					if($password!=$cpassword)
					{
						$this->session->set_flashdata("message","<div class='alert alert-danger'>New Password and Confirm Password Do Not Matched!.</div>");
						redirect('vendor/reset_password/'.$args[0]);
					}
					else
					{
						$data['password']= $password;
						$data['activate_token']='';
						$this->user_model->update($userdata[0]->id,$data);					
						$this->session->set_flashdata("message","<div class='alert alert-success'>Password successfully changed.</div>");
						redirect('vendor/login');
					}	
				}		
				
			}
			else
			{
				$this->session->set_flashdata("message","<div class='alert alert-danger'>The token is invalid or expired!.</div>");
				redirect('vendor/forgot_password');
			}		
		}
		else
		{
				$this->session->set_flashdata("message","<div class='alert alert-danger'>The token is invalid or expired!.</div>");
				redirect('vendor/forgot_password');
		}
		$this->load->view('front/respassword');
	}
	
	
	function manage_store()
	{
		$loginUserId = $this->user_model->getLoginUserVar('USER_ID');
		if(isset($_POST["createstore"]))
		{	
		
			if($_FILES['logo']['name']!="")
			{
				$logo = createUniqueFile($_FILES['logo']['name']);
				$file_temp = $_FILES['logo']['tmp_name'];
				$path = 'uploads/store/'.$logo;				
				move_uploaded_file($file_temp,$path);				
			}
			else
			{
				$logo = $this->input->post("old_logo");
			}
			if($_FILES['banner']['name']!="")
			{
				$banner = createUniqueFile($_FILES['banner']['name']);
				$file_temp = $_FILES['banner']['tmp_name'];
				$path = 'uploads/store/'.$banner;				
				move_uploaded_file($file_temp,$path);				
			}
			else
			{
				$banner = $this->input->post("old_banner");
			}
			$store_id = $this->input->post("store_id");
			$data1["title"]=$this->input->post("title");
			$data1["logo"]=$logo;
			$data1["banner"]=$banner;
			$data1["content"]=$this->input->post("content");
			$this->session->set_flashdata('message','<div class="alert alert-success">store has been successsully updated</div>'); 			//$this->db->insert("tbl_product",$data);

			$insert=$this->store_model->update_store($store_id,$data1);			
			redirect("index.php/vendor/manage_store/");
		}
		$data['store']=$this->store_model->selectstoreby_uid($loginUserId);		
		$this->load->view('front/vendor/edit-store',$data);
	}
	
	
	function bussiness()
	{
		$loginUserId = $this->user_model->getLoginUserVar('USER_ID');
		if(isset($_POST["bussinessdetails"]))
		{	
			$data1["company_type"]=$this->input->post("company_type");
			$data1["company_name"]=$this->input->post("company_name");
			$data1["tin_vat"]=$this->input->post("tin_vat");			
			$data1["tan"]=$this->input->post("tan");
			$data1["company_pan"]=$this->input->post("company_pan");
			$data1["gstin"]=$this->input->post("gstin");			
			
			$this->session->set_flashdata('message','<div class="alert alert-success">bussiness details has been successsully updated</div>'); 			//$this->db->insert("tbl_product",$data);

			$insert = $this->user_model->update($loginUserId,$data1);			
			redirect("index.php/vendor/bussiness/");
		}
		$data['user']=$this->user_model->selectuserby_id($loginUserId);		
		$this->load->view('front/vendor/bussiness-details',$data);
	}
	
	function bank()
	{
		$loginUserId = $this->user_model->getLoginUserVar('USER_ID');
		if(isset($_POST["bankdetails"]))
		{	
			if($_FILES['passbook']['name']!="")
			{
				$passbook = createUniqueFile($_FILES['passbook']['name']);
				$file_temp = $_FILES['passbook']['tmp_name'];
				$path = 'uploads/vendor/'.$passbook;				
				move_uploaded_file($file_temp,$path);				
			}
			else
			{
				$passbook = $this->input->post("old_passbook");
			}
			$data1["passbook"]=$passbook;
			$data1["account_no"]=$this->input->post("account_no");			
			$data1["account_holder"]=$this->input->post("account_holder");
			$data1["ifsc_code"]=$this->input->post("ifsc_code");
			$data1["bank_name"]=$this->input->post("bank_name");
			$data1["bank_branch"]=$this->input->post("bank_branch");
			$data1["bank_city"]=$this->input->post("bank_city");
			$data1["bank_state"]=$this->input->post("bank_state");			
			
			$this->session->set_flashdata('message','<div class="alert alert-success">bank details has been successsully updated</div>'); 			//$this->db->insert("tbl_product",$data);

			$insert = $this->user_model->update($loginUserId,$data1);			
			redirect("index.php/vendor/bank/");
		}
		$data['user']=$this->user_model->selectuserby_id($loginUserId);
		$this->load->view('front/vendor/bank-details',$data);
	}
	
	function store()
	{
		$args = func_get_args(); 		
		$data['categoryimages']=$this->store_model->select_store_image($args[1]);
		$storedata = $this->store_model->selectstorebyid($args[1]);		
		$data['productdata']=$this->product_model->selectProductByVedorID($storedata[0]->uid);		
		$data['store']= $storedata; //$this->store_model->selectstorebyid($args[1]);		
		$this->load->view('front/vendor/store',$data);
	}
	
	function allstore()
	{
		$data['store']= $this->store_model->selectAllStore();		
		$this->load->view('admin/store/list',$data);
	}
	
	function store_edit_admin()
	{
		$args = func_get_args(); 
		
		if(isset($_POST["createstore"]))
		{	
		
			if($_FILES['logo']['name']!="")
			{
				$logo = createUniqueFile($_FILES['logo']['name']);
				$file_temp = $_FILES['logo']['tmp_name'];
				$path = 'uploads/store/'.$logo;				
				move_uploaded_file($file_temp,$path);				
			}
			else
			{
				$logo = $this->input->post("old_logo");
			}
			//$store_id = $this->input->post("store_id");
			$data1["title"]=$this->input->post("title");
			$data1["logo"]=$logo;
			$data1["content"]=$this->input->post("content");
			$data1["status"]=$this->input->post("status");
			$this->session->set_flashdata('message','<div class="alert alert-success">store has been successsully updated</div>'); 			//$this->db->insert("tbl_product",$data);

			$insert=$this->store_model->update_store($args[0],$data1);			
			redirect("index.php/vendor/allstore/");
		}
		
		if(isset($_POST['uploadfiles']))
		{	
			if(count($_FILES)>0)
			{
				$filesCount = count($_FILES['image']['name']);
				for($i = 0; $i < $filesCount; $i++)
				{						
					$name = $_FILES['image']['name'][$i];
					$tmpname1 = $_FILES['image']['tmp_name'][$i];
					$exten=explode(".",$_FILES['image']['name'][$i]);
					$exten=$exten[1];	
					$imagename=time().rand(1,10000).".".$exten;
					move_uploaded_file($tmpname1,'uploads/store/'.$imagename);									
					$datasd["store_id"]=$args[0];	  					
					$datasd["image"] = $imagename;
					$datasd["image_url"] = $_POST['image_url'][$i];
					$this->store_model->insert_store_image($datasd);
				}
			}
			$this->session->set_flashdata('message1','<div class="alert alert-success">image has been successfully uploaded</div>');
			redirect("index.php/vendor/store_edit_admin/".$args[0]);
		}			
		
		$data['LISTCATEGORYIMAGE']=$this->store_model->select_store_image($args[0]);
		$data['storedata']= $this->store_model->selectstorebyid($args[0]);			
		$this->load->view('admin/store/edit-store',$data);
	}
	
	function store_active()
	{
		$args = func_get_args(); 
		if($args[0]=='inactive')
		{
			$status = '0';
		}
		else
		{
			$status = '1';
		}
		//echo $status; die;
		$data1["status"]= $status;		 
		$insert=$this->store_model->update_store($args[1],$data1) ;	
        $this->session->set_flashdata('message','<div class="alert alert-success">store has been successsully updated</div>');		
		redirect("index.php/vendor/allstore/");
	}
	public function deletestoreimages($id)
	{
		$args = func_get_args();
		$this->store_model->deleteStoreImageById($args[0]);
		$this->session->set_flashdata('message1','<div class="alert alert-success">image has been successsully Deleted  </div>'); 
		redirect("index.php/vendor/store_edit_admin/".$args[1]);
    }
	
	public function addfilteroption()
	{
		$this->load->model('filters_model');   
		$data['p_id'] = $this->input->post('filter_cat');
		$data['title'] = $this->input->post('filter_val');
		$data['type'] = 'option';
		$this->filters_model->insert_filters($data);
		//print_r($_POST);
		//$args = func_get_args();
    }
	
	function manage_blog()
	{
		$args = func_get_args(); 		
		$loginUserId = $this->user_model->getLoginUserVar('USER_ID');
		$data['blogdata']=$this->blog_model->select_blog_by_uid($loginUserId);		
		$this->load->view('front/vendor/blog',$data);
	}
	
	function add_blog()
	{
		$args = func_get_args(); 		
		$loginUserId = $this->user_model->getLoginUserVar('USER_ID');
		if(isset($_POST['savedata']))
		{			
			if($_FILES['image']['name']!="")
			{
				$file = createUniqueFile($_FILES['image']['name']);
				$file_temp = $_FILES['image']['tmp_name'];
				$path = 'uploads/blog/'.$file;				
				move_uploaded_file($file_temp,$path);				
			}
			else
			{
				$file = "";
			}
			$data['image'] = $file;
			$data['cat_id'] = $this->input->post('cat_id');
			$data['uid'] = $loginUserId;
			$data['title'] = $this->input->post('title');
			$data['content'] = $this->input->post('content');	
			$data['status'] = 1;//$this->input->post('status');
			$data['create_date'] = date('Y-m-d');
			
			$this->blog_model->insert($data);			
			$this->session->set_flashdata('message','<div class="alert alert-success">blog has been successfully saved.</div>');
			redirect('index.php/vendor/manage_blog');			
		}		
		$catdata['CATEGORY'] = $this->blog_model->select_all_active_blogcat();		
		$this->load->view('front/vendor/add-blog',$catdata);
	}
	
	function edit_blog()
	{
		$args = func_get_args(); 		
		$loginUserId = $this->user_model->getLoginUserVar('USER_ID');
		if(isset($_POST['updatedata']))
		{
			if($_FILES['image']['name']=="")
			{
				$file = $_POST['oldimage'];
			}
			else
			{
				$file = createUniqueFile($_FILES['image']['name']);
				$file_temp = $_FILES['image']['tmp_name'];
				$path = 'uploads/blog/'.$file;				
				move_uploaded_file($file_temp,$path);	
			}
			$data['image'] = $file;
			$data['cat_id'] = $this->input->post('cat_id');
			$data['uid'] = $loginUserId;
			$data['title'] = $this->input->post('title');
			$data['content'] = $this->input->post('content');	
			$this->blog_model->update($args[0],$data);		
			$this->session->set_flashdata('message','<div class="alert alert-success">blog has been successfully updated.</div>');
			redirect('index.php/vendor/manage_blog');			
		}		
		$catdata['CATEGORY'] = $this->blog_model->select_all_active_blogcat();	
		$catdata['EDITBLOG']= $this->blog_model->select_blog_byid($args[0]);	
		$this->load->view('front/vendor/edit-blog',$catdata);
	}
	
}


?>