<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Adminproducts extends CI_Controller 
{
	public function add_product()
	{
			//$args=func_get_args();

		if(isset($_POST["add_product"]))
		{	
			$data["category_id"]=$this->input->post("category_id");
			//$data["product_sku"]="0001";
			$data["name"]=$this->input->post("name");
			$data["meta_title"]=$this->input->post('producttitle');
			$data['meta_keyword']=$this->input->post('productkeyword');
			$data['meta_description']=$this->input->post('productdescription');
			$data["sortdesc"]=$this->input->post("sortdesc");
			$data["description"]=$this->input->post("description");
			$data["price"]=$this->input->post("price");
			$data["spacel_price"]=$this->input->post("spacel_price");
			$data["descount"]=$this->input->post("descount");
			$data["vender_type"]=$this->input->post("vendername");
			//$data["reletedproduct"]=serialize($this->input->post("reletedproduct"));
			$val=$this->input->post("reletedproduct");
			if($val!=""){
			$data1 = implode(',',$val);
			$data["reletedproduct"] = $data1;
		    }
			
		    $pincode_ids = $this->input->post("pincode_ids");
			if(!empty($related_product))
			{
				$data["pincode_ids"] = implode(',',$pincode_ids);
			}		
			$data["product_stock"]=$this->input->post("product_stock");
			$data["create_date"]=time();
			$data["status"]=$this->input->post("status");
			$this->session->set_flashdata('message','<div class="alert alert-success">Add Product successsully</div>'); 			//$this->db->insert("tbl_product",$data);
		    //$this->product_model->insert($data);

			$insert=$this->product_model->insert($data);
			$pro_id=mysql_insert_id();
			
			
			redirect("index.php/adminproducts/listing/");
		}
		$this->load->view("admin/product/add");
	}
	public function listing()
	{
		//echo "dfssdfdsfd";die;
		$data["LISTPRODUCT"]=$this->product_model->selectAllProduct();
		$this->load->view("admin/product/listing",$data);
	}
	public function edit()
	{
		$args=func_get_args();
		if(isset($_POST["edit"]))
		{
			
			
			$data["name"]=$this->input->post("name");
			$data["meta_title"]=$this->input->post("producttitle");
			$data["meta_keyword"]=$this->input->post("productkeyword");
			$data["meta_description"]=$this->input->post("productdescription");
			$data["vender_type"]=$this->input->post("vendername");
			$data["category_id"]=$this->input->post("category_id");
			$data["sortdesc"]=$this->input->post("sortdesc");
			$data["description"]=$this->input->post("description");
			$data["price"]=$this->input->post("price");
			$data["spacel_price"]=$this->input->post("spacel_price");
			$data["descount"]=$this->input->post("descount");
			$related_product = $this->input->post("reletedproduct");
			if(!empty($related_product))
			{
				$data["reletedproduct"] = implode(',',$related_product);
			}
			
			$pincode_ids = $this->input->post("pincode_ids");
			if(!empty($related_product))
			{
				$data["pincode_ids"] = implode(',',$pincode_ids);
			}
			  
			$data["product_stock"]=$this->input->post("product_stock");
			$data["create_date"]=time();
			$data["status"]=$this->input->post("status");
			$this->session->set_flashdata('message','<div class="alert alert-success">Product Update successsully</div>');
			
			$filters_id = $this->input->post("filter");
			if(!empty($filters_id) && count($filters_id)>0)
			{
				$data['filter_ids'] = implode(",",$filters_id);
			}			
			$this->product_model->Productupdate($args[0],$data); 
			
			$options = $this->input->post("options");
			if(!empty($options) && count($options)>0)
			{	
				$this->filters_model->delete_product_options($args[0]);
				foreach($options as $option)
				{
					$filterdata['p_id'] = $args[0];
					$filterdata['o_id'] = $option;
					$this->filters_model->insertProductFiltersOption($filterdata);
				}	
			}
					
		redirect("index.php/adminproducts/listing/");
		}
		$data["EDITPRODUCT"]=$this->product_model->selectProductById($args[0]);
		$this->load->view("admin/product/edit",$data);

	}
	public function deleteproduct($id)
	{
			
         $this->db->where('id',$id);
         $this->db->delete("tbl_product");
		 $this->product_model->deletesize($id);
		 $this->product_model->deletecolor($id);
         $this->session->set_flashdata('message','<div class="alert alert-success">Product Delete successsully</div>');
		 
		 $data=$this->product_model->selectProductImages($id);
			$imagecount=count($data);
			if($imagecount>0)
			{
				for($i=0;$i <$imagecount; $i++ )
				{
					$file=$data[$i]->image;
					$path = 'uploads/product/'.$file;
					$image_thumbs = 'uploads/product/thumbs/'.$file;
					@unlink($path);
					@unlink($image_thumbs);
				}
			}
			$this->product_model->deleteimage($id);
		 
        redirect("index.php/adminproducts/listing/".$id);
	}
		
	public function manageimage()
    {
		$args = func_get_args();
		if(count($args)>0)
		{
			if(isset($_POST['uploadfiles']))
			{
				include_once APPPATH."libraries/thumbnail.php";
				if(count($_FILES)>0)
				{
					$filesCount = count($_FILES['image']['name']);
					for($i = 0; $i < $filesCount; $i++)
					{						
						$name = $_FILES['image']['name'][$i];
						$tmpname1 = $_FILES['image']['tmp_name'][$i];
						$exten=explode(".",$_FILES['image']['name'][$i]);
						$exten=$exten[1];	
						$imagename=time().rand(1,10000).".".$exten;
						$path1='uploads/product/thumbs/';
						move_uploaded_file($tmpname1,'uploads/product/'.$imagename);
						$thumb = new Thumbnail('uploads/product/'.$imagename);
						$thumb->resize(265,265);
						$thumb->save($path1."".$imagename);
						
						$datasd["product_id"]=$args[0];						
						$datasd["image"] = $imagename;
						$this->product_model->insertProductImage($datasd);
					}
				}
				
				$this->session->set_flashdata('message','<div class="alert alert-success">file has been successfully uploaded</div>'); 
				redirect('index.php/adminproducts/manageimage/'.$args[0]);					
			}	
			
			$data["LISTCATEGORYIMAGE"]=$this->product_model->selectProductImages($args[0]);
			$this->load->view("admin/product/manage-images",$data);			
		}
		else
		{
			redirect('index.php/adminproducts/listing');
		}	
	}
		
	public function deleteproductimages($id)
	{
		$args = func_get_args();
		$img_data = $this->product_model->selectProductImageById($args[0]);
		if(count($img_data)>0)
		{
			$image = 'uploads/product/'.$img_data[0]->image;
			$image_thumbs = 'uploads/product/thumbs/'.$img_data[0]->image;
			@unlink($image);
			@unlink($image_thumbs);
		}	
		$this->product_model->deleteProductImageById($args[0]);
		$this->session->set_flashdata('message','<div class="alert alert-success">file successsully Deleted  </div>'); 
		redirect('index.php/adminproducts/manageimage/'.$args[1]);	
    }
	
	public function test()
	{
		include_once APPPATH."libraries/thumbnail.php";
		if(isset($_POST['uploadfiles']))
		{
			$name = $_FILES['file_name']['name'];
			$tmpname1 = $_FILES['file_name']['tmp_name'];
			$exten=explode(".",$_FILES['file_name']['name']);
			$exten=$exten[1];	
			$imagename=time().".".$exten;
			$path1='uploads/product/thumbs/';
			move_uploaded_file($tmpname1,'uploads/product/'.$imagename);
			$thumb = new Thumbnail('uploads/product/'.$imagename);
			$thumb->resize(265,265);
			$thumb->save($path1."".$imagename); 			 
		}
		$this->load->view("file-test");	
	}
	function dowloadcsv()
	{
		$this->load->library('user_agent');
		$data=$this->product_model->ExportCSV();
		if(!empty($data))
		{
			redirect($this->agent->referrer());
		}		
		$this->load->view('admin/product/listing');
	}
	function reviewlistingbyid($id)
	{
		
		$data['reviewlisting']= $this->product_model->selectreviewbyproid($id);
		$this->load->view('admin/product/review_listing',$data);
		
	}
	function deletereivew($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('tbl_review');
		redirect('index.php/adminproducts/listing');
	}
	function recordstatus($id)
	{
		//echo "sdfsdfd";die;
		$this->load->library('user_agent');
		$data=$this->product_model->selectreviewid($id);
		//print_r($data);
		if($data[0]->status==0)
		{
			//echo "rahul";
			$status['status']=1;
			$this->db->where('id',$id);
			$this->db->update('tbl_review',$status);
			
		}
		if($data[0]->status==1)
		{
			//echo "mukesh";
			$status1['status']=0;
			$this->db->where('id',$id);
			$this->db->update('tbl_review',$status1);
		}
		
		redirect($this->agent->referrer());
		
	}
}	